#include <geometry_msgs/Pose2D.h>
#include <if_node_msgs/powerState.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/UInt8.h>
#include <tf/tf.h>

#include <if_node_msgs/state.h>
#include <std_msgs/Int8.h>

#include <geometry_msgs/TransformStamped.h>
#include <move_base_msgs/MoveBaseActionFeedback.h>
#include <tf/LinearMath/Transform.h>
#include <tf2_ros/transform_listener.h>

#include <if_node_msgs/motor_reset.h>
#include <md2k_can_driver_msgs/motor_status.h>

#include <if_node_msgs/coil.h>
#include <stdint.h>
class State
{
public:
  State();
  ~State();

  bool init();
  void spin();

  void odometryCallback_(const nav_msgs::Odometry::ConstPtr msg);
  void
  posCallback_(const move_base_msgs::MoveBaseActionFeedback::ConstPtr& msg);
  void powerCallback_(const if_node_msgs::powerState::ConstPtr& msg);
  void moveCallback_(const std_msgs::UInt8::ConstPtr& msg);
  void loadCallback_(const std_msgs::Int8::ConstPtr& msg);
  /*
  void motor_FR_Callback_(const md2k_can_driver_msgs::motor_status::ConstPtr&
  msg); void motor_FL_Callback_(const
  md2k_can_driver_msgs::motor_status::ConstPtr& msg); void
  motor_RR_Callback_(const md2k_can_driver_msgs::motor_status::ConstPtr& msg);
  void motor_RL_Callback_(const md2k_can_driver_msgs::motor_status::ConstPtr&
  msg);
  */

  void motorCallback_(const md2k_can_driver_msgs::motor_status::ConstPtr& msg);

  void emgCallback_(const std_msgs::Int8::ConstPtr& msg);
  void error_Callback_(const std_msgs::Int8::ConstPtr& msg);
  void coilCallback_(const if_node_msgs::coil::ConstPtr& msg);
  void irCallback_(const std_msgs::Int8::ConstPtr& msg);
  void slideCallback_(const std_msgs::Int8::ConstPtr& msg);

private:
  ros::NodeHandle nh_;
  ros::Publisher pub_state;

  ros::ServiceClient client_motor;

  int64_t X;
  int64_t Y;
  int64_t A;

  int16_t voltage;
  int16_t current;
  int16_t soc;
  int16_t charge;

  int16_t move;

  int16_t load;

  //    int16_t bit_data;

  int8_t emg;
  int8_t cmd_error;
  int8_t top_coil;
  int8_t bot_coil;
  int8_t ir;
  int8_t slide;
};

State::State()
    : X(0), Y(0), A(0), voltage(0), current(0), soc(0), charge(0), move(0),
      load(0)
      /////////////////////////////
      ,
      emg(0), cmd_error(0), top_coil(0), bot_coil(0), ir(0), slide(0)
{
  init();
}

State::~State()
{
  if (ros::isStarted())
  {
    ros::shutdown(); // explicitly needed since we use ros::start();
    ros::waitForShutdown();
  }
  // wait();
}

bool State::init()
{
  if (!ros::master::check())
  {
    return false;
  }
  pub_state = nh_.advertise<if_node_msgs::state>("/AGV/State", 1, this);

  client_motor =
      nh_.serviceClient<if_node_msgs::motor_reset>("/motor_reset", this);

  ros::Subscriber sub_bat_ =
      nh_.subscribe("/BMS/State/Power", 1, &State::powerCallback_, this);
  ros::Subscriber sub_move =
      nh_.subscribe("/IFNode/goal_reach", 1, &State::moveCallback_, this);
  ros::Subscriber sub_load =
      nh_.subscribe("/LOAD/STATE", 1, &State::loadCallback_, this);
  /*
  ros::Subscriber sub_motor[4];
  sub_motor[0] = nh_.subscribe("/motor_front_right/motor_status", 1,
  &State::motor_FR_Callback_, this); sub_motor[1] =
  nh_.subscribe("/motor_front_left/motor_status", 1, &State::motor_FL_Callback_,
  this); sub_motor[2] = nh_.subscribe("/motor_rear_left/motor_status", 1,
  &State::motor_RL_Callback_, this); sub_motor[3] =
  nh_.subscribe("/motor_rear_right/motor_status", 1, &State::motor_RR_Callback_,
  this);
  */
  ros::Subscriber sub_motor;
  sub_motor = nh_.subscribe("/motor_status", 1, &State::motorCallback_, this);

  // from PLC directly
  ros::Subscriber sub_EMG =
      nh_.subscribe("/PLC/Error/Emergency", 1, &State::emgCallback_, this);
  ros::Subscriber sub_ERROR_cmd =
      nh_.subscribe("/PLC/Error/WrongCmd", 1, &State::error_Callback_, this);
  ros::Subscriber sub_coil =
      nh_.subscribe("/PLC/Sensor/COIL", 1, &State::coilCallback_, this);
  ros::Subscriber sub_ir =
      nh_.subscribe("/PLC/Sensor/IR", 1, &State::irCallback_, this);
  ros::Subscriber sub_slide =
      nh_.subscribe("/PLC/Sensor/Slide", 1, &State::slideCallback_, this);

  ros::start(); // explicitly needed since our nodehandle is going out of scope.

  spin();

  return true;
}

void State::spin()
{
  if_node_msgs::state state_msgs;

  ros::Rate loop_rate(5);

  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener(tfBuffer);

  while (ros::ok())
  {
    // ROS_INFO("POSE(%ld, %ld, %ld), POWER(%d, %d, %d, %d), MOVE(%d),
    // LOAD(%d)", X, Y, A, voltage, current, soc, charge, move, load);

    geometry_msgs::TransformStamped transformStamped;
    try
    {
      transformStamped =
          tfBuffer.lookupTransform("map", "base_link", ros::Time(0));

      double roll, pitch, yaw;
      tf::Quaternion q(transformStamped.transform.rotation.x,
                       transformStamped.transform.rotation.y,
                       transformStamped.transform.rotation.z,
                       transformStamped.transform.rotation.w);

      tf::Matrix3x3(q).getRPY(roll, pitch, yaw);

      X = static_cast<int64_t>(transformStamped.transform.translation.x * 1000);
      Y = static_cast<int64_t>(transformStamped.transform.translation.y * 1000);
      A = static_cast<int64_t>(yaw * 180 / M_PI * 1000);
    }
    catch (tf2::TransformException& ex)
    {
      ROS_WARN("%s", ex.what());
      ros::Duration(0.3).sleep();
      //		continue;
    }

    state_msgs.X = X;
    state_msgs.Y = Y;
    state_msgs.A = A;

    state_msgs.voltage = voltage;
    state_msgs.current = current;
    state_msgs.soc = soc;
    state_msgs.charge = charge;

    state_msgs.move = move;
    state_msgs.load = load;

    state_msgs.emg = emg;
    state_msgs.cmd_error = cmd_error;
    state_msgs.top_coil = top_coil;
    state_msgs.bot_coil = bot_coil;
    state_msgs.ir = ir;
    state_msgs.slide_front = slide;

    pub_state.publish(state_msgs);

    ros::spinOnce();
    loop_rate.sleep();
  }
}

void State::odometryCallback_(const nav_msgs::Odometry::ConstPtr msg)
{
  X = static_cast<int64_t>(msg->pose.pose.position.x * 1000);
  Y = static_cast<int64_t>(msg->pose.pose.position.y * 1000);

  tf::Quaternion q(msg->pose.pose.orientation.x, msg->pose.pose.orientation.y,
                   msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
  tf::Matrix3x3 m(q);

  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);

  A = static_cast<int64_t>(yaw * 180 / M_PI * 1000);

  // std::cout<<"X"<<X<<" Y"<<Y<<" A"<<A<<std::endl;
}

void State::posCallback_(
    const move_base_msgs::MoveBaseActionFeedback::ConstPtr& msg)
{
  X = static_cast<int64_t>(msg->feedback.base_position.pose.position.x * 1000);
  Y = static_cast<int64_t>(msg->feedback.base_position.pose.position.y * 1000);

  tf::Quaternion q(msg->feedback.base_position.pose.orientation.x,
                   msg->feedback.base_position.pose.orientation.y,
                   msg->feedback.base_position.pose.orientation.z,
                   msg->feedback.base_position.pose.orientation.w);
  tf::Matrix3x3 m(q);

  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);

  A = static_cast<int64_t>(yaw * 180 / M_PI * 1000);

  // std::cout<<"X"<<X<<" Y"<<Y<<" A"<<A<<std::endl;
}

void State::powerCallback_(const if_node_msgs::powerState::ConstPtr& msg)
{
  voltage = static_cast<int16_t>(msg->voltage * 10);
  current = static_cast<int16_t>(msg->current * 10);
  soc = msg->soc;
  charge = msg->state;
}

// Modify 210116
void State::moveCallback_(const std_msgs::UInt8::ConstPtr& msg)
{
  if (msg->data == 0 || msg->data == 7 || msg->data == 9)
    move = 0;
  else if (msg->data == 8)
    move = 2;
  else if (msg->data == 3)
    move = 3;
  else
    move = 1;
}

void State::loadCallback_(const std_msgs::Int8::ConstPtr& msg)
{
  load = msg->data;
}
/*
void State::motor_FR_Callback_(const
md2k_can_driver_msgs::motor_status::ConstPtr& msg)
{
    uint8_t m_2 = msg->front_right_bit_Alarm;


    if(m_2 != 0)
    {
        if_node_msgs::motor_reset reset_srv;
        reset_srv.request.request_1 = 0;
        reset_srv.request.request_2 = m_2;
        reset_srv.request.request_3 = 0;
        reset_srv.request.request_4 = 0;

        if(client_motor.call(reset_srv))
        {
            ROS_INFO("MOTOR RESET - success");
        }
        else
        {
            ROS_INFO("MOTOR RESET - failed");
        }
    }
}

void State::motor_FL_Callback_(const
md2k_can_driver_msgs::motor_status::ConstPtr& msg)
{
    uint8_t m_1 = msg->front_left_bit_Alarm;

    if(m_1 != 0)
    {
        if_node_msgs::motor_reset reset_srv;
        reset_srv.request.request_1 = m_1;
        reset_srv.request.request_2 = 0;
        reset_srv.request.request_3 = 0;
        reset_srv.request.request_4 = 0;

        if(client_motor.call(reset_srv))
        {
            ROS_INFO("MOTOR RESET - success");
        }
        else
        {
            ROS_INFO("MOTOR RESET - failed");
        }
    }
}

void State::motor_RR_Callback_(const
md2k_can_driver_msgs::motor_status::ConstPtr& msg)
{
    uint8_t m_4 = msg->rear_right_bit_Alarm;

    if(m_4 != 0)
    {
        if_node_msgs::motor_reset reset_srv;
        reset_srv.request.request_1 = 0;
        reset_srv.request.request_2 = 0;
        reset_srv.request.request_3 = 0;
        reset_srv.request.request_4 = m_4;

        if(client_motor.call(reset_srv))
        {
            ROS_INFO("MOTOR RESET - success");
        }
        else
        {
            ROS_INFO("MOTOR RESET - failed");
        }
    }
}

void State::motor_RL_Callback_(const
md2k_can_driver_msgs::motor_status::ConstPtr& msg)
{
    uint8_t m_3 = msg->rear_left_bit_Alarm;

    if(m_3 != 0 )
    {
        if_node_msgs::motor_reset reset_srv;
        reset_srv.request.request_1 = 0;
        reset_srv.request.request_2 = 0;
        reset_srv.request.request_3 = m_3;
        reset_srv.request.request_4 = 0;

        if(client_motor.call(reset_srv))
        {
            ROS_INFO("MOTOR RESET - success");
        }
        else
        {
            ROS_INFO("MOTOR RESET - failed");
        }
    }
}
*/

void State::motorCallback_(
    const md2k_can_driver_msgs::motor_status::ConstPtr& msg)
{
  int8_t m_1 = msg->front_left_bit_Alarm;
  int8_t m_2 = msg->front_right_bit_Alarm;
  int8_t m_3 = msg->rear_left_bit_Alarm;
  int8_t m_4 = msg->rear_right_bit_Alarm;

  if (m_1 != 0)
  {
    if_node_msgs::motor_reset reset_srv;
    reset_srv.request.request_1 = m_1;
    reset_srv.request.request_2 = 0;
    reset_srv.request.request_3 = 0;
    reset_srv.request.request_4 = 0;

    if (client_motor.call(reset_srv))
    {
      ROS_INFO("MOTOR RESET - success");
    }
    else
    {
      ROS_INFO("MOTOR RESET - failed");
    }
  }
  if (m_2 != 0)
  {
    if_node_msgs::motor_reset reset_srv;
    reset_srv.request.request_1 = 0;
    reset_srv.request.request_2 = m_2;
    reset_srv.request.request_3 = 0;
    reset_srv.request.request_4 = 0;

    if (client_motor.call(reset_srv))
    {
      ROS_INFO("MOTOR RESET - success");
    }
    else
    {
      ROS_INFO("MOTOR RESET - failed");
    }
  }
  if (m_3 != 0)
  {
    if_node_msgs::motor_reset reset_srv;
    reset_srv.request.request_1 = 0;
    reset_srv.request.request_2 = 0;
    reset_srv.request.request_3 = m_3;
    reset_srv.request.request_4 = 0;

    if (client_motor.call(reset_srv))
    {
      ROS_INFO("MOTOR RESET - success");
    }
    else
    {
      ROS_INFO("MOTOR RESET - failed");
    }
  }
  if (m_4 != 0)
  {
    if_node_msgs::motor_reset reset_srv;
    reset_srv.request.request_1 = 0;
    reset_srv.request.request_2 = 0;
    reset_srv.request.request_3 = 0;
    reset_srv.request.request_4 = m_4;

    if (client_motor.call(reset_srv))
    {
      ROS_INFO("MOTOR RESET - success");
    }
    else
    {
      ROS_INFO("MOTOR RESET - failed");
    }
  }
}
void State::emgCallback_(const std_msgs::Int8::ConstPtr& msg)
{
  emg = msg->data;
}

void State::error_Callback_(const std_msgs::Int8::ConstPtr& msg)
{
  cmd_error = msg->data;
}

void State::coilCallback_(const if_node_msgs::coil::ConstPtr& msg)
{
  top_coil = msg->top;
  bot_coil = msg->bot;
}

void State::irCallback_(const std_msgs::Int8::ConstPtr& msg) { ir = msg->data; }

void State::slideCallback_(const std_msgs::Int8::ConstPtr& msg)
{
  slide = msg->data;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "state_node");

  State state;

  return 0;
}
