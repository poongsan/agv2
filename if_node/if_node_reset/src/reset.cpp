#include <ros/ros.h>
#include <std_msgs/Int8.h>
#include "ros/time.h"
#include <pgv/vision_msg.h>

#include <actionlib_msgs/GoalID.h>

#include <QProcess>
#include <QDebug>

class Reset
{
public:
    Reset();
    ~Reset();

    bool init();
    void spin();

    void emgCallback(const std_msgs::Int8::ConstPtr& msg);

    void pgv_cb(const pgv::vision_msg& pgv_msg);

private:
    ros::NodeHandle nh_;

    ros::Publisher pub_cancel;
    ros::Subscriber sub_emg;

    ros::Subscriber pgv_sub;
    int fault, line_id;

    int8_t old_msg;

    QProcess process;
    int64_t count_ = 0;
    int8_t cnt_pgv_ = 0;
};

Reset::Reset() : old_msg(0)
{
    if(init())
        spin();
}

Reset::~Reset()
{

}

bool Reset::init()
{
    if( !ros::master::check() )
        return false;

    sub_emg = nh_.subscribe("PLC/Error/Emergency",1, &Reset::emgCallback, this);

    pgv_sub = nh_.subscribe("/pgv_data", 10, &Reset::pgv_cb, this);

    return true;
}


void Reset::pgv_cb(const pgv::vision_msg& pgv_msg)
{
  line_id = pgv_msg.LineID;
  fault = pgv_msg.Fault;

  if((fault != 0) || (line_id > 0))
  {
    // Kill node
      if(cnt_pgv_ >=20)
      {
          QStringList list;
          list << "kill"<<"pgvQR";
          process.execute("rosnode",list);
          ros::Duration(0.05).sleep();
          // Restore
          QStringList list2;
          list2 << "pgv"<<"pgvQR";
          //        process.execute("rosrun",list);
          process.startDetached("roslaunch pgv pgv.launch");
          cnt_pgv_ = 0;
      }
      else
      {
          cnt_pgv_ ++;
      }
  }
  else
      cnt_pgv_ = 0;
}

void Reset::spin()
{
    ros::Rate loop_rate(10);

    while(ros::ok())
    {
        ros::spinOnce();
        count_ ++;
        if(count_ == 1200)
        {
            count_ = 0;
            QProcess process;
            QStringList list;
            list << "/home/sis/sh/clean.sh";
            process.execute("/bin/bash",list);
        }
        loop_rate.sleep();
    }
}

void Reset::emgCallback(const std_msgs::Int8::ConstPtr& msg)
{

    if( (old_msg == 0) && (msg->data == 1) )
    {
        // TODO RESET
        //qDebug()<<"RESET";


        //QStringList list;
        //list << "kill"<<"bongsanAGV_motor_driver";
        // Modify 200726
        //list << "kill"<<"-a";
        //process.startDetached("killall -9 rosmaster");
       // process.execute("rosnode",list);
        //ros::Duration(5).sleep();
      //  process.execute("rosnode",list);
       // ros::Duration(0.05).sleep();
    }
    /*
    else if( (old_msg == 1) && (msg->data == 0) )
    {
        // TODO RESTORE
        qDebug()<<"RESTORE";

        QStringList list;
        // Modify 200726
        //list << "md2k_can_driver"<<"bongsanAGV_motor_driver";
//        process.execute("rosrun",list);
        //process.startDetached("rosrun md2k_can_driver bongsanAGV_motor_driver");
        process.startDetached("roslaunch sis_agv test_pongsan.launch");
        ros::Duration(2).sleep();
        process.startDetached("roslaunch if_node_launch agv1_bridge.launch");


    }
    old_msg = msg->data;
    */
}

int main(int argc, char** argv)
{
    ros::init(argc, argv,"reset_node");

    Reset();

    return 0;
}


