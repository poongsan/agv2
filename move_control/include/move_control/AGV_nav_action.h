#include <ros/ros.h>
#include "ros/time.h"
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <angles/angles.h>
#include "move_control/nav_status.h"
#include "move_control/nav_control.h"
#include "obstacle_detector/Obstacles.h"



#include "tf/transform_datatypes.h"
#include "tf/transform_listener.h"



enum { NONE = 0,
       GOAL = 1,
       LOOP = 2
     } mode_;

enum { IDLE = 0,
       START = 1,
       ACTIVE = 2,
       COMPLETED = 3,
       OBSTACLE = 4
     } state_;



typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

class NavigationManagement{
private:
	ros::NodeHandle	nh_;	
	ros::Subscriber   	sub_HMICtrlCmd;
	ros::Subscriber  	sub_moveBaseStatus;
  ros::Subscriber sub_obstacle;
  ros::Subscriber sub_currentPos;
	
  ros::Publisher    	pub_sendGoal, pub_curPos;
	ros::Publisher    	pub_cancelGoal;
	ros::Publisher    	pub_navStatus;
	
	ros::Timer  		timer_;

	ros::ServiceServer  srv_NavCtrlCmd;	
	
  MoveBaseClient      *AGV_move_base;
//	movebaseStatusLists  movebaseStatus;

  bool idle_status_update_sent_;
  int frequency_ = 10;
  bool start_bool = false;

	
  geometry_msgs::PoseStamped current_pose;
  geometry_msgs::PoseStamped stop_pose;
  geometry_msgs::PoseStamped  current_goal;
  move_control::nav_status NavStatus;
  void obstacleDataCallback(const obstacle_detector::Obstacles::ConstPtr &msg);
  void currentPosCallback(const move_base_msgs::MoveBaseActionFeedback::ConstPtr &pos);
  void publishStatusUpdate(const uint8_t& status);

  bool stop_moving();
  void reset();
  double getDistance(double x1, double y1, double x2, double y2);


  void getCommandCallback(const move_control::nav_control::ConstPtr& msg);
  void getMoveBaseStatusCallback(const actionlib_msgs::GoalStatusArray::ConstPtr& msg);
  bool ob_detect = false;

		
public:	
  NavigationManagement();
  ~NavigationManagement();
	
  void spin();



	

};
