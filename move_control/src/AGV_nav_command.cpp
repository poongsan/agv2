#include <ros/ros.h>
#include "ros/time.h"
#include <math.h>
#include "move_control/nav_control.h"
#include "move_control/nav_status.h"

#include "tf/transform_datatypes.h"
#include "tf/transform_listener.h"

class NavigationCommand{
private:
  ros::NodeHandle	nh_;

  ros::Publisher    pub_navCommand;
  ros::Subscriber    sub_navStatus;
  ros::Subscriber    sub_currentPose;

  ros::Timer  		timer_;
  int navigation_status = 0;


  geometry_msgs::Pose2D  goal;
  geometry_msgs::Pose2D current_pose;
  void navStatusCallback(const move_control::nav_status::ConstPtr &msg);
  void currentPoseCallback(const geometry_msgs::PoseStamped pos);
  void publishMoveControl(const ros::TimerEvent& e);

public:
  NavigationCommand(double x_cmd,double y_cmd, double theta_cmd);
  ~NavigationCommand();

};

NavigationCommand::NavigationCommand(double x_cmd,double y_cmd, double theta_cmd)
{

  sub_navStatus = nh_.subscribe<move_control::nav_status>("/nav_status",1,&NavigationCommand::navStatusCallback,this);
  sub_currentPose = nh_.subscribe<geometry_msgs::PoseStamped>("/current_pose",1,&NavigationCommand::currentPoseCallback,this);
  pub_navCommand = nh_.advertise<move_control::nav_control>("/move_control", 1);

  timer_ = nh_.createTimer(ros::Duration(0.05), &NavigationCommand::publishMoveControl, this);

  goal.x = x_cmd;
  goal.y = y_cmd;
  goal.theta = theta_cmd;

}
NavigationCommand::~NavigationCommand()
{

}

void NavigationCommand::publishMoveControl(const ros::TimerEvent& e)
{
  move_control::nav_control control_cmd;
  control_cmd.header.stamp = ros::Time::now();
  if(navigation_status == move_control::nav_status::IDLING || navigation_status == move_control::nav_status::COMPLETED)
  {
    control_cmd.control = move_control::nav_control::START;
    control_cmd.pose = goal;
  }
  else if(navigation_status == move_control::nav_status::RUNNING)
  {
    control_cmd.control = move_control::nav_control::RUNNING;
    control_cmd.pose = goal;
  }
  else if(navigation_status == move_control::nav_status::OBSTACLE)
  {
    control_cmd.control = move_control::nav_control::STOP;
    control_cmd.pose = goal;
  }
  else
  {
    control_cmd.control = move_control::nav_control::STOP;
    control_cmd.pose = goal;
  }
  pub_navCommand.publish(control_cmd);
}

void NavigationCommand::navStatusCallback(const move_control::nav_status::ConstPtr &msg)
{
  navigation_status = msg->status;
}

void NavigationCommand::currentPoseCallback(const geometry_msgs::PoseStamped pos)
{
  current_pose.x = pos.pose.position.x;
  current_pose.y = pos.pose.position.y;
  //tf::Pose posi;
  //tf::poseMsgToTF(pos->pose, posi);
  current_pose.theta = tf::getYaw(pos.pose.orientation);
}
int main(int argc, char** argv)
{
  ros::init(argc, argv, "~");
  ros::NodeHandle nh("~");

  double x_cmd_ = 0;
  double y_cmd_ = 0;
  double theta_cmd_ = 0;
  nh.param<double>("x_cmd", x_cmd_, x_cmd_);
  nh.param<double>("y_cmd", y_cmd_, y_cmd_);
  nh.param<double>("theta_cmd", theta_cmd_, theta_cmd_);
  double theta_cmd_r = theta_cmd_*M_PI/180;

  NavigationCommand AGV_control(x_cmd_,y_cmd_,theta_cmd_r);
	while (!ros::isShuttingDown()) {
		ros::spinOnce();
		ros::Duration(0.01).sleep();
	}
	return 0;
}
