/**
* @file : AGV_nav_action.cpp
* @brief :
*/
#define WAIT_TIME_CONNECTING 5.0f


#include "move_control/AGV_nav_action.h"
std::string node_name("navigation_management");


NavigationManagement::NavigationManagement()
{


  AGV_move_base = new MoveBaseClient("move_base", true);

  sub_obstacle = nh_.subscribe<obstacle_detector::Obstacles>("/obstacles",1,&NavigationManagement::obstacleDataCallback,this);


  sub_HMICtrlCmd = nh_.subscribe<move_control::nav_control>("/move_control", 1, &NavigationManagement::getCommandCallback, this);

  sub_moveBaseStatus = nh_.subscribe<actionlib_msgs::GoalStatusArray>("/move_base/status", 1, &NavigationManagement::getMoveBaseStatusCallback, this);

  sub_currentPos = nh_.subscribe<move_base_msgs::MoveBaseActionFeedback>("/move_base/feedback", 1, &NavigationManagement::currentPosCallback, this);

	pub_sendGoal = nh_.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1);
  pub_curPos = nh_.advertise<geometry_msgs::PoseStamped>("/current_pose", 1);
	pub_cancelGoal = nh_.advertise<actionlib_msgs::GoalID>("/move_base/cancel", 1);
  pub_navStatus = nh_.advertise<move_control::nav_status>("/nav_status", 1);

  //timer_ = nh_.createTimer(ros::Duration(0.05), &NavigationManagement::publishMoveBaseStatus, this);

  state_ = IDLE;
  mode_ = NONE;
  start_bool = false;
}


NavigationManagement::~NavigationManagement()
{
  //pub_navStatus.publish(NavStatus);
}


void NavigationManagement::obstacleDataCallback(const obstacle_detector::Obstacles::ConstPtr & msg)
{

  if(!msg->circles.empty())
  {
     publishStatusUpdate(move_control::nav_status::OBSTACLE);
     ob_detect = true;
     state_ = OBSTACLE;
  }
  else
  {
     if(ob_detect)
     {
       ob_detect = false;
       state_ = START;
     }
     else
     {
       if(start_bool) state_ = ACTIVE;
       else {
         state_ = IDLE;
       }
     }
  }

}

void NavigationManagement::currentPosCallback(const move_base_msgs::MoveBaseActionFeedback::ConstPtr &pos)
{

  current_pose.header.stamp = ros::Time::now();
  current_pose.pose.position.x = pos->feedback.base_position.pose.position.x;
  current_pose.pose.position.y = pos->feedback.base_position.pose.position.y;
  current_pose.pose.orientation = pos->feedback.base_position.pose.orientation;
  pub_curPos.publish(current_pose);
}


bool NavigationManagement::stop_moving()
{
	double timeout = 2.0;


  ///AGV_move_base->stopTrackingGoal();

  actionlib::SimpleClientGoalState goal_state = AGV_move_base->getState();

	if ((goal_state != actionlib::SimpleClientGoalState::ACTIVE) &&
		(goal_state != actionlib::SimpleClientGoalState::PENDING) &&
		(goal_state != actionlib::SimpleClientGoalState::RECALLED) &&
		(goal_state != actionlib::SimpleClientGoalState::PREEMPTED))
	{
		//We cannot cancel a REJECTED, ABORTED, SUCCEEDED or LOST goal
		ROS_WARN("Cannot cancel move base goal, as it has %s state!", goal_state.toString().c_str());
    publishStatusUpdate(move_control::nav_status::ERROR);
		return true;

	}
	else {

    AGV_move_base->cancelAllGoals();
    if (AGV_move_base->waitForResult(ros::Duration(timeout)) == false)
		{
			ROS_WARN("Cancel move base goal didn't finish after %.2f seconds: %s", timeout, goal_state.toString().c_str());
			return false;
      publishStatusUpdate(move_control::nav_status::ERROR);
		}
		else {
			ROS_INFO("Cancel move base goal succeed. New state is %s", goal_state.toString().c_str());
			return true;
      publishStatusUpdate(move_control::nav_status::CANCELLED);
		}
	}
}

void NavigationManagement::reset()
{
  ROS_DEBUG("Full reset: clear markers, delete waypoints and goal and set state to IDLE");
  //goal_  = NOWHERE;
  mode_  = NONE;
}

double NavigationManagement::getDistance(double x1, double y1, double x2, double y2)
{
	return sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));
}



/**
* @func: get command from main control system
*/
void NavigationManagement::getCommandCallback(const move_control::nav_control::ConstPtr& msg)
{

	switch (msg->control)
	{
  case move_control::nav_control::STOP:
    if ((state_ == START) || (state_ == ACTIVE))
    {
      ROS_INFO("GOAL STOP!!!!\n");
      stop_moving();
      reset();
      idle_status_update_sent_ = false;
      state_ = IDLE;
      publishStatusUpdate(move_control::nav_status::CANCELLED);
    }
    else
    {
      ROS_WARN_STREAM("Cannot stop way point/trajectory execution, because nothing is being executed.");
    }

		break;
  case move_control::nav_control::START:
    //bool goal_found = false;
		ROS_INFO("GOAL SEND!!!!\n");

    if ((state_ == IDLE) || (state_ == COMPLETED))
		{
      reset();
      geometry_msgs::PoseStamped points;
			tf::Quaternion quat;

      points.header.frame_id = "map";
      points.header.stamp = ros::Time::now();

      points.pose.position.x = msg->pose.x;
      points.pose.position.y = msg->pose.y;
      points.pose.position.z = 0;

      //quat.setRPY(0.0, 0.0, msg->pose.theta);
      //tf::quaternionTFToMsg(quat, points.pose.orientation);
      points.pose.orientation = tf::createQuaternionMsgFromYaw(msg->pose.theta);

      current_goal = points;
      state_ = START;
      mode_  = GOAL;
		}
    else
    {
      ROS_WARN_STREAM("Cannot start way point/trajectory execution, because navigator is currently active. "
                      << "Please stop current activity first.");
    }
		break;
  case move_control::nav_control::PAUSE:
    state_ = IDLE;
    break;
  case move_control::nav_control::RUNNING:
    state_ = ACTIVE;
		break;
	default:break;
	}

}

/**
* @func: receive movebase status
*/
void NavigationManagement::getMoveBaseStatusCallback(const actionlib_msgs::GoalStatusArray::ConstPtr& msg)
{

	int GoalIndex = 0;
	if (msg->status_list.size() >= 1)
	{
		GoalIndex = msg->status_list.size() - 1;


		//ROS_INFO(" GoalIndex->status= %d /%d \n", msg->status_list[GoalIndex].status,actionlib_msgs::GoalStatus::PENDING);
		switch (msg->status_list[GoalIndex].status)
		{
		case actionlib_msgs::GoalStatus::PENDING:
      NavStatus.status = move_control::nav_status::IDLING; //
			break;
		case actionlib_msgs::GoalStatus::ACTIVE:
      NavStatus.status = move_control::nav_status::RUNNING;
			break;
		case actionlib_msgs::GoalStatus::PREEMPTED:
      NavStatus.status = move_control::nav_status::IDLING; //canceled;
			break;
		case actionlib_msgs::GoalStatus::SUCCEEDED:
      NavStatus.status = move_control::nav_status::COMPLETED;
			break;
		case actionlib_msgs::GoalStatus::ABORTED:
      NavStatus.status = move_control::nav_status::FAILED;
			break;
		case actionlib_msgs::GoalStatus::REJECTED:
      NavStatus.status = move_control::nav_status::ERROR;
      break;
		case actionlib_msgs::GoalStatus::LOST:
			//ROS_INFO("LOST[%s]", msg->status_list[GoalIndex].goal_id.id.c_str());
      NavStatus.status = move_control::nav_status::FAILED;
			break;
		default:
			ROS_INFO("Unknown[%s]", msg->status_list[GoalIndex].goal_id.id.c_str());
      NavStatus.status = move_control::nav_status::UNKNOWN;
			break;
    }
	}
}

void NavigationManagement::spin()
{


  ros::Rate rate(frequency_);

  while (ros::ok())
  {
    rate.sleep();
    ros::spinOnce();
    if (state_ == START)
    {
        start_bool = true;
        move_base_msgs::MoveBaseGoal mb_goal;
        mb_goal.target_pose.header.stamp = ros::Time::now();
        mb_goal.target_pose.header.frame_id = current_goal.header.frame_id;
        mb_goal.target_pose.pose = current_goal.pose;
   //     mb_goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(0.0);  // TODO use the heading from robot loc to next (front)

        ROS_INFO("New goal: %.2f, %.2f, %.2f",
                 mb_goal.target_pose.pose.position.x, mb_goal.target_pose.pose.position.y,
                 tf::getYaw(mb_goal.target_pose.pose.orientation));
        AGV_move_base->sendGoal(mb_goal);
        pub_sendGoal.publish(current_goal);
        publishStatusUpdate(move_control::nav_status::RUNNING);

        state_ = ACTIVE;
    }

    else if (state_ == ACTIVE)
    {

      actionlib::SimpleClientGoalState goal_state = AGV_move_base->getState();
      if(goal_state == actionlib::SimpleClientGoalState::SUCCEEDED)
      {
        ROS_INFO_STREAM("Goal reached.");
        state_ = COMPLETED;
      }
      else
      {
        publishStatusUpdate(move_control::nav_status::RUNNING);
      }
    }
    else if(state_ == COMPLETED)
    {
      // publish update
      publishStatusUpdate(move_control::nav_status::COMPLETED);
      idle_status_update_sent_ = false;
      state_ = IDLE;
    }
    else if(state_ == OBSTACLE)
    {
      stop_moving();
      ROS_INFO_STREAM("Obstacle.");
    }
    else // IDLE
    {
      start_bool = false;
      if (!idle_status_update_sent_)
      {
        publishStatusUpdate(move_control::nav_status::IDLING);
        idle_status_update_sent_ = true;
      }
    }
  }
}

void NavigationManagement::publishStatusUpdate(const uint8_t& status)
{
  move_control::nav_status msg;
  if (status == move_control::nav_status::IDLING)
  {
    msg.status = move_control::nav_status::IDLING;
    msg.status_desc = "Idling";
    pub_navStatus.publish(msg);
  }
  else if (status == move_control::nav_status::RUNNING)
  {
    msg.status = move_control::nav_status::RUNNING;
    msg.status_desc = "Running";
    pub_navStatus.publish(msg);
  }
  else if (status == move_control::nav_status::PAUSED)
  {
    msg.status = move_control::nav_status::PAUSED;
    msg.status_desc = "Paused";
    pub_navStatus.publish(msg);
  }
  else if (status == move_control::nav_status::COMPLETED)
  {
    msg.status = move_control::nav_status::COMPLETED;
    msg.status_desc = "Completed";
    pub_navStatus.publish(msg);
  }
  else if (status == move_control::nav_status::CANCELLED)
  {
    msg.status = move_control::nav_status::CANCELLED;
    msg.status_desc = "Cancelled";
    pub_navStatus.publish(msg);
  }
  else if (status == move_control::nav_status::ERROR)
  {
    msg.status = move_control::nav_status::ERROR;
    msg.status_desc = "Error";
    pub_navStatus.publish(msg);
  }
  else if (status == move_control::nav_status::FAILED)
  {
    msg.status = move_control::nav_status::FAILED;
    msg.status_desc = "Failed.";
    pub_navStatus.publish(msg);
  }
  else if (status == move_control::nav_status::OBSTACLE)
  {
    msg.status = move_control::nav_status::OBSTACLE;
    msg.status_desc = "Obstacle.";
    pub_navStatus.publish(msg);
  }
  else
  {
    ROS_ERROR_STREAM("Cannot publish unknown status updated!");
    msg.status = move_control::nav_status::UNKNOWN;
    msg.status_desc = "Unknown.";
    pub_navStatus.publish(msg);
  }
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "AGV_nav_action");

  NavigationManagement AGV_nav;
  AGV_nav.spin();
	return 0;
}
