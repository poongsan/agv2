
/**
* @file : roboteq_interface.cpp
* @author : SIS
* @date : 2020.02.06
* @brief : CONTROL
* @todo
* @bug
* @warning
*/

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <memory.h>
#include <math.h>

#include "ros/ros.h"
#include "ros/time.h"
#include "boost/thread/thread.hpp"

#include <std_msgs/UInt8.h>
#include <std_srvs/SetBool.h>

// include message in md2k motor driver
#include <md2k_can_driver_msgs/four_ENC_data.h>
#include <md2k_can_driver_msgs/four_SPD_data.h>
#include <md2k_can_driver_msgs/four_motor_command.h>


// include message to advertise wheel speed data
#include "move_control/WheelSpeed.h"
#include "geometry_msgs/Twist.h"
#include "move_control/teleop.h"



// #define PI 3.1415927


using namespace std;
using namespace ros;

string node_name("PongsanAGV");


template<class T> std::string timeToStr(T ros_t)
{
	char buf[1024] = "";
	time_t t = ros_t.sec;
	struct tm* tms = localtime(&t);
	strftime(buf, 1024, "%Y-%m-%d_%H-%M-%S", tms);
	return string(buf);
}



class PongsanAGV {

private:
        ros::NodeHandle nh_;

        ros::Publisher pub_moveCmd;

        ros::Publisher pub_wheel_speed;

        ros::Subscriber sub_joyMoveCmd, sub_nav_cmd; // Motor driver movement control command

        ros::Subscriber sub_speed_AGV;

        ros::Subscriber sub_encoder_AGV;
        ros::Timer PublishWheelSpeedTimer;
        ros::Timer pub_command_timer;

        enum {
                FRONT_LEFT = 0, FRONT_RIGHT = 2, REAR_LEFT = 1, REAR_RIGHT = 3
        };


            double measured_speed[4];
            double encoder_abs[4];
            double pre_measured_speed[4];
            double cal_acc[4];
            float set_speed[4];
            int front_left = 0;
            int front_right = 2;
            int rear_left = 1;
            int rear_right = 3;


              // AGV data
              double AGV_len = 2.19;
              double AGV_wid = 2.2;
              double wheel_wid = 0.306;
              double wheel_rad = 0.275;
              double gear_ratio = 99.7;

              double scale_factor_l = 1;
              double scale_factor_a = 1;
              double angz_scale = 0.25;

              double front_right_nav = 0, front_left_nav = 0, rear_left_nav =0, rear_right_nav = 0;
              double front_right_joy = 0, front_left_joy = 0, rear_left_joy =0, rear_right_joy = 0;

              ros::Time cmdJoy_header;

        void AGV_Speed_Callback(const md2k_can_driver_msgs::four_SPD_data Speed_Value);
        void AGV_Encoder_Callback(const md2k_can_driver_msgs::four_ENC_data Encoder_Value);
        void publish_Speed_data(const ros::TimerEvent& e);
        void publish_cmd(const ros::TimerEvent& e);
        bool setVelocityMoveCommand();
        void moveControlCallback(const geometry_msgs::Twist::ConstPtr& motorCmd_);
        void joystickControlCallback(const move_control::teleop::ConstPtr& motorCmd_);
public:
        PongsanAGV();
        ~PongsanAGV();
        };

        PongsanAGV::PongsanAGV()
	{

          set_speed[FRONT_LEFT] = 0;
          set_speed[FRONT_RIGHT] = 0;
          set_speed[REAR_LEFT] = 0;
          set_speed[REAR_RIGHT] = 0;

          measured_speed[FRONT_LEFT] = 0;
          measured_speed[FRONT_RIGHT] = 0;
          measured_speed[REAR_LEFT] = 0;
          measured_speed[REAR_RIGHT] = 0;

          encoder_abs[FRONT_LEFT] = 0;
          encoder_abs[FRONT_RIGHT] = 0;
          encoder_abs[REAR_LEFT] = 0;
          encoder_abs[REAR_RIGHT] = 0;

          pre_measured_speed[FRONT_LEFT] = 0;
          pre_measured_speed[FRONT_RIGHT] = 0;
          pre_measured_speed[REAR_LEFT] = 0;
          pre_measured_speed[REAR_RIGHT] = 0;

          cal_acc[FRONT_LEFT] = 0;
          cal_acc[FRONT_RIGHT] = 0;
          cal_acc[REAR_LEFT] = 0;
          cal_acc[REAR_RIGHT] = 0;

          front_right_nav = 0, front_left_nav = 0, rear_left_nav =0, rear_right_nav = 0;
          front_right_joy = 0, front_left_joy = 0, rear_left_joy =0, rear_right_joy = 0;

          // subcribe speed data from driver
          sub_speed_AGV = nh_.subscribe("/speed_data", 1, &PongsanAGV::AGV_Speed_Callback, this);

          // subcribe absolute encoder data from driver
          sub_encoder_AGV = nh_.subscribe("/encoder_data", 1, &PongsanAGV::AGV_Encoder_Callback, this);

          // subcribe joystick command
          sub_joyMoveCmd = nh_.subscribe("cmd_vel_joy", 1, &PongsanAGV::joystickControlCallback, this);

          // subcribe navigation command
          sub_nav_cmd = nh_.subscribe("cmd_vel", 1, &PongsanAGV::moveControlCallback, this);

          // advertise message about motor command
          pub_moveCmd= nh_.advertise<md2k_can_driver_msgs::four_motor_command>("/cmd", 1, true);


          // advertise wheel speed to odom node
          pub_wheel_speed = nh_.advertise<move_control::WheelSpeed>("/WheelSpeed", 1, true);

          // Make sampling for wheel data publish
          PublishWheelSpeedTimer = nh_.createTimer(ros::Duration(0.01), &PongsanAGV::publish_Speed_data, this);
          pub_command_timer = nh_.createTimer(ros::Duration(0.01), &PongsanAGV::publish_cmd, this);
            ros::NodeHandle nh_local("~");
          nh_local.param("scale_l", scale_factor_l, scale_factor_l);
          nh_local.param("scale_a", scale_factor_a, scale_factor_a);
          nh_local.param("angz_scale", angz_scale, angz_scale);
	}

        PongsanAGV::~PongsanAGV()
        {
        }

        void PongsanAGV::AGV_Speed_Callback(const md2k_can_driver_msgs::four_SPD_data Speed_Value)
        {
          measured_speed[FRONT_LEFT] = Speed_Value.front_left_rpm;
          measured_speed[FRONT_RIGHT] = Speed_Value.front_right_rpm;
          measured_speed[REAR_RIGHT] = Speed_Value.rear_right_rpm;
          measured_speed[REAR_LEFT] = Speed_Value.rear_left_rpm;
        }

        void PongsanAGV::AGV_Encoder_Callback(const md2k_can_driver_msgs::four_ENC_data Encoder_Value)
        {
          encoder_abs[FRONT_LEFT] = Encoder_Value.front_left_encoder_stick;
          encoder_abs[FRONT_RIGHT] = Encoder_Value.front_right_encoder_stick;
          encoder_abs[REAR_RIGHT] = Encoder_Value.rear_right_encoder_stick;
          encoder_abs[REAR_LEFT] = Encoder_Value.rear_left_encoder_stick;
        }

        void PongsanAGV::publish_Speed_data(const ros::TimerEvent& e)
        {
          move_control::WheelSpeed wheel_data_;

          if (pre_measured_speed[FRONT_LEFT] == 0) {
            cal_acc[FRONT_LEFT] = 0;
          }
          else {
            int temp = (measured_speed[FRONT_LEFT] - pre_measured_speed[FRONT_LEFT]);
            cal_acc[FRONT_LEFT] = temp;
          }
          pre_measured_speed[FRONT_LEFT] = measured_speed[FRONT_LEFT];


          if (pre_measured_speed[FRONT_RIGHT] == 0) {
            cal_acc[FRONT_RIGHT] = 0;
          }
          else {
            int temp = (measured_speed[FRONT_RIGHT] - pre_measured_speed[FRONT_RIGHT]);
            cal_acc[FRONT_RIGHT] = 1 * temp;
          }
          pre_measured_speed[FRONT_RIGHT] = measured_speed[FRONT_RIGHT];

          if (pre_measured_speed[REAR_LEFT] == 0) {
            cal_acc[REAR_LEFT] = 0;
          }
          else {
            //int temp=abs(measured_speed[REAR_LEFT]-pre_measured_speed[REAR_LEFT]);
            int temp = (measured_speed[REAR_LEFT] - pre_measured_speed[REAR_LEFT]);
            cal_acc[REAR_LEFT] = temp;

          }
          pre_measured_speed[REAR_LEFT] = measured_speed[REAR_LEFT];


          if (pre_measured_speed[REAR_RIGHT] == 0) {
            cal_acc[REAR_RIGHT] = 0;
          }
          else {
            //int temp=abs(measured_speed[REAR_RIGHT]-pre_measured_speed[REAR_RIGHT]);
            int temp = (measured_speed[REAR_RIGHT] - pre_measured_speed[REAR_RIGHT]);
            cal_acc[REAR_RIGHT] = 1 * temp;
          }
          pre_measured_speed[REAR_RIGHT] = measured_speed[REAR_RIGHT];

          wheel_data_.header.stamp = ros::Time::now();

          wheel_data_.front_left_speed = measured_speed[FRONT_LEFT];
          wheel_data_.front_right_speed = measured_speed[FRONT_RIGHT];
          wheel_data_.rear_left_speed = measured_speed[REAR_LEFT];
          wheel_data_.rear_right_speed = measured_speed[REAR_RIGHT];

          wheel_data_.front_left_acc = cal_acc[FRONT_LEFT];
          wheel_data_.front_right_acc = cal_acc[FRONT_RIGHT];
          wheel_data_.rear_left_acc = cal_acc[REAR_LEFT];
          wheel_data_.rear_right_acc = cal_acc[REAR_RIGHT];

          wheel_data_.front_left_encoder = encoder_abs[FRONT_LEFT];
          wheel_data_.front_right_encoder = encoder_abs[FRONT_RIGHT];
          wheel_data_.rear_left_encoder = encoder_abs[REAR_LEFT];
          wheel_data_.rear_right_encoder = encoder_abs[REAR_RIGHT];

          pub_wheel_speed.publish(wheel_data_);
        }

        void PongsanAGV::moveControlCallback(const geometry_msgs::Twist::ConstPtr& motorCmd_)
        {
            double linear_x = motorCmd_->linear.x;
            double linear_y = -motorCmd_->linear.y;
            double angular_z = motorCmd_->angular.z;

            front_right_nav = 1/(2 * M_PI * wheel_rad) * ((1)*linear_x + (-1)*linear_y - (AGV_len+AGV_wid)*angular_z/2) * gear_ratio;

            front_left_nav = 1/(2 * M_PI * wheel_rad) * (1*linear_x + (1)*linear_y + (AGV_len+AGV_wid)*angular_z/2) * gear_ratio;

            rear_left_nav = 1/(2 * M_PI * wheel_rad) * ((1)*linear_x + (-1)*linear_y + (AGV_len+AGV_wid)*angular_z/2) * gear_ratio;

            rear_right_nav = 1/(2 * M_PI * wheel_rad) * (1*linear_x + 1*linear_y - (AGV_len+AGV_wid)*angular_z/2) * gear_ratio;
        }

        void PongsanAGV::joystickControlCallback(const move_control::teleop::ConstPtr& motorCmd_)
        {
          cmdJoy_header = motorCmd_->header.stamp;
          if(motorCmd_->control_mode == 1)
          {
            double linear_x = motorCmd_->linear_x*scale_factor_l;
          double linear_y = motorCmd_->linear_y*scale_factor_l;
          double angular_z = motorCmd_->angular_z*scale_factor_a;

          front_right_joy = 1/(2 * M_PI * wheel_rad) * ((1)*linear_x + (-1)*linear_y - (AGV_len+AGV_wid)*angular_z/2) * gear_ratio;

          front_left_joy = 1/(2 * M_PI * wheel_rad) * (1*linear_x + (1)*linear_y + (AGV_len+AGV_wid)*angular_z/2) * gear_ratio;

          rear_left_joy = 1/(2 * M_PI * wheel_rad) * ((1)*linear_x + (-1)*linear_y + (AGV_len+AGV_wid)*angular_z/2) * gear_ratio;

          rear_right_joy = 1/(2 * M_PI * wheel_rad) * (1*linear_x + 1*linear_y - (AGV_len+AGV_wid)*angular_z/2) * gear_ratio;

          front_right_nav = 0;
          front_left_nav = 0;
          rear_left_nav = 0;
          rear_right_nav = 0;

          }
          else if(motorCmd_->control_mode == 2) // QR turning mode
          {

            double angular_z = motorCmd_->angular_z;

            front_right_joy = -1/(2 * M_PI * wheel_rad) * angular_z * gear_ratio * angz_scale;

            front_left_joy = 1/(2 * M_PI * wheel_rad) * angular_z * gear_ratio;

            rear_left_joy = 1/(2 * M_PI * wheel_rad) * angular_z * gear_ratio;

            rear_right_joy = -1/(2 * M_PI * wheel_rad) * angular_z * gear_ratio * angz_scale;
            
            front_right_nav = 0;
            front_left_nav = 0;
            rear_left_nav = 0;
            rear_right_nav = 0;

          }
          else
          {
              front_right_joy = 0;
              front_left_joy = 0;
              rear_left_joy = 0;
              rear_right_joy = 0;
          }
      }
  void PongsanAGV::publish_cmd(const ros::TimerEvent& e)
  {
      ros::Time time = ros::Time::now();
      double dt = (time - cmdJoy_header).toSec();
      if(fabs(dt) < 0.1)
      {
        set_speed[FRONT_LEFT] = front_left_joy + front_left_nav;
        set_speed[FRONT_RIGHT] = front_right_joy + front_right_nav;
        set_speed[REAR_LEFT] = rear_left_joy + rear_left_nav;
        set_speed[REAR_RIGHT] = rear_right_joy + rear_right_nav;
      }
      else
      {
          set_speed[FRONT_LEFT] =  front_left_nav;
          set_speed[FRONT_RIGHT] =  front_right_nav;
          set_speed[REAR_LEFT] =  rear_left_nav;
          set_speed[REAR_RIGHT] =  rear_right_nav;
      }


    setVelocityMoveCommand();
  }

        // set mode and speed command for the channel
  bool PongsanAGV::setVelocityMoveCommand()
	{

    md2k_can_driver_msgs::four_motor_command DRIVER;

    DRIVER.mode = 1;
    DRIVER.front_left_cmd_spd = set_speed[FRONT_LEFT];
    DRIVER.rear_left_cmd_spd = set_speed[REAR_LEFT];
    DRIVER.front_right_cmd_spd = set_speed[FRONT_RIGHT];
    DRIVER.rear_right_cmd_spd = set_speed[REAR_RIGHT];

    pub_moveCmd.publish(DRIVER);

	}



int main(int argc, char** argv)
{
	ros::init(argc, argv, node_name);
  PongsanAGV Pongsan;

	//ros::spin();
	usleep(100000);

        while (!ros::isShuttingDown())
        {
		ros::spinOnce();
		//--------------------------------------
		ros::Duration(0.01).sleep();
        }

	return 1;
}
