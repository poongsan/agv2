﻿
/**
* @file : roboteq_interface.cpp
* @author : SIS
* @date : 2020.02.06
* @brief : CONTROL
* @todo
* @bug
* @warning
*/

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <memory.h>
#include <math.h>

#include "ros/ros.h" 
#include "ros/time.h"
#include "boost/thread/thread.hpp"

#include <std_msgs/UInt8.h>
#include <std_srvs/SetBool.h>

// include message in md2k motor driver
#include <md2k_can_driver_msgs/motor_ENC_data.h>
#include <md2k_can_driver_msgs/motor_SPD_data.h>
#include <md2k_can_driver_msgs/motor_command.h>

// include message for choosing mode and desired velocity
#include "docent_msg/AGV_mecanum_move.h"
#include "docent_msg/move_lock.h"

// include message to advertise wheel speed data
#include "move_control/WheelSpeed.h"
#include "geometry_msgs/Twist.h"


// #define PI 3.1415927


using namespace std;
using namespace ros;

string node_name("MD2KInterface");


template<class T> std::string timeToStr(T ros_t)
{
	char buf[1024] = "";
	time_t t = ros_t.sec;
	struct tm* tms = localtime(&t);
	strftime(buf, 1024, "%Y-%m-%d_%H-%M-%S", tms);
	return string(buf);
}



class Bongsan_AGV {

        enum {
                FRONT_LEFT = 0, FRONT_RIGHT = 2, REAR_LEFT = 1, REAR_RIGHT = 3
        };
	ros::NodeHandle nh_;

      ros::Publisher pub_moveCmd[4];

      ros::Publisher pub_wheel_speed;

      ros::Subscriber sub_joyMoveCmd; // Motor driver movement control command

      ros::Subscriber sub_speed_AGV[4];

      ros::Subscriber sub_encoder_AGV[4];

      double measured_speed[4];
      double encoder_abs[4];
      double pre_measured_speed[4];
      double cal_acc[4];
      double set_speed[4];
      int front_left = 0;
      int front_right = 2;
      int rear_left = 1;
      int rear_right = 3;


        // AGV data
        double AGV_len = 2.19;
        double AGV_wid = 2.2;
        double wheel_wid = 0.306;
        double wheel_rad = 0.275;
        double gear_ratio = 20;


private:
        ros::Timer PublishWheelSpeedTimer;
public:

	/**
	* @brief Constructor
	*/
        //Bongsan_AGV(string portName_, int portBaudrate_ )
        Bongsan_AGV()
	{
          set_speed[FRONT_LEFT] = 0;
          set_speed[FRONT_RIGHT] = 0;
          set_speed[REAR_LEFT] = 0;
          set_speed[REAR_RIGHT] = 0;

          measured_speed[FRONT_LEFT] = 0;
          measured_speed[FRONT_RIGHT] = 0;
          measured_speed[REAR_LEFT] = 0;
          measured_speed[REAR_RIGHT] = 0;

          encoder_abs[FRONT_LEFT] = 0;
          encoder_abs[FRONT_RIGHT] = 0;
          encoder_abs[REAR_LEFT] = 0;
          encoder_abs[REAR_RIGHT] = 0;

          pre_measured_speed[FRONT_LEFT] = 0;
          pre_measured_speed[FRONT_RIGHT] = 0;
          pre_measured_speed[REAR_LEFT] = 0;
          pre_measured_speed[REAR_RIGHT] = 0;

          cal_acc[FRONT_LEFT] = 0;
          cal_acc[FRONT_RIGHT] = 0;
          cal_acc[REAR_LEFT] = 0;
          cal_acc[REAR_RIGHT] = 0;

          // subcribe speed data from driver
          sub_speed_AGV[FRONT_LEFT] = nh_.subscribe("motor_front_left/speed_data", 1, &Bongsan_AGV::front_left_AGV_Speed_Callback, this);
          sub_speed_AGV[FRONT_RIGHT] = nh_.subscribe("motor_front_right/speed_data", 1, &Bongsan_AGV::front_right_AGV_Speed_Callback, this);
          sub_speed_AGV[REAR_LEFT] = nh_.subscribe("motor_rear_left/speed_data", 1, &Bongsan_AGV::rear_left_AGV_Speed_Callback, this);
          sub_speed_AGV[REAR_RIGHT] = nh_.subscribe("motor_rear_right/speed_data", 1, &Bongsan_AGV::rear_right_AGV_Speed_Callback, this);

          // subcribe absolute encoder data from driver
          sub_encoder_AGV[FRONT_LEFT] = nh_.subscribe("motor_front_left/encoder_data", 1, &Bongsan_AGV::front_left_AGV_Encoder_Callback, this);
          sub_encoder_AGV[FRONT_RIGHT] = nh_.subscribe("motor_front_right/encoder_data", 1, &Bongsan_AGV::front_right_AGV_Encoder_Callback, this);
          sub_encoder_AGV[REAR_LEFT] = nh_.subscribe("motor_front_left/encoder_data", 1, &Bongsan_AGV::front_left_AGV_Encoder_Callback, this);
          sub_encoder_AGV[REAR_RIGHT] = nh_.subscribe("motor_front_right/encoder_data", 1, &Bongsan_AGV::front_right_AGV_Encoder_Callback, this);

          // subcribe commanded velocity
          sub_joyMoveCmd = nh_.subscribe("cmd_vel", 1, &Bongsan_AGV::moveControlCallback, this);

          // advertise message about motor command
          pub_moveCmd[FRONT_LEFT] = nh_.advertise<md2k_can_driver_msgs::motor_command>("/FRONT_LEFT/cmd", 1, true);
          pub_moveCmd[FRONT_RIGHT] = nh_.advertise<md2k_can_driver_msgs::motor_command>("/FRONT_RIGHT/cmd", 1, true);
          pub_moveCmd[REAR_LEFT] = nh_.advertise<md2k_can_driver_msgs::motor_command>("/FRONT_LEFT/cmd", 1, true);
          pub_moveCmd[REAR_RIGHT] = nh_.advertise<md2k_can_driver_msgs::motor_command>("/FRONT_RIGHT/cmd", 1, true);

          // advertise wheel speed to odom node
          pub_wheel_speed = nh_.advertise<move_control::WheelSpeed>("/WheelSpeed", 1, true);

          // Make sampling for wheel data publish
          PublishWheelSpeedTimer = nh_.createTimer(ros::Duration(0.01), &Bongsan_AGV::publish_Speed_data, this);
	}

        ~Bongsan_AGV()
	{
	}

        void front_left_AGV_Speed_Callback(const md2k_can_driver_msgs::motor_SPD_data Speed_Value)
        {
          measured_speed[FRONT_LEFT] = Speed_Value.rpm;
        }
        void front_right_AGV_Speed_Callback(const md2k_can_driver_msgs::motor_SPD_data Speed_Value)
        {
          measured_speed[FRONT_RIGHT] = Speed_Value.rpm;
        }
        void rear_left_AGV_Speed_Callback(const md2k_can_driver_msgs::motor_SPD_data Speed_Value)
        {
          measured_speed[REAR_LEFT] = Speed_Value.rpm;
        }
        void rear_right_AGV_Speed_Callback(const md2k_can_driver_msgs::motor_SPD_data Speed_Value)
        {
          measured_speed[REAR_RIGHT] = Speed_Value.rpm;
        }

        void front_left_AGV_Encoder_Callback(const md2k_can_driver_msgs::motor_ENC_data Encoder_Value)
        {
          encoder_abs[FRONT_LEFT] = Encoder_Value.encoder_stick;
        }

        void front_right_AGV_Encoder_Callback(const md2k_can_driver_msgs::motor_ENC_data Encoder_Value)
        {
          encoder_abs[FRONT_RIGHT] = Encoder_Value.encoder_stick;
        }

        void rear_right_AGV_Encoder_Callback(const md2k_can_driver_msgs::motor_ENC_data Encoder_Value)
        {
          encoder_abs[REAR_RIGHT] = Encoder_Value.encoder_stick;
        }

        void rear_left_AGV_Encoder_Callback(const md2k_can_driver_msgs::motor_ENC_data Encoder_Value)
        {
          encoder_abs[REAR_LEFT] = Encoder_Value.encoder_stick;
        }

        void publish_Speed_data(const ros::TimerEvent& e)
        {
          move_control::WheelSpeed wheel_data_;

          if (pre_measured_speed[FRONT_LEFT] == 0) {
            cal_acc[FRONT_LEFT] = 0;
          }
          else {
            int temp = (measured_speed[FRONT_LEFT] - pre_measured_speed[FRONT_LEFT]);
            cal_acc[FRONT_LEFT] = temp;
          }
          pre_measured_speed[FRONT_LEFT] = measured_speed[FRONT_LEFT];


          if (pre_measured_speed[FRONT_RIGHT] == 0) {
            cal_acc[FRONT_RIGHT] = 0;
          }
          else {
            int temp = (measured_speed[FRONT_RIGHT] - pre_measured_speed[FRONT_RIGHT]);
            cal_acc[FRONT_RIGHT] = 1 * temp;
          }
          pre_measured_speed[FRONT_RIGHT] = measured_speed[FRONT_RIGHT];

          if (pre_measured_speed[REAR_LEFT] == 0) {
            cal_acc[REAR_LEFT] = 0;
          }
          else {
            //int temp=abs(measured_speed[REAR_LEFT]-pre_measured_speed[REAR_LEFT]);
            int temp = (measured_speed[REAR_LEFT] - pre_measured_speed[REAR_LEFT]);
            cal_acc[REAR_LEFT] = temp;

          }
          pre_measured_speed[REAR_LEFT] = measured_speed[REAR_LEFT];


          if (pre_measured_speed[REAR_RIGHT] == 0) {
            cal_acc[REAR_RIGHT] = 0;
          }
          else {
            //int temp=abs(measured_speed[REAR_RIGHT]-pre_measured_speed[REAR_RIGHT]);
            int temp = (measured_speed[REAR_RIGHT] - pre_measured_speed[REAR_RIGHT]);
            cal_acc[REAR_RIGHT] = 1 * temp;
          }
          pre_measured_speed[REAR_RIGHT] = measured_speed[REAR_RIGHT];

          wheel_data_.header.stamp = ros::Time::now();

          wheel_data_.front_left_speed = measured_speed[FRONT_LEFT];
          wheel_data_.front_right_speed = measured_speed[FRONT_RIGHT];
          wheel_data_.rear_left_speed = measured_speed[REAR_LEFT];
          wheel_data_.rear_right_speed = measured_speed[REAR_RIGHT];

          wheel_data_.front_left_acc = cal_acc[FRONT_LEFT];
          wheel_data_.front_right_acc = cal_acc[FRONT_RIGHT];
          wheel_data_.rear_left_acc = cal_acc[REAR_LEFT];
          wheel_data_.rear_right_acc = cal_acc[REAR_RIGHT];

          wheel_data_.front_left_encoder = encoder_abs[FRONT_LEFT];
          wheel_data_.front_right_encoder = encoder_abs[FRONT_RIGHT];
          wheel_data_.rear_left_encoder = encoder_abs[REAR_LEFT];
          wheel_data_.rear_right_encoder = encoder_abs[REAR_RIGHT];

          pub_wheel_speed.publish(wheel_data_);
        }

        //// function declare which information that driving_one_motor node subcribed from teleop node
        void moveControlCallback(const geometry_msgs::Twist::ConstPtr& motorCmd_)
	{

            double linear_x = motorCmd_->linear.x;
            double linear_y = motorCmd_->linear.y;
            double angular_z = motorCmd_->angular.z;
            double front_left = 0, front_right = 0, rear_left = 0, rear_right = 0;

            front_right = 1/(2 * M_PI * wheel_rad) * ((1)*linear_x + (-1)*linear_y - (AGV_len+AGV_wid)*angular_z/2) * gear_ratio;

            front_left = 1/(2 * M_PI * wheel_rad) * (1*linear_x + (1)*linear_y + (AGV_len+AGV_wid)*angular_z/2) * gear_ratio;

            rear_left = 1/(2 * M_PI * wheel_rad) * ((1)*linear_x + (-1)*linear_y + (AGV_len+AGV_wid)*angular_z/2) * gear_ratio;

            rear_right = 1/(2 * M_PI * wheel_rad) * (1*linear_x + 1*linear_y - (AGV_len+AGV_wid)*angular_z/2) * gear_ratio;


            set_speed[FRONT_LEFT] = front_left;
            set_speed[FRONT_RIGHT] = front_right;
            set_speed[REAR_LEFT] = rear_left;
            set_speed[REAR_RIGHT] = rear_right;

            setVelocityMoveCommand();
		return;
	}

        // set mode and speed command for the channel
	bool setVelocityMoveCommand()
	{

            md2k_can_driver_msgs::motor_command DRIVER[4];

            DRIVER[FRONT_LEFT].mode = 1;
            DRIVER[FRONT_LEFT].cmd_spd = set_speed[FRONT_LEFT];
            DRIVER[FRONT_LEFT].motor_ID = front_left;

            DRIVER[FRONT_RIGHT].mode = 1;
            DRIVER[FRONT_RIGHT].cmd_spd = set_speed[FRONT_RIGHT];
            DRIVER[FRONT_RIGHT].motor_ID = front_right;

            DRIVER[REAR_LEFT].mode = 1;
            DRIVER[REAR_LEFT].cmd_spd = set_speed[REAR_LEFT];
            DRIVER[REAR_LEFT].motor_ID = rear_left;

            DRIVER[REAR_RIGHT].mode = 1;
            DRIVER[REAR_RIGHT].cmd_spd = set_speed[REAR_RIGHT];
            DRIVER[REAR_RIGHT].motor_ID = rear_right;

            pub_moveCmd[FRONT_LEFT].publish(DRIVER[FRONT_LEFT]);
            pub_moveCmd[FRONT_RIGHT].publish(DRIVER[FRONT_RIGHT]);
            pub_moveCmd[REAR_LEFT].publish(DRIVER[REAR_LEFT]);
            pub_moveCmd[REAR_RIGHT].publish(DRIVER[REAR_RIGHT]);

	}


	//
	bool setStopMoveCommand()
	{	

    md2k_can_driver_msgs::motor_command DRIVER[4];

    DRIVER[FRONT_LEFT].mode = 0;
    DRIVER[FRONT_LEFT].cmd_spd = 0;
    DRIVER[FRONT_LEFT].motor_ID = front_left;

    DRIVER[FRONT_RIGHT].mode = 0;
    DRIVER[FRONT_RIGHT].cmd_spd = 0;
    DRIVER[FRONT_RIGHT].motor_ID = front_right;

    DRIVER[REAR_LEFT].mode = 0;
    DRIVER[REAR_LEFT].cmd_spd = 0;
    DRIVER[REAR_LEFT].motor_ID = rear_left;

    DRIVER[REAR_RIGHT].mode = 0;
    DRIVER[REAR_RIGHT].cmd_spd = 0;
    DRIVER[REAR_RIGHT].motor_ID = rear_right;

    pub_moveCmd[FRONT_LEFT].publish(DRIVER[FRONT_LEFT]);
    pub_moveCmd[FRONT_RIGHT].publish(DRIVER[FRONT_RIGHT]);
    pub_moveCmd[REAR_LEFT].publish(DRIVER[REAR_LEFT]);
    pub_moveCmd[REAR_RIGHT].publish(DRIVER[REAR_RIGHT]);

	}


};

int main(int argc, char** argv)
{
	ros::init(argc, argv, node_name);
        Bongsan_AGV BongsanAGV;

	//ros::spin();
	usleep(100000);

        while (!ros::isShuttingDown())
        {
		ros::spinOnce();
		//--------------------------------------
		ros::Duration(0.01).sleep();
        }

	return 1;
}
