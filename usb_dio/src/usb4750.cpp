#include "ros/ros.h"
#include "ros/time.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <memory.h>
#include <termios.h>
#include <errno.h>
#include <unistd.h>

#include <iostream>
#include <string>

#include <math.h>
#include "bdaqctrl.h"
#include "compatibility.h"
#include <if_node_msgs/plcStat.h>
#include "usb_dio/dIOSig.h"

//using namespace std;
using namespace Automation::BDaq;
//-----------------------------------------------------------------------------------
// Configure the following parameters before running the demo
//-----------------------------------------------------------------------------------
typedef unsigned char byte;
#define  deviceDescription  L"USB-4750,BID#0"
//const wchar_t* profilePath = L"../../profile/DemoDevice.xml";


class usb4750
{
public:
    usb4750();
    ~usb4750();
    int spin();

protected:


    private:

        ros::NodeHandle nh_;
        ros::Publisher pubDIO;
        ros::Subscriber subPlc;

        void dIOPublish();
        void ioControlCallback(const if_node_msgs::plcStat::ConstPtr& msg);

        int rate_ = 20;
        int32_t    startPort = 0;
        int32_t    portCount = 2;

        int32_t plcPort_ = 1;
        uint32 plcVal_ = 1;

        bool DIs_[16], DOs_[16];
        const wchar_t* profilePath = L"../../profile/DemoDevice.xml";


        bool plcSig_ = false, oldPlcSig_ = false;
        ErrorCode ret, ret2;
        InstantDiCtrl * instantDiCtrl;
        InstantDoCtrl * instantDoCtrl ;




};


usb4750::usb4750()
{

    pubDIO = nh_.advertise<usb_dio::dIOSig>("/io_in", 1000);
    subPlc = nh_.subscribe("/PLC/Status", 1, &usb4750::ioControlCallback, this);

    for(int i = 0; i<16; i++)
    {
        DOs_[i] = true;
    }

    for(int i = 0; i<16; i++)
    {
        DIs_[i] = true;
    }



}

void usb4750::ioControlCallback(const if_node_msgs::plcStat::ConstPtr& msg)
{
    plcSig_ = msg->status;
    if(!plcSig_ && oldPlcSig_)
        plcVal_ = 0;
    else if(!oldPlcSig_ && plcSig_)
        plcVal_ = 1;

    oldPlcSig_ = plcSig_;
}


usb4750::~usb4750()
{
   // instantDiCtrl->Dispose();
   // instantDoCtrl->Dispose();
}



void usb4750::dIOPublish()
{
    usb_dio::dIOSig dIOSignal;

    for(int i=0; i<16; i++)
    {
        dIOSignal.DIs[i] = DIs_[i];
        dIOSignal.DOs[i] = DOs_[i];
    }

    pubDIO.publish(dIOSignal);

}

int usb4750::spin()
{

        ros::Rate loop_rate(rate_);

           // CHK_RESULT(ret);

        while(ros::ok() & (!kbhit()))    // continouslly receive signals from a serial port
        {
            ret = Success, ret2 = Success;
            // Step 1: Create a 'InstantDiCtrl' for DI function.
            instantDiCtrl = InstantDiCtrl::Create();
            instantDoCtrl = InstantDoCtrl::Create();

            DeviceInformation devInfo(deviceDescription);
            ret = instantDiCtrl->setSelectedDevice(devInfo);
                CHK_RESULT(ret);
            ret = instantDiCtrl->LoadProfile(profilePath);//Loads a profile to initialize the device.
                CHK_RESULT(ret);

             ret2 = instantDoCtrl->setSelectedDevice(devInfo);
                CHK_RESULT(ret);
             ret2 = instantDoCtrl->LoadProfile(profilePath);//Loads a profile to initialize the device.
            byte  bufferForReading[64] = {0};//the first element of this array is used for start port


           ret = instantDiCtrl->Read(startPort, portCount, bufferForReading);

           // CHK_RESULT(ret);

           for(int i = 0; i<8; i++)
           {
               DIs_[i] = (bool) ((bufferForReading[startPort] >> i) & 0x01);
           }

           for(int i = 0; i<8; i++)
           {
               DIs_[i+8] = (bool) ((bufferForReading[startPort+1] >> i) & 0x01);
           }

           if(plcVal_ == 0)
               DOs_[1] = false;
           else
               DOs_[1] = true;

           dIOPublish();


           ret2 = instantDoCtrl->WriteBit(startPort, plcPort_, plcVal_);
           //ret2 = instantDoCtrl->WriteBit(startPort, plcPort_, 1);


           ros::spinOnce();
           loop_rate.sleep();

        }
        ret2 = instantDoCtrl->WriteBit(startPort, plcPort_, 0);


        instantDiCtrl->Dispose();
        instantDoCtrl->Dispose();

        // If something wrong in this execution, print the error code on screen for tracking.
       if(BioFailed(ret))
       {
          wchar_t enumString[256];
          AdxEnumToString(L"ErrorCode", (int32)ret, 256, enumString);
          printf("Some error occurred. And the last error code is 0x%X. [%ls]\n", ret, enumString);
       }

        return 0;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "~");
  usb4750 usbIO;
  return usbIO.spin();
}


