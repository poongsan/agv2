#include <ros/ros.h>
#include "ros/time.h"
#include <ros/console.h>
#include <serial/serial.h>
#include <signal.h>
#include <string>
#include <sstream>
#include "md2k_can_driver.h"
#include <md2k_can_driver_msgs/motor_ENC_data.h>
#include <md2k_can_driver_msgs/motor_SPD_data.h>



md2k_can_driver::md2k_can_driver()
{
  // CBA Read local params (from launch file)
  ros::NodeHandle nhLocal("~");

  nhLocal.param<std::string>("cmdvel_topic", cmdvel_topic, "cmd_vel");
  nhLocal.param<std::string>("port", port, port);
  nhLocal.param("encoder_ppr", encoder_ppr, encoder_ppr);
  nhLocal.param("speed_scale", speed_scale, speed_scale);

  cmdvel_sub = nh.subscribe(cmdvel_topic, 10, &md2k_can_driver::cmdvel_callback, this);

  vel_sampling_time = nh.createTimer(ros::Duration(0.01), &md2k_can_driver::timer_callback, this);
  publish_sampling_time = nh.createTimer(ros::Duration(0.01), &md2k_can_driver::pub_callback, this);

  encoder_data = nh.advertise<md2k_can_driver_msgs::motor_ENC_data>("/encoder_data",10);
  speed_data = nh.advertise<md2k_can_driver_msgs::motor_ENC_data>("/speed_data",10);

}


void md2k_can_driver::cmdvel_callback(const geometry_msgs::Twist& twist_msg)
{
   motor_speed = twist_msg.linear.x;
}

void md2k_can_driver::timer_callback(const ros::TimerEvent& e)
{
  int32_t motor_power = static_cast<int> (motor_speed*speed_scale);
  Send_VEL_COMMAND(MOTOR_DRIVER_3,motor_power);
}
void md2k_can_driver::pub_callback(const ros::TimerEvent& e2)
{
  encoder_publish();
  speed_publish();
}

int md2k_can_driver::SendCAN(unsigned char byID, unsigned char byPID, unsigned char byDataNum, unsigned char byArray[])
{
 DWORD dwID;
 unsigned char i, byData[10];
 for(i=0; i<8; i++) byData[i] = 0;
 dwID = byID;
 dwID |= (DWORD)MID_PC<<8;
 dwID |= (DWORD)MID_BLDC_CTR<<16;
 byData[0] = byPID;
 if(byDataNum>7) byDataNum = 7;
 for(i=0; i<byDataNum; i++) byData[i+1] = byArray[i];

 CanMsg PCMsg;
 PCMsg.m_iID = dwID;
 PCMsg.m_iLen = 8;
 PCMsg.set(byData[0], byData[1], byData[2], byData[3],
         byData[4], byData[5], byData[6], byData[7]);
 PCAN_MOTOR->transmitMsg(PCMsg,true);
 return 1;
}

int md2k_can_driver::Send_VEL_COMMAND(unsigned char MotorDriver_ID,int speed_cmd)
{
  unsigned char bydata[7];
  IByte Ret = Int2Byte(speed_cmd);
  bydata[0] = Ret.byLow;
  bydata[1] = Ret.byHigh;
  for(int i=2;i<7;i++)
    bydata[i]= 0;

  SendCAN(MotorDriver_ID,PID_VEL_CMD,7,bydata);
  return 1;
}

int md2k_can_driver::STOP_MOTOR_NATURAL(unsigned char MotorDriver_ID)
{
  unsigned char bydata[7];
  for (int i = 0; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_TQ_OFF,7,bydata);
  return 1;
}

int md2k_can_driver::STOP_MOTOR_FAST(unsigned char MotorDriver_ID)
{
  unsigned char bydata[7];
  for (int i = 0; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_BRAKE,7,bydata);
  return 1;
}

int md2k_can_driver::SET_CLUTCH_OFF(unsigned char MotorDriver_ID)
{
  unsigned char bydata[7];
  bydata[0] = CMD_CLUTCH_OFF;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}

int md2k_can_driver::SET_CLUTCH_ON(unsigned char MotorDriver_ID)
{
  unsigned char bydata[7];
  bydata[0] = CMD_CLUTCH_ON;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}

int md2k_can_driver::SET_INV_COMMAND(unsigned char MotorDriver_ID)
{
  unsigned char bydata[7];
  bydata[0] = 1;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_INV_SIGN_CMD,7,bydata);
  return 1;
}
int md2k_can_driver::SET_NORMAL_COMMAND(unsigned char MotorDriver_ID)
{
  unsigned char bydata[7];
  bydata[0] = 0;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_INV_SIGN_CMD,7,bydata);
  return 1;
}

int md2k_can_driver::SET_MONITOR_ON(unsigned char MotorDriver_ID)
{
  unsigned char bydata[7];
  bydata[0] = CMD_MONITOR_BC_ON;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}

int md2k_can_driver::SET_MONITOR_OFF(unsigned char MotorDriver_ID)
{
  unsigned char bydata[7];
  bydata[0] = CMD_MONITOR_BC_OFF;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}

int md2k_can_driver::POSITION_RESET(unsigned char MotorDriver_ID)
{
  unsigned char bydata[7];
  bydata[0] = CMD_POSI_RESET;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}

int md2k_can_driver::REQUEST_MOTOR_SPEED(unsigned char MotorDriver_ID)
{
  unsigned char bydata[7];
  bydata[0] = PID_INT_RPM_DATA;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_REQ_PID_DATA,7,bydata);
  return 1;
}

int md2k_can_driver::GET_MOTOR_SPEED(unsigned char MotorDriver_ID)
{
  int motor_sp = 0;
  LByte header;
  // Parsing CAN message
  CanMsg Msg;
  bool bRet;
    // read from can
  bRet = PCAN_MOTOR->receiveMsg(&Msg);
  if(bRet)
  {
    header = Long2Byte(Msg.m_iID);
    if((header.byData1 == 0) &&(header.byData2 == MID_PC) &&(header.byData3 == MID_BLDC_CTR))
    {
      if((header.byData4 == MotorDriver_ID) && (Msg.getAt(0) == PID_INT_RPM_DATA))
      {
        motor_sp = Byte2Int(Msg.getAt(1),Msg.getAt(2));
      }
    }
  }
  return motor_sp;
}

int md2k_can_driver::GET_MOTOR_POS(unsigned char MotorDriver_ID)
{
  int motor_pos = 0;
  LByte header;
  // Parsing CAN message
  CanMsg Msg;
  bool bRet;
    // read from can
  bRet = PCAN_MOTOR->receiveMsg(&Msg);
  if(bRet)
  {
    header = Long2Byte(Msg.m_iID);
    if((header.byData1 == 0) &&(header.byData2 == MID_PC) &&(header.byData3 == MID_BLDC_CTR))
    {
      if((header.byData4 == MotorDriver_ID) && (Msg.getAt(0) == PID_POSI_DATA))
      {
        motor_pos = Byte2Long(Msg.getAt(1),Msg.getAt(2),Msg.getAt(3),Msg.getAt(4));
      }
    }
  }
  return motor_pos;
}


short md2k_can_driver::GetMonitor()
{
  // Parsing CAN message
  CanMsg Msg;
  unsigned char byData[7];
  bool bRet;
    // read from can
  bRet = PCAN_MOTOR->receiveMsg(&Msg);

  while(bRet == true)
  {
    if((Msg.m_iID == DRIVER3_TO_PC_HEADER) && (Msg.getAt(0) == PID_MONITOR))
    {
      for(int i=0;i++;i<7)
      byData[i] = Msg.getAt(i+1);
    }
  }

  // Status
  byAlarm = (byData[0] & 0x01);
  byCtrlFail = (byData[0]>>1) & 0x01;
  byOverVolt = (byData[0]>>2) & 0x01;
  byOverTemp = (byData[0]>>3) & 0x01;
  byOverLoad = (byData[0]>>4) & 0x01;
  byHallFail = (byData[0]>>5) & 0x01;
  byInvVel = (byData[0]>>6) & 0x01;
  byStall = (byData[0]>>7) & 0x01;
  motor_RPM = Byte2Int(byData[1], byData[2]);
  enc_STICK = Byte2Long(byData[3], byData[4], byData[5], byData[6]);
  return 1;
}


void md2k_can_driver::MD2K_driver_configure()
{
  SET_CLUTCH_OFF(MOTOR_DRIVER_3);
  POSITION_RESET(MOTOR_DRIVER_3);
  STOP_MOTOR_NATURAL(MOTOR_DRIVER_3);
  SET_NORMAL_COMMAND(MOTOR_DRIVER_3);
  SET_MONITOR_ON(MOTOR_DRIVER_3);
}
void md2k_can_driver::encoder_publish()
{
  md2k_can_driver_msgs::motor_ENC_data encoder_value;
  encoder_value.header.stamp = ros::Time::now();
  encoder_value.encoder_stick = enc_STICK;

  encoder_data.publish(encoder_value);
}

void md2k_can_driver::speed_publish()
{
  md2k_can_driver_msgs::motor_SPD_data speed_value;
  speed_value.header.stamp = ros::Time::now();
  speed_value.rpm = motor_RPM;
  speed_data.publish(speed_value);
}

int md2k_can_driver::run()
{

  ROS_INFO("Setup CAN Port...");
  //PCAN_MOTOR->setPort("/dev/pcanusb32");
  //PCAN_MOTOR->setBaud(CANITFBAUD_500K);

  //ROS_INFO("Initial CAN Port...");
  //PCAN_MOTOR->init_ret();
  unsigned char bydata[7];
  bydata[0] = 0x56;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  PCAN_MOTOR->init();
  CanMsg PCMsg;
  PCMsg.m_iID = 0xb7b801;
  PCMsg.m_iLen = 8;
  PCMsg.set(bydata[0], bydata[1], bydata[2], bydata[3],
          bydata[4], bydata[5], bydata[6], bydata[7]);

  //MD2K_driver_configure();
  // GetMonitor();


  ROS_INFO("Beginning looping...");

  while (ros::ok())
  {
    //GetMonitor();
    PCAN_MOTOR->transmitMsg(PCMsg,true);
    ros::spinOnce();
  }

  ros::AsyncSpinner spinner(4);

   spinner.start();

   ros::waitForShutdown();
   //spinner.stop();
  ROS_INFO("Exiting");

  return 0;
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "Motor_driver_node");

  md2k_can_driver motor_driver;

  signal(SIGINT, mySigintHandler);

  return motor_driver.run();
}
