#include <ros/ros.h>
#include "ros/time.h"
#include <ros/console.h>
#include <signal.h>
#include <string>
#include <sstream>
#include "bongsanAGV_motor_driver_2.h"

md2k_pcan_driver::md2k_pcan_driver():
  encoder_ppr(2048),
  speed_scale(1000)
{
  ros::NodeHandle nhLocal("~");

  nhLocal.param<std::string>("cmdvel_topic", cmdvel_topic, "cmd");
  nhLocal.param("encoder_ppr", encoder_ppr, encoder_ppr);
  nhLocal.param("speed_scale", speed_scale, speed_scale);

  cmdvel_sub = nh.subscribe(cmdvel_topic, 1000, &md2k_pcan_driver::cmdvel_callback, this);

  vel_sampling_time = nh.createTimer(ros::Duration(0.01), &md2k_pcan_driver::timer_callback, this);
  publish_sampling_time = nh.createTimer(ros::Duration(0.01), &md2k_pcan_driver::pub_callback, this);

  ser_reset = nh.advertiseService("motor_reset", &md2k_pcan_driver::reset_callback, this);

  encoder_data = nh.advertise<md2k_can_driver_msgs::four_ENC_data>("/encoder_data",10);
  speed_data = nh.advertise<md2k_can_driver_msgs::four_SPD_data>("/speed_data",10);
  status_data = nh.advertise<md2k_can_driver_msgs::motor_status>("/motor_status",10);

  pub_motorStatus[front_left] = nh.advertise<std_msgs::UInt8>("FL/motor_er",10);
  pub_motorStatus[rear_left] = nh.advertise<std_msgs::UInt8>("RL/motor_er",10);
  pub_motorStatus[front_right] = nh.advertise<std_msgs::UInt8>("FR/motor_er",10);
  pub_motorStatus[rear_right] = nh.advertise<std_msgs::UInt8>("RR/motor_er",10);


  for(int i = 0;i<4;i++)
  {
    motor_speed[i] = 0;
    motor_RPM[i] = 0;
    enc_STICK[i] = 0;

    // Add Dinh 20200506
    bit_Alarm[i] = 0;
    bit_CtrlFail[i] = 0;
    bit_OverVolt[i] = 0;
    bit_OverTemp[i] = 0;
    bit_OverLoad[i] = 0;
    bit_HallFail[i] = 0;
    bit_InvVel[i] = 0;
    bit_Stall[i] = 0;
  }
}

md2k_pcan_driver::~md2k_pcan_driver() {
}

bool md2k_pcan_driver::reset_callback(if_node_msgs::motor_reset::Request &req, if_node_msgs::motor_reset::Response &res)
{
  if(req.request_1)
  {
    stop_fast_all_motor();
    TPCANStatus Status;
    Status = ALARM_RESET(MOTOR_FRONT_LEFT);
    if (Status == PCAN_ERROR_OK)
    {
      res.response_1 = true;
    }
    else
    {
      res.response_1 = false;
    }
  }

  if(req.request_2)
  {
    stop_fast_all_motor();
    TPCANStatus Status;
    Status = ALARM_RESET(MOTOR_FRONT_RIGHT);
    if (Status == PCAN_ERROR_OK)
    {
      res.response_2 = true;
    }
    else
    {
      res.response_2 = false;
    }
  }
  if(req.request_3)
  {
    stop_fast_all_motor();
    TPCANStatus Status;
    Status = ALARM_RESET(MOTOR_REAR_LEFT);
    if (Status == PCAN_ERROR_OK)
    {
      res.response_3 = true;
    }
    else
    {
      res.response_3 = false;
    }
  }
  if(req.request_4)
  {
    stop_fast_all_motor();
    TPCANStatus Status;
    Status = ALARM_RESET(MOTOR_REAR_RIGHT);
    if (Status == PCAN_ERROR_OK)
    {
      res.response_4 = true;
    }
    else
    {
      res.response_4 = false;
    }
  }

  return true;
}

void md2k_pcan_driver::cmdvel_callback(const md2k_can_driver_msgs::four_motor_command& command_spd)
{

  if(command_spd.mode == 1)
   { motor_speed[front_left] = command_spd.front_left_cmd_spd;
  motor_speed[front_right] = command_spd.front_right_cmd_spd;
  motor_speed[rear_left] = command_spd.rear_left_cmd_spd;
  motor_speed[rear_right] = command_spd.rear_right_cmd_spd;}
  else
  {
     motor_speed[front_left] = 0;
     motor_speed[front_right] = 0;
     motor_speed[rear_left] = 0;
     motor_speed[rear_right] = 0;
  }
}

void md2k_pcan_driver::timer_callback(const ros::TimerEvent& e)
{
    int32_t fl_motor_power = static_cast<int> (motor_speed[front_left]*speed_scale);
    Send_VEL_COMMAND(MOTOR_FRONT_LEFT,fl_motor_power);

    int32_t fr_motor_power = static_cast<int> (motor_speed[front_right]*speed_scale);
    Send_VEL_COMMAND(MOTOR_FRONT_RIGHT,fr_motor_power);

    int32_t rl_motor_power = static_cast<int> (motor_speed[rear_left]*speed_scale);
    Send_VEL_COMMAND(MOTOR_REAR_LEFT,rl_motor_power);

    int32_t rr_motor_power = static_cast<int> (motor_speed[rear_right]*speed_scale);
    Send_VEL_COMMAND(MOTOR_REAR_RIGHT,rr_motor_power);

}
void md2k_pcan_driver::pub_callback(const ros::TimerEvent& e2)
{
  encoder_publish();
  speed_publish();
}

TPCANStatus md2k_pcan_driver::SendCAN(BYTE byID, BYTE byPID, BYTE byDataNum, BYTE byArray[])
{
  DWORD dwID;
 BYTE i, byData[10];
 for(i=0; i<8; i++) byData[i] = 0;
 dwID = byID;
 dwID |= (DWORD)MID_PC<<8;
 dwID |= (DWORD)MID_BLDC_CTR<<16;
 byData[0] = byPID;
 if(byDataNum>7) byDataNum = 7;
 for(i=0; i<byDataNum; i++) byData[i+1] = byArray[i];

 TPCANMsg Message;
 TPCANStatus Status;
 Message.ID = dwID;
 Message.LEN = 8;
 Message.MSGTYPE = PCAN_MESSAGE_EXTENDED;
 for(i=0;i<8;i++) Message.DATA[i] = byData[i];

 Status = CAN_Write(PCAN_DEVICE, &Message);
 return Status;
}


int md2k_pcan_driver::Send_VEL_COMMAND(BYTE MotorDriver_ID,int speed_cmd)
{
  BYTE bydata[7];
  IByte Ret = Int2Byte(speed_cmd);
  bydata[0] = Ret.byLow;
  bydata[1] = Ret.byHigh;
  for(int i=2;i<7;i++)
    bydata[i]= 0;

  SendCAN(MotorDriver_ID,PID_VEL_CMD,7,bydata);
  return 1;
}

int md2k_pcan_driver::STOP_MOTOR_NATURAL(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  for (int i = 0; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_TQ_OFF,7,bydata);
  return 1;
}

int md2k_pcan_driver::STOP_MOTOR_FAST(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  for (int i = 0; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_BRAKE,7,bydata);
  return 1;
}

int md2k_pcan_driver::SET_CLUTCH_OFF(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = CMD_CLUTCH_OFF;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}

int md2k_pcan_driver::SET_CLUTCH_ON(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = CMD_CLUTCH_ON;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}

int md2k_pcan_driver::SET_INV_COMMAND(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = 1;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_INV_SIGN_CMD,7,bydata);
  return 1;
}
int md2k_pcan_driver::SET_NORMAL_COMMAND(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = 0;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_INV_SIGN_CMD,7,bydata);
  return 1;
}

int md2k_pcan_driver::SET_MONITOR_ON(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = (BYTE)CMD_MONITOR_BC_ON;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,(BYTE)PID_COMMAND,7,bydata);
  return 1;
}

int md2k_pcan_driver::SET_MONITOR_OFF(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = CMD_MONITOR_BC_OFF;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}

int md2k_pcan_driver::POSITION_RESET(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = CMD_POSI_RESET;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}
// Add Dinh 200507
TPCANStatus md2k_pcan_driver::ALARM_RESET(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = CMD_ALARM_RESET;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  TPCANStatus Status;
  Status=SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return Status;
}

int md2k_pcan_driver::REQUEST_MOTOR_SPEED(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = PID_INT_RPM_DATA;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  TPCANStatus Status;
  Status=SendCAN(MotorDriver_ID,PID_REQ_PID_DATA,7,bydata);
  if (Status == PCAN_ERROR_OK)
  {
    ROS_INFO("Request speed");
  }
  else
  {
    ROS_INFO("Fail to request speed");
  }
  return 1;
}

int md2k_pcan_driver::REQUEST_MOTOR_POS(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = PID_POSI_DATA;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  TPCANStatus Status;
  Status=SendCAN(MotorDriver_ID,PID_REQ_PID_DATA,7,bydata);
  if (Status == PCAN_ERROR_OK)
  {
    ROS_INFO("Request POS");
  }
  else
  {
    ROS_INFO("Fail to request POS");
  }
  return 1;
}


int md2k_pcan_driver::GET_MOTOR_SPEED(BYTE MotorDriver_ID)
{
  int motor_sp = 0;
  DWORD MsgID = 0;
  if(MotorDriver_ID == MOTOR_DRIVER_1) MsgID = DRIVER1_TO_PC_HEADER;
  if(MotorDriver_ID == MOTOR_DRIVER_2) MsgID = DRIVER2_TO_PC_HEADER;
  if(MotorDriver_ID == MOTOR_DRIVER_3) MsgID = DRIVER3_TO_PC_HEADER;
  if(MotorDriver_ID == MOTOR_DRIVER_4) MsgID = DRIVER4_TO_PC_HEADER;
  // Parsing CAN message
  TPCANMsg Message;
  bool Status = CAN_Read(PCAN_DEVICE, &Message, NULL);
  if(!Status)
  {

    if(Message.ID == MsgID)
    {
      //if(Message.DATA[0] == (BYTE)PID_INT_RPM_DATA)
      if(Message.DATA[0] == 0x8a)
      {
        motor_sp = Byte2Int(Message.DATA[1],Message.DATA[2]);
      }
      ROS_INFO("Receiving motor speed");
    }
    else
    {
      ROS_INFO("Cannot receive motor speed");
    }
  }
  return motor_sp;
}

long md2k_pcan_driver::GET_MOTOR_POS(BYTE MotorDriver_ID)
{
  long motor_pos = 0;
  DWORD MsgID = 0x0;
  if(MotorDriver_ID == MOTOR_DRIVER_1) MsgID = DRIVER1_TO_PC_HEADER;
  if(MotorDriver_ID == MOTOR_DRIVER_2) MsgID = DRIVER2_TO_PC_HEADER;
  if(MotorDriver_ID == MOTOR_DRIVER_3) MsgID = DRIVER3_TO_PC_HEADER;
  if(MotorDriver_ID == MOTOR_DRIVER_4) MsgID = DRIVER4_TO_PC_HEADER;
  // Parsing CAN message
  TPCANMsg Message;
  bool Status = CAN_Read(PCAN_DEVICE, &Message, NULL);

  if(!Status)
  {

      if(Message.ID == MsgID)
      {
       //if(Message.DATA[0] == (BYTE)PID_POSI_DATA)
        if(Message.DATA[0] == 0xc5)
        {
        motor_pos = Byte2Long(Message.DATA[1],Message.DATA[2],Message.DATA[3],Message.DATA[4]);
        }
      //ROS_INFO("Receiving motor pos");
       }
    else
    {
      ROS_INFO("Cannot receive motor pos");
    }
  }
  return motor_pos;
}

short md2k_pcan_driver::GetMonitor()
{
  // Parsing CAN message
  TPCANMsg Message;
  bool Status = CAN_Read(PCAN_DEVICE, &Message, NULL);

  if(!Status)
  {
    if(Message.ID == FRONT_LEFT_TO_PC_HEADER)
    {
      //if(Message.DATA[0] == (BYTE)PID_MONITOR)
      if(Message.DATA[0] == 0xc4)
      {
          motor_RPM[front_left] = Byte2Int(Message.DATA[2], Message.DATA[3]);
          enc_STICK[front_left] = Byte2Long(Message.DATA[3], Message.DATA[5], Message.DATA[6], Message.DATA[7]);
          // Add Dinh 20200506
          bit_Alarm[front_left] = (Message.DATA[1] & 0x01);
          bit_CtrlFail[front_left] = (Message.DATA[1]>>1) & 0x01;
          bit_OverVolt[front_left] = (Message.DATA[1]>>2) & 0x01;
          bit_OverTemp[front_left] = (Message.DATA[1]>>3) & 0x01;
          bit_OverLoad[front_left] = (Message.DATA[1]>>4) & 0x01;
          bit_HallFail[front_left] = (Message.DATA[1]>>5) & 0x01;
          bit_InvVel[front_left] = (Message.DATA[1]>>6) & 0x01;
          bit_Stall[front_left] = (Message.DATA[1]>>7) & 0x01;
          std_msgs::UInt8 mot_er;
          /*
          if(bit_Alarm[front_left]==1 || bit_CtrlFail[front_left]==1 || bit_OverVolt[front_left]==1 || bit_OverTemp[front_left]==1 || bit_OverLoad[front_left]==1 || bit_HallFail[front_left]==1 || bit_InvVel[front_left]==1 || bit_Stall[front_left]==1)
          {
              mot_er.data = 1;
          }
          else
          {
              mot_er.data = 0;
          }
          */
          mot_er.data = (uint8_t) Message.DATA[1];
          pub_motorStatus[front_left].publish(mot_er);

          status_.front_left_bit_Alarm = (int8_t) Message.DATA[1];

/*
          md2k_can_driver_msgs::motor_error error_fl;
          error_fl.alm = bit_Alarm[front_left];
          error_fl.ctrlFail = bit_CtrlFail[front_left];
          error_fl.overVolt = bit_OverVolt[front_left];
          error_fl.overTemp = bit_OverTemp[front_left];
          error_fl.overLoad = bit_OverLoad[front_left];
          error_fl.hallFail = bit_HallFail[front_left];
          error_fl.invVel = bit_InvVel[front_left];
          error_fl.stall = bit_Stall[front_left];
          pub_motorStatus[front_left].publish(error_fl);
          */

      }
    }
    if(Message.ID == FRONT_RIGHT_TO_PC_HEADER)
    {
      //if(Message.DATA[0] == (BYTE)PID_MONITOR)
      if(Message.DATA[0] == 0xc4)
      {
        motor_RPM[front_right] = Byte2Int(Message.DATA[2], Message.DATA[3]);
        enc_STICK[front_right] = Byte2Long(Message.DATA[3], Message.DATA[5], Message.DATA[6], Message.DATA[7]);
        // Add Dinh 20200506
        bit_Alarm[front_right] = (Message.DATA[1] & 0x01);
        bit_CtrlFail[front_right] = (Message.DATA[1]>>1) & 0x01;
        bit_OverVolt[front_right] = (Message.DATA[1]>>2) & 0x01;
        bit_OverTemp[front_right] = (Message.DATA[1]>>3) & 0x01;
        bit_OverLoad[front_right] = (Message.DATA[1]>>4) & 0x01;
        bit_HallFail[front_right] = (Message.DATA[1]>>5) & 0x01;
        bit_InvVel[front_right] = (Message.DATA[1]>>6) & 0x01;
        bit_Stall[front_right] = (Message.DATA[1]>>7) & 0x01;
        std_msgs::UInt8 mot_er;
        /*
        if(bit_Alarm[front_right]==1 || bit_CtrlFail[front_right]==1 || bit_OverVolt[front_right]==1 || bit_OverTemp[front_right]==1 || bit_OverLoad[front_right]==1 || bit_HallFail[front_right]==1 || bit_InvVel[front_right]==1 || bit_Stall[front_right]==1)
        {
            mot_er.data = 1;
        }
        else
        {
            mot_er.data = 0;
        }
        */
        mot_er.data = (uint8_t) Message.DATA[1];
        pub_motorStatus[front_right].publish(mot_er);
        status_.front_right_bit_Alarm = (int8_t) Message.DATA[1];
/*
        md2k_can_driver_msgs::motor_error error_fr;
        error_fr.alm = bit_Alarm[front_right];
        error_fr.ctrlFail = bit_CtrlFail[front_right];
        error_fr.overVolt = bit_OverVolt[front_right];
        error_fr.overTemp = bit_OverTemp[front_right];
        error_fr.overLoad = bit_OverLoad[front_right];
        error_fr.hallFail = bit_HallFail[front_right];
        error_fr.invVel = bit_InvVel[front_right];
        error_fr.stall = bit_Stall[front_right];
        pub_motorStatus[front_right].publish(error_fr);
        */
      }
    }
    if(Message.ID == REAR_LEFT_TO_PC_HEADER)
    {
      //if(Message.DATA[0] == (BYTE)PID_MONITOR)
      if(Message.DATA[0] == 0xc4)
      {
        motor_RPM[rear_left] = -Byte2Int(Message.DATA[2], Message.DATA[3]);
        enc_STICK[rear_left] = -Byte2Long(Message.DATA[3], Message.DATA[5], Message.DATA[6], Message.DATA[7]);

        bit_Alarm[rear_left] = (Message.DATA[1] & 0x01);
        bit_CtrlFail[rear_left] = (Message.DATA[1]>>1) & 0x01;
        bit_OverVolt[rear_left] = (Message.DATA[1]>>2) & 0x01;
        bit_OverTemp[rear_left] = (Message.DATA[1]>>3) & 0x01;
        bit_OverLoad[rear_left] = (Message.DATA[1]>>4) & 0x01;
        bit_HallFail[rear_left] = (Message.DATA[1]>>5) & 0x01;
        bit_InvVel[rear_left] = (Message.DATA[1]>>6) & 0x01;
        bit_Stall[rear_left] = (Message.DATA[1]>>7) & 0x01;
/*
        md2k_can_driver_msgs::motor_error error_rl;
        error_rl.alm = bit_Alarm[rear_left];
        error_rl.ctrlFail = bit_CtrlFail[rear_left];
        error_rl.overVolt = bit_OverVolt[rear_left];
        error_rl.overTemp = bit_OverTemp[rear_left];
        error_rl.overLoad = bit_OverLoad[rear_left];
        error_rl.hallFail = bit_HallFail[rear_left];
        error_rl.invVel = bit_InvVel[rear_left];
        error_rl.stall = bit_Stall[rear_left];
        pub_motorStatus[rear_left].publish(error_rl);
        */

        std_msgs::UInt8 mot_er;
        /*
        if(bit_Alarm[rear_left]==1 || bit_CtrlFail[rear_left]==1 || bit_OverVolt[rear_left]==1 || bit_OverTemp[rear_left]==1 || bit_OverLoad[rear_left]==1 || bit_HallFail[rear_left]==1 || bit_InvVel[rear_left]==1 || bit_Stall[rear_left]==1)
        {
            mot_er.data = 1;
        }
        else
        {
            mot_er.data = 0;
        }
        */
        mot_er.data = (uint8_t) Message.DATA[1];
        pub_motorStatus[rear_left].publish(mot_er);
        status_.rear_left_bit_Alarm = (int8_t) Message.DATA[1];
      }
    }
    if(Message.ID == REAR_RIGHT_TO_PC_HEADER)
    {
      //if(Message.DATA[0] == (BYTE)PID_MONITOR)
      if(Message.DATA[0] == 0xc4)
      {
        motor_RPM[rear_right] = -Byte2Int(Message.DATA[2], Message.DATA[3]);
        enc_STICK[rear_right] = -Byte2Long(Message.DATA[3], Message.DATA[5], Message.DATA[6], Message.DATA[7]);

        // Add Dinh 20200506
        bit_Alarm[rear_right] = (Message.DATA[1] & 0x01);
        bit_CtrlFail[rear_right] = (Message.DATA[1]>>1) & 0x01;
        bit_OverVolt[rear_right] = (Message.DATA[1]>>2) & 0x01;
        bit_OverTemp[rear_right] = (Message.DATA[1]>>3) & 0x01;
        bit_OverLoad[rear_right] = (Message.DATA[1]>>4) & 0x01;
        bit_HallFail[rear_right] = (Message.DATA[1]>>5) & 0x01;
        bit_InvVel[rear_right] = (Message.DATA[1]>>6) & 0x01;
        bit_Stall[rear_right] = (Message.DATA[1]>>7) & 0x01;

        std_msgs::UInt8 mot_er;
        /*
        if(bit_Alarm[rear_right]==1 || bit_CtrlFail[rear_right]==1 || bit_OverVolt[rear_right]==1 || bit_OverTemp[rear_right]==1 || bit_OverLoad[rear_right]==1 || bit_HallFail[rear_right]==1 || bit_InvVel[rear_right]==1 || bit_Stall[rear_right]==1)
        {
            mot_er.data = 1;
        }
        else
        {
            mot_er.data = 0;
        }
        */
        mot_er.data = (uint8_t) Message.DATA[1];
        pub_motorStatus[rear_right].publish(mot_er);

        status_.rear_right_bit_Alarm = (int8_t) Message.DATA[1];
/*
        md2k_can_driver_msgs::motor_error error_rr;
        error_rr.alm = bit_Alarm[rear_right];
        error_rr.ctrlFail = bit_CtrlFail[rear_right];
        error_rr.overVolt = bit_OverVolt[rear_right];
        error_rr.overTemp = bit_OverTemp[rear_right];
        error_rr.overLoad = bit_OverLoad[rear_right];
        error_rr.hallFail = bit_HallFail[rear_right];
        error_rr.invVel = bit_InvVel[rear_right];
        error_rr.stall = bit_Stall[rear_right];
        pub_motorStatus[rear_right].publish(error_rr);
        */
      }
    }
  }
  return 1;
}

void md2k_pcan_driver::stop_all_motor()
{
  STOP_MOTOR_NATURAL(MOTOR_FRONT_LEFT);
  STOP_MOTOR_NATURAL(MOTOR_REAR_LEFT);
  STOP_MOTOR_NATURAL(MOTOR_FRONT_RIGHT);
  STOP_MOTOR_NATURAL(MOTOR_REAR_RIGHT);
}

void md2k_pcan_driver::stop_fast_all_motor()
{
  STOP_MOTOR_FAST(MOTOR_FRONT_LEFT);
  STOP_MOTOR_FAST(MOTOR_FRONT_RIGHT);
  STOP_MOTOR_FAST(MOTOR_REAR_LEFT);
  STOP_MOTOR_FAST(MOTOR_REAR_RIGHT);
}

int md2k_pcan_driver::SET_ACC(BYTE MotorDriver_ID, int acc)
{
    BYTE bydata[7];
    IByte Ret = Int2Byte(acc);
    bydata[0] = Ret.byLow;
    bydata[1] = Ret.byHigh;
    for (int i = 1; i < 7; i++)
    {
      bydata[i] = 0;
    }
    SendCAN(MotorDriver_ID,PID_SLOW_START,7,bydata);
    return 1;
}

void md2k_pcan_driver::MD2K_driver_configure()
{
  SET_CLUTCH_OFF(MOTOR_FRONT_LEFT);
  POSITION_RESET(MOTOR_FRONT_LEFT);
  STOP_MOTOR_NATURAL(MOTOR_FRONT_LEFT);
  SET_MONITOR_ON(MOTOR_FRONT_LEFT);


  SET_CLUTCH_OFF(MOTOR_REAR_LEFT);
  POSITION_RESET(MOTOR_REAR_LEFT);
  STOP_MOTOR_NATURAL(MOTOR_REAR_LEFT);
  SET_MONITOR_ON(MOTOR_REAR_LEFT);

  SET_CLUTCH_OFF(MOTOR_FRONT_RIGHT);
  POSITION_RESET(MOTOR_FRONT_RIGHT);
  STOP_MOTOR_NATURAL(MOTOR_FRONT_RIGHT);
  SET_MONITOR_ON(MOTOR_FRONT_RIGHT);

  SET_CLUTCH_OFF(MOTOR_REAR_RIGHT);
  POSITION_RESET(MOTOR_REAR_RIGHT);
  STOP_MOTOR_NATURAL(MOTOR_REAR_RIGHT);
  SET_MONITOR_ON(MOTOR_REAR_RIGHT);

  SET_ACC(MOTOR_FRONT_LEFT, 1000);
  SET_ACC(MOTOR_REAR_LEFT, 1000);
  SET_ACC(MOTOR_FRONT_RIGHT, 1000);
  SET_ACC(MOTOR_REAR_RIGHT, 1000);

/*
  SET_NORMAL_COMMAND(MOTOR_FRONT_LEFT);
  SET_NORNAL_COMMAND(MOTOR_FRONT_RIGHT);
  SET_INV_COMMAND(MOTOR_REAR_LEFT);
  SET_INV_COMMAND(MOTOR_REAR_RIGHT);
  */

  // Modify 211215

  SET_INV_COMMAND(MOTOR_FRONT_LEFT);
  SET_INV_COMMAND(MOTOR_FRONT_RIGHT);
  SET_NORMAL_COMMAND(MOTOR_REAR_LEFT);
  SET_NORMAL_COMMAND(MOTOR_REAR_RIGHT);
}


void md2k_pcan_driver::encoder_publish()
{
  md2k_can_driver_msgs::four_ENC_data encoder_value;
  encoder_value.header.stamp = ros::Time::now();
  encoder_value.front_left_encoder_stick = -enc_STICK[front_left];
  encoder_value.front_right_encoder_stick = -enc_STICK[front_right];
  encoder_value.rear_left_encoder_stick = -enc_STICK[rear_left];
  encoder_value.rear_right_encoder_stick = -enc_STICK[rear_right];

  encoder_data.publish(encoder_value);
}

void md2k_pcan_driver::status_publish()
{
//  md2k_can_driver_msgs::motor_status status_;
  status_.header.stamp = ros::Time::now();
//  status_.front_left_bit_Alarm = bit_Alarm[front_left];
//  status_.front_right_bit_Alarm = bit_Alarm[front_right];
//  status_.rear_left_bit_Alarm = bit_Alarm[rear_left];
//  status_.rear_right_bit_Alarm = bit_Alarm[rear_right];

  status_data.publish(status_);
}

void md2k_pcan_driver::speed_publish()
{
  md2k_can_driver_msgs::four_SPD_data speed_value;
  speed_value.header.stamp = ros::Time::now();
  speed_value.front_left_rpm = -motor_RPM[front_left];
  speed_value.front_right_rpm = -motor_RPM[front_right];
  speed_value.rear_left_rpm = -motor_RPM[rear_left];
  speed_value.rear_right_rpm = -motor_RPM[rear_right];
  speed_data.publish(speed_value);
}

int md2k_pcan_driver::run()
{

  ROS_INFO("Setup CAN Port...");
  ros::Rate loop_rate(100);

  // Modify Dinh 20200324
  TPCANStatus Status;
  DWORD pcan_device = PCAN_DEVICE;

#ifndef NO_RT
  mlockall(MCL_CURRENT | MCL_FUTURE);
#endif

  // Status = CAN_Initialize(pcan_device, PCAN_BAUD_1M, 0, 0, 0);
   Status = CAN_Initialize(pcan_device, PCAN_BAUD_500K, 0, 0, 0); // AGV1 and 2
  printf("CAN_Initialize(%xh): Status=0x%x\n", pcan_device, (int)Status);
  if (Status == PCAN_ERROR_OK)
    goto lbl_cont;
  else {
    goto lbl_exit;
  }

  lbl_cont:
  int fd;
  Status = CAN_GetValue(pcan_device, PCAN_RECEIVE_EVENT, &fd, sizeof fd);
  printf("CAN_GetValue(%xh): Status=0x%x\n", pcan_device, (int)Status);
  if (Status)
    goto lbl_close;

  MD2K_driver_configure();


  //starttime = millis();
  //mstimer = starttime;

  // forever loop
  while (ros::ok())
  {
    //GetMonitor();
      /*
    uint32_t nowtime = millis();
    // Handle 100 Hz request
    if (DELTAT(nowtime,mstimer) >= 10)
    {
      mstimer = nowtime;
      GetMonitor();
    }
    */

    GetMonitor();

    ros::spinOnce();
    loop_rate.sleep();
  }
  stop_all_motor();
  //stop_all_motor();

lbl_close:
  stop_all_motor();
  CAN_Uninitialize(pcan_device);

lbl_exit:
   stop_all_motor();
   ros::waitForShutdown();

  return 0;
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "Motor_driver_node");

  md2k_pcan_driver motor_driver;

   signal(SIGINT, mySigintHandler);

  return motor_driver.run();
}
