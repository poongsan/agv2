/* SPDX-License-Identifier: LGPL-2.1-only */
/*
 * pcaneventwrite.cpp - PCANBasic Example: Event Write
 *
 * Copyright (C) 2001-2020  PEAK System-Technik GmbH <www.peak-system.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Contact:    <linux@peak-system.com>
 * Author:     Stephane Grosjean <s.grosjean@peak-system.com>
 */
#include <ros/ros.h>
#include "ros/time.h"
#include <ros/console.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <asm/types.h>
#include <unistd.h>

#ifndef NO_RT
#include <sys/mman.h>

#ifdef RTAI
#include <rtai_lxrt.h>
#include <rtdm/rtdm.h>

/* overload the select() system call with the RTDM one, while it isn't by
 * RTAI 5.1. Note that this example doesn't care about the timeout
 */
#define select(n, r, w, e, t)	rt_dev_select(n, r, w, e, RTDM_TIMEOUT_INFINITE)
#endif

// PCAN-Basic device used to read on
// (RT version doesn't handle USB devices)
// #define PCAN_DEVICE     PCAN_PCIBUS2
#define PCAN_DEVICE     PCAN_USBBUS1
#else

// PCAN-Basic device used to read on
#define PCAN_DEVICE     PCAN_USBBUS1
#endif



#include "PCANBasic.h"


void mySigintHandler(int sig)
{
  ROS_INFO("Received SIGINT signal, shutting down...");
  ros::shutdown();
}

int main(int argc, char* argv[]) 
{
	TPCANStatus Status;
	unsigned int tx_count = 0;
	unsigned int pcan_device = PCAN_DEVICE;

#ifndef NO_RT
	mlockall(MCL_CURRENT | MCL_FUTURE);
#endif

  signal(SIGINT, mySigintHandler);

	Status = CAN_Initialize(pcan_device, PCAN_BAUD_500K, 0, 0, 0);
	printf("CAN_Initialize(%xh): Status=0x%x\n", pcan_device, (int)Status);
	if (Status)
		goto lbl_exit;

	int fd;
	Status = CAN_GetValue(pcan_device, PCAN_RECEIVE_EVENT, &fd, sizeof fd);
	printf("CAN_GetValue(%xh): Status=0x%x\n", pcan_device, (int)Status);
	if (Status)
		goto lbl_close;


  while (ros::ok()) {
      ros::spinOnce();
  }


lbl_close:
	CAN_Uninitialize(pcan_device);

lbl_exit:
  ros::waitForShutdown();
	return 0;
}
