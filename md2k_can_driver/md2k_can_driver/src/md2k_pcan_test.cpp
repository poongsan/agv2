#include <ros/ros.h>
#include "ros/time.h"
#include <ros/console.h>
#include <signal.h>
#include <string>
#include <sstream>
#include "md2k_pcan_driver.h"
#include <md2k_can_driver_msgs/motor_ENC_data.h>
#include <md2k_can_driver_msgs/motor_SPD_data.h>


md2k_pcan_driver::md2k_pcan_driver():
  encoder_ppr(2048),
  speed_scale(1000),
  inverse(false)
{
  ros::NodeHandle nhLocal("~");

  nhLocal.param<std::string>("cmdvel_topic", cmdvel_topic, "cmd_vel");
  nhLocal.param("encoder_ppr", encoder_ppr, encoder_ppr);
  nhLocal.param("speed_scale", speed_scale, speed_scale);
  nhLocal.param("motor_id", motor_id, motor_id);
  nhLocal.param<bool>("set_inverse", inverse, inverse);
  cmdvel_sub = nh.subscribe(cmdvel_topic, 1000, &md2k_pcan_driver::cmdvel_callback, this);

  vel_sampling_time = nh.createTimer(ros::Duration(0.01), &md2k_pcan_driver::timer_callback, this);
  publish_sampling_time = nh.createTimer(ros::Duration(0.01), &md2k_pcan_driver::pub_callback, this);

  encoder_data = nh.advertise<md2k_can_driver_msgs::motor_ENC_data>("/encoder_data",10);
  speed_data = nh.advertise<md2k_can_driver_msgs::motor_SPD_data>("/speed_data",10);

}

md2k_pcan_driver::~md2k_pcan_driver() {
}
void md2k_pcan_driver::cmdvel_callback(const geometry_msgs::Twist& twist_msg)
{
   motor_speed = twist_msg.linear.x;
}

void md2k_pcan_driver::timer_callback(const ros::TimerEvent& e)
{
  int32_t motor_power = static_cast<int> (motor_speed*speed_scale);
  if(motor_id == 1)
  Send_VEL_COMMAND(MOTOR_DRIVER_1,motor_power);
  if(motor_id == 2)
  Send_VEL_COMMAND(MOTOR_DRIVER_2,motor_power);
  if(motor_id == 3)
  Send_VEL_COMMAND(MOTOR_DRIVER_3,motor_power);
  if(motor_id == 4)
  Send_VEL_COMMAND(MOTOR_DRIVER_4,motor_power);
}
void md2k_pcan_driver::pub_callback(const ros::TimerEvent& e2)
{
  encoder_publish();
  speed_publish();
}

TPCANStatus md2k_pcan_driver::SendCAN(BYTE byID, BYTE byPID, BYTE byDataNum, BYTE byArray[])
{
  DWORD dwID;
 BYTE i, byData[10];
 for(i=0; i<8; i++) byData[i] = 0;
 dwID = byID;
 dwID |= (DWORD)MID_PC<<8;
 dwID |= (DWORD)MID_BLDC_CTR<<16;
 byData[0] = byPID;
 if(byDataNum>7) byDataNum = 7;
 for(i=0; i<byDataNum; i++) byData[i+1] = byArray[i];

 TPCANMsg Message;
 TPCANStatus Status;
 Message.ID = dwID;
 Message.LEN = 8;
 Message.MSGTYPE = PCAN_MESSAGE_EXTENDED;
 for(i=0;i<8;i++) Message.DATA[i] = byData[i];

 Status = CAN_Write(PCAN_DEVICE, &Message);
 return Status;
}

int md2k_pcan_driver::Send_VEL_COMMAND(BYTE MotorDriver_ID,int speed_cmd)
{
  BYTE bydata[7];
  IByte Ret = Int2Byte(speed_cmd);
  bydata[0] = Ret.byLow;
  bydata[1] = Ret.byHigh;
  for(int i=2;i<7;i++)
    bydata[i]= 0;

  SendCAN(MotorDriver_ID,PID_VEL_CMD,7,bydata);
  return 1;
}

int md2k_pcan_driver::STOP_MOTOR_NATURAL(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  for (int i = 0; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_TQ_OFF,7,bydata);
  return 1;
}

int md2k_pcan_driver::STOP_MOTOR_FAST(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  for (int i = 0; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_BRAKE,7,bydata);
  return 1;
}

int md2k_pcan_driver::SET_CLUTCH_OFF(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = CMD_CLUTCH_OFF;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}

int md2k_pcan_driver::SET_CLUTCH_ON(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = CMD_CLUTCH_ON;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}

int md2k_pcan_driver::SET_INV_COMMAND(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = 1;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_INV_SIGN_CMD,7,bydata);
  return 1;
}
int md2k_pcan_driver::SET_NORMAL_COMMAND(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = 0;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_INV_SIGN_CMD,7,bydata);
  return 1;
}

int md2k_pcan_driver::SET_MONITOR_ON(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = (BYTE)CMD_MONITOR_BC_ON;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,(BYTE)PID_COMMAND,7,bydata);
  return 1;
}

int md2k_pcan_driver::SET_MONITOR_OFF(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = CMD_MONITOR_BC_OFF;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}

int md2k_pcan_driver::POSITION_RESET(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = CMD_POSI_RESET;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  SendCAN(MotorDriver_ID,PID_COMMAND,7,bydata);
  return 1;
}

int md2k_pcan_driver::REQUEST_MOTOR_SPEED(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = PID_INT_RPM_DATA;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  TPCANStatus Status;
  Status=SendCAN(MotorDriver_ID,PID_REQ_PID_DATA,7,bydata);
  if (Status == PCAN_ERROR_OK)
  {
    ROS_INFO("Request speed");
  }
  else
  {
    ROS_INFO("Fail to request speed");
  }
  return 1;
}

int md2k_pcan_driver::REQUEST_MOTOR_POS(BYTE MotorDriver_ID)
{
  BYTE bydata[7];
  bydata[0] = PID_POSI_DATA;
  for (int i = 1; i < 7; i++)
  {
    bydata[i] = 0;
  }
  TPCANStatus Status;
  Status=SendCAN(MotorDriver_ID,PID_REQ_PID_DATA,7,bydata);
  if (Status == PCAN_ERROR_OK)
  {
    ROS_INFO("Request POS");
  }
  else
  {
    ROS_INFO("Fail to request POS");
  }
  return 1;
}
/*
int md2k_pcan_driver::GET_MOTOR_SPEED(BYTE MotorDriver_ID)
{
  int motor_sp = 0;
  LByte header;
  // Parsing CAN message
  TPCANMsg Message;
  bool Status = CAN_Read(PCAN_DEVICE, &Message, NULL);
  if(!Status)
  {
    header = Long2Byte(Message.ID);
    //if((header.byData1 == 0) &&(header.byData2 == MID_PC) &&(header.byData3 == MID_BLDC_CTR))
    if((header.byData1 == 0x00) &&(header.byData2 == 0xb8) &&(header.byData3 == 0xb7))
    {
      if((header.byData4 == MotorDriver_ID) && (Message.DATA[0] == PID_INT_RPM_DATA))
      {
        motor_sp = Byte2Int(Message.DATA[1],Message.DATA[2]);
      }
      ROS_INFO("Receiving motor speed");
    }
    else
    {
      ROS_INFO("Cannot receive motor speed");
    }
  }
  return motor_sp;
}

int md2k_pcan_driver::GET_MOTOR_POS(BYTE MotorDriver_ID)
{
  int motor_pos = 0;
  LByte header;
  // Parsing CAN message
  TPCANMsg Message;
  bool Status = CAN_Read(PCAN_DEVICE, &Message, NULL);

  if(!Status)
  {
    header = Long2Byte(Message.ID);
    //if((header.byData1 == 0x00) &&(header.byData2 == (BYTE)MID_PC) &&(header.byData3 == (BYTE)MID_BLDC_CTR))
      if((header.byData1 == 0x00) &&(header.byData2 == 0xb8) &&(header.byData3 == 0xb7))
    {
      //if((header.byData4 == MotorDriver_ID) && (Message.DATA[0] == PID_POSI_DATA))
       if((header.byData4 == MotorDriver_ID) && (Message.DATA[0] == 0xc5))
      {
        motor_pos = Byte2Long(Message.DATA[1],Message.DATA[2],Message.DATA[3],Message.DATA[4]);
      }
      ROS_INFO("Receiving motor pos");
    }
    else
    {
      ROS_INFO("Cannot receive motor pos");
    }
  }
  return motor_pos;
}
*/

int md2k_pcan_driver::GET_MOTOR_SPEED(BYTE MotorDriver_ID)
{
  int motor_sp = 0;
  DWORD MsgID = 0;
  if(MotorDriver_ID == MOTOR_DRIVER_1) MsgID = DRIVER1_TO_PC_HEADER;
  if(MotorDriver_ID == MOTOR_DRIVER_2) MsgID = DRIVER2_TO_PC_HEADER;
  if(MotorDriver_ID == MOTOR_DRIVER_3) MsgID = DRIVER3_TO_PC_HEADER;
  if(MotorDriver_ID == MOTOR_DRIVER_4) MsgID = DRIVER4_TO_PC_HEADER;
  // Parsing CAN message
  TPCANMsg Message;
  bool Status = CAN_Read(PCAN_DEVICE, &Message, NULL);
  if(!Status)
  {

    if(Message.ID == MsgID)
    {
      //if(Message.DATA[0] == (BYTE)PID_INT_RPM_DATA)
      if(Message.DATA[0] == 0x8a)
      {
        motor_sp = Byte2Int(Message.DATA[1],Message.DATA[2]);
      }
      ROS_INFO("Receiving motor speed");
    }
    else
    {
      ROS_INFO("Cannot receive motor speed");
    }
  }
  return motor_sp;
}

long md2k_pcan_driver::GET_MOTOR_POS(BYTE MotorDriver_ID)
{
  long motor_pos = 0;
  BYTE MsgID = 0;
  if(MotorDriver_ID == MOTOR_DRIVER_1) MsgID = DRIVER1_TO_PC_HEADER;
  if(MotorDriver_ID == MOTOR_DRIVER_2) MsgID = DRIVER2_TO_PC_HEADER;
  if(MotorDriver_ID == MOTOR_DRIVER_3) MsgID = DRIVER3_TO_PC_HEADER;
  if(MotorDriver_ID == MOTOR_DRIVER_4) MsgID = DRIVER4_TO_PC_HEADER;
  // Parsing CAN message
  TPCANMsg Message;
  bool Status = CAN_Read(PCAN_DEVICE, &Message, NULL);

  if(!Status)
  {

      if(Message.ID == MsgID)
      {
       //if(Message.DATA[0] == (BYTE)PID_POSI_DATA)
        if(Message.DATA[0] == 0xc5)
        {
        motor_pos = Byte2Long(Message.DATA[1],Message.DATA[2],Message.DATA[3],Message.DATA[4]);
        }
      //ROS_INFO("Receiving motor pos");
       }
    else
    {
      ROS_INFO("Cannot receive motor pos");
    }
  }
  return motor_pos;
}

short md2k_pcan_driver::GetMonitor()
{
  // Parsing CAN message
  TPCANMsg Message;
  bool Status = CAN_Read(PCAN_DEVICE, &Message, NULL);

  //BYTE byData[7];
  DWORD MsgID = 0;
  if(motor_id == 1) MsgID = DRIVER1_TO_PC_HEADER;
  if(motor_id == 2) MsgID = DRIVER2_TO_PC_HEADER;
  if(motor_id == 3) MsgID = DRIVER3_TO_PC_HEADER;
  if(motor_id == 4) MsgID = DRIVER4_TO_PC_HEADER;
  if(!Status)
  {
    if(Message.ID == MsgID)
    {
      //if(Message.DATA[0] == (BYTE)PID_MONITOR)
      if(Message.DATA[0] == 0xc4)
      {
          byAlarm = (Message.DATA[1] & 0x01);
          byCtrlFail = (Message.DATA[1]>>1) & 0x01;
          byOverVolt = (Message.DATA[1]>>2) & 0x01;
          byOverTemp = (Message.DATA[1]>>3) & 0x01;
          byOverLoad = (Message.DATA[1]>>4) & 0x01;
          byHallFail = (Message.DATA[1]>>5) & 0x01;
          byInvVel = (Message.DATA[1]>>6) & 0x01;
          byStall = (Message.DATA[1]>>7) & 0x01;
          motor_RPM = Byte2Int(Message.DATA[2], Message.DATA[3]);
          enc_STICK = Byte2Long(Message.DATA[3], Message.DATA[5], Message.DATA[6], Message.DATA[7]);
      }
    }
    else
    {
      ROS_INFO("Cannot receive motor data");
    }
  }
  return 1;
}


void md2k_pcan_driver::MD2K_driver_configure()
{
  BYTE motordriver = 0x00;
  if(motor_id == 1) motordriver = MOTOR_DRIVER_1;
  if(motor_id == 2) motordriver = MOTOR_DRIVER_2;
  if(motor_id == 3) motordriver = MOTOR_DRIVER_3;
  if(motor_id == 4) motordriver = MOTOR_DRIVER_4;

  SET_CLUTCH_OFF(motordriver);
  POSITION_RESET(motordriver);
  STOP_MOTOR_NATURAL(motordriver);
  SET_MONITOR_ON(motordriver);
  if(inverse) {SET_INV_COMMAND(motordriver);}
  else {SET_NORMAL_COMMAND(motordriver);}
}


void md2k_pcan_driver::encoder_publish()
{
  md2k_can_driver_msgs::motor_ENC_data encoder_value;
  encoder_value.header.stamp = ros::Time::now();
  encoder_value.encoder_stick = enc_STICK;

  encoder_data.publish(encoder_value);
}

void md2k_pcan_driver::speed_publish()
{
  md2k_can_driver_msgs::motor_SPD_data speed_value;
  speed_value.header.stamp = ros::Time::now();
  speed_value.rpm = motor_RPM;
  speed_data.publish(speed_value);
}

int md2k_pcan_driver::run()
{

  ROS_INFO("Setup CAN Port...");

  // Modify Dinh 20200324
  TPCANStatus Status;
  DWORD pcan_device = PCAN_DEVICE;

#ifndef NO_RT
  mlockall(MCL_CURRENT | MCL_FUTURE);
#endif


  Status = CAN_Initialize(pcan_device, PCAN_BAUD_500K, 0, 0, 0);
  printf("CAN_Initialize(%xh): Status=0x%x\n", pcan_device, (int)Status);
  if (Status == PCAN_ERROR_OK || Status == PCAN_ERROR_INITIALIZE)
    goto lbl_cont;
  else {
    goto lbl_exit;
  }

  lbl_cont:
  int fd;
  Status = CAN_GetValue(pcan_device, PCAN_RECEIVE_EVENT, &fd, sizeof fd);
  printf("CAN_GetValue(%xh): Status=0x%x\n", pcan_device, (int)Status);
  if (Status)
    goto lbl_close;

  MD2K_driver_configure();

  starttime = millis();
  mstimer = starttime;

  // forever loop
  while (ros::ok())
  {
    //GetMonitor();
    uint32_t nowtime = millis();
    // Handle 100 Hz request
    if (DELTAT(nowtime,mstimer) >= 10)
    {
      mstimer = nowtime;
      GetMonitor();
    }

    ros::spinOnce();
  }


lbl_close:
  CAN_Uninitialize(pcan_device);

lbl_exit:

   ros::waitForShutdown();

  return 0;
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "Motor_driver_node");

  md2k_pcan_driver motor_driver;

   signal(SIGINT, mySigintHandler);

  return motor_driver.run();
}
