#include <ros/ros.h>
#include "ros/time.h"
#include <ros/console.h>
#include <signal.h>
#include <string>
#include <sstream>
#include <cob_generic_can/CanPeakSysUSB.h>
#include <cob_generic_can/CanMsg.h>
#include <cob_utilities/IniFile.h>
#include "md2k_can_driver_parameter.h"
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
#include <stdlib.h>
#include <fcntl.h>    // O_RDWR
#include <libpcan.h>
#include <ctype.h>
#include <unistd.h>   // exit




 void mySigintHandler(int sig)
{
  ROS_INFO("Received SIGINT signal, shutting down...");
  ros::shutdown();
}


class md2k_can_driver
{

public:
  md2k_can_driver();
  void cmdvel_callback(const geometry_msgs::Twist& twist_msg);
  void timer_callback(const ros::TimerEvent& e);
  void pub_callback(const ros::TimerEvent& e2);
  void MD2K_driver_configure();
  int run();
  std::string port= "/dev/pcanusb32";
  int baud = CANITFBAUD_500K;
  CANPeakSysUSB* PCAN_MOTOR;

protected:
  ros::NodeHandle nh;

  //CANPeakSysUSB PCAN_MOTOR();
  int encoder_ppr;
  double max_rpm = 2000;
  float motor_speed = 0;
  int speed_scale = 1;
  long enc_STICK = 0;
  int motor_RPM = 0;

  std::string cmdvel_topic;

  ros::Subscriber cmdvel_sub;

  ros::Publisher encoder_data;
  ros::Publisher speed_data;

  ros::Timer vel_sampling_time, publish_sampling_time;

  void speed_publish();
  void encoder_publish();

  short GetMonitor();
  int GET_MOTOR_POS(unsigned char MotorDriver_ID);
  int GET_MOTOR_SPEED(unsigned char MotorDriver_ID);
  int REQUEST_MOTOR_SPEED(unsigned char MotorDriver_ID);
  int POSITION_RESET(unsigned char MotorDriver_ID);
  int SET_MONITOR_OFF(unsigned char MotorDriver_ID);
  int SET_MONITOR_ON(unsigned char MotorDriver_ID);
  int SET_NORMAL_COMMAND(unsigned char MotorDriver_ID);
  int SET_INV_COMMAND(unsigned char MotorDriver_ID);
  int SET_CLUTCH_ON(unsigned char MotorDriver_ID);
  int SET_CLUTCH_OFF(unsigned char MotorDriver_ID);
  int STOP_MOTOR_FAST(unsigned char MotorDriver_ID);
  int STOP_MOTOR_NATURAL(unsigned char MotorDriver_ID);
  int Send_VEL_COMMAND(unsigned char MotorDriver_ID,int speed_cmd);
  int SendCAN(unsigned char byID, unsigned char byPID, unsigned char byDataNum, unsigned char byArray[]);

  bool byAlarm;
  bool byCtrlFail;
  bool byOverVolt;
  bool byOverTemp;
  bool byOverLoad;
  bool byHallFail;
  bool byInvVel;
  bool byStall;

};

short Byte2Int(unsigned char byLow, unsigned char byHigh)
{
return (byLow | (short)byHigh<<8);
}

// Make long type data from four bytes
int Byte2Long(unsigned char byData1, unsigned char byData2, unsigned char byData3, unsigned char byData4)
{
return((int)byData1 | (int)byData2<<8 | (int)byData3<<16 | (int)byData4<<24);
}


typedef struct
{
unsigned char byLow;
unsigned char byHigh;
} IByte;

typedef struct
{
unsigned char byData1;
unsigned char byData2;
unsigned char byData3;
unsigned char byData4;
} LByte;

// Get the low and high byte from interger
IByte Int2Byte(short nIn)
{
IByte Ret;
Ret.byLow = nIn & 0xff;
Ret.byHigh = nIn>>8 & 0xff;
return Ret;
}

// Get the data bytes from long type data
LByte Long2Byte(int nIn)
{
  LByte Ret;
  Ret.byData1 = nIn & 0xff;
  Ret.byData2 = nIn>>8 & 0xff;
  Ret.byData3 = nIn>>16 & 0xff;
  Ret.byData4 = nIn>>24 & 0xff;
  return Ret;
}


