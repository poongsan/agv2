// Machine ID definition

#define MID_STX			0x02

#define MID_MAIN_CTR		128	// Main robot controller.
#define MID_HEAD_CTR		129	// Controller for head. 
#define MID_IR_REMOCON		130
#define MID_USM			131
#define MID_INFRARED		132
#define MID_REMOCON		133	
#define MID_BMS			134
#define MID_MMI			172
#define MID_RS			180 
#define MID_BLDC_CTR		183	// BLDC 50W Controller.
#define MID_PC		184
#define MID_BRIDGE_OPT		185

#define MID_FILE		0xfe	// For file in/out.
#define MID_ALL			0xfe

#define MY_MID			MID_BLDC_CTR	
#define MY_TMID			MID_BRIDGE_CTR




// PID Definition

// General ID definition
#define ID_ALL				0xfe
#define WRITE_CHK			0xaa
#define DEFAULT_WRITE_CHK		0x55	// Default setting(write)

///////////////////////////////////////////////////////
// Command : RMID, TMID, ID, PID, Data number, Data.., CHK 
/////////////////////////////////////////////
// PID one-byte data : PID 0~127

#define PID_VER				1
#define PID_DEFAULT_SET			3
#define PID_REQ_PID_DATA		4
#define PID_TQ_OFF			5
#define PID_BRAKE			6
#define PID_ACK				7

/////////////////////////////////////////
#define PID_COMMAND			10

#define CMD_TQ_OFF			2
#define CMD_BRAKE			4
#define CMD_MAIN_DATA_BC_ON		5
#define CMD_MAIN_DATA_BC_OFF		6

#define CMD_ALARM_RESET			8

#define CMD_POSI_RESET			10
#define CMD_MONITOR_BC_ON		11
#define CMD_MONITOR_BC_OFF		12
#define CMD_IO_MONITOR_BC_ON		13
#define CMD_IO_MONITOR_BC_OFF		14

#define CMD_FAN_ON			15
#define CMD_FAN_OFF			16
#define CMD_CLUTCH_ON			17
#define CMD_CLUTCH_OFF			18 

#define CMD_TAR_VEL_OFF			20
#define CMD_SLOW_START_OFF		21
#define CMD_SLOW_DOWN_OFF		22
#define CMD_CAN_RESEND_ON		23
#define CMD_CAN_RESEND_OFF		24
#define CMD_MAX_TQ_OFF			25
#define CMD_ENC_OFF			26
#define CMD_LOW_SPEED_LIMIT_OFF		27
#define CMD_HIGH_SPEED_LIMIT_OFF	28

//////////////////////////////////////////////
// Reset to default value of input range.
#define CMD_SPEED_LIMIT_OFF		29	

#define CMD_CURVE_FITTING_OFF		31
#define CMD_STEP_INPUT_OFF		32

#define CMD_DEVELOPER_ON		40
#define CMD_DEVELOPER_OFF		41
#define CMD_PHASE_FIL_ON		42
#define CMD_PHASE_FIL_OFF		43

#define CMD_UICOM_OFF			44
#define CMD_UICOM_ON			45
#define CMD_MAX_RPM_OFF			46
#define CMD_HALL_TYPE_OFF		47

#define CMD_LOW_POT_LIMIT_OFF		48
#define CMD_HIGH_POT_LIMIT_OFF		49

#define CMD_MAIN_DATA_BC_ON2		50
#define CMD_MAIN_DATA_BC_OFF2		51
#define CMD_MONITOR_BC_ON2		52
#define CMD_MONITOR_BC_OFF2		53
#define CMD_IO_MONITOR_BC_ON2		54
#define CMD_IO_MONITOR_BC_OFF2		55

#define CMD_SYS_START			56
#define CMD_SYS_STOP			57
#define CMD_SAVE_POSI_N_TIME		58
#define CMD_REGEN_TEST_ON		59
#define CMD_REGEN_TEST_OFF		60

///////////////////////////////////////////////
#define PID_ALARM_RESET			12
#define PID_POSI_RESET			13
#define PID_MAIN_BC_STATUS		14
#define PID_MONITOR_BC_STATUS		15
#define PID_INV_SIGN_CMD		16
#define PID_USE_LIMIT_SW		17
#define PID_INV_SIGN_CMD2		18
#define PID_INV_ALARM			19
#define PID_CYCLIC_FAULT                 1
#define PID_DCU_CMD			20

#define CMD_INIT_DOOR			1
#define CMD_OPEN_DOOR			2
#define CMD_CLS_DOOR			3
#define	CMD_STOP_DOOR			4
#define CMD_ALARM_ON			5
#define CMD_ALARM_OFF			6
#define CMD_TURN_CW			7
#define CMD_TURN_CCW			8
#define CMD_SOL_ON			9	// About 1s solenoid ON
#define CMD_OPEN_DOOR2			10
#define CMD_AGING_DOOR			11

#define PID_HALL_TYPE			21
// HALL_TYPE_4P			0
// HALL_TYPE_8P			1
// HALL_TYPE_10P		2
// HALL_TYPE_12P		3 
// HALL_TYPE_2P			4
// HALL_TYPE_6P			5

#define PID_SENSORLESS			22
#define PID_MOT_TYPE			23	// DC motor or BLDC
#define PID_STOP_STATUS			24
#define PID_INPUT_TYPE			25

// INPUT_ANALOG				0
// INPUT_JS				1
// INPUT_PULSE				2
// INPUT_RC				3
// INPUT_STEP				4
// INPUT_POT				5	// Position control by Potentiometer.

#define PID_PRESET_SAVE			30
#define PID_PRESET_RECALL		31
#define PID_PHASE_FILTER		32

#define PID_CTRL_STATUS			34
#define PID_INIT_SET			35	// AutoPan Limit detection.

#define PID_START_INV_SIGN		36
#define PID_RUN_INV_SIGN		37
#define PID_REGENERATION		38
#define PID_CTRL_STATUS2		39
#define PID_LIMIT_STOP_COND		40	// 0:TqOff, 1:Brake
#define PID_TQ_LIMIT_SW			41	// Use Tq as limit switchs.
#define PID_POSI_INPUT_MODE		42
#define PID_SINE_CTRL			43
#define PID_TQ_CTRL			44
#define PID_BLUETOOTH			45
#define PID_USE_EPOSI			46
#define PID_PWM_OUT_FAC			47	// 2 or 3
#define PID_DI				48
#define PID_IN_POSITION_OK		49
#define PID_DI2				50
#define PID_HOLD_ALARM_ON		51
#define PID_DIP_INV			52
#define PID_DIP_OPEN			53
#define PID_DIP_CHG			54
#define PID_RECT_SINE			55
#define PID_TURN_RATIO			56

#define PID_START_STOP			100
#define PID_CHG_BOOT_MODE		101
			 
/////////////////////////////////////////////////
// PID two-byte data : PID 128 ~ 192
#define PID_VEL_CMD			130
#define PID_VEL_CMD2			131

#define PID_ID				133	// default : 1
#define PID_OPEN_VEL_CMD		134
#define PID_BAUDRATE			135	// default : 7(115200bps) 
#define PID_MAIN_DATA_BC_STATE		136	// default : OFF
#define PID_ECAN_BITRATE		137	// 50K,100K,250K,500K,1M
#define PID_INT_RPM_DATA		138
#define PID_TQ_DATA			139
#define PID_TQ_CMD			140

#define PID_VOLT_IN			143

#define PID_DIFF_PO			145

// 0 no return, 1:Monitor, 2:Ack return
#define PID_RETURN_TYPE			149	
#define RETURN_TYPE_MONITOR		1
#define RETURN_TYPE_ACK			2
#define RETURN_TYPE_IO_MONITOR		3

#define PID_TQ_PO			150
#define PID_SPEED_PO			151
#define PID_MODULATION_TYPE		152
#define PID_SLOW_START			153
#define PID_SLOW_DOWN			154
#define PID_TAR_VEL			155	// Write target data.
#define PID_ENC_PPR			156	
#define PID_LOW_SPEED_LIMIT		157
#define PID_HIGH_SPEED_LIMIT		158
#define PID_QUICK_SLOW_DOWN		159

#define PID_PWM_OUT			160	// 0~1023 PWM output of OUT3
#define PID_SPEED_RESOLUTION		161
#define PID_DEAD_ZONE			162	// 0~1023
#define PID_READ_ADDR			163
#define PID_REQ_PID_DATA2		164	// PID, and more one variable
#define PID_AUTO_PAN			165
#define PID_REF_RPM			166
#define PID_PV_GAIN			167
#define PID_P_GAIN			168
#define PID_I_GAIN			169

#define PID_IN_POSITION			171
#define PID_LOW_POT_LIMIT		172
#define PID_HIGH_POT_LIMIT		173

#define PID_PNT_TQ_OFF			174
#define PID_PNT_BRAKE			175
#define PID_TAR_POSI_VEL		176

#define PID_POSI_SS			178
#define PID_POSI_SD			179
#define PID_COM_TAR_SPEED		180
#define PID_CW_MAX_RPM			181
#define PID_CCW_MAX_RPM			182
#define PID_FUNC_CMD_TYPE		183
#define PID_FUNC_CMD			184

#define PID_COM_WATCH_DELAY		185
#define PID_TQ_LIMIT_SW_VAL		187
#define PID_PWM_OUT2			188
#define PID_REGEN_START_VOLT		189
#define PID_MAX_OPEN_OUT		190


///////////////////////////////////////////////////
// PID N-byte data : PID 193 ~ 240
#define PID_MAIN_DATA			193	// States(vel, posi,etc)
#define PID_IO_MONITOR			194
#define PID_TAR_POSI			195
#define PID_MONITOR			196	// Velocity. Position.
#define PID_POSI_DATA			197				
#define PID_RPM_DATA			198				
#define PID_INC_TAR_POSI		199
#define PID_MAIN_DATA2			200	// Data on 2nd motor
#define PID_MONITOR2			201	// For PNT controller
#define PID_IO_MONITOR2			202			
#define PID_GAIN			203
#define PID_POSI_VEL_DATA		204

#define PID_TYPE			205

//////////// For Pan/Tilt control
#define PID_PNT_POSI_VEL_CMD		206
#define PID_PNT_VEL_CMD			207
#define PID_PNT_OPEN_VEL_CMD		208
#define PID_PNT_TQ_CMD			209
#define PID_PNT_MAIN_DATA		210
#define PID_MAX_TQ			211	// PID_MAX_CUR(LOAD)
#define PID_LIMIT_TQ			212
#define PID_RPM_CMD			213	// Unit is 1/10 rpm.
#define PID_RPM_CMD2			214	// Unit is 1/10 rpm.
#define PID_PNT_INC_POSI_CMD		215
#define PID_PNT_MONITOR			216

#define PID_POSI_SET			217
#define PID_POSI_SET2			218
#define PID_POSI_VEL_CMD		219
#define PID_INC_POSI_VEL_CMD		220	// Incremental posi. cmd.
#define PID_MAX_RPM			221
#define PID_SPEED_LIMIT			222
#define PID_MIN_RPM			223

#define PID_STEP_INPUT			225	// No, input.
#define PID_CURVE_PT			226	// No. PtX(int), PtY(int)
#define PID_PRESET_DATA			227	// only position.
#define PID_PRESET_DATA2		228	// For auto pan action.


#define PID_REF_POSI			230
#define PID_POSI_MIN_LIMIT		231
#define PID_POSI_CEN			232
#define PID_POSI_MAX_LIMIT		233

///////////// Developer accecible only.
#define PID_TIME			234
#define PID_TQ_GAIN			235
#define PID_POSI_VEL_CMD2		236
#define PID_VECTOR_CMD			237
#define PID_CAN_RESEND			238	// CAN Packet resending.
#define PID_FUNC_SPEED			239
#define PID_POSTURE_DATA		240			

//////////// For sinusoidal control
#define PID_PHASE_OFFSET		241	// Only read data.
#define PID_PNT_INC_POSI_VEL_CMD	242

#define PID_POSI_CMD			243
#define PID_INC_POSI_CMD		244
#define PID_WRITE_ADDR			245
#define PID_PNT_POSI_CMD		246
#define PID_PARAM			247
#define PID_DCU_SET_SPEED		248	// Open, Cls speed
#define PID_DCU_INIT_SET_SPEED		249

#define PID_FUNC_POSI			250
#define PID_INC_POSI_CMD2		251	
#define PID_ROBOT_CMD			252	
#define PID_ROBOT_MONITOR		253

///////////////////////////////////////////////////
#define PID_EXTRA_PID			254

// EXTRA PID 1~254
#define EPID_CCT_SET			193	// ACC, DEC, CON, 

// No. of Pole
#define NO_POLE_4		0
#define NO_POLE_8		1
#define NO_POLE_12	2

// CAN bitrate
#define CAN_50K		1
#define CAN_100K	2
#define CAN_250K	3
#define CAN_500K	4
#define CAN_1M		5

// Header
#define DRIVER1_TO_PC_HEADER 0xb8b701
#define DRIVER2_TO_PC_HEADER 0xb8b702
#define DRIVER3_TO_PC_HEADER 0xb8b703
#define DRIVER4_TO_PC_HEADER 0xb8b704

#define PC_TO_DRIVER1_HEADER 0xb7b801
#define PC_TO_DRIVER2_HEADER 0xb7b802
#define PC_TO_DRIVER3_HEADER 0xb7b803
#define PC_TO_DRIVER4_HEADER 0xb7b804

#define FRONT_LEFT_TO_PC_HEADER 0xb8b701
#define REAR_LEFT_TO_PC_HEADER 0xb8b702
#define FRONT_RIGHT_TO_PC_HEADER 0xb8b703
#define REAR_RIGHT_TO_PC_HEADER 0xb8b704

#define PC_TO_FRONT_LEFT_HEADER 0xb7b801
#define PC_TO_REAR_LEFT_HEADER 0xb7b802
#define PC_TO_FRONT_RIGHT_HEADER 0xb7b803
#define PC_TO_REAR_RIGHT_HEADER 0xb7b804

// MOTOR_DRIVER_ID
#define MOTOR_DRIVER_1 0x01
#define MOTOR_DRIVER_2 0x02
#define MOTOR_DRIVER_3 0x03
#define MOTOR_DRIVER_4 0x04

#define MOTOR_FRONT_LEFT 0x01
#define MOTOR_REAR_LEFT 0x02
#define MOTOR_FRONT_RIGHT 0x03
#define MOTOR_REAR_RIGHT 0x04

