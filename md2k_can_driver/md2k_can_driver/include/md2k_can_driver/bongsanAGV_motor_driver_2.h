#include "PCANBasic.h"
#include "md2k_can_driver_parameter.h"
#include "ros/time.h"
#include <asm/types.h>
#include <geometry_msgs/Twist.h>
#include <if_node_msgs/motor_reset.h>
#include <md2k_can_driver_msgs/four_ENC_data.h>
#include <md2k_can_driver_msgs/four_SPD_data.h>
#include <md2k_can_driver_msgs/four_motor_command.h>
#include <md2k_can_driver_msgs/motor_status.h>
#include <nav_msgs/Odometry.h>
#include <ros/console.h>
#include <ros/ros.h>
#include <signal.h>
#include <sstream>
#include <std_msgs/UInt8.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sys/mman.h>
#include <tf/transform_broadcaster.h>
#include <unistd.h>

#ifndef NO_RT
#include <sys/mman.h>

#ifdef RTAI
#include <rtai_lxrt.h>
#include <rtdm/rtdm.h>

/* overload the select() system call with the RTDM one, while it isn't by
 * RTAI 5.1. Note that this example doesn't care about the timeout
 */
#define select(n, r, w, e, t) rt_dev_select(n, r, w, e, RTDM_TIMEOUT_INFINITE)
#endif

// PCAN-Basic device used to read on
// (RT version doesn't handle USB devices)
#define PCAN_DEVICE PCAN_USBBUS1
#else

// PCAN-Basic device used to read on
#define PCAN_DEVICE PCAN_USBBUS1
#endif

#define DELTAT(_nowtime, _thentime)                                            \
  ((_thentime > _nowtime) ? ((0xffffffff - _thentime) + _nowtime)              \
                          : (_nowtime - _thentime))
uint32_t millis()
{
  ros::WallTime walltime = ros::WallTime::now();
  return (uint32_t)(walltime.toNSec() / 1000000);
}
void mySigintHandler(int sig)
{
  ROS_INFO("Received SIGINT signal, shutting down...");
  ros::shutdown();
}

class md2k_pcan_driver
{

public:
  md2k_pcan_driver();
  ~md2k_pcan_driver();
  void
  cmdvel_callback(const md2k_can_driver_msgs::four_motor_command& command_spd);
  void timer_callback(const ros::TimerEvent& e);
  void pub_callback(const ros::TimerEvent& e2);
  void MD2K_driver_configure();
  int run();

protected:
  ros::NodeHandle nh;

  // CANPeakSysUSB PCAN_MOTOR();
  int encoder_ppr;
  double max_rpm = 2000;
  float motor_speed[4];
  int speed_scale = 1;
  long enc_STICK[4];
  int motor_RPM[4];
  int front_left = 0;
  int front_right = 2;
  int rear_left = 1;
  int rear_right = 3;

  int8_t bit_Alarm[4];
  int8_t bit_CtrlFail[4];
  int8_t bit_OverVolt[4];
  int8_t bit_OverTemp[4];
  int8_t bit_OverLoad[4];
  int8_t bit_HallFail[4];
  int8_t bit_InvVel[4];
  int8_t bit_Stall[4];

  std::string cmdvel_topic;
  md2k_can_driver_msgs::motor_status status_;

  ros::Subscriber cmdvel_sub;

  ros::Publisher encoder_data;
  ros::Publisher speed_data;
  ros::Publisher status_data;
  ros::Publisher pub_motorStatus[4];

  ros::Timer vel_sampling_time, publish_sampling_time;

  void speed_publish();
  void encoder_publish();

  // Add Dinh 200507
  void status_publish();
  TPCANStatus ALARM_RESET(BYTE MotorDriver_ID);
  // TPCANStatus Com_Status;
  ros::ServiceServer ser_reset;
  bool reset_callback(if_node_msgs::motor_reset::Request& req,
                      if_node_msgs::motor_reset::Response& res);
  void stop_all_motor();
  void stop_fast_all_motor();

  short GetMonitor();
  long GET_MOTOR_POS(BYTE MotorDriver_ID);
  int GET_MOTOR_SPEED(BYTE MotorDriver_ID);
  int REQUEST_MOTOR_SPEED(BYTE MotorDriver_ID);
  int REQUEST_MOTOR_POS(BYTE MotorDriver_ID);
  int POSITION_RESET(BYTE MotorDriver_ID);
  int SET_MONITOR_OFF(BYTE MotorDriver_ID);
  int SET_MONITOR_ON(BYTE MotorDriver_ID);
  int SET_NORMAL_COMMAND(BYTE MotorDriver_ID);
  int SET_INV_COMMAND(BYTE MotorDriver_ID);
  int SET_CLUTCH_ON(BYTE MotorDriver_ID);
  int SET_CLUTCH_OFF(BYTE MotorDriver_ID);
  int STOP_MOTOR_FAST(BYTE MotorDriver_ID);
  int STOP_MOTOR_NATURAL(BYTE MotorDriver_ID);
  int Send_VEL_COMMAND(BYTE MotorDriver_ID, int speed_cmd);
  int SET_ACC(BYTE MotorDriver_ID, int acc);
  TPCANStatus SendCAN(BYTE byID, BYTE byPID, BYTE byDataNum, BYTE byArray[]);

  uint32_t starttime;
  uint32_t mstimer;
};

short Byte2Int(BYTE byLow, BYTE byHigh) { return (byLow | (short)byHigh << 8); }

// Make long type data from four bytes
int Byte2Long(BYTE byData1, BYTE byData2, BYTE byData3, BYTE byData4)
{
  return ((int)byData1 | (int)byData2 << 8 | (int)byData3 << 16 |
          (int)byData4 << 24);
}

typedef struct
{
  BYTE byLow;
  BYTE byHigh;
} IByte;

typedef struct
{
  BYTE byData1;
  BYTE byData2;
  BYTE byData3;
  BYTE byData4;
} LByte;

// Get the low and high byte from interger
IByte Int2Byte(short nIn)
{
  IByte Ret;
  Ret.byLow = nIn & 0xff;
  Ret.byHigh = nIn >> 8 & 0xff;
  return Ret;
}

// Get the data bytes from long type data
LByte Long2Byte(int nIn)
{
  LByte Ret;
  Ret.byData1 = nIn & 0xff;
  Ret.byData2 = nIn >> 8 & 0xff;
  Ret.byData3 = nIn >> 16 & 0xff;
  Ret.byData4 = nIn >> 24 & 0xff;
  return Ret;
}
