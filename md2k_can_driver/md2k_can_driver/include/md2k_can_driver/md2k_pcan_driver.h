#include <ros/ros.h>
#include "ros/time.h"
#include <ros/console.h>
#include <signal.h>
#include <string>
#include <sstream>
#include "md2k_can_driver_parameter.h"
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
#include <stdio.h>
#include <stdlib.h>
#include <asm/types.h>
#include <unistd.h>
#include "PCANBasic.h"
#include <sys/mman.h>


#ifndef NO_RT
#include <sys/mman.h>

#ifdef RTAI
#include <rtai_lxrt.h>
#include <rtdm/rtdm.h>

/* overload the select() system call with the RTDM one, while it isn't by
 * RTAI 5.1. Note that this example doesn't care about the timeout
 */
#define select(n, r, w, e, t)	rt_dev_select(n, r, w, e, RTDM_TIMEOUT_INFINITE)
#endif

// PCAN-Basic device used to read on
// (RT version doesn't handle USB devices)
#define PCAN_DEVICE     PCAN_USBBUS1
#else

// PCAN-Basic device used to read on
#define PCAN_DEVICE     PCAN_USBBUS1
#endif

#define DELTAT(_nowtime,_thentime) ((_thentime>_nowtime)?((0xffffffff-_thentime)+_nowtime):(_nowtime-_thentime))
uint32_t millis()
{
  ros::WallTime walltime = ros::WallTime::now();
  return (uint32_t)(walltime.toNSec()/1000000);
}
void mySigintHandler(int sig)
{
  ROS_INFO("Received SIGINT signal, shutting down...");
  ros::shutdown();
}

class md2k_pcan_driver
{

public:
  md2k_pcan_driver();
  ~md2k_pcan_driver();
  void cmdvel_callback(const geometry_msgs::Twist& twist_msg);
  void timer_callback(const ros::TimerEvent& e);
  void pub_callback(const ros::TimerEvent& e2);
  void MD2K_driver_configure();
  int run();

protected:
  ros::NodeHandle nh;

  //CANPeakSysUSB PCAN_MOTOR();
  int encoder_ppr;
  double max_rpm = 2000;
  float motor_speed = 0;
  int speed_scale = 1;
  long enc_STICK = 0;
  int motor_RPM = 0;
  int motor_id = 0;
  bool inverse = false;
  int front_left = 1;
  int front_right = 3;
  int rear_left = 2;
  int rear_right = 4;
  std::string cmdvel_topic;

  ros::Subscriber cmdvel_sub;

  ros::Publisher encoder_data;
  ros::Publisher speed_data;

  ros::Timer vel_sampling_time, publish_sampling_time;

  void speed_publish();
  void encoder_publish();

  short GetMonitor();
  long GET_MOTOR_POS(BYTE MotorDriver_ID);
  int GET_MOTOR_SPEED(BYTE MotorDriver_ID);
  int REQUEST_MOTOR_SPEED(BYTE MotorDriver_ID);
  int REQUEST_MOTOR_POS(BYTE MotorDriver_ID);
  int POSITION_RESET(BYTE MotorDriver_ID);
  int SET_MONITOR_OFF(BYTE MotorDriver_ID);
  int SET_MONITOR_ON(BYTE MotorDriver_ID);
  int SET_NORMAL_COMMAND(BYTE MotorDriver_ID);
  int SET_INV_COMMAND(BYTE MotorDriver_ID);
  int SET_CLUTCH_ON(BYTE MotorDriver_ID);
  int SET_CLUTCH_OFF(BYTE MotorDriver_ID);
  int STOP_MOTOR_FAST(BYTE MotorDriver_ID);
  int STOP_MOTOR_NATURAL(BYTE MotorDriver_ID);
  int Send_VEL_COMMAND(BYTE MotorDriver_ID,int speed_cmd);
  TPCANStatus SendCAN(BYTE byID, BYTE byPID, BYTE byDataNum, BYTE byArray[]);

  bool byAlarm = false;
  bool byCtrlFail = false;
  bool byOverVolt= false;
  bool byOverTemp= false;
  bool byOverLoad= false;
  bool byHallFail= false;
  bool byInvVel= false;
  bool byStall= false;

  uint32_t starttime;
  uint32_t mstimer;

};

short Byte2Int(BYTE byLow, BYTE byHigh)
{
return (byLow | (short)byHigh<<8);
}

// Make long type data from four bytes
int Byte2Long(BYTE byData1, BYTE byData2, BYTE byData3, BYTE byData4)
{
return((int)byData1 | (int)byData2<<8 | (int)byData3<<16 | (int)byData4<<24);
}


typedef struct
{
BYTE byLow;
BYTE byHigh;
} IByte;

typedef struct
{
BYTE byData1;
BYTE byData2;
BYTE byData3;
BYTE byData4;
} LByte;

// Get the low and high byte from interger
IByte Int2Byte(short nIn)
{
IByte Ret;
Ret.byLow = nIn & 0xff;
Ret.byHigh = nIn>>8 & 0xff;
return Ret;
}

// Get the data bytes from long type data
LByte Long2Byte(int nIn)
{
  LByte Ret;
  Ret.byData1 = nIn & 0xff;
  Ret.byData2 = nIn>>8 & 0xff;
  Ret.byData3 = nIn>>16 & 0xff;
  Ret.byData4 = nIn>>24 & 0xff;
  return Ret;
}


