#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32
from if_node_msgs.msg import powerState
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
import time

client  = ModbusClient('192.168.101.50', port=502)

bias = 9.5
      
def vcal():
        pub = rospy.Publisher('/BMS/State/Power', powerState, queue_size=10)
        rospy.init_node('wago_bms', anonymous=True)
        rate = rospy.Rate(10) # 10hz
        
        while not rospy.is_shutdown():
           readAnalog = client.read_input_registers(0,1,unit=1)
           vout = (readAnalog.registers[0] * 10/4095 + bias)
           #rospy.loginfo(vout)
           batt = powerState()
           batt.voltage = vout
           #about 50v * 2 +- bias
	   batt.soc = vout * 2 + 1 
           pub.publish(batt)
           rate.sleep()

if __name__ == "__main__":
  try:
    vcal()
  except rospy.ROSInterruptException:
    pass
    
