#include <stdint.h>

// PROTOCOL
// |1		|2		|3		|4		|5		|6		|7		|8		|
// |CMD 	|INDEX			|SUB-IDX|VALUE							|
// |CMD		|ERR CODE		|NULL (0x00)							|

// Access Code
#define AC_OBJECT_WRITE_REQ		0x10
#define AC_OBJECT_WRITE_RES		0x20
#define AC_OBJECT_READ_REQ		0x30
#define AC_OBJECT_READ_RES		0x40
#define AC_OBJECT_ERROR_RES		0x80

// Object Type
#define OT_INT8					0x00
#define OT_INT16				0x04
#define OT_INT32				0x08
#define OT_FLOAT				0x0C

// Error Code
#define UNDEFINED_NAME			1
#define PACKET_FORMAT_ERROR		2
#define OBJECT_ACCESS_ERROR		3
#define WRONG_VALUE_ASSIGNMENT	4            //ex) id=5000

#define GET_LOWBYTE_16(X)		(X & 0xFF)
#define GET_HIGHBYTE_16(X)		((X >> 8) & 0xFF)

#define GET_LOWWORD_32(X)		(X & 0xFFFF)
#define GET_HIGHWORD_32(X)		((X >> 16) & 0xFFFF)

#define GET_S16_BYTE(LOW,HIGH)	((int16_t)(LOW + ((uint16_t)(HIGH) << 8)))
#define GET_U16_BYTE(LOW,HIGH)	((uint16_t)(LOW + ((uint16_t)(HIGH) << 8)))
#define GET_S32_WORD(LOW,HIGH)	((int32_t)(LOW + ((uint32_t)(HIGH) << 16)))
#define GET_U32_WORD(LOW,HIGH)	((uint32_t)(LOW + ((uint32_t)(HIGH) << 16)))

#define COMMAND 0x07
#define CAN_BUS_SPEED_1000 1000
#define SET_SERIAL_DATA 0x15
#define SET_CAN_DATA 0x16
#define SET_GYRO_SCALE 0x0F
#define SET_ACC_SCALE 0x10
#define SET_DATA_CYCLE 0x18
#define RESET_EULER 0x05
#define RESET_CALIB_DATA 21
#define ACCELERATION 0x33
#define GYROSCOPE 0x34
#define ANGLE 0x35
#define MAGNETIC 0x36
#define TEMPERATURE 0x39
#define DT_ACC 0
#define DT_GYRO 1
#define DT_ANGLE 2
#define DT_MAGNETIC 3

#define GS_250 0
#define GS_500 1
#define GS_1000 2
#define GS_2000 3

#define AS_2g 0
#define AS_4g 1
#define AS_8g 2
#define AS_16g 3



