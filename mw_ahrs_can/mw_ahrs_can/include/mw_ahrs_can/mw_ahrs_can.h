#include <ros/ros.h>
#include "ros/time.h"
#include <ros/console.h>
#include <signal.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <asm/types.h>
#include <unistd.h>
#include "PCANBasic.h"
#include "mw_ahrs_parameter.h"
#include "mw_ahrs_can_msgs/reset_euler.h"
#include <sensor_msgs/Imu.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

#ifndef NO_RT
#include <sys/mman.h>

#ifdef RTAI
#include <rtai_lxrt.h>
#include <rtdm/rtdm.h>

/* overload the select() system call with the RTDM one, while it isn't by
 * RTAI 5.1. Note that this example doesn't care about the timeout
 */
#define select(n, r, w, e, t)	rt_dev_select(n, r, w, e, RTDM_TIMEOUT_INFINITE)
#endif

// PCAN-Basic device used to read on
// (RT version doesn't handle USB devices)
#define PCAN_DEVICE     PCAN_USBBUS3
#else

// PCAN-Basic device used to read on
#define PCAN_DEVICE     PCAN_USBBUS3
#endif

#define DELTAT(_nowtime,_thentime) ((_thentime>_nowtime)?((0xffffffff-_thentime)+_nowtime):(_nowtime-_thentime))
uint32_t millis()
{
  ros::WallTime walltime = ros::WallTime::now();
  return (uint32_t)(walltime.toNSec()/1000000);
}
void mySigintHandler(int sig)
{
  ROS_INFO("Received SIGINT signal, shutting down...");
  ros::shutdown();
}

class mw_ahrs_can
{

public:
  mw_ahrs_can();
  ~mw_ahrs_can();
  //void timer_callback(const ros::TimerEvent& e);
  void reset_euler_callback(mw_ahrs_can_msgs::reset_euler reset_command);
  void mw_ahrs_configure();
  int run();

private:
  ros::NodeHandle nh;

  ros::Subscriber sub_IMU_reset;

  ros::Publisher pub_IMU_data;
  ros::Publisher imu_data_pub_, imu_data_raw_pub_;
  void publish_imuData2();
  std::string parent_frame_id_ = "base_link";
  std::string frame_id_ = "base_link";

  ros::Timer publish_sampling_time;

  void imu_publish();
  void gyro_reset_callback();
  int set_SERIAL_data_type();
  int set_CAN_data_type();
  int set_gyro_scale(uint32_t scale);
  int set_acc_scale(uint32_t scale);
  int set_cycle(uint32_t time);
  int reset_euler();
  int request_gyro();
  int request_acc();
  int request_ang();
  short data_parsing();

  //short GetMonitor();
  TPCANStatus SendCAN(BYTE byArray[]);
  // Acceleration
  float a_x;
  float a_y;
  float a_z;

  // Angular velocity
  float g_x;
  float g_y;
  float g_z;

  // Euler
  float e_yaw;
  float e_pitch;
  float e_roll;

  //Magnetic
  float mag_x;
  float mag_y;
  float mag_z;

  uint32_t starttime;
  uint32_t mstimer;

};



