#include "mw_ahrs_can.h"
#include "mw_ahrs_can_msgs/mw_imu.h"
#include "ros/time.h"
#include <ros/console.h>
#include <ros/ros.h>
#include <signal.h>
#include <sstream>
#include <string>

mw_ahrs_can::mw_ahrs_can()
    : a_x(0), a_y(0), a_z(0), g_x(0), g_y(0), g_z(0), e_roll(0), e_pitch(0),
      e_yaw(0), mag_x(0), mag_y(0), mag_z(0)
{

  sub_IMU_reset = nh.subscribe("reset_euler", 1000,
                               &mw_ahrs_can::reset_euler_callback, this);

  //  publish_sampling_time = nh.createTimer(ros::Duration(0.01),
  //  &mw_ahrs_can::timer_callback, this);

  pub_IMU_data = nh.advertise<mw_ahrs_can_msgs::mw_imu>("/imu_data", 1);
  imu_data_pub_ = nh.advertise<sensor_msgs::Imu>("imu/data", 30, true);
  imu_data_raw_pub_ = nh.advertise<sensor_msgs::Imu>("imu/data_raw", 1);
}

mw_ahrs_can::~mw_ahrs_can() {}

TPCANStatus mw_ahrs_can::SendCAN(BYTE byArray[])
{
  DWORD dwID;
  BYTE i;
  dwID = 0x01;

  TPCANMsg Message;
  TPCANStatus Status;
  Message.ID = dwID;
  Message.LEN = 8;
  Message.MSGTYPE = PCAN_MESSAGE_STANDARD;
  for (i = 0; i < 8; i++)
    Message.DATA[i] = byArray[i];

  Status = CAN_Write(PCAN_DEVICE, &Message);
  return Status;
}

int mw_ahrs_can::set_CAN_data_type()
{
  BYTE bydata[8];
  bydata[0] = AC_OBJECT_WRITE_REQ + OT_INT32;
  bydata[1] = SET_CAN_DATA;
  bydata[2] = 0;
  bydata[3] = 0;
  bydata[4] =
      (1 << DT_ACC) + (1 << DT_GYRO) + (1 << DT_ANGLE) + (0 << DT_MAGNETIC);
  // bydata[4] = 0x07;
  for (int i = 5; i < 8; i++)
    bydata[i] = 0;
  SendCAN(bydata);
  return 1;
}

int mw_ahrs_can::set_SERIAL_data_type()
{
  BYTE bydata[8];
  bydata[0] = AC_OBJECT_WRITE_REQ + OT_INT32;
  bydata[1] = SET_SERIAL_DATA;

  for (int i = 2; i < 8; i++)
    bydata[i] = 0;
  SendCAN(bydata);
  return 1;
}

int mw_ahrs_can::set_gyro_scale(uint32_t scale)
{
  BYTE bydata[8];
  bydata[0] = AC_OBJECT_WRITE_REQ + OT_INT32;
  bydata[1] = SET_GYRO_SCALE;
  bydata[2] = 0;
  bydata[3] = 0;
  bydata[4] = GET_LOWBYTE_16(GET_LOWWORD_32(scale));
  bydata[5] = GET_HIGHBYTE_16(GET_LOWWORD_32(scale));
  bydata[6] = GET_LOWBYTE_16(GET_HIGHWORD_32(scale));
  bydata[7] = GET_HIGHBYTE_16(GET_HIGHWORD_32(scale));
  // bydata[4] = scale;
  // for(int i=5;i<8;i++)
  // bydata[i]= 0;
  SendCAN(bydata);
  return 1;
}
int mw_ahrs_can::set_acc_scale(uint32_t scale)
{
  BYTE bydata[8];
  bydata[0] = AC_OBJECT_WRITE_REQ + OT_INT32;
  bydata[1] = SET_ACC_SCALE;
  bydata[2] = 0;
  bydata[3] = 0;
  bydata[4] = GET_LOWBYTE_16(GET_LOWWORD_32(scale));
  bydata[5] = GET_HIGHBYTE_16(GET_LOWWORD_32(scale));
  bydata[6] = GET_LOWBYTE_16(GET_HIGHWORD_32(scale));
  bydata[7] = GET_HIGHBYTE_16(GET_HIGHWORD_32(scale));
  // bydata[4] = scale;
  // for(int i=5;i<8;i++)
  // bydata[i]= 0;
  SendCAN(bydata);
  return 1;
}

int mw_ahrs_can::set_cycle(uint32_t time)
{
  BYTE bydata[8];
  bydata[0] = AC_OBJECT_WRITE_REQ + OT_INT32;
  bydata[1] = SET_DATA_CYCLE;
  bydata[2] = 0;
  bydata[3] = 0;
  bydata[4] = GET_LOWBYTE_16(GET_LOWWORD_32(time));
  bydata[5] = GET_HIGHBYTE_16(GET_LOWWORD_32(time));
  bydata[6] = GET_LOWBYTE_16(GET_HIGHWORD_32(time));
  bydata[7] = GET_HIGHBYTE_16(GET_HIGHWORD_32(time));
  SendCAN(bydata);
  return 1;
}

int mw_ahrs_can::reset_euler()
{
  BYTE bydata[8];
  bydata[0] = AC_OBJECT_WRITE_REQ + OT_INT32;
  bydata[1] = COMMAND;
  bydata[2] = 0;
  bydata[3] = 0;
  bydata[4] = RESET_EULER;
  for (int i = 5; i < 8; i++)
    bydata[i] = 0;
  SendCAN(bydata);
  return 1;
}

int mw_ahrs_can::request_gyro()
{
  BYTE bydata[8];
  bydata[0] = AC_OBJECT_READ_REQ + OT_FLOAT;
  bydata[1] = GYROSCOPE;

  for (int i = 2; i < 8; i++)
    bydata[i] = 0;
  SendCAN(bydata);
  return 1;
}

int mw_ahrs_can::request_acc()
{
  BYTE bydata[8];
  bydata[0] = AC_OBJECT_READ_REQ + OT_FLOAT;
  bydata[1] = ACCELERATION;

  for (int i = 2; i < 8; i++)
    bydata[i] = 0;
  SendCAN(bydata);
  return 1;
}

int mw_ahrs_can::request_ang()
{
  BYTE bydata[8];
  bydata[0] = AC_OBJECT_READ_REQ + OT_FLOAT;
  bydata[1] = ANGLE;

  for (int i = 2; i < 8; i++)
    bydata[i] = 0;
  SendCAN(bydata);
  return 1;
}

short mw_ahrs_can::data_parsing()
{
  // Parsing CAN message
  TPCANMsg Message;
  bool Status = CAN_Read(PCAN_DEVICE, &Message, NULL);

  if (!Status)
  {
    if (Message.ID == 0x01)
    {
      // if(Message.DATA[0] == (BYTE)PID_MONITOR)
      if (Message.DATA[0] == 0xF0)
      {
        switch (Message.DATA[1])
        {
        case ACCELERATION:
          a_x =
              (float)(GET_S16_BYTE(Message.DATA[2], Message.DATA[3]) / 1000.0f);
          a_y =
              (float)(GET_S16_BYTE(Message.DATA[4], Message.DATA[5]) / 1000.0f);
          a_z = (float)(GET_S16_BYTE(Message.DATA[6], Message.DATA[7]) /
                        1000.00f);
          // printf("receive acc \n");
          break;
        case GYROSCOPE:
          g_x = (float)(GET_S16_BYTE(Message.DATA[2], Message.DATA[3]) / 10.0f);
          g_y = (float)(GET_S16_BYTE(Message.DATA[4], Message.DATA[5]) / 10.0f);
          g_z = (float)(GET_S16_BYTE(Message.DATA[6], Message.DATA[7]) / 10.0f);
          // printf("receive gyro \n");
          break;
        case ANGLE:
          e_roll =
              (float)(GET_S16_BYTE(Message.DATA[2], Message.DATA[3]) / 100.0f);
          e_pitch =
              (float)(GET_S16_BYTE(Message.DATA[4], Message.DATA[5]) / 100.0f);
          e_yaw =
              (float)(GET_S16_BYTE(Message.DATA[6], Message.DATA[7]) / 100.0f);
          // printf("receive angle \n");
          break;
        case MAGNETIC:
          mag_x = GET_S16_BYTE(Message.DATA[2], Message.DATA[3]) / 10.0f;
          mag_y = GET_S16_BYTE(Message.DATA[4], Message.DATA[5]) / 10.0f;
          mag_z = GET_S16_BYTE(Message.DATA[6], Message.DATA[7]) / 10.0f;
          // printf("receive mag \n");
          break;
        }
      }
      else if (Message.DATA[0] == AC_OBJECT_ERROR_RES)
      {
        printf("Error response \n");
      }
      else if (Message.DATA[0] == AC_OBJECT_WRITE_RES + OT_INT32)
      {
        printf("Successful write ");
        switch (Message.DATA[1])
        {
        case SET_CAN_DATA:
          printf("set can data \n");
          break;
        case SET_GYRO_SCALE:
          printf("set gyro scale \n");
          break;
        case SET_ACC_SCALE:
          printf("set acc scale \n");
          break;
        case SET_DATA_CYCLE:
          printf("set cycle \n");
          break;
        }
      }
      else
      {
        printf("Unknown message \n");
      }
    }
    else
    {
      printf("Cannot receive message \n");
    }
  }
  return 1;
}

void mw_ahrs_can::publish_imuData2()
{

  // Publish ROS msgs.
  sensor_msgs::Imu imu_data_raw_msg;
  sensor_msgs::Imu imu_data_msg;
  double roll, pitch, yaw;
  // Set covariance value of each measurements.
  imu_data_raw_msg.linear_acceleration_covariance[0] =
      imu_data_raw_msg.linear_acceleration_covariance[4] =
          imu_data_raw_msg.linear_acceleration_covariance[8] =
              imu_data_msg.linear_acceleration_covariance[0] =
                  imu_data_msg.linear_acceleration_covariance[4] =
                      imu_data_msg.linear_acceleration_covariance[8] = -1;

  imu_data_raw_msg.angular_velocity_covariance[0] =
      imu_data_raw_msg.angular_velocity_covariance[4] =
          imu_data_raw_msg.angular_velocity_covariance[8] =
              imu_data_msg.angular_velocity_covariance[0] =
                  imu_data_msg.angular_velocity_covariance[4] =
                      imu_data_msg.angular_velocity_covariance[8] = -1;

  imu_data_msg.orientation_covariance[0] =
      imu_data_msg.orientation_covariance[4] =
          imu_data_msg.orientation_covariance[8] = -1;

  static double convertor_d2r =
      M_PI / 180.0; // for angular_velocity (degree to radian)
  static double convertor_r2d =
      180.0 / M_PI; // for easy understanding (radian to degree)

  roll = 0.0;
  pitch = 0.0;
  yaw = e_yaw * convertor_d2r;

  // Get Quaternion fro RPY.
  tf::Quaternion orientation = tf::createQuaternionFromRPY(roll, pitch, yaw);

  ros::Time now = ros::Time::now();

  imu_data_raw_msg.header.stamp = imu_data_msg.header.stamp = now;

  imu_data_raw_msg.header.frame_id = imu_data_msg.header.frame_id = frame_id_;
  imu_data_raw_msg.header.frame_id = imu_data_msg.header.frame_id = frame_id_;

  // orientation
  imu_data_msg.orientation.x = orientation[0];
  imu_data_msg.orientation.y = orientation[1];
  imu_data_msg.orientation.z = orientation[2];
  imu_data_msg.orientation.w = orientation[3];

  // original data used the g unit, convert to m/s^2
  imu_data_raw_msg.linear_acceleration.x = imu_data_msg.linear_acceleration.x =
      a_x;
  imu_data_raw_msg.linear_acceleration.y = imu_data_msg.linear_acceleration.y =
      a_y;
  imu_data_raw_msg.linear_acceleration.z = imu_data_msg.linear_acceleration.z =
      a_z;

  // imu.gx gy gz.
  // original data used the degree/s unit, convert to radian/s
  imu_data_raw_msg.angular_velocity.x = imu_data_msg.angular_velocity.x =
      g_x * convertor_d2r;
  imu_data_raw_msg.angular_velocity.y = imu_data_msg.angular_velocity.y =
      g_y * convertor_d2r;
  imu_data_raw_msg.angular_velocity.z = imu_data_msg.angular_velocity.z =
      g_z * convertor_d2r;

  // publish the IMU data
  // imu_data_raw_pub_.publish(imu_data_raw_msg);
  imu_data_pub_.publish(imu_data_msg);

  // publish tf
  geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(yaw);

  geometry_msgs::TransformStamped imu_trans;
  imu_trans.header.stamp = ros::Time::now();
  // imu_trans.header.stamp = ros::Time::now();
  imu_trans.header.frame_id = parent_frame_id_;
  imu_trans.child_frame_id = frame_id_;

  imu_trans.transform.translation.x = 0;
  imu_trans.transform.translation.y = 0;
  imu_trans.transform.translation.z = 0.0;
  imu_trans.transform.rotation = odom_quat;

  // broadcaster_.sendTransform(imu_trans);
}

void mw_ahrs_can::mw_ahrs_configure()
{
  reset_euler();
  // set_SERIAL_data_type();
  set_CAN_data_type();
  set_gyro_scale(GS_1000);
  set_acc_scale(AS_2g);
  set_cycle(10);

  // request_acc();
  // request_ang();
  // request_gyro();
}

void mw_ahrs_can::reset_euler_callback(
    mw_ahrs_can_msgs::reset_euler reset_command)
{
  if (reset_command.reset)
    reset_euler();
}

void mw_ahrs_can::imu_publish()
{
  mw_ahrs_can_msgs::mw_imu imu_data;
  imu_data.header.stamp = ros::Time::now();
  imu_data.acc_x = a_x;
  imu_data.acc_y = a_y;
  imu_data.acc_z = a_z;
  imu_data.gyr_x = g_x;
  imu_data.gyr_y = g_y;
  imu_data.gyr_z = g_z;
  imu_data.roll = e_roll;
  imu_data.pitch = e_pitch;
  imu_data.yaw = e_yaw;
  imu_data.mag_x = mag_x;
  imu_data.mag_y = mag_y;
  imu_data.mag_z = mag_z;
  pub_IMU_data.publish(imu_data);
}
/*
void mw_ahrs_can::timer_callback(const ros::TimerEvent& e)
{
  imu_publish();
}
*/

int mw_ahrs_can::run()
{

  ROS_INFO("Setup CAN Port...");
  ros::Rate loop_rate(20);

  // Modify Dinh 20200324
  TPCANStatus Status;
  DWORD pcan_device = PCAN_DEVICE;

#ifndef NO_RT
  mlockall(MCL_CURRENT | MCL_FUTURE);
#endif

  Status = CAN_Initialize(pcan_device, PCAN_BAUD_1M, 0, 0, 0);   // AGV2
  //Status = CAN_Initialize(pcan_device, PCAN_BAUD_500K, 0, 0, 0);  // AGV1
  printf("CAN_Initialize(%xh): Status=0x%x\n", pcan_device, (int)Status);
  if (Status == PCAN_ERROR_OK)
    goto lbl_cont;
  else
  {
    goto lbl_exit;
  }

lbl_cont:
  int fd;
  Status = CAN_GetValue(pcan_device, PCAN_RECEIVE_EVENT, &fd, sizeof fd);
  printf("CAN_GetValue(%xh): Status=0x%x\n", pcan_device, (int)Status);
  if (Status)
    goto lbl_close;

  mw_ahrs_configure();

  // starttime = millis();
  // mstimer = starttime;


  // forever loop
  while (ros::ok())
  {
    // GetMonitor();
    // data_parsing Should be here
    /*
  data_parsing();
  uint32_t nowtime = millis();

  // Handle 100 Hz request
  if (DELTAT(nowtime,mstimer) >= 20)
  {
    mstimer = nowtime;
    //data_parsing();
    // Add Dinh 20200401
    //imu_publish();
    publish_imuData2();
  }
  */

    data_parsing();
    publish_imuData2();

    ros::spinOnce();
    loop_rate.sleep();
  }

lbl_close:
  CAN_Uninitialize(pcan_device);
  printf("CAN close \n");

lbl_exit:

  printf("Shutting down ... \n");
  ros::waitForShutdown();

  return 0;
}

int main(int argc, char** argv)
{

  ros::init(argc, argv, "MW_AHRS_node");

  mw_ahrs_can mw_ahrs;

  signal(SIGINT, mySigintHandler);

  return mw_ahrs.run();
}
