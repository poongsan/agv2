/*
 * Author: To Xuan Dinh & Tran Thi Trang
*/

#pragma once

#include <ros/ros.h>
//#include <vector>
#include <tf/transform_listener.h>
#include "tf/transform_datatypes.h"
#include <std_srvs/Empty.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud.h>
#include <obstacle_detector/Obstacles.h>

#include "obstacle_detector/utilities/point.h"
#include "obstacle_detector/utilities/segment.h"
#include "obstacle_detector/utilities/circle.h"
#include "obstacle_detector/utilities/point_set.h"
#include "obstacle_detector/agv_dir.h"
#include "obstacle_detector/ob_range.h"
#include <vector>
#include <geometry_msgs/Pose2D.h>

namespace obstacle_detector
{

enum AREA{
    NO_OBS_ = 0, ABU_ = 1, ABL_=2, CD_=3, HAGV2_=4, FA34_=5, FA1_=6, FA2_=7, FA5_=8,CENTER_ = 9, HAGV1_=10
};

class ObstacleExtractor
{
public:
  ObstacleExtractor(ros::NodeHandle& nh, ros::NodeHandle& nh_local);
  ~ObstacleExtractor();

private:
  bool updateParams(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res);
  void scanCallback(const sensor_msgs::LaserScan::ConstPtr scan_msg);
  void pclCallback(const sensor_msgs::PointCloud::ConstPtr pcl_msg);
  void currentPosCallback(const geometry_msgs::Pose2D msg);
  void currentGoalCallback(const geometry_msgs::Pose2D goal);

  void initialize() { std_srvs::Empty empt; updateParams(empt.request, empt.response); }

  void processPoints();
  void groupPoints();
  void publishObstacles();

  void detectSegments(const PointSet& point_set);
  void mergeSegments();
  bool compareSegments(const Segment& s1, const Segment& s2, Segment& merged_segment);
  bool checkSegmentsProximity(const Segment& s1, const Segment& s2);
  bool checkSegmentsCollinearity(const Segment& segment, const Segment& s1, const Segment& s2);

  void detectCircles();
  void mergeCircles();
  bool compareCircles(const Circle& c1, const Circle& c2, Circle& merged_circle);
  void publish_move_dir(const uint8_t& dir);
  void calAGV_dir();
  void north();
  void north_west();
  void north_east();
  void south();
  void south_east();
  void south_west();
  void west();
  void east();
  void near_goal();
  AREA obs_area;

  ros::NodeHandle nh_;
  ros::NodeHandle nh_local_;

  std::vector<double> area_limit_north, area_limit_south, area_limit_west, area_limit_east;
  void SetParams();

  ros::Subscriber scan_sub_;
  ros::Subscriber pcl_sub_;
  //ros::Subscriber sub_move_dir;
  ros::Subscriber sub_currentPos;
  ros::Subscriber sub_currentGoal;
  ros::Publisher pub_dirStatus;

  ros::Publisher pub_ob_range;
  geometry_msgs::Pose robot_pose;

  ros::Publisher obstacles_pub_;
  ros::ServiceServer params_srv_;


  geometry_msgs::Pose2D current_pose;
  geometry_msgs::Pose2D  current_goal;
  float AGV_yaw;

  ros::Time stamp_;
  std::string base_frame_id_;
  tf::TransformListener tf_listener_;

  std::list<Point> input_points_;
  std::list<Segment> segments_;
  std::list<Circle> circles_;

  // Parameters
  bool p_active_;
  bool p_use_scan_;
  bool p_use_pcl_;

  bool p_use_split_and_merge_;
  bool p_circles_from_visibles_;
  bool p_discard_converted_segments_;
  bool p_transform_coordinates_;

  int p_min_group_points_;

  double p_distance_proportion_;
  double p_max_group_distance_;
  double p_max_split_distance_;
  double p_max_merge_separation_;
  double p_max_merge_spread_;
  double p_max_circle_radius_;
  double p_radius_enlargement_;
  double max_range_x, max_range_y, min_range_x, min_range_y;

  double p_min_x_limit_;
  double p_max_x_limit_;
  double p_min_y_limit_;
  double p_max_y_limit_;

  std::string p_frame_id_;
};

} // namespace obstacle_detector
