/*
 * Author: To Xuan Dinh & Tran Thi Trang
*/

#pragma once

#include <ros/ros.h>
//#include <vector>
#include <tf/transform_listener.h>
#include "tf/transform_datatypes.h"
#include <std_srvs/Empty.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud.h>
#include <obstacle_detector/Obstacles.h>

#include "obstacle_detector/utilities/point.h"
#include "obstacle_detector/utilities/segment.h"
#include "obstacle_detector/utilities/circle.h"
#include "obstacle_detector/utilities/point_set.h"
#include "obstacle_detector/agv_dir.h"
#include "obstacle_detector/ob_range.h"
#include <vector>
#include <geometry_msgs/Pose2D.h>


namespace obstacle_detector
{

class ObstacleExtractor
{
public:
  ObstacleExtractor(ros::NodeHandle& nh, ros::NodeHandle& nh_local);
  ~ObstacleExtractor();

private:
  bool updateParams(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res);
  void scanCallback(const sensor_msgs::LaserScan::ConstPtr scan_msg);
  void pclCallback(const sensor_msgs::PointCloud::ConstPtr pcl_msg);
  void currentPosCallback(const geometry_msgs::Pose2D msg);
  void currentGoalCallback(const geometry_msgs::Pose2D goal);

  void initialize() { std_srvs::Empty empt; updateParams(empt.request, empt.response); }

  void processPoints();
  void groupPoints();
  void publishObstacles();

  void detectSegments(const PointSet& point_set);
  void mergeSegments();
  bool compareSegments(const Segment& s1, const Segment& s2, Segment& merged_segment);
  bool checkSegmentsProximity(const Segment& s1, const Segment& s2);
  bool checkSegmentsCollinearity(const Segment& segment, const Segment& s1, const Segment& s2);

  void detectCircles();
  void mergeCircles();
  bool compareCircles(const Circle& c1, const Circle& c2, Circle& merged_circle);
  void publish_move_dir(const uint8_t& dir);
  void calAGV_dir();

  void north(double x);
  void south(double x);
  void west(double y);
  void east(double y);
  void north_west(double x, double y);
  void north_east(double x, double y);
  void south_west(double x, double y);
  void south_east(double x, double y);
  void near_goal();

  ros::NodeHandle nh_;
  ros::NodeHandle nh_local_;

  ros::Subscriber scan_sub_;
  ros::Subscriber pcl_sub_;
  //ros::Subscriber sub_move_dir;
  ros::Subscriber sub_currentPos;
  ros::Subscriber sub_currentGoal;
  ros::Publisher pub_dirStatus;

  ros::Publisher pub_ob_range, pub_info;
  geometry_msgs::Pose robot_pose;

  ros::Publisher obstacles_pub_;
  ros::ServiceServer params_srv_;


  geometry_msgs::Pose2D current_pose;
  geometry_msgs::Pose2D  current_goal;
  float AGV_yaw;

  bool init_goal = true;

  ros::Time stamp_;
  std::string base_frame_id_;
  tf::TransformListener tf_listener_;

  std::list<Point> input_points_;
  std::list<Segment> segments_;
  std::list<Circle> circles_;

  // Parameters
  bool p_active_;
  bool p_use_scan_;
  bool p_use_pcl_;

  bool p_use_split_and_merge_;
  bool p_circles_from_visibles_;
  bool p_discard_converted_segments_;
  bool p_transform_coordinates_;

  int p_min_group_points_;

  double p_distance_proportion_;
  double p_max_group_distance_;
  double p_max_split_distance_;
  double p_max_merge_separation_;
  double p_max_merge_spread_;
  double p_max_circle_radius_;
  double p_radius_enlargement_;
  double range_x, range_y, min_range_x, min_range_y;

  double delx = 0, dely = 0;

  double p_min_x_limit_;
  double p_max_x_limit_;
  double p_min_y_limit_;
  double p_max_y_limit_;

  std::string p_frame_id_;
};

} // namespace obstacle_detector
