/*
SIS Software Team.
To Xuan Dinh & Tran Thi Trang
*/

#include "obstacle_detector/obstacle_extractor33.h"
#include "obstacle_detector/utilities/figure_fitting.h"
#include "obstacle_detector/utilities/math_utilities.h"

using namespace std;
using namespace obstacle_detector;

ObstacleExtractor::ObstacleExtractor(ros::NodeHandle& nh,
                                     ros::NodeHandle& nh_local)
    : nh_(nh), nh_local_(nh_local)
{
  p_active_ = false;

  params_srv_ = nh_local_.advertiseService(
      "params", &ObstacleExtractor::updateParams, this);

  initialize();
}

ObstacleExtractor::~ObstacleExtractor()
{
  nh_local_.deleteParam("active");
  nh_local_.deleteParam("use_scan");
  nh_local_.deleteParam("use_pcl");

  nh_local_.deleteParam("use_split_and_merge");
  nh_local_.deleteParam("circles_from_visibles");
  nh_local_.deleteParam("discard_converted_segments");
  nh_local_.deleteParam("transform_coordinates");

  nh_local_.deleteParam("min_group_points");

  nh_local_.deleteParam("max_group_distance");
  nh_local_.deleteParam("distance_proportion");
  nh_local_.deleteParam("max_split_distance");
  nh_local_.deleteParam("max_merge_separation");
  nh_local_.deleteParam("max_merge_spread");
  nh_local_.deleteParam("max_circle_radius");
  nh_local_.deleteParam("radius_enlargement");

  nh_local_.deleteParam("min_x_limit");
  nh_local_.deleteParam("max_x_limit");
  nh_local_.deleteParam("min_y_limit");
  nh_local_.deleteParam("max_y_limit");

  nh_local_.deleteParam("frame_id");
}

bool ObstacleExtractor::updateParams(std_srvs::Empty::Request& req,
                                     std_srvs::Empty::Response& res)
{
  bool prev_active = p_active_;

  nh_local_.param<bool>("active", p_active_, true);
  nh_local_.param<bool>("use_scan", p_use_scan_, true);
  nh_local_.param<bool>("use_pcl", p_use_pcl_, false);

  nh_local_.param<bool>("use_split_and_merge", p_use_split_and_merge_, true);
  nh_local_.param<bool>("circles_from_visibles", p_circles_from_visibles_,
                        true);
  nh_local_.param<bool>("discard_converted_segments",
                        p_discard_converted_segments_, true);
  nh_local_.param<bool>("transform_coordinates", p_transform_coordinates_,
                        true);

  nh_local_.param<int>("min_group_points", p_min_group_points_, 5);

  nh_local_.param<double>("max_group_distance", p_max_group_distance_, 0.1);
  nh_local_.param<double>("distance_proportion", p_distance_proportion_,
                          0.00628);
  nh_local_.param<double>("max_split_distance", p_max_split_distance_, 0.2);
  nh_local_.param<double>("max_merge_separation", p_max_merge_separation_, 0.2);
  nh_local_.param<double>("max_merge_spread", p_max_merge_spread_, 0.2);
  nh_local_.param<double>("max_circle_radius", p_max_circle_radius_, 0.6);
  nh_local_.param<double>("radius_enlargement", p_radius_enlargement_, 0.25);
  nh_local_.param<double>("range_x", range_x, 2.5);
  nh_local_.param<double>("range_y", range_y, 1.5);
  nh_local_.param<double>("min_range_x", min_range_x, 2);
  nh_local_.param<double>("min_range_y", min_range_y, 2);

  nh_local_.param<double>("min_x_limit", p_min_x_limit_, -10.0);
  nh_local_.param<double>("max_x_limit", p_max_x_limit_, 10.0);
  nh_local_.param<double>("min_y_limit", p_min_y_limit_, -10.0);
  nh_local_.param<double>("max_y_limit", p_max_y_limit_, 10.0);

  nh_local_.param<string>("frame_id", p_frame_id_, "map");

  if (p_active_ != prev_active)
  {
    if (p_active_)
    {
      if (p_use_scan_)
        scan_sub_ =
            nh_.subscribe("scan", 10, &ObstacleExtractor::scanCallback, this);
      else if (p_use_pcl_)
        pcl_sub_ =
            nh_.subscribe("pcl", 10, &ObstacleExtractor::pclCallback, this);

      obstacles_pub_ =
          nh_.advertise<obstacle_detector::Obstacles>("raw_obstacles", 10);
      sub_currentPos = nh_.subscribe(
          "/agv_pos", 1, &ObstacleExtractor::currentPosCallback, this);
      sub_currentGoal = nh_.subscribe(
          "/current_goal", 1, &ObstacleExtractor::currentGoalCallback, this);
      pub_dirStatus =
          nh_.advertise<obstacle_detector::agv_dir>("/agv_move_dir", 1);
      pub_ob_range = nh_.advertise<obstacle_detector::ob_range>("/ob_range", 1);
      pub_info = nh_.advertise<geometry_msgs::Pose2D>("/obs_pos", 1);

      calAGV_dir();
    }
    else
    {
      // Send empty message
      obstacle_detector::ObstaclesPtr obstacles_msg(
          new obstacle_detector::Obstacles);
      obstacles_msg->header.frame_id = p_frame_id_;
      obstacles_msg->header.stamp = ros::Time::now();
      obstacles_pub_.publish(obstacles_msg);

      scan_sub_.shutdown();
      pcl_sub_.shutdown();
      obstacles_pub_.shutdown();
      sub_currentPos.shutdown();
      sub_currentGoal.shutdown();
      pub_dirStatus.shutdown();
    }
  }

  return true;
}
void ObstacleExtractor::publish_move_dir(const uint8_t& dir)
{
  obstacle_detector::agv_dir msg;
  if (dir == obstacle_detector::agv_dir::NORTH)
  {
    msg.dir = obstacle_detector::agv_dir::NORTH;
    msg.dir_desc = "NORTH";
    pub_dirStatus.publish(msg);
  }
  else if (dir == obstacle_detector::agv_dir::SOUTH)
  {
    msg.dir = obstacle_detector::agv_dir::SOUTH;
    msg.dir_desc = "SOUTH";
    pub_dirStatus.publish(msg);
  }
  else if (dir == obstacle_detector::agv_dir::WEST)
  {
    msg.dir = obstacle_detector::agv_dir::WEST;
    msg.dir_desc = "WEST";
    pub_dirStatus.publish(msg);
  }
  else if (dir == obstacle_detector::agv_dir::EAST)
  {
    msg.dir = obstacle_detector::agv_dir::EAST;
    msg.dir_desc = "EAST";
    pub_dirStatus.publish(msg);
  }
  else if (dir == obstacle_detector::agv_dir::NORTH_WEST)
  {
    msg.dir = obstacle_detector::agv_dir::NORTH_WEST;
    msg.dir_desc = "NORTH_WEST";
    pub_dirStatus.publish(msg);
  }
  else if (dir == obstacle_detector::agv_dir::NORTH_EAST)
  {
    msg.dir = obstacle_detector::agv_dir::NORTH_EAST;
    msg.dir_desc = "NORTH_EAST";
    pub_dirStatus.publish(msg);
  }
  else if (dir == obstacle_detector::agv_dir::SOUTH_WEST)
  {
    msg.dir = obstacle_detector::agv_dir::SOUTH_WEST;
    msg.dir_desc = "SOUTH_WEST";
    pub_dirStatus.publish(msg);
  }
  else if (dir == obstacle_detector::agv_dir::SOUTH_EAST)
  {
    msg.dir = obstacle_detector::agv_dir::SOUTH_EAST;
    msg.dir_desc = "SOUTH_EAST";
    pub_dirStatus.publish(msg);
  }
  else
  {
    msg.dir = obstacle_detector::agv_dir::NEAR_GOAL;
    msg.dir_desc = "NEAR_GOAL";
    pub_dirStatus.publish(msg);
  }
}

void ObstacleExtractor::currentPosCallback(const geometry_msgs::Pose2D msg)
{
  current_pose.x = msg.x;
  current_pose.y = msg.y;
  AGV_yaw = msg.theta;
/*
  if (init_goal)
  {
    current_goal.x = msg.x;
    current_goal.y = msg.y;
    init_goal = false;
  }
  */
}

void ObstacleExtractor::currentGoalCallback(const geometry_msgs::Pose2D goal)
{
  current_goal.x = goal.x;
  current_goal.y = goal.y;
  current_goal.theta = goal.theta;
}

void ObstacleExtractor::calAGV_dir()
{

  double dx, dy;
  dx = current_goal.x - current_pose.x;
  dy = current_goal.y - current_pose.y;
/*
  if (dx >= range_x)
      delx = range_x;
  else if (dx <= -range_x)
      delx = -range_x;
  else
      delx = dx;

  if (dy >= range_y)
      dely = range_y;
  else if (dy <= -range_y)
      dely = -range_y;
  else
      dely = dy;
      */

  if (AGV_yaw > M_PI / 4 && AGV_yaw <= 3 * M_PI / 4)
  {
    if (current_pose.x < 8.1 && current_pose.x > 6.8)
    {
      if (current_pose.y < 12.5 && current_pose.y > 12.2)
      {
        if (dx >= 0.5)
          delx = 0.5;
        else if (dx <= -0.5)
          delx = -0.5;
        else
          delx = dx;

        if (dy >= range_y)
          dely = range_y;
        else if (dy <= -range_y)
          dely = -range_y;
        else
          dely = dy;
      }
      else if (current_pose.y < -2)
      {
        if (dx >= 0.5)
          delx = 0.5;
        else if (dx <= -0.5)
          delx = -0.5;
        else
          delx = dx;

        if (dy >= range_y)
          dely = range_y - 0.5;
        else if (dy <= -range_y)
          dely = -range_y + 0.5;
        else
        {
          if (dy > 0.5)
            dely = dy - 0.5;
          else if (dy > -0.5 && dy < 0.5)
            dely = 0;
          else
            dely = dy + 0.5;
        }
      }
      else
      {
        if (dx >= range_x)
          delx = range_x;
        else if (dx <= -range_x)
          delx = -range_x;
        else
          delx = dx;

        if (dy >= range_y)
          dely = range_y;
        else if (dy <= -range_y)
          dely = -range_y;
        else
          dely = dy;
      }
    }
  }
  else if (AGV_yaw > -M_PI / 4 && AGV_yaw <= M_PI / 4)
  {
    if (current_pose.x < 47.2 && current_pose.x > 45 && current_pose.y < 9.1 &&
        current_pose.y > 8.7)
    {
      if (dx >= 0.5)
        delx = 0.5;
      else if (dx <= -0.5)
        delx = -0.5;
      else
        delx = dx;

      if (dy >= range_y)
        dely = range_y;
      else if (dy <= -range_y)
        dely = -range_y;
      else
        dely = dy;
    }
    else if (current_pose.x <= 45 && current_pose.x > 8.1)
    {
      if (dx >= range_x)
        delx = range_x;
      else if (dx <= -range_x)
        delx = -range_x;
      else
        delx = dx;

      if (dy >= 0.8)
        dely = dy - 0.8;
      else if (dy < 0.8 && dy >= 0)
        dely = 0;
      else
        dely = dy;
    }
    else
    {
      if (dx >= range_x)
        delx = range_x;
      else if (dx <= -range_x)
        delx = -range_x;
      else
        delx = dx;

      if (dy >= range_y)
        dely = range_y;
      else if (dy <= -range_y)
        dely = -range_y;
      else
        dely = dy;
    }
  }
  else if ((AGV_yaw > 3 * M_PI / 4 && AGV_yaw <= 4 * M_PI / 4) ||
           (AGV_yaw > -4 * M_PI / 4 && AGV_yaw <= -3 * M_PI / 4) ||
           (AGV_yaw > 4 * M_PI / 4 && AGV_yaw <= 5 * M_PI / 4) ||
           (AGV_yaw <= -4 * M_PI / 4 && AGV_yaw > -5 * M_PI / 4))
  {
    if (current_pose.x < 45 && current_pose.x > 8.1)
    {
      if (dx >= range_x)
        delx = range_x;
      else if (dx <= -range_x)
        delx = -range_x;
      else
        delx = dx;

      if (dy >= range_y)
        dely = range_y ;
      else if (dy <= -range_y)
        dely = -range_y + 0.8;
      else
      {
        if (dy > 0)
          dely = dy;
        else if (dy <= 0 && dy > -0.8)
          dely = 0;
        else if (dy < -0.8)
          dely = dy + 0.8;
      }
    }
    else
    {
      if (dx >= range_x)
        delx = range_x;
      else if (dx <= -range_x)
        delx = -range_x;
      else
        delx = dx;

      if (dy >= range_y)
        dely = range_y;
      else if (dy <= -range_y)
        dely = -range_y;
      else
        dely = dy;
    }
  }
  else
  {
      if (dx >= range_x)
          delx = range_x;
      else if (dx <= -range_x)
          delx = -range_x;
      else
          delx = dx;

      if (dy >= range_y)
          dely = range_y;
      else if (dy <= -range_y)
          dely = -range_y;
      else
          dely = dy;
  }


  /*
  if (dx >= 2.5)
    delx = 2.5;
  else if (dx <= -2.5)
    delx = -2.5;
  else
    delx = dx;

  if (dy >= 1.5)
    dely = 1.5;
  else if (dy <= -1.5)
    dely = -1.5;
  else
    dely = dy;
    */

  geometry_msgs::Pose2D obs_info;
  obs_info.x = delx;
  obs_info.y = dely;
  obs_info.theta = AGV_yaw * 180 / M_PI;
  pub_info.publish(obs_info);

  // printf("dx:%f, dy:%f, delx:%f, dely:%f, yaw:%f", dx, dy, delx, dely,
  // AGV_yaw);

  if (AGV_yaw > -M_PI / 4 && AGV_yaw <= M_PI / 4)
  {
    if (dx > 0.1)
    {
      if (dy > 0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::NORTH_WEST);
        north_west(delx, dely);
      }
      else if (dy < -0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::NORTH_EAST);
        north_east(delx, dely);
      }
      else
      {
        publish_move_dir(obstacle_detector::agv_dir::NORTH);
        north(delx);
      }
    }
    else if (dx < -0.1)
    {
      if (dy > 0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::SOUTH_WEST);
        south_west(delx, dely);
      }
      else if (dy < -0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::SOUTH_EAST);
        south_east(delx, dely);
      }
      else
      {
        publish_move_dir(obstacle_detector::agv_dir::SOUTH);
        south(delx);
      }
    }
    else
    {
      if (dy > 0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::WEST);
        west(dely);
      }
      else if (dy < -0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::EAST);
        east(dely);
      }
      else
      {
        publish_move_dir(obstacle_detector::agv_dir::NEAR_GOAL);
        near_goal();
      }
    }
  }
  else if (AGV_yaw > M_PI / 4 && AGV_yaw <= 3 * M_PI / 4)
  {
    if (dx > 0.1)
    {
      if (dy > 0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::NORTH_WEST);
        north_east(dely, -delx);
      }
      else if (dy < -0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::SOUTH_EAST);
        south_east(dely, -delx);
      }
      else
      {
        publish_move_dir(obstacle_detector::agv_dir::EAST);
        east(-delx);
      }
    }
    else if (dx < -0.1)
    {
      if (dy > 0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::NORTH_WEST);
        north_west(dely, -delx);
      }
      else if (dy < -0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::SOUTH_WEST);
        south_west(dely, -delx);
      }
      else
      {
        publish_move_dir(obstacle_detector::agv_dir::WEST);
        west(-delx);
      }
    }
    else
    {
      if (dy > 0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::NORTH);
        north(dely);
      }
      else if (dy < -0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::SOUTH);
        south(dely);
      }
      else
      {
        publish_move_dir(obstacle_detector::agv_dir::NEAR_GOAL);
        near_goal();
      }
    }
  }
  else if ((AGV_yaw > 3 * M_PI / 4 && AGV_yaw <= 4 * M_PI / 4) ||
           (AGV_yaw > -4 * M_PI / 4 && AGV_yaw <= -3 * M_PI / 4) ||
           (AGV_yaw > 4 * M_PI / 4 && AGV_yaw <= 5 * M_PI / 4))
  {
    if (dx > 0.1)
    {
      if (dy > 0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::SOUTH_EAST);
        south_east(-delx, -dely);
      }
      else if (dy < -0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::SOUTH_WEST);
        south_west(-delx, -dely);
      }
      else
      {
        publish_move_dir(obstacle_detector::agv_dir::SOUTH);
        south(-delx);
      }
    }
    else if (dx < -0.1)
    {
      if (dy > 0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::NORTH_EAST);
        north_east(-delx, -dely);
      }
      else if (dy < -0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::NORTH_WEST);
        north_west(-delx, -dely);
      }
      else
      {
        publish_move_dir(obstacle_detector::agv_dir::NORTH);
        north(-delx);
      }
    }
    else
    {
      if (dy > 0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::EAST);
        east(-dely);
      }
      else if (dy < -0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::WEST);
        west(-dely);
      }
      else
      {
        publish_move_dir(obstacle_detector::agv_dir::NEAR_GOAL);
        near_goal();
      }
    }
  }
  else if ((AGV_yaw > -3 * M_PI / 4 && AGV_yaw <= -M_PI / 4) ||
           (AGV_yaw > 5 * M_PI / 4 && AGV_yaw <= 7 * M_PI / 4))
  {
    if (dx > 0.1)
    {
      if (dy > 0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::SOUTH_WEST);
        south_west(-dely, delx);
      }
      else if (dy < -0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::NORTH_WEST);
        north_west(-dely, delx);
      }
      else
      {
        publish_move_dir(obstacle_detector::agv_dir::WEST);
        west(delx);
      }
    }
    else if (dx < -0.1)
    {
      if (dy > 0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::SOUTH_EAST);
        south_east(-dely, delx);
      }
      else if (dy < -0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::NORTH_EAST);
        north_east(-dely, delx);
      }
      else
      {
        publish_move_dir(obstacle_detector::agv_dir::EAST);
        east(delx);
      }
    }
    else
    {
      if (dy > 0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::SOUTH);
        south(-dely);
      }
      else if (dy < -0.1)
      {
        publish_move_dir(obstacle_detector::agv_dir::NORTH);
        north(-dely);
      }
      else
      {
        publish_move_dir(obstacle_detector::agv_dir::NEAR_GOAL);
        near_goal();
      }
    }
  }

  obstacle_detector::ob_range ob_range_;
  ob_range_.x_pos = p_max_x_limit_;
  ob_range_.x_neg = p_min_x_limit_;
  ob_range_.y_pos = p_max_y_limit_;
  ob_range_.y_neg = p_min_y_limit_;

  pub_ob_range.publish(ob_range_);
}
void ObstacleExtractor::north(double x)
{
  p_max_x_limit_ = min_range_x + x;
  p_min_x_limit_ = -min_range_x;
  p_min_y_limit_ = -min_range_y;
  p_max_y_limit_ = min_range_y;
}
void ObstacleExtractor::north_west(double x, double y)
{
  p_max_x_limit_ = min_range_x + x;
  p_min_x_limit_ = -min_range_x;
  p_min_y_limit_ = -min_range_y;
  p_max_y_limit_ = min_range_y + y;
}
void ObstacleExtractor::north_east(double x, double y)
{
  p_max_x_limit_ = min_range_x + x;
  p_min_x_limit_ = -min_range_x;
  p_min_y_limit_ = -min_range_y + y;
  p_max_y_limit_ = min_range_y;
}
void ObstacleExtractor::south(double x)
{
  p_max_x_limit_ = min_range_x;
  p_min_x_limit_ = -min_range_x + x;
  p_min_y_limit_ = -min_range_y;
  p_max_y_limit_ = min_range_y;
}
void ObstacleExtractor::south_west(double x, double y)
{
  p_max_x_limit_ = min_range_x;
  p_min_x_limit_ = -min_range_x + x;
  p_min_y_limit_ = -min_range_y;
  p_max_y_limit_ = min_range_y + y;
}
void ObstacleExtractor::south_east(double x, double y)
{
  p_max_x_limit_ = min_range_x;
  p_min_x_limit_ = -min_range_x + x;
  p_min_y_limit_ = -min_range_y + y;
  p_max_y_limit_ = min_range_y;
}
void ObstacleExtractor::west(double y)
{
  p_max_x_limit_ = min_range_x;
  p_min_x_limit_ = -min_range_x;
  p_min_y_limit_ = -min_range_y;
  p_max_y_limit_ = min_range_y + y;
}
void ObstacleExtractor::east(double y)
{
  p_max_x_limit_ = min_range_x;
  p_min_x_limit_ = -min_range_x;
  p_min_y_limit_ = -min_range_y + y;
  p_max_y_limit_ = min_range_y;
}
void ObstacleExtractor::near_goal()
{
  p_max_x_limit_ = min_range_x;
  p_min_x_limit_ = -min_range_x;
  p_min_y_limit_ = -min_range_y;
  p_max_y_limit_ = min_range_y;
}

void ObstacleExtractor::scanCallback(
    const sensor_msgs::LaserScan::ConstPtr scan_msg)
{
  base_frame_id_ = scan_msg->header.frame_id;
  stamp_ = scan_msg->header.stamp;

  double phi = scan_msg->angle_min;

  for (const float r : scan_msg->ranges)
  {
    if (r >= scan_msg->range_min && r <= scan_msg->range_max)
      input_points_.push_back(Point::fromPoolarCoords(r, phi));

    phi += scan_msg->angle_increment;
  }

  processPoints();
}

void ObstacleExtractor::pclCallback(
    const sensor_msgs::PointCloud::ConstPtr pcl_msg)
{
  base_frame_id_ = pcl_msg->header.frame_id;
  stamp_ = pcl_msg->header.stamp;

  for (const geometry_msgs::Point32& point : pcl_msg->points)
    input_points_.push_back(Point(point.x, point.y));

  processPoints();
}

void ObstacleExtractor::processPoints()
{
  segments_.clear();
  circles_.clear();
  // calAGV_dir();

  groupPoints(); // Grouping points simultaneously detects segments
  mergeSegments();

  detectCircles();
  mergeCircles();

  publishObstacles();

  input_points_.clear();
}

void ObstacleExtractor::groupPoints()
{
  static double sin_dp = sin(2.0 * p_distance_proportion_);

  PointSet point_set;
  point_set.begin = input_points_.begin();
  point_set.end = input_points_.begin();
  point_set.num_points = 1;
  point_set.is_visible = true;

  for (PointIterator point = input_points_.begin()++;
       point != input_points_.end(); ++point)
  {
    double range = (*point).length();
    double distance = (*point - *point_set.end).length();

    if (distance < p_max_group_distance_ + range * p_distance_proportion_)
    {
      point_set.end = point;
      point_set.num_points++;
    }
    else
    {
      double prev_range = (*point_set.end).length();

      // Heron's equation
      double p = (range + prev_range + distance) / 2.0;
      double S = sqrt(p * (p - range) * (p - prev_range) * (p - distance));
      double sin_d =
          2.0 * S / (range * prev_range); // Sine of angle between beams

      // TODO: This condition can be fulfilled if the point are on the
      // opposite sides of the scanner (angle = 180 deg). Needs another check.
      if (abs(sin_d) < sin_dp && range < prev_range)
        point_set.is_visible = false;

      detectSegments(point_set);

      // Begin new point set
      point_set.begin = point;
      point_set.end = point;
      point_set.num_points = 1;
      point_set.is_visible = (abs(sin_d) > sin_dp || range < prev_range);
    }
  }

  detectSegments(point_set); // Check the last point set too!
}

void ObstacleExtractor::detectSegments(const PointSet& point_set)
{
  if (point_set.num_points < p_min_group_points_)
    return;

  Segment segment(*point_set.begin,
                  *point_set.end); // Use Iterative End Point Fit

  if (p_use_split_and_merge_)
    segment = fitSegment(point_set);

  PointIterator set_divider;
  double max_distance = 0.0;
  double distance = 0.0;

  int split_index = 0; // Natural index of splitting point (counting from 1)
  int point_index = 0; // Natural index of current point in the set

  // Seek the point of division
  for (PointIterator point = point_set.begin; point != point_set.end; ++point)
  {
    ++point_index;

    if ((distance = segment.distanceTo(*point)) >= max_distance)
    {
      double r = (*point).length();

      if (distance > p_max_split_distance_ + r * p_distance_proportion_)
      {
        max_distance = distance;
        set_divider = point;
        split_index = point_index;
      }
    }
  }

  // Split the set only if the sub-groups are not 'small'
  if (max_distance > 0.0 && split_index > p_min_group_points_ &&
      split_index < point_set.num_points - p_min_group_points_)
  {
    set_divider = input_points_.insert(
        set_divider, *set_divider); // Clone the dividing point for each group

    PointSet subset1, subset2;
    subset1.begin = point_set.begin;
    subset1.end = set_divider;
    subset1.num_points = split_index;
    subset1.is_visible = point_set.is_visible;

    subset2.begin = ++set_divider;
    subset2.end = point_set.end;
    subset2.num_points = point_set.num_points - split_index;
    subset2.is_visible = point_set.is_visible;

    detectSegments(subset1);
    detectSegments(subset2);
  }
  else
  { // Add the segment
    if (!p_use_split_and_merge_)
      segment = fitSegment(point_set);

    segments_.push_back(segment);
  }
}

void ObstacleExtractor::mergeSegments()
{
  for (auto i = segments_.begin(); i != segments_.end(); ++i)
  {
    for (auto j = i; j != segments_.end(); ++j)
    {
      Segment merged_segment;

      if (compareSegments(*i, *j, merged_segment))
      {
        auto temp_itr = segments_.insert(i, merged_segment);
        segments_.erase(i);
        segments_.erase(j);
        i = --temp_itr; // Check the new segment against others
        break;
      }
    }
  }
}

bool ObstacleExtractor::compareSegments(const Segment& s1, const Segment& s2,
                                        Segment& merged_segment)
{
  if (&s1 == &s2)
    return false;

  // Segments must be provided counter-clockwise
  if (s1.first_point.cross(s2.first_point) < 0.0)
    return compareSegments(s2, s1, merged_segment);

  if (checkSegmentsProximity(s1, s2))
  {
    vector<PointSet> point_sets;
    point_sets.insert(point_sets.end(), s1.point_sets.begin(),
                      s1.point_sets.end());
    point_sets.insert(point_sets.end(), s2.point_sets.begin(),
                      s2.point_sets.end());

    Segment segment = fitSegment(point_sets);

    if (checkSegmentsCollinearity(segment, s1, s2))
    {
      merged_segment = segment;
      return true;
    }
  }

  return false;
}

bool ObstacleExtractor::checkSegmentsProximity(const Segment& s1,
                                               const Segment& s2)
{
  return (s1.trueDistanceTo(s2.first_point) < p_max_merge_separation_ ||
          s1.trueDistanceTo(s2.last_point) < p_max_merge_separation_ ||
          s2.trueDistanceTo(s1.first_point) < p_max_merge_separation_ ||
          s2.trueDistanceTo(s1.last_point) < p_max_merge_separation_);
}

bool ObstacleExtractor::checkSegmentsCollinearity(const Segment& segment,
                                                  const Segment& s1,
                                                  const Segment& s2)
{
  return (segment.distanceTo(s1.first_point) < p_max_merge_spread_ &&
          segment.distanceTo(s1.last_point) < p_max_merge_spread_ &&
          segment.distanceTo(s2.first_point) < p_max_merge_spread_ &&
          segment.distanceTo(s2.last_point) < p_max_merge_spread_);
}

void ObstacleExtractor::detectCircles()
{
  for (auto segment = segments_.begin(); segment != segments_.end(); ++segment)
  {
    if (p_circles_from_visibles_)
    {
      bool segment_is_visible = true;
      for (const PointSet& ps : segment->point_sets)
      {
        if (!ps.is_visible)
        {
          segment_is_visible = false;
          break;
        }
      }
      if (!segment_is_visible)
        continue;
    }

    Circle circle(*segment);
    circle.radius += p_radius_enlargement_;

    if (circle.radius < p_max_circle_radius_)
    {
      circles_.push_back(circle);

      if (p_discard_converted_segments_)
      {
        segment = segments_.erase(segment);
        --segment;
      }
    }
  }
}

void ObstacleExtractor::mergeCircles()
{
  for (auto i = circles_.begin(); i != circles_.end(); ++i)
  {
    for (auto j = i; j != circles_.end(); ++j)
    {
      Circle merged_circle;

      if (compareCircles(*i, *j, merged_circle))
      {
        auto temp_itr = circles_.insert(i, merged_circle);
        circles_.erase(i);
        circles_.erase(j);
        i = --temp_itr;
        break;
      }
    }
  }
}

bool ObstacleExtractor::compareCircles(const Circle& c1, const Circle& c2,
                                       Circle& merged_circle)
{
  if (&c1 == &c2)
    return false;

  // If circle c1 is fully inside c2 - merge and leave as c2
  if (c2.radius - c1.radius >= (c2.center - c1.center).length())
  {
    merged_circle = c2;
    return true;
  }

  // If circle c2 is fully inside c1 - merge and leave as c1
  if (c1.radius - c2.radius >= (c2.center - c1.center).length())
  {
    merged_circle = c1;
    return true;
  }

  // If circles intersect and are 'small' - merge
  if (c1.radius + c2.radius >= (c2.center - c1.center).length())
  {
    Point center = c1.center + (c2.center - c1.center) * c1.radius /
                                   (c1.radius + c2.radius);
    double radius = (c1.center - center).length() + c1.radius;

    Circle circle(center, radius);
    circle.radius += max(c1.radius, c2.radius);

    if (circle.radius < p_max_circle_radius_)
    {
      circle.point_sets.insert(circle.point_sets.end(), c1.point_sets.begin(),
                               c1.point_sets.end());
      circle.point_sets.insert(circle.point_sets.end(), c2.point_sets.begin(),
                               c2.point_sets.end());
      merged_circle = circle;
      return true;
    }
  }

  return false;
}

void ObstacleExtractor::publishObstacles()
{
  // Add Dinh 20200420
  calAGV_dir();

  obstacle_detector::ObstaclesPtr obstacles_msg(
      new obstacle_detector::Obstacles);
  obstacles_msg->header.stamp = stamp_;

  if (p_transform_coordinates_)
  {
    tf::StampedTransform transform;

    try
    {
      tf_listener_.waitForTransform(p_frame_id_, base_frame_id_, stamp_,
                                    ros::Duration(0.1));
      tf_listener_.lookupTransform(p_frame_id_, base_frame_id_, stamp_,
                                   transform);
    }
    catch (tf::TransformException& ex)
    {
      ROS_INFO_STREAM(ex.what());
      return;
    }

    for (Segment& s : segments_)
    {
      s.first_point = transformPoint(s.first_point, transform);
      s.last_point = transformPoint(s.last_point, transform);
    }

    for (Circle& c : circles_)
      c.center = transformPoint(c.center, transform);

    obstacles_msg->header.frame_id = p_frame_id_;
  }
  else
    obstacles_msg->header.frame_id = base_frame_id_;

  for (const Segment& s : segments_)
  {
      // Modify 210705
      if ((s.first_point.x > p_min_x_limit_ && s.first_point.x < p_max_x_limit_ &&
          s.first_point.y > p_min_y_limit_ && s.first_point.y < p_max_y_limit_)
              || (s.last_point.x > p_min_x_limit_ && s.last_point.x < p_max_x_limit_ &&
                  s.last_point.y > p_min_y_limit_ && s.last_point.y < p_max_y_limit_))
      {
        SegmentObstacle segment;

        segment.first_point.x = s.first_point.x;
        segment.first_point.y = s.first_point.y;
        segment.last_point.x = s.last_point.x;
        segment.last_point.y = s.last_point.y;

        obstacles_msg->segments.push_back(segment);
      }
  }

  for (const Circle& c : circles_)
  {
    if (c.center.x > p_min_x_limit_ && c.center.x < p_max_x_limit_ &&
        c.center.y > p_min_y_limit_ && c.center.y < p_max_y_limit_)
    {
      CircleObstacle circle;

      circle.center.x = c.center.x;
      circle.center.y = c.center.y;
      circle.velocity.x = 0.0;
      circle.velocity.y = 0.0;
      circle.radius = c.radius;
      circle.true_radius = c.radius - p_radius_enlargement_;

      obstacles_msg->circles.push_back(circle);
    }
  }

  obstacles_pub_.publish(obstacles_msg);
}
