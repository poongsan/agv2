
#include "obstacle_detector/obstacle_tracker.h"

using namespace obstacle_detector;

int main(int argc, char** argv) {
  ros::init(argc, argv, "obstacle_tracker", ros::init_options::NoRosout);
  ros::NodeHandle nh("");
  ros::NodeHandle nh_local("~");

  try {
    ROS_INFO("[Obstacle Tracker]: Initializing node");
    ObstacleTracker ot(nh, nh_local);
    ros::spin();
  }
  catch (const char* s) {
    ROS_FATAL_STREAM("[Obstacle Tracker]: " << s);
  }
  catch (...) {
    ROS_FATAL_STREAM("[Obstacle Tracker]: Unexpected error");
  }

  return 0;
}
