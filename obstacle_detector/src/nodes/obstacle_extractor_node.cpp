
#include "obstacle_detector/obstacle_extractor33.h"

using namespace obstacle_detector;

int main(int argc, char** argv) {
  ros::init(argc, argv, "obstacle_extractor", ros::init_options::NoRosout);
  ros::NodeHandle nh("");
  ros::NodeHandle nh_local("~");

  try {
    ROS_INFO("[Obstacle Extractor]: Initializing node");
    ObstacleExtractor od(nh, nh_local);
    ros::spin();
  }
  catch (const char* s) {
    ROS_FATAL_STREAM("[Obstacle Extractor]: "  << s);
  }
  catch (...) {
    ROS_FATAL_STREAM("[Obstacle Extractor]: Unexpected error");
  }

  return 0;
}
