#include <ros/ros.h>
#include "ros/time.h"
#include <ros/console.h>
#include <signal.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <asm/types.h>
#include <unistd.h>
#include "PCANBasic.h"

#include "sebang_msgs/se_bat.h"
#include "sebang_param.h"
#include "std_msgs/Int16.h"


#ifndef NO_RT
#include <sys/mman.h>

#ifdef RTAI
#include <rtai_lxrt.h>
#include <rtdm/rtdm.h>

/* overload the select() system call with the RTDM one, while it isn't by
 * RTAI 5.1. Note that this example doesn't care about the timeout
 */
#define select(n, r, w, e, t)	rt_dev_select(n, r, w, e, RTDM_TIMEOUT_INFINITE)
#endif

// PCAN-Basic device used to read on
// (RT version doesn't handle USB devices)
#define PCAN_DEVICE     PCAN_USBBUS2
#else

// PCAN-Basic device used to read on
#define PCAN_DEVICE     PCAN_USBBUS2
#endif


void mySigintHandler(int sig)
{
  ROS_INFO("Received SIGINT signal, shutting down...");
  ros::shutdown();
}

class sebang_bms
{

public:
  sebang_bms();
  ~sebang_bms();
  bool sebang_req();
  int run();

private:
  ros::NodeHandle nh;

  ros::Publisher pub_bms, pub_cnt;
  void publish_bms();


  short data_parsing();

  TPCANStatus SendCAN(BYTE byArray[]);

  // Power data
  float vol_ = 0.0;
  float cur_ = 0.0;
  uint8_t soc_ = 0;
  uint8_t state_ = 0;
  int16_t cnt_ = 0;

  int rate_ = 50;

};



