#include "sebang_bms.h"

sebang_bms::sebang_bms()
{
  pub_bms = nh.advertise<sebang_msgs::se_bat>("/bms/sebang", 1);
  pub_cnt = nh.advertise<std_msgs::Int16>("/bms/cnt", 1);
}

sebang_bms::~sebang_bms() {}

TPCANStatus sebang_bms::SendCAN(BYTE byArray[])
{
  BYTE i;
  TPCANMsg Message;
  TPCANStatus Status;
  Message.ID = 0x3C9;
  Message.LEN = 3;
  Message.MSGTYPE = PCAN_MESSAGE_STANDARD;
  for (i = 0; i < 3; i++)
    Message.DATA[i] = byArray[i];

  Status = CAN_Write(PCAN_DEVICE, &Message);
  return Status;
}

bool sebang_bms::sebang_req()
{
  BYTE bydata[3];
  bydata[0] = 0x13;
  bydata[1] = 0x15;
  bydata[2] = 0x14;
  /*
  for (int i = 3; i < 8; i++)
    bydata[i] = 0;
    */
  TPCANStatus data_sent = SendCAN(bydata);
  if(data_sent == PCAN_ERROR_OK)
    return true;
  else
    return false;
}



short sebang_bms::data_parsing()
{
  // Parsing CAN message
  TPCANMsg Message;
  bool Status = CAN_Read(PCAN_DEVICE, &Message, NULL);
  //int8_t size_ = sizeof(Message.DATA)/sizeof(Message.DATA[0]);

  if (!Status)
  {
    if (Message.ID == 0x3CA)
    {
      // if(Message.DATA[0] == (BYTE)PID_MONITOR)

      if (Message.DATA[0] == 0xD1)
      {
          /*
        switch (Message.DATA[1])
        {
        case 0x03:
        {
          vol_ = (float)GET_U16_BYTE(Message.DATA[3], Message.DATA[2]) / 100.0f;
          //printf("vol \n");
        }
          break;
        case 0x05:
        {
          cur_ = (float)GET_U16_BYTE(Message.DATA[3], Message.DATA[2]) / 100.0f;
          //printf("current \n");
          if(cur_>0)
              state_ = 1; // discharge
          else if(cur_<0)
              state_ = 2; // charge
          else
              state_ = 0;
        }
          break;
        case 0x15:
        {
          soc_ = (uint8_t) GET_U16_BYTE(Message.DATA[3], Message.DATA[2]);

          if(soc_>=100)
              soc_ = 100;


          //printf("soc \n");
        }
          break;
        }
    */
          if(Message.DATA[1] == 0x03)
          {
              vol_ = (float)GET_U16_BYTE(Message.DATA[3], Message.DATA[2]) / 100.0f;
          }

          if(Message.DATA[1] == 0x05)
          {
              cur_ = (float)GET_16_BYTE(Message.DATA[3], Message.DATA[2]) /10.0f;


              if(cur_>5.0)
                  state_ = 2; // discharge
              else if(cur_<-10.0)
                  state_ = 1; // charge
              else
                  state_ = 0;

          }

          if(Message.DATA[1] == 0x15)
          {
              soc_ = (uint8_t) GET_U16_BYTE(Message.DATA[3], Message.DATA[2]);
          }



        //printf("Enter D1 \n");
      }
      else if(Message.DATA[0] == 0xD0)
      {
          cnt_ = (int16_t)GET_U16_BYTE(Message.DATA[2], Message.DATA[1]);

          std_msgs::Int16 cnt_msgs;
          cnt_msgs.data = cnt_;
          pub_cnt.publish(cnt_msgs);

      }
      else
      {
        //printf("Unknown message \n");
      }
    }
    else
    {
      printf("Cannot receive message \n");
    }
  }
  return 1;
}



void sebang_bms::publish_bms()
{
  sebang_msgs::se_bat sebang_data;
  sebang_data.header.stamp = ros::Time::now();
  sebang_data.vol = vol_;
  sebang_data.cur = cur_;
  sebang_data.soc = soc_ -1;
  sebang_data.state = state_;

  pub_bms.publish(sebang_data);
}


int sebang_bms::run()
{

  ROS_INFO("Setup CAN Port...");
  ros::Rate loop_rate(rate_);

  TPCANStatus Status;
  DWORD pcan_device = PCAN_DEVICE;

#ifndef NO_RT
  mlockall(MCL_CURRENT | MCL_FUTURE);
#endif

  // Status = CAN_Initialize(pcan_device, PCAN_BAUD_1M, 0, 0, 0);
  Status = CAN_Initialize(pcan_device, PCAN_BAUD_250K, 0, 0, 0);
  printf("CAN_Initialize(%xh): Status=0x%x\n", pcan_device, (int)Status);
  if (Status == PCAN_ERROR_OK)
    goto lbl_cont;
  else
  {
    goto lbl_exit;
  }

lbl_cont:
  int fd;
  Status = CAN_GetValue(pcan_device, PCAN_RECEIVE_EVENT, &fd, sizeof fd);
  printf("CAN_GetValue(%xh): Status=0x%x\n", pcan_device, (int)Status);
  if (Status)
    goto lbl_close;

  if(sebang_req())
     printf("Send request OK\n");
  else
      printf("Send request FAIL\n");


  while (ros::ok())
  {
     // sebang_req();
    data_parsing();
    publish_bms();

    ros::spinOnce();
    loop_rate.sleep();
  }

lbl_close:
  CAN_Uninitialize(pcan_device);
  printf("CAN close \n");

lbl_exit:

  printf("Shutting down ... \n");
  ros::waitForShutdown();

  return 0;
}

int main(int argc, char** argv)
{

  ros::init(argc, argv, "sebang_node");

  sebang_bms sebang_bat;

 // signal(SIGINT, mySigintHandler);

  return sebang_bat.run();
}
