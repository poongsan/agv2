#include <ros/ros.h>
#include "docent_msg/Roboteq_driver_command.h"
#include <sensor_msgs/Joy.h>


class TeleopMotor
{
public:
  TeleopMotor();

private:
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);

  ros::NodeHandle nh_;

  int linear_x, linear_y, angular_z;
  int enable_btn, turbo_btn;
  double l_scale, a_scale;
 // double tl_scale_,ta_scale_;
  ros::Publisher vel_pub_;
  ros::Subscriber joy_sub_;

};


TeleopMotor::TeleopMotor():
  linear_x(1),linear_y(2),angular_z(3),enable_btn(4),turbo_btn(5),a_scale(6),l_scale(7)
{

  nh_.param("axis_linear_x", linear_x, linear_x);
  nh_.param("axis_linear_y", linear_y, linear_y);
  nh_.param("axis_angular", angular_z, angular_z);
  nh_.param("enable_button", enable_btn, enable_btn);
  nh_.param("turbo_button", turbo_btn, turbo_btn);
  nh_.param("scale_angular", a_scale, a_scale);
  nh_.param("scale_linear", l_scale, l_scale);
//  nh_.param("turbo_scale_angular", ta_scale_, ta_scale_);
// nh_.param("turbo_scale_linear", tl_scale_, tl_scale_;


  vel_pub_ = nh_.advertise<docent_msg::Roboteq_driver_command>("cmd_vel", 1);


  joy_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 10, &TeleopMotor::joyCallback, this);

}

void TeleopMotor::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  docent_msg::Roboteq_driver_command Driver_command;
  if(joy->buttons[enable_btn])
  {
    if(joy->buttons[turbo_btn])
    {
        Driver_command.control_mode = 0x01; // Velocity mode
        Driver_command.motor_data = l_scale*joy->axes[linear_x];
    }
    else
    {
        Driver_command.control_mode = 0x01; // Velocity mode
        Driver_command.motor_data = joy->axes[linear_x];
    }

  }
  else
  {
      Driver_command.control_mode = 0x00;
      Driver_command.motor_data = 0;
  }
  vel_pub_.publish(Driver_command);
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "teleop_motor");
  TeleopMotor teleop_motor;
  ros::spin();
}
