#include <ros/ros.h>
#include "docent_msg/MecanumMotorCtrlCmd.h"
#include <sensor_msgs/Joy.h>


class TeleopMotor
{
public:
  TeleopMotor();

private:
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);
  int sgn(double speed);
  void publish_motor_command(int speed_FL,int speed_FR,int speed_RL,int speed_RR);


  ros::NodeHandle nh_;

  int linear_x, linear_y, angular_z;
  int enable_btn, lock_btn, mode_btn, turbo_btn;
  double l_scale;
  int move_mode;
  int front_left, front_right, rear_left, rear_right;
 // double tl_scale_,ta_scale_;
  ros::Publisher vel_pub_;
  ros::Subscriber joy_sub_;

};


TeleopMotor::TeleopMotor():
  linear_x(1),linear_y(2),angular_z(3),mode_btn(4),enable_btn(5),lock_btn(6), turbo_btn(7),l_scale(8)
{

  nh_.param("axis_linear_x", linear_x, linear_x);
  nh_.param("axis_linear_y", linear_y, linear_y);
  nh_.param("axis_angular", angular_z, angular_z);
  nh_.param("mode_button", mode_btn, mode_btn);
  nh_.param("enable_button", enable_btn, enable_btn);
  nh_.param("lock_button", lock_btn, lock_btn);
  nh_.param("turbo_button", turbo_btn, turbo_btn);
  nh_.param("scale_linear", l_scale, l_scale);


  vel_pub_ = nh_.advertise<docent_msg::MecanumMotorCtrlCmd>("cmd_vel", 1);

  joy_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 10, &TeleopMotor::joyCallback, this);

  move_mode = -1;
  front_left = 0, front_right = 0, rear_left = 0, rear_right = 0;
}

void TeleopMotor::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  if(joy->buttons[mode_btn] == 1 && joy->buttons[enable_btn] == 1)
  {
    move_mode = 1;
  }
  if(joy->buttons[mode_btn] == 1 && joy->buttons[lock_btn] == 1)
  {
    move_mode = 0;
  }

  if(move_mode == 1)
  {
    int motor_speed = 0, motor_dir = 0;

       if(abs(joy->buttons[turbo_btn]) > 0.8)
       {
           motor_speed = l_scale*int((abs(joy->axes[linear_x]*200)));
           motor_dir=sgn(joy->axes[linear_x]);
       }
       else
       {
          motor_speed = int((abs(joy->axes[linear_x]*200)));
          motor_dir=sgn(joy->axes[linear_x]);
       }
     front_left = motor_speed * motor_dir;
     front_right = motor_speed * motor_dir *(-1);
     rear_left = motor_speed * motor_dir;
     rear_right = motor_speed * motor_dir*(-1);
     publish_motor_command(front_left,front_right,rear_left,rear_right);
  }

  if(move_mode == 0)
  {
    front_left = 0, front_right = 0, rear_left = 0, rear_right = 0;
    publish_motor_command(front_left,front_right,rear_left,rear_right);
  }

}

void TeleopMotor::publish_motor_command(int speed_FL,int speed_FR,int speed_RL,int speed_RR)
{
  docent_msg::MecanumMotorCtrlCmd move_mecanum;
  if(move_mode == 1) {move_mecanum.ControlMode = 0x01;}//VELOCITY MODE
  else {move_mecanum.ControlMode = 0x00;}//STOP MODE
  move_mecanum.FrontLeftSpeed = speed_FL;
  move_mecanum.FrontRightSpeed = speed_FR;
  move_mecanum.RearLeftSpeed = speed_RL;
  move_mecanum.RearRightSpeed = speed_RR;
  vel_pub_.publish(move_mecanum);

}

int TeleopMotor::sgn(double speed)
{
  if (speed < 0) {return -1;}
  else if (speed > 0) { return 1;}
  else {return 0;}
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "teleop_blueagv");
  TeleopMotor teleop_blueagv;
  ros::spin();
}
