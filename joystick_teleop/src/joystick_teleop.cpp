#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>


class TeleopTurtle
{
public:
  TeleopTurtle();

private:
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);

  ros::NodeHandle nh_;

  int linear_x, linear_y, angular_z;
  int enable_btn, turbo_btn;
  double l_scale, a_scale;
 // double tl_scale_,ta_scale_;
  ros::Publisher vel_pub_;
  ros::Subscriber joy_sub_;

};


TeleopTurtle::TeleopTurtle():
  linear_x(1),linear_y(2),angular_z(3),enable_btn(4),turbo_btn(5),a_scale(6),l_scale(7)
{

  nh_.param("axis_linear_x", linear_x, linear_x);
  nh_.param("axis_linear_y", linear_y, linear_y);
  nh_.param("axis_angular", angular_z, angular_z);
  nh_.param("enable_button", enable_btn, enable_btn);
  nh_.param("turbo_button", turbo_btn, turbo_btn);
  nh_.param("scale_angular", a_scale, a_scale);
  nh_.param("scale_linear", l_scale, l_scale);
//  nh_.param("turbo_scale_angular", ta_scale_, ta_scale_);
// nh_.param("turbo_scale_linear", tl_scale_, tl_scale_;


  vel_pub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1);


  joy_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 10, &TeleopTurtle::joyCallback, this);

}

void TeleopTurtle::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  geometry_msgs::Twist twist;
  if(joy->buttons[enable_btn])
  {
    if(joy->buttons[turbo_btn])
    {
        twist.angular.z = a_scale*joy->axes[angular_z];
        twist.linear.x = l_scale*joy->axes[linear_x];
        twist.linear.y = l_scale*joy->axes[linear_y];
    }
    else
    {
      twist.angular.z = joy->axes[angular_z];
      twist.linear.x = joy->axes[linear_x];
      twist.linear.y = joy->axes[linear_y];
    }

  }
  else
  {
      twist.angular.z = 0;
      twist.linear.x = 0;
      twist.linear.y = 0;
  }
  vel_pub_.publish(twist);
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "teleop_turtle");
  TeleopTurtle teleop_turtle;

  ros::spin();
}
