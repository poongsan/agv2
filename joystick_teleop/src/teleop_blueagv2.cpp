#include <ros/ros.h>
#include "docent_msg/AGV_mecanum_move.h"
#include <sensor_msgs/Joy.h>


class TeleopMotor
{
public:
  TeleopMotor();

private:
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);
  int sgn(double speed);
  void publish_motor_command(double velx,double vely,double angz);


  ros::NodeHandle nh_;

  int linear_x, linear_y, angular_z;
  int enable_btn, lock_btn, mode_btn, turbo_btn;
  double l_scale_x, l_scale_y, a_scale_z;
  int move_mode;
  double vel_x, vel_y, ang_z;
 // double tl_scale_,ta_scale_;
  ros::Publisher vel_pub_;
  ros::Subscriber joy_sub_;

};


TeleopMotor::TeleopMotor():
  linear_x(1),linear_y(0),angular_z(3),mode_btn(5),enable_btn(1),lock_btn(0), turbo_btn(4),l_scale_x(1.5),l_scale_y(1.2),a_scale_z(1.2)
{

  nh_.param("axis_linear_x", linear_x, linear_x);
  nh_.param("axis_linear_y", linear_y, linear_y);
  nh_.param("axis_angular", angular_z, angular_z);
  nh_.param("mode_button", mode_btn, mode_btn);
  nh_.param("enable_button", enable_btn, enable_btn);
  nh_.param("lock_button", lock_btn, lock_btn);
  nh_.param("turbo_button", turbo_btn, turbo_btn);
  nh_.param("scale_linear_x", l_scale_x, l_scale_x);
  nh_.param("scale_linear_y", l_scale_y, l_scale_y);
  nh_.param("scale_ang_z", a_scale_z, a_scale_z);

  vel_pub_ = nh_.advertise<docent_msg::AGV_mecanum_move>("cmd_vel", 1);

  joy_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 1, &TeleopMotor::joyCallback, this);

  move_mode = -1;
  vel_x = 0, vel_y = 0, ang_z = 0;
}

void TeleopMotor::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  if(joy->buttons[mode_btn] == 1 && joy->buttons[enable_btn] == 1)
  {
    move_mode = 1;
  }
  if(joy->buttons[mode_btn] == 1 && joy->buttons[lock_btn] == 1)
  {
    move_mode = 0;
  }

  if(move_mode == 1)
  {
    double lin_spd_x = 0, lin_spd_y = 0, rot_spd_z = 0;
    int dir_x = 0, dir_y = 0, dir_z = 0;

       if(abs(joy->buttons[turbo_btn]) > 0.8)
       {
           lin_spd_x = l_scale_x*double((fabs(joy->axes[linear_x]*100)));
           lin_spd_y = l_scale_y*double((fabs(joy->axes[linear_y]*100)));
           rot_spd_z = a_scale_z*double((fabs(joy->axes[angular_z]*80)));
       }
       else
       {
           lin_spd_x = double((fabs(joy->axes[linear_x]*100)));
           lin_spd_y = double((fabs(joy->axes[linear_y]*100)));
           rot_spd_z = double((fabs(joy->axes[angular_z]*80)));
       }

     dir_x=sgn(joy->axes[linear_x]);
     dir_y=sgn(joy->axes[linear_y]);
     dir_z=sgn(joy->axes[angular_z]);

     vel_x = double(lin_spd_x * dir_x);
     vel_y = double(lin_spd_y * dir_y);
     ang_z = double(rot_spd_z * dir_z);
     publish_motor_command(vel_x,vel_y,ang_z);
  }

  if(move_mode == 0)
  {
    vel_x = 0, vel_y = 0, ang_z = 0;
    publish_motor_command(vel_x,vel_y,ang_z);
  }

}

void TeleopMotor::publish_motor_command(double velx,double vely,double angz)
{
  docent_msg::AGV_mecanum_move move_mecanum;
  if(move_mode == 1) {move_mecanum.control_mode = 0x01;}//VELOCITY MODE
  else {move_mecanum.control_mode = 0x00;}//STOP MODE
  move_mecanum.linear_x = velx;
  move_mecanum.linear_y = vely;
  move_mecanum.angular_z = angz;

  vel_pub_.publish(move_mecanum);

}

int TeleopMotor::sgn(double speed)
{
  if (speed < 0) {return -1;}
  else if (speed > 0) { return 1;}
  else {return 0;}
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "ps4_blueAGV");
  TeleopMotor ps4_blueAGV;
  ros::spin();
}
