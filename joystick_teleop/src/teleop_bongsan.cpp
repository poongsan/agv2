#include "move_control/teleop.h"
#include "ros/time.h"
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <signal.h>
void mySigintHandler(int sig)
{
  ROS_INFO("Receive SIGINT signal, shutting down ...");
  ros::shutdown();
}

class TeleopMotor
{
public:
  TeleopMotor();
  int run();

private:
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);
  int sgn(double speed);
  void publish_motor_command(double velx, double vely, double angz);
  ros::Time joy_header;

  ros::NodeHandle nh_;

  int linear_x, linear_y, angular_z;
  int enable_btn, lock_btn, mode_btn, turbo_btn, slow_btn, constant_btn,
      qr_turn_btn;
  double l_scale_x, l_scale_y, a_scale_z, speed_scale, slow_scale;
  int move_mode;
  double vel_x, vel_y, ang_z;
  // double tl_scale_,ta_scale_;
  ros::Publisher vel_pub_;
  ros::Subscriber joy_sub_;

  int rate_ = 200;
};

TeleopMotor::TeleopMotor()
    : linear_x(1), linear_y(0), angular_z(3), mode_btn(5), enable_btn(1),
      qr_turn_btn(2), lock_btn(0), turbo_btn(4), slow_btn(6), constant_btn(3),
      l_scale_x(1.5), l_scale_y(1.2), a_scale_z(1.2), speed_scale(60),
      slow_scale(0.5)
{

  nh_.param("axis_linear_x", linear_x, linear_x);
  nh_.param("axis_linear_y", linear_y, linear_y);
  nh_.param("axis_angular", angular_z, angular_z);
  nh_.param("mode_button", mode_btn, mode_btn);
  nh_.param("enable_button", enable_btn, enable_btn);
  nh_.param("lock_button", lock_btn, lock_btn);
  nh_.param("constant_button", constant_btn, constant_btn);
  nh_.param("turn_of_QR", qr_turn_btn, qr_turn_btn);
  nh_.param("turbo_button", turbo_btn, turbo_btn);
  nh_.param("slow_button", slow_btn, slow_btn);
  nh_.param("scale_linear_x", l_scale_x, l_scale_x);
  nh_.param("scale_linear_y", l_scale_y, l_scale_y);
  nh_.param("scale_ang_z", a_scale_z, a_scale_z);
  nh_.param("speed_scale", speed_scale, speed_scale);
  nh_.param("slow_scale", slow_scale, slow_scale);

  vel_pub_ = nh_.advertise<move_control::teleop>("cmd_vel", 1);

  joy_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 1,
                                             &TeleopMotor::joyCallback, this);

  move_mode = 1;
  vel_x = 0, vel_y = 0, ang_z = 0;
}

void TeleopMotor::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  // Modify Dinh 20200330
  joy_header = joy->header.stamp;
  if (joy->buttons[mode_btn] == 1 && joy->buttons[enable_btn] == 1)
  {
    move_mode = 1;
  }
  if (joy->buttons[mode_btn] == 1 && joy->buttons[lock_btn] == 1)
  {
    move_mode = 0;
  }
  if (joy->buttons[mode_btn] == 1 && joy->buttons[constant_btn] == 1)
  {
    move_mode = 2;
  }
  if (joy->buttons[mode_btn] == 1 && joy->buttons[qr_turn_btn] == 1)
  {
    move_mode = 3;
  }

  if (move_mode == 1)
  {
    double lin_spd_x = 0, lin_spd_y = 0, rot_spd_z = 0;
    int dir_x = 0, dir_y = 0, dir_z = 0;

    if (abs(joy->buttons[turbo_btn]) > 0.8 && abs(joy->buttons[slow_btn]) < 0.2)
    {
      if (fabs(joy->axes[linear_x]) > 0.15 && fabs(joy->axes[linear_y]) < 0.15)
      {
        lin_spd_x = double((fabs(joy->axes[linear_x]) - 0.1) * l_scale_x);
        lin_spd_y = 0;
      }
      else
      {
        if (fabs(joy->axes[linear_x]) < 0.15 &&
            fabs(joy->axes[linear_y]) > 0.15)
        {
          lin_spd_x = 0;
          lin_spd_y = double((fabs(joy->axes[linear_y]) - 0.1) * l_scale_y);
        }
        else
        {
          if (fabs(joy->axes[linear_x]) > 0.15 &&
              fabs(joy->axes[linear_y]) > 0.15)
          {
            lin_spd_x = double((fabs(joy->axes[linear_x]) - 0.1) * l_scale_x);
            lin_spd_y = double((fabs(joy->axes[linear_y]) - 0.1) * l_scale_y);
          }
          else
          {
            lin_spd_x = 0;
            lin_spd_y = 0;
          }
        }
      }
      rot_spd_z = 0.2 * double(fabs(joy->axes[angular_z] * a_scale_z));
    }
    else if (abs(joy->buttons[slow_btn]) > 0.8)
    {
      if (fabs(joy->axes[linear_x]) > 0.15 && fabs(joy->axes[linear_y]) < 0.15)
      {
        lin_spd_x = double((fabs(joy->axes[linear_x]) - 0.1) * slow_scale);
        lin_spd_y = 0;
      }
      else
      {
        if (fabs(joy->axes[linear_x]) < 0.15 &&
            fabs(joy->axes[linear_y]) > 0.15)
        {
          lin_spd_x = 0;
          lin_spd_y = double((fabs(joy->axes[linear_y]) - 0.1) * slow_scale);
        }
        else
        {
          if (fabs(joy->axes[linear_x]) > 0.15 &&
              fabs(joy->axes[linear_y]) > 0.15)
          {
            lin_spd_x = ((fabs(joy->axes[linear_x]) - 0.1) * slow_scale);
            lin_spd_y = ((fabs(joy->axes[linear_y]) - 0.1) * slow_scale);
          }
          else
          {
            lin_spd_x = 0;
            lin_spd_y = 0;
          }
        }
      }
      rot_spd_z = 0.2 * double(fabs(joy->axes[angular_z] * slow_scale));
    }
    else
    {
      if (fabs(joy->axes[linear_x]) > 0.15 && fabs(joy->axes[linear_y]) < 0.15)
      {
        lin_spd_x = double(fabs(joy->axes[linear_x]) - 0.1);
        lin_spd_y = 0;
      }
      else
      {
        if (fabs(joy->axes[linear_x]) < 0.15 &&
            fabs(joy->axes[linear_y]) > 0.15)
        {
          lin_spd_x = 0;
          lin_spd_y = double(fabs(joy->axes[linear_y]) - 0.1);
        }
        else
        {
          if (fabs(joy->axes[linear_x]) > 0.15 &&
              fabs(joy->axes[linear_y]) > 0.15)
          {
            lin_spd_x = double(fabs(joy->axes[linear_x]) - 0.1);
            lin_spd_y = double(fabs(joy->axes[linear_y]) - 0.1);
          }
          else
          {
            lin_spd_x = 0;
            lin_spd_y = 0;
          }
        }
      }
      rot_spd_z = 0.2 * double(fabs(joy->axes[angular_z]));
    }

    dir_x = sgn(joy->axes[linear_x]);
    dir_y = -sgn(joy->axes[linear_y]);
    dir_z = -sgn(joy->axes[angular_z]);

    vel_x = double(lin_spd_x * dir_x * speed_scale);
    vel_y = double(lin_spd_y * dir_y * speed_scale);
    ang_z = double(rot_spd_z * dir_z * speed_scale);
    // publish_motor_command(vel_x,vel_y,ang_z);
  }
  else if (move_mode == 2)
  {
    double lin_spd_x = 0, lin_spd_y = 0, rot_spd_z = 0;
    int dir_x = 0, dir_y = 0, dir_z = 0;

    if (abs(joy->buttons[turbo_btn]) > 0.8 && abs(joy->buttons[slow_btn]) < 0.2)
    {
      if (fabs(joy->axes[linear_x]) > 0.15 && fabs(joy->axes[linear_y]) < 0.15)
      {
        lin_spd_x = 0.8;
        lin_spd_y = 0;
      }
      else
      {
        if (fabs(joy->axes[linear_x]) < 0.15 &&
            fabs(joy->axes[linear_y]) > 0.15)
        {
          lin_spd_x = 0;
          lin_spd_y = 0.8;
        }
        else
        {
          if (fabs(joy->axes[linear_x]) > 0.15 &&
              fabs(joy->axes[linear_y]) > 0.15)
          {
            lin_spd_x = 0.8;
            lin_spd_y = 0.8;
          }
          else
          {
            lin_spd_x = 0;
            lin_spd_y = 0;
          }
        }
      }
      rot_spd_z = 0.2;
    }
    else if (abs(joy->buttons[slow_btn]) > 0.8)
    {
      if (fabs(joy->axes[linear_x]) > 0.15 && fabs(joy->axes[linear_y]) < 0.15)
      {
        lin_spd_x = 0.25;
        lin_spd_y = 0;
      }
      else
      {
        if (fabs(joy->axes[linear_x]) < 0.15 &&
            fabs(joy->axes[linear_y]) > 0.15)
        {
          lin_spd_x = 0;
          lin_spd_y = 0.25;
        }
        else
        {
          if (fabs(joy->axes[linear_x]) > 0.15 &&
              fabs(joy->axes[linear_y]) > 0.15)
          {
            lin_spd_x = 0.25;
            lin_spd_y = 0.25;
          }
          else
          {
            lin_spd_x = 0;
            lin_spd_y = 0;
          }
        }
      }
      rot_spd_z = 0.06;
    }
    else
    {
      if (fabs(joy->axes[linear_x]) > 0.15 && fabs(joy->axes[linear_y]) < 0.15)
      {
        lin_spd_x = 0.4;
        lin_spd_y = 0;
      }
      else
      {
        if (fabs(joy->axes[linear_x]) < 0.15 &&
            fabs(joy->axes[linear_y]) > 0.15)
        {
          lin_spd_x = 0;
          lin_spd_y = 0.4;
        }
        else
        {
          if (fabs(joy->axes[linear_x]) > 0.15 &&
              fabs(joy->axes[linear_y]) > 0.15)
          {
            lin_spd_x = 0.4;
            lin_spd_y = 0.4;
          }
          else
          {
            lin_spd_x = 0;
            lin_spd_y = 0;
          }
        }
      }
      rot_spd_z = 0.1;
    }

    dir_x = sgn(joy->axes[linear_x]);
    dir_y = -sgn(joy->axes[linear_y]);
    dir_z = -sgn(joy->axes[angular_z]);

    vel_x = double(lin_spd_x * dir_x);
    vel_y = double(lin_spd_y * dir_y);
    ang_z = double(rot_spd_z * dir_z);
  }
  else if (move_mode == 3)
  {
    vel_x = 0, vel_y = 0;
    double rot_spd_z = 0;
    int dir_z = 0;
    rot_spd_z = double(fabs(joy->axes[angular_z]));
    dir_z = -sgn(joy->axes[angular_z]);
    ang_z = double(rot_spd_z * dir_z);
  }
  else
  {
    vel_x = 0, vel_y = 0, ang_z = 0;
  }

  // if(move_mode == 0)
  //{
  // vel_x = 0, vel_y = 0, ang_z = 0;
  // Modify Dinh 20200330
  // publish_motor_command(vel_x,vel_y,ang_z);
  //}
}

void TeleopMotor::publish_motor_command(double velx, double vely, double angz)
{
  move_control::teleop move_mecanum;
  move_mecanum.header.stamp = ros::Time::now();
  if (move_mode == 1) // VARIABLE SPEED MODE
  {
    move_mecanum.control_mode = 0x01;
    move_mecanum.linear_x = velx;
    move_mecanum.linear_y = vely;
    move_mecanum.angular_z = angz;
  }
  else if (move_mode == 2) // CONSTANT SPEED MODE
  {
    move_mecanum.control_mode = 0x01;
    move_mecanum.linear_x = velx;
    move_mecanum.linear_y = vely;
    move_mecanum.angular_z = angz;
  }
  else if (move_mode == 3) // QR TURNING MODE
  {
    move_mecanum.control_mode = 0x02;
    move_mecanum.angular_z = angz;
  }
  else
  {
    move_mecanum.control_mode = 0x00;
    move_mecanum.linear_x = 0;
    move_mecanum.linear_y = 0;
    move_mecanum.angular_z = 0;
  } // STOP MODE

  vel_pub_.publish(move_mecanum);
}

int TeleopMotor::sgn(double speed)
{
  if (speed < 0)
  {
    return -1;
  }
  else if (speed > 0)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

int TeleopMotor::run()
{
  ros::Rate loop_rate(rate_);
  while (ros::ok())
  {
    ros::Time time = ros::Time::now();
    double dt = (time - joy_header).toSec();
    if (fabs(dt) < 0.5)
    {
      publish_motor_command(-vel_x, -vel_y, -ang_z);
    }
    else
    {
      publish_motor_command(0, 0, 0);
    }
    ros::spinOnce();
    loop_rate.sleep();
  }
  publish_motor_command(0, 0, 0);
  return 0;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "ps4_blueAGV");
  TeleopMotor ps4_blueAGV;
  // Modify Dinh 20200330
  signal(SIGINT, mySigintHandler);
  return ps4_blueAGV.run();
  // ros::spin();
}
