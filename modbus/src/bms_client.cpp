
//   * header file    *//
#include "unit-modbus-rtu.h"
// path :  ${project}/include/modbus/unit-modbus-rtu.h
// lib : libmodbus [ need to install --> $ sudo apt-get install libmodbus-dev
//					&& sudo apt-get install libmodbus5

 /* for ROS */
#include <ros/ros.h>
#include <ros/time.h>

#include <if_node_msgs/powerState.h>
//////////////////////////

//*   Global Variable   *//
 /* for read byte */
uint16_t byte_In[3] = {0,};

#define current_reg 0x0000
#define voltage_reg 0x0001
#define SoC_rge     0x0002

///////////////////////////////////////////////////////
// RTU read & write Example
///////////////////////////////////////////////////////
/* write single Byte */
// int rc = modbus_write_register(ctx, int16_t addr, int16_t Byte);
// ex] rc = modbus_write_register(ctx, 0xC00, 0/x0F);	//@ D영역 00번 주소에  0x0F 입력. 
//     usleep(0.05*1000000);

// read single or Multi Bytes
// int read_byte(int addr, int nb, uint16_t *dst);
// ex] rc = read_byte(0xC00, 1, byte_In);		//@ D영역 00번 주소에서 1byte 값을 dst에 저장. 
//     printf("%d\n", byte_Out[0]);			//@ print value 
//     usleep(0.05*1000000);
///////////////////////////////////////////////////////

ros::Publisher pub_state;

void readPowerState()
{
	int rc;
	if_node_msgs::powerState state_msgs;			//완료 상태 1: 로딩 완료,  2: 언로딩 , 0:??
	rc = read_byte(0x0000, 3, byte_In);		//@ D영역 101번 주소에서 1byte 값을 dst에 저장.

    signed short x = byte_In[0];

    state_msgs.voltage = byte_In[1]/100.0;
    state_msgs.current = x/100.0;

    state_msgs.soc = byte_In[2];
    if(x > 0)
        state_msgs.state = 1;
    else
        state_msgs.state = 0;

    //printf("current: %d --> %.2f A \n", byte_In[0], state_msgs.current );
    //printf("voltage: %d --> %.2f V \n", byte_In[1], state_msgs.voltage);
    //printf("SoC: %d --> %d %c \n", byte_In[2], state_msgs.soc, '%');

	pub_state.publish(state_msgs);

	usleep(0.05*1000000);
}

int main(int argc, char *argv[])
{
	///////////////////////////////////////////////////////
	// setting For ROS
	///////////////////////////////////////////////////////
	/* ROS INIT */
	ros::init(argc, argv, "bms_client");
	ros::NodeHandle nh;
	/* TOPIC declare */
    // ros::Publisher pub_state;
	/* TOPIC define */
	pub_state = nh.advertise<if_node_msgs::powerState>("BMS/State/Power", 1);

	///////////////////////////////////////////////////////
	// setting For MODBUS_RTU
	///////////////////////////////////////////////////////
	int use_backend;
	int rc;
	modbus_parmeter modbus_parm;
	/* argument */
	if(argc>1) // use arg
	{
		use_backend = RTU;			//RTU mode
		NB_REPORT_SLAVE_ID = atoi(argv[6]);	//국번 1

		modbus_parm.device = argv[1];
		modbus_parm.baud = atoi(argv[2]);
		modbus_parm.parity = argv[3][0];
		modbus_parm.data_bit = atoi(argv[4]);
		modbus_parm.stop_bit = atoi(argv[5]);
	}
	else // don't use arg (default setting)
	{
		use_backend = RTU;			//RTU mode
		NB_REPORT_SLAVE_ID = 1;//국번 1

		modbus_parm.device = "/dev/ttyUSB2";
		modbus_parm.baud = 119200;
		modbus_parm.parity = 'N';
		modbus_parm.data_bit = 8;
		modbus_parm.stop_bit = 1;
	}
	printf("------------------Set MODBUS------------------\n");
	printf("  PORT: %s,  BAUD_RATE: %d \n", modbus_parm.device.c_str(),modbus_parm. baud);
	printf("  PARITY_BIT: %c\n  DATA_BIT: %d\n  STOP_BIT: %d \n", modbus_parm.parity, modbus_parm.data_bit, modbus_parm.stop_bit);
	printf("  SALVE_ID: %d\n", NB_REPORT_SLAVE_ID);
	printf("----------------------------------------------\n");

	/* Create MODBUS socket */
	rc = modbus_RTU_connect(use_backend, modbus_parm.device.c_str(), modbus_parm.baud, modbus_parm.parity, modbus_parm.data_bit, modbus_parm.stop_bit);
	if(rc == -1)
		return rc;

	/* modbus config */
	set_Config();

	// 응답 대기시간 설정
	response_timeout.tv_sec = 0;
	response_timeout.tv_usec = 500 * 1000;
	modbus_set_response_timeout(ctx, &response_timeout);

	/* Connect to Slave(Server) */
	rc = modbus_connect(ctx);
	if (rc == -1) // Connection 실패시 종료.
	{
		fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
		modbus_free(ctx);
		return -1;
	}
	else
	{
		std::cout << "response_time: " <<response_timeout.tv_sec << "sec " << response_timeout.tv_usec << "usec" << std::endl;
		usleep(0.05*1000000);
	}

	///////////////////////////////////////////////////////
	// ROS LOOP
	///////////////////////////////////////////////////////
	ros::Time::now();
	printf("START LOOP\n");
	ros::Rate rate(0.5);

	while(ros::ok())
	{
		readPowerState();				// read address :101

		usleep(0.05*1000000);
		ros::spinOnce();
//		rate.sleep();
	}
	return 0;

	close:
	// 응답시간 초기화 후 종료.
	modbus_set_response_timeout(ctx, &old_response_timeout);
	return -1;
}

