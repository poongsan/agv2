
//   * header file    *//
#include "unit-modbus-rtu.h"
// path :  ${project}/include/modbus/unit-modbus-rtu.h
// lib : libmodbus [ need to install --> $ sudo apt-get install libmodbus-dev
//					&& sudo apt-get install libmodbus5

 /* for ROS */
#include <ros/ros.h>
#include <ros/time.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <if_node_msgs/coil.h>
#include <if_node_msgs/lampState.h>
#include <if_node_msgs/loadState.h>
#include <if_node_msgs/plcStat.h>
//////////////////////////


//*   Global Variable   *//
 /* for read byte */
uint16_t byte_Out[8] = {0, };

uint16_t byte_In[8] = {0, };

#define FORK_CMD 0xC00
#define LAMP_CMD 0xC01
#define RESET_CMD 0xc03
#define FORK_STOP 0Xc06

///////////////////////////////////////////////////////
// RTU read & write Example
///////////////////////////////////////////////////////
/* write single Byte */
// int rc = modbus_write_register(ctx, int16_t addr, int16_t Byte);
// ex] rc = modbus_write_register(ctx, 0xC00, 0x0F);	//@ D영역 00번 주소에  0x0F 입력. 
//     usleep(0.05*1000000);

// read single or Multi Bytes
// int read_byte(int addr, int nb, uint16_t *dst);
// ex] rc = read_byte(0xC00, 1, byte_In);		//@ D영역 00번 주소에서 1byte 값을 dst에 저장. 
//     printf("%d\n", byte_Out[0]);			//@ print value 
//     usleep(0.05*1000000);
///////////////////////////////////////////////////////

ros::Publisher pub_Work;
ros::Publisher pub_Complete;
ros::Publisher pub_IR;
ros::Publisher pub_COIL;
ros::Publisher pub_Error;
ros::Publisher pub_EMG;
ros::Publisher pub_Slide;

ros::Subscriber sub_forkstp;




int fork_stop = 0;
int coil_top = 0;
int coil_bot = 0;
int control_mode = 1;
int emergency_signal = 0;

uint16_t old_ir = 0;


void cmdCallback(const std_msgs::Int8::ConstPtr& msg)
{
	// 1 : 로딩,  2 : 언로딩
	int rc;
        std_msgs::Int8 error_msgs;

        if(control_mode == 0 || emergency_signal == 1)
        {
            error_msgs.data = 2;
            pub_Error.publish(error_msgs);
            return;
        }
	switch(msg->data)
	{
		case 0: byte_Out[0] = 0;
                        error_msgs.data = 0;
                        break;

		case 1: // Top OUT
                        if(coil_top == 0 )
                        {
                            error_msgs.data = 1;
                        }
                        else
                        {
                            error_msgs.data = 0;
                            byte_Out[0] = 1;
                        }
                        break;

		case 2: // Bot OUT
                        if(coil_bot == 0)
                        {
                            error_msgs.data = 1;
                        }
                        else
                        {
                            error_msgs.data = 0;
                            byte_Out[0] = 4;
                        }
                        break;

		case 3: // Top IN
                        if(coil_top == 1)
                        {
                            error_msgs.data = 1;
                        }
                        else
                        {
                            error_msgs.data = 0;
                            byte_Out[0] = 16;
                        }
                        break;

		case 4: // Bot IN
                        if(coil_bot == 1)
                        {
                            error_msgs.data = 1;
                        }
                        else
                        {
                            error_msgs.data = 0;
                            byte_Out[0] = 64;
                        }
                        break;
	}

        pub_Error.publish(error_msgs);

        if(error_msgs.data == 1)
            return;

        rc = modbus_write_register(ctx, FORK_CMD, byte_Out[0]);		//@ D영역 00번 주소에  Loading/Unloading command 입력. 

	if(rc==-1)
	{// write CMD 실패
		printf("CMD fail\n");
	}
	else
	{// write CMD 성공
		printf("CMD write\n");
	}
	usleep(0.05*1000000);
}

//void alarmCallback(const std_msgs::Int8MultiArray::ConstPtr& msg)
void alarmCallback(const if_node_msgs::lampState::ConstPtr& msg)
{
	// RGBS, 0000
	int rc;

    int R = msg->red;
    int Y = msg->yellow;
    int G = msg->green;
    int S = msg->sound;

    byte_Out[1] = 0;
    switch(R)
    {
	case 0: byte_Out[1] += (0 << 0); break;
        case 1: byte_Out[1] += (1 << 0); break;
        case 2: byte_Out[1] += (2 << 0); break;
    }

    switch(Y)
    {
        case 0: byte_Out[1] += (0 << 2); break;
        case 1: byte_Out[1] += (1 << 2); break;
        case 2: byte_Out[1] += (2 << 2); break;
    }

    switch(G)
    {
        case 0: byte_Out[1] += (0 << 4); break;
        case 1: byte_Out[1] += (1 << 4); break;
        case 2: byte_Out[1] += (2 << 4); break;
    }

    switch(S)
    {
        case 0: byte_Out[1] += (0 << 6); break;
        case 1: byte_Out[1] += (1 << 6); break;
    }

    rc = modbus_write_register(ctx, LAMP_CMD, byte_Out[1]);

	if(rc==-1)
	{// write CMD 실패
		printf("CMD fail\n");
	}
	else
	{// write CMD 성공
		printf("CMD write\n");
	}
	usleep(0.05*1000000);
}

void forkStopCallback(const std_msgs::UInt8 &msg)
{
    int rc;
    printf("FORK STOP/RELEASE %d\n", msg.data);
    byte_Out[6] = msg.data;
    rc =  modbus_write_register(ctx, FORK_STOP, byte_Out[6]);

    if(rc==-1)
    {// write CMD 실패
        printf("CMD fail\n");
    }
    else
    {// write CMD 성공
        printf("CMD write\n");
    }

    usleep(0.05*1000000);
}


void restoreEMG(int reset_bit)
{
    int rc;
    printf("reset func %d\n", reset_bit);
    byte_Out[3] = reset_bit;

    rc = modbus_write_register(ctx, RESET_CMD, byte_Out[3]);

    if(rc==-1)
    {// write CMD 실패
        printf("CMD fail\n");
    }
    else
    {// write CMD 성공
        printf("CMD write\n");
    }
    usleep(0.05*1000000);
}

void readState()
{
    int rc;
    rc = read_byte(0xC64, 8, byte_In);		//@ D영역 00번 주소에서 1byte 값을 dst에 저장.

    if_node_msgs::loadState state_msgs;
    if_node_msgs::loadState complete_msgs;
    std_msgs::Int8 IR_msgs;
    if_node_msgs::coil coil_msgs;
    std_msgs::Int8 emg_msgs;
    std_msgs::Int8 slide_msgs;

    //0x64 byte_In[0]
    printf("0x64: %d\n", byte_In[0]);
    switch(byte_In[0])
    {
        case 0: slide_msgs.data = 0; break;
        case 1: slide_msgs.data = 0; break; // back_pos
        case 2: slide_msgs.data = 1; break; // front_pos
        case 4: slide_msgs.data = 0; break; // origin
        case 5: slide_msgs.data = 0; break;
        case 6: slide_msgs.data = 1; break; // front_pos
        case 7: slide_msgs.data = 1; break; // front_pos
    }

    //0x65 byte_In[1]
    printf("0x65: %d\n", byte_In[1]);
    switch(byte_In[1])
    {
        case 0: break;
        case 1:  if(coil_top==1) state_msgs.top_Out = 1; break;
        case 4:  if(coil_bot==1) state_msgs.bot_Out = 1; break;
        case 16: if(coil_top==0) state_msgs.top_IN  = 1; break;
        case 64: if(coil_bot==0) state_msgs.bot_IN = 1;  break;
    }
    //0x66 byte_In[2]
    printf("0x66: %d\n", byte_In[2]);
    switch(byte_In[2])
    {
        case 0:   complete_msgs.coil = 0; break;
        case 1:   complete_msgs.coil = 0; complete_msgs.top_Out = 1; break;
        case 4:   complete_msgs.coil = 0; complete_msgs.bot_Out = 1; break;
        case 16:  complete_msgs.coil = 0; complete_msgs.top_IN  = 1; break;
        case 64:  complete_msgs.coil = 0; complete_msgs.bot_IN  = 1; break;
	case 256: complete_msgs.coil = 1; break;
	case 257: complete_msgs.coil = 1; complete_msgs.top_Out = 1; break;
	case 260: complete_msgs.coil = 1; complete_msgs.bot_Out = 1; break;
	case 272: complete_msgs.coil = 1; complete_msgs.top_IN  = 1; break;
	case 320: complete_msgs.coil = 1; complete_msgs.bot_IN  = 1; break;
    }
    //0x67 byte_In[3]
    printf("0x67: %d\n", byte_In[3]);
int error_signal;
    switch(byte_In[3])
    {
        case 0:	std::cout<<"MANUL"<<std::endl;
                control_mode = 0;
                emergency_signal = 0;
                error_signal = 0;
                break;
        case 1: std::cout<<"MANUAL-EMERGENCY"<<std::endl;
                control_mode = 0;
                emergency_signal = 1;
                error_signal = 0;
                break;
        case 2: std::cout<<"AUTO"<<std::endl;
                control_mode = 1;
                emergency_signal = 0;
                error_signal = 0;
                break;
        case 3: std::cout<<"AUTO-EMERGENCY"<<std::endl;
                control_mode = 1;
                emergency_signal = 1;
                error_signal = 0;
                break;
        case 4: std::cout<<"MANUL-NONERROR"<<std::endl;
                control_mode = 0;
                emergency_signal = 0;
                error_signal = 1;
                break;
        case 5: std::cout<<"AUTO-NONERROR"<<std::endl;
                control_mode = 0;
                emergency_signal = 1;
                error_signal = 1;
                break;
        case 6:
                control_mode = 1;
                emergency_signal = 0;
                error_signal = 1;
                break;
        case 7:
                control_mode = 1;
                emergency_signal = 1;
                error_signal = 1;
                break;
    }
    emg_msgs.data = emergency_signal;

    //0x68 byte_In[4]
    printf("0x68: %d\n", byte_In[4]);
    switch(byte_In[4])
    {
        case 0: std::cout<<"not-detection"<<std::endl;
                coil_top = coil_msgs.top=0;
                coil_bot = coil_msgs.bot=0;
                break;

        case 1: std::cout<<"top-detection"<<std::endl;
                coil_top = coil_msgs.top=1;
                coil_bot = coil_msgs.bot=0;
                break;

        case 2: std::cout<<"bot-detection"<<std::endl;
                coil_top = coil_msgs.top=0;
                coil_bot = coil_msgs.bot=1;
                break;

        case 3: std::cout<<"full-detection"<<std::endl;
                coil_top = coil_msgs.top=1;
                coil_bot = coil_msgs.bot=1;
                break;
    }
    //0x69 byte_In[5]
    printf("0x69: %d\n", byte_In[5]);
    //0x6A byte_In[6] IR senosr
    printf("0x6A: %d\n", byte_In[6]);
    IR_msgs.data = byte_In[6];
    //0x6B byte_In[7]

    if(byte_In[6]==0 && error_signal == 1)
    {
        restoreEMG(1);
    }
    else if(byte_In[6]==1 && error_signal == 0)
    {
	ROS_INFO("RESET");
        restoreEMG(0);
    }

    old_ir = byte_In[6];


    pub_Work.publish(state_msgs);
    pub_Complete.publish(complete_msgs);
    pub_IR.publish(IR_msgs);
    pub_COIL.publish(coil_msgs);
    pub_EMG.publish(emg_msgs);
    pub_Slide.publish(slide_msgs);

    usleep(0.05*1000000);
}



int main(int argc, char *argv[])
{
///////////////////////////////////////////////////////
// setting For ROS
///////////////////////////////////////////////////////
    /* ROS INIT */
    ros::init(argc, argv, "plc_client");
    ros::NodeHandle nh;
    /* TOPIC declare */
    ros::Subscriber cmd_sub;
    ros::Subscriber light_sub;
    /* TOPIC define */
    pub_Work = nh.advertise<if_node_msgs::loadState>("PLC/Task/State", 1);
    pub_Complete = nh.advertise<if_node_msgs::loadState>("PLC/Task/Complete", 1);
    pub_IR = nh.advertise<std_msgs::Int8>("PLC/Sensor/IR", 1);
    pub_COIL = nh.advertise<if_node_msgs::coil>("PLC/Sensor/COIL", 1);
    pub_Error = nh.advertise<std_msgs::Int8>("PLC/Error/WrongCmd",1);
    pub_EMG = nh.advertise<std_msgs::Int8>("PLC/Error/Emergency",1);
    pub_Slide = nh.advertise<std_msgs::Int8>("PLC/Sensor/Slide",1);
    pub_PlcStatus = nh.advertise<if_node_msgs::plcStat>("PLC/Status", 1);

    cmd_sub = nh.subscribe("/PLC/Task/CMD", 1, cmdCallback);
    light_sub = nh.subscribe("/PLC/SignalTower/CMD", 1, alarmCallback);


	///////////////////////////////////////////////////////
	// setting For MODBUS_RTU
	///////////////////////////////////////////////////////
	int use_backend;
	int rc;
	modbus_parmeter modbus_parm;
	/* argument */
	if(argc>1) // use arg
	{
		use_backend = RTU;			//RTU mode
		NB_REPORT_SLAVE_ID = atoi(argv[6]);	//국번 1

		modbus_parm.device = argv[1];
		modbus_parm.baud = atoi(argv[2]);
		modbus_parm.parity = argv[3][0];
		modbus_parm.data_bit = atoi(argv[4]);
		modbus_parm.stop_bit = atoi(argv[5]);
	}
	else // don't use arg (default setting)
	{
		use_backend = RTU;			//RTU mode
		NB_REPORT_SLAVE_ID = 0;//국번 1

		modbus_parm.device = "/dev/ttyUSB1";
		modbus_parm.baud = 115200;//9600;
		modbus_parm.parity = 'N';
		modbus_parm.data_bit = 8;
		modbus_parm.stop_bit = 1;
	}
	printf("------------------Set MODBUS------------------\n");
	printf("  PORT: %s,  BAUD_RATE: %d \n", modbus_parm.device.c_str(),modbus_parm. baud);
	printf("  PARITY_BIT: %c\n  DATA_BIT: %d\n  STOP_BIT: %d \n", modbus_parm.parity, modbus_parm.data_bit, modbus_parm.stop_bit);
	printf("  SALVE_ID: %d\n", NB_REPORT_SLAVE_ID);
	printf("----------------------------------------------\n");


	/* Create MODBUS socket */
	rc = modbus_RTU_connect(use_backend, modbus_parm.device.c_str(), modbus_parm.baud, modbus_parm.parity, modbus_parm.data_bit, modbus_parm.stop_bit);
	if(rc == -1)
		return rc;

	/* modbus config */
	set_Config();

	// 응답 대기시간 설정
	response_timeout.tv_sec = 0;
	response_timeout.tv_usec = 100 * 1000;
	modbus_set_response_timeout(ctx, &response_timeout);

	/* Connect to Slave(Server) */
	rc = modbus_connect(ctx);
	if (rc == -1) // Connection 실패시 종료.
	{
		fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
		modbus_free(ctx);
		return -1;
	}
	else
	{
		std::cout << "response_time: " <<response_timeout.tv_sec << "sec " << response_timeout.tv_usec << "usec" << std::endl;
		usleep(0.05*1000000);
	}

	///////////////////////////////////////////////////////
	// ROS LOOP
	///////////////////////////////////////////////////////
	ros::Time::now();
	printf("START LOOP\n");

//	ros::Rate rate(0.5);
 //   startTime_ = millis();

	while(ros::ok())
	{
        readState();

		usleep(0.05*1000000);
		ros::spinOnce();
//		rate.sleep();
	}

	return 0;

	close:
	// 응답시간 초기화 후 종료.
	modbus_set_response_timeout(ctx, &old_response_timeout);
	return -1;
}

