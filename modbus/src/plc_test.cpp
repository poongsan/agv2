
//   * header file    *//
#include "unit-modbus-rtu.h"
// path :  ${project}/include/modbus/unit-modbus-rtu.h
// lib : libmodbus [ need to install --> $ sudo apt-get install libmodbus-dev
//					&& sudo apt-get install libmodbus5

 /* for ROS */
#include <ros/ros.h>
#include <ros/time.h>

#include <std_msgs/Int8.h>

//////////////////////////

//*   Global Variable   *//
 /* for read byte */
uint16_t byte_Out[8] = {0, };
uint16_t byte_In[8] = {0, };

#define LAMP_CMD 0xC00
///////////////////////////////////////////////////////
// RTU read & write Example
///////////////////////////////////////////////////////
/* write single Byte */
// int rc = modbus_write_register(ctx, int16_t addr, int16_t Byte);
// ex] rc = modbus_write_register(ctx, 0xC00, 0x0F);	//@ D영역 00번 주소에  0x0F 입력. 
//     usleep(0.05*1000000);

// read single or Multi Bytes
// int read_byte(int addr, int nb, uint16_t *dst);
// ex] rc = read_byte(0xC00, 1, byte_In);		//@ D영역 00번 주소에서 1byte 값을 dst에 저장. 
//     printf("%d\n", byte_Out[0]);			//@ print value 
//     usleep(0.05*1000000);
///////////////////////////////////////////////////////

ros::Publisher pub_EMG;

int emergency_signal = 0;



//void alarmCallback(const std_msgs::Int8MultiArray::ConstPtr& msg)
void alarm_write(int R, int Y, int G, int S)
{
	// RGBS, 0000
	int rc;

    byte_Out[1] = 0;
    switch(R)
    {
	case 0: byte_Out[1] += (0 << 0); break;
        case 1: byte_Out[1] += (1 << 0); break;
        case 2: byte_Out[1] += (2 << 0); break;
    }

    switch(Y)
    {
        case 0: byte_Out[1] += (0 << 2); break;
        case 1: byte_Out[1] += (1 << 2); break;
        case 2: byte_Out[1] += (2 << 2); break;
    }

    switch(G)
    {
        case 0: byte_Out[1] += (0 << 4); break;
        case 1: byte_Out[1] += (1 << 4); break;
        case 2: byte_Out[1] += (2 << 4); break;
    }

    switch(S)
    {
        case 0: byte_Out[1] += (0 << 6); break;
        case 1: byte_Out[1] += (1 << 6); break;
    }

    rc = modbus_write_register(ctx, LAMP_CMD, byte_Out[1]);

	if(rc==-1)
	{// write CMD 실패
		printf("CMD fail\n");
	}
	else
	{// write CMD 성공
		printf("CMD write\n");
	}
	usleep(0.05*1000000);
}


void readState()
{
    int rc;
    rc = read_byte(0xC64, 1, byte_In);		//@ D영역 00번 주소에서 1byte 값을 dst에 저장.

    std_msgs::Int8 emg_msgs;


    switch(byte_In[0])
    {
        case 0:	std::cout<<"NO EMERGENCY"<<std::endl;
                emergency_signal = 0;
                break;
        case 1: std::cout<<"EMERGENCY"<<std::endl;
                emergency_signal = 1;
                break;
        case 2: std::cout<<"EMERGENCY"<<std::endl;
                emergency_signal = 0;
                break;
        case 3: std::cout<<"EMERGENCY"<<std::endl;
                emergency_signal = 1;
                break;
        case 4: std::cout<<"EMERGENCY"<<std::endl;
                emergency_signal = 0;
                break;
        case 5: std::cout<<"EMERGENCY"<<std::endl;
                emergency_signal = 1;
                break;
        case 6: std::cout<<"EMERGENCY"<<std::endl;
                emergency_signal = 0;
                break;
        case 7: std::cout<<"EMERGENCY"<<std::endl;
                emergency_signal = 1;
                break;
    }
    emg_msgs.data = emergency_signal;
    pub_EMG.publish(emg_msgs);
    if(emergency_signal == 1)
        alarm_write(2,0,0,0);
    else
        alarm_write(1,0,0,0);

    usleep(0.05*1000000);
}

int main(int argc, char *argv[])
{
///////////////////////////////////////////////////////
// setting For ROS
///////////////////////////////////////////////////////
    /* ROS INIT */
    ros::init(argc, argv, "plc_client");
    ros::NodeHandle nh;

    /* TOPIC define */
    pub_EMG = nh.advertise<std_msgs::Int8>("PLC/Error/Emergency",1);


	///////////////////////////////////////////////////////
	// setting For MODBUS_RTU
	///////////////////////////////////////////////////////
	int use_backend;
	int rc;
	modbus_parmeter modbus_parm;
	/* argument */
	if(argc>1) // use arg
	{
		use_backend = RTU;			//RTU mode
		NB_REPORT_SLAVE_ID = atoi(argv[6]);	//국번 1

		modbus_parm.device = argv[1];
		modbus_parm.baud = atoi(argv[2]);
		modbus_parm.parity = argv[3][0];
		modbus_parm.data_bit = atoi(argv[4]);
		modbus_parm.stop_bit = atoi(argv[5]);
	}
	else // don't use arg (default setting)
	{
		use_backend = RTU;			//RTU mode
		NB_REPORT_SLAVE_ID = 0;//국번 1

		modbus_parm.device = "/dev/ttyUSB1";
		modbus_parm.baud = 115200;//9600;
		modbus_parm.parity = 'N';
		modbus_parm.data_bit = 8;
		modbus_parm.stop_bit = 1;
	}
	printf("------------------Set MODBUS------------------\n");
	printf("  PORT: %s,  BAUD_RATE: %d \n", modbus_parm.device.c_str(),modbus_parm. baud);
	printf("  PARITY_BIT: %c\n  DATA_BIT: %d\n  STOP_BIT: %d \n", modbus_parm.parity, modbus_parm.data_bit, modbus_parm.stop_bit);
	printf("  SALVE_ID: %d\n", NB_REPORT_SLAVE_ID);
	printf("----------------------------------------------\n");


	/* Create MODBUS socket */
	rc = modbus_RTU_connect(use_backend, modbus_parm.device.c_str(), modbus_parm.baud, modbus_parm.parity, modbus_parm.data_bit, modbus_parm.stop_bit);
	if(rc == -1)
		return rc;

	/* modbus config */
	set_Config();

	// 응답 대기시간 설정
	response_timeout.tv_sec = 0;
	response_timeout.tv_usec = 100 * 1000;
	modbus_set_response_timeout(ctx, &response_timeout);

	/* Connect to Slave(Server) */
	rc = modbus_connect(ctx);
	if (rc == -1) // Connection 실패시 종료.
	{
		fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
		modbus_free(ctx);
		return -1;
	}
	else
	{
		std::cout << "response_time: " <<response_timeout.tv_sec << "sec " << response_timeout.tv_usec << "usec" << std::endl;
		usleep(0.05*1000000);
	}

	///////////////////////////////////////////////////////
	// ROS LOOP
	///////////////////////////////////////////////////////
	ros::Time::now();
	printf("START LOOP\n");

//	ros::Rate rate(0.5);

	while(ros::ok())
	{

        readState();

		usleep(0.05*1000000);
		ros::spinOnce();
//		rate.sleep();
	}

	return 0;

	close:
	// 응답시간 초기화 후 종료.
	modbus_set_response_timeout(ctx, &old_response_timeout);
	return -1;
}

