#include <unit-modbus-rtu.h>

int modbus_RTU_connect(int use_backend, const char *device, int baud, char parity,
                         int data_bit, int stop_bit)
{
	if(use_backend == RTU)
	{
		ctx = modbus_new_rtu(device, baud, parity, data_bit, stop_bit);
	}
	else if(use_backend == TCP)
	{
	//	ctx = modbus_new_tcp("127.0.0.1", 502);
	}
	else if (use_backend == TCP_PI)
	{
	//	ctx = modbus_new_tcp_pi("::1", "1502");
	}

	if (ctx == NULL) {
		fprintf(stderr, "Unable to allocate libmodbus context\n");
		return -1;
	}
	else
    {
		return 1;
    }
}

int set_Config()
{
	modbus_set_slave(ctx, NB_REPORT_SLAVE_ID); // 국번 세팅
	modbus_set_debug(ctx, true);
	modbus_set_error_recovery(ctx, (modbus_error_recovery_mode)(MODBUS_ERROR_RECOVERY_LINK |MODBUS_ERROR_RECOVERY_PROTOCOL));
	modbus_rtu_set_serial_mode(ctx, MODBUS_RTU_RS232); // MODBUS_RTU_RS232 = 0: 232통신, MODBUS_RTU_RS485 = 1: 485통신
	modbus_get_response_timeout(ctx, &old_response_timeout);  // 기존 응답 대기시간 설정값 저장
	usleep(0.05*1000000);
	if(ctx == NULL)
		return -1;
	return 1;
}

int send_rawdDta(uint8_t* raw_req, int size)
{
	int rc;

	rc = modbus_send_raw_request(ctx, raw_req, size * sizeof(uint8_t));
	return rc;
}

int write_bit(int addr, int status)
{
	int rc;
	rc = modbus_write_bit(ctx, addr, status);
	std::cout<<"write"<<rc<<std::endl;    
	//    ASSERT_TRUE( rc == 1, "%s\n", modbus_strerror(errno));
	return rc;
}

int write_bits(int addr, int nb, uint8_t *src )
{
	int rc;
	rc = modbus_write_bits(ctx, addr, nb, src);
	return rc;
}

int write_byte(int addr, int nb, uint16_t *array_uint16_t )
{
	int rc;

	rc = modbus_write_registers(ctx, addr, nb, array_uint16_t);
	return rc;
}

int read_bit(int addr, int nb, uint8_t *dst)
{
	int rc;

	rc = modbus_read_bits(ctx, 0,2, dst);
	//    ASSERT_TRUE( rc == 1, "%s\n", modbus_strerror(errno));
	return rc;
}

int read_byte(int addr, int nb, uint16_t *dst)
{
	int rc;

	rc = modbus_read_registers(ctx, addr, nb, dst);
	//    ASSERT_TRUE( rc == 1, "%s\n", modbus_strerror(errno));
	return rc;
}
