#ifndef UNIT_MODBUS_RTU_H
#define UNIT_MODBUS_RTU_H

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

/* for ROS */
#include <ros/ros.h>
#include <ros/time.h>
/* for libmodbus */
#include <modbus.h>
#include <modbus-rtu.h>
#include <modbus-version.h>

#include <sys/socket.h>

// #include "test.h"  // #include "../include/modbus/test.h"

/*
#define MODBUS_FC_READ_COILS                0x01
#define MODBUS_FC_READ_DISCRETE_INPUTS      0x02
#define MODBUS_FC_READ_HOLDING_REGISTERS    0x03
#define MODBUS_FC_READ_INPUT_REGISTERS      0x04
#define MODBUS_FC_WRITE_SINGLE_COIL         0x05
#define MODBUS_FC_WRITE_SINGLE_REGISTER     0x06
#define MODBUS_FC_READ_EXCEPTION_STATUS     0x07
#define MODBUS_FC_WRITE_MULTIPLE_COILS      0x0F
#define MODBUS_FC_WRITE_MULTIPLE_REGISTERS  0x10
#define MODBUS_FC_REPORT_SLAVE_ID           0x11
#define MODBUS_FC_MASK_WRITE_REGISTER       0x16
#define MODBUS_FC_WRITE_AND_READ_REGISTERS 0x17

#define MODBUS_MAX_READ_REGISTERS          125
#define MODBUS_MAX_WRITE_REGISTERS         123
#define MODBUS_MAX_WR_WRITE_REGISTERS      121
#define MODBUS_MAX_WR_READ_REGISTERS       125
*/

/* Mecro */
#define BUG_REPORT(_cond, _format, _args ...) \
printf("\nLine %d: assertion error for '%s': " _format "\n", __LINE__, # _cond, ## _args)

#define ASSERT_TRUE(_cond, _format, __args...) {  \
    if (_cond) {                                  \
        printf("OK\n");                           \
    } else {                                      \
        BUG_REPORT(_cond, _format, ## __args);    \
        goto close;                               \
    }                                             \
};

/* Global Variable Declaration */
modbus_t *ctx;

enum {
	TCP,
	TCP_PI,
	RTU
};

typedef struct {
	std::string device;
	int baud;
	char parity;
	int data_bit;
	int stop_bit;
} modbus_parmeter;

struct timeval old_response_timeout;
struct timeval response_timeout;
int NB_REPORT_SLAVE_ID;

/* Function Declaration */
//setting For RTU & try connect to Slave
int modbus_RTU_connect(int use_backend, const char *device, int baud, char parity,
                         int data_bit, int stop_bit);
// set config [국번, Baud, party...]
int set_Config();
// send raw data
int send_rawdDta(uint8_t* raw_req, int size);
// write a bit
int write_bit(int addr, int status);
// write bits (or bit array)
int write_bits(int addr, int nb, uint8_t *src );
// write multi biytes (or byte array)
int write_byte(int addr, int nb, uint16_t *array_uint16_t );
// read a bit
int read_bit(int addr, int nb, uint8_t *dst);
// read a byte
int read_byte(int addr, int nb, uint16_t *dst);

/* Function Definition */
int modbus_RTU_connect(int use_backend, const char *device, int baud, char parity,
                         int data_bit, int stop_bit)
{
	if(use_backend == RTU)
	{
		ctx = modbus_new_rtu(device, baud, parity, data_bit, stop_bit);
	}
	else if(use_backend == TCP)
	{
	//	ctx = modbus_new_tcp("127.0.0.1", 502);
	}
	else if (use_backend == TCP_PI)
	{
	//	ctx = modbus_new_tcp_pi("::1", "1502");
	}

	if (ctx == NULL) {
		fprintf(stderr, "Unable to allocate libmodbus context\n");
		return -1;
	}
	else
    {
		return 1;
    }
}

int set_Config()
{
	modbus_set_slave(ctx, NB_REPORT_SLAVE_ID); // 국번 세팅
	modbus_set_debug(ctx, true);
	modbus_set_error_recovery(ctx, (modbus_error_recovery_mode)(MODBUS_ERROR_RECOVERY_LINK |MODBUS_ERROR_RECOVERY_PROTOCOL));
	modbus_rtu_set_serial_mode(ctx, MODBUS_RTU_RS232); // MODBUS_RTU_RS232 = 0: 232통신, MODBUS_RTU_RS485 = 1: 485통신
	modbus_get_response_timeout(ctx, &old_response_timeout);  // 기존 응답 대기시간 설정값 저장
	usleep(0.05*1000000);
	if(ctx == NULL)
		return -1;
	return 1;
}

int send_rawdDta(uint8_t* raw_req, int size)
{
	int rc;

	rc = modbus_send_raw_request(ctx, raw_req, size * sizeof(uint8_t));
	return rc;
}

int write_bit(int addr, int status)
{
	int rc;
	rc = modbus_write_bit(ctx, addr, status);
	std::cout<<"write"<<rc<<std::endl;    
	//    ASSERT_TRUE( rc == 1, "%s\n", modbus_strerror(errno));
	return rc;
}

int write_bits(int addr, int nb, uint8_t *src )
{
	int rc;
	rc = modbus_write_bits(ctx, addr, nb, src);
	return rc;
}

int write_byte(int addr, int nb, uint16_t *array_uint16_t )
{
	int rc;

	rc = modbus_write_registers(ctx, addr, nb, array_uint16_t);
	return rc;
}

int read_bit(int addr, int nb, uint8_t *dst)
{
	int rc;

	rc = modbus_read_bits(ctx, 0,2, dst);
	//    ASSERT_TRUE( rc == 1, "%s\n", modbus_strerror(errno));
	return rc;
}

int read_byte(int addr, int nb, uint16_t *dst)
{
	int rc;

	rc = modbus_read_registers(ctx, addr, nb, dst);
	//    ASSERT_TRUE( rc == 1, "%s\n", modbus_strerror(errno));
	return rc;
}

#endif // UNIT_MODBUS_RTU_H
