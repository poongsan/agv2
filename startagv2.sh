#!/bin/bash

## Remove Pcan
cd ~/Downloads/peak_driver11
sudo make uninstall

sleep 15s

## Install Pcan
sudo make
sudo make install
sudo modprobe pcan

sleep 15s

## ROS setup - path & work-space
source /opt/ros/melodic/setup.bash
source ~/catkin_ws/devel/setup.bash


## ROS network
export ROS_HOSTNAME=192.168.2.102 #101
export ROS_MASTER_URI=http://192.168.2.102:11311

#sleep 60s
sleep 5s

#/opt/ros/kinetic/bin/roscore &
#sleep 1s

/opt/ros/melodic/bin/roslaunch sis_agv test_pongsan.launch 


