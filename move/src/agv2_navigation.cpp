// AGV 2 movement
// 4-arm 1-2: Home position -> wp1-2 (rotate +-90)  -> wp3-4 -> arm_move -> QR
// move charge pos: Home position  -> wp1 -> charge_pos

#include "ros/time.h"
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <if_node_msgs/cmdMove.h>
#include <if_node_msgs/waypoint.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <nav_msgs/Odometry.h>
#include <obstacle_detector/obstacles_detect.h>
#include <pgv/vision_msg.h>
#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/UInt8.h>
#include <tf/LinearMath/Transform.h>
#include <tf/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>
#include <vector>

#include "md2k_can_driver_msgs/motor_status.h"
#include "move_control/teleop.h"

#define QR_VEL 0.019
#define QR_AVEL 0.009
#define CART_VEL 0.016
#define QR_NUM 5

using namespace std;

namespace agv_wp
{

tf2_ros::Buffer tfBuffer;

enum start_pos
{
  start_null = 0,
  from_home,
  from_arm12,
  from_arm34,
  from_arm5,
  from_arm6,
  from_arm7,
  from_charger,
  from_A,
  from_B,
  from_C,
  from_D,
  from_home1,
  from_cart
};

enum stop_pos
{
  stop_null = 0,
  to_home,
  to_arm12,
  to_arm34,
  to_arm5,
  to_arm6,
  to_arm7,
  to_charger,
  to_A,
  to_B,
  to_C,
  to_D,
  to_home1,
  to_cart
};

enum Move_Point
{
  no_point_ = 0,
  home_,
  home1_,
  cart_,
  arm12_,
  charger_,
  charger1_,
  charger2_,
  arm34_,
  arm5_,
  arm6_,
  arm7_,
  A_,
  B_,
  C_,
  D_,
  wp1_,
  wp2_,
  wp3_,
  wp4_,
  wp5_,
  wp51_,
  wp6_,
  wp7_,
  wp8_,
  wp9_,
  wp10_,
  wp11_,
  wp12_,
  wp13_,
  wp14_,
  wp15_,
  wp16_,
  rot1_,
  rot2_,
  rot3_,
  rot4_,
  rot5_,
  rot6_,
  rack_,
  h_reset_
};

enum MODE_AGV
{
  mode_stop_ = 0,
  mode_start_ = 1,
  mode_obs_ = 2,
  motor_er_ = 3
};
enum MODE_MOVE
{
  MOVE_ZERO_ = 0,
  MOVE_LRF_ = 1,
  MOVE_QR_ = 2,
};

enum LRF_MOVE
{
  LRF_NONE_ = 0,
  LRF_XY_ = 1,
  LRF_YAW_ = 2,
  LRF_DIAG_ = 3
};

enum AGV_STATUS
{
  AGV_STP = 0,
  WP_REACH = 2,
  GOAL_REACH = 3,
  QR_COMP = 5,
  LRF_RUN = 1,
  QR_CTRL = 4,
  OBS = 7,
  MANUAL = 8,
  LRF_XY_COMP = 6,
  MOTOR_ER_ = 9,
  LRF_DIAG_COMP = 10
};

start_pos start_;
stop_pos stop_;
Move_Point move_point_;
MODE_AGV mode_agv_;
MODE_MOVE moveMode_;
LRF_MOVE lrfMove_;
AGV_STATUS agvStatus_;

class NavigationManagement
{
public:
  NavigationManagement();
  ~NavigationManagement();
  void init();
  void spin();
  void update();
  void param_set();

private:
  void charge_move();
  void charge1_move();
  void charge2_move();
  void rack_move();
  void qr_move();

  void RTH();
  void wp1_move();
  void wp2_move();
  void wp3_move();
  void wp4_move();
  void wp5_move();
  void wp51_move();
  void wp6_move();
  void wp7_move();
  void wp8_move();
  void wp9_move();
  void wp10_move();

  void wp11_move();
  void wp12_move();
  void wp13_move();
  void wp14_move();

  void wp15_move();
  void wp16_move();
  void cart_move();
  void home1_move();

  void rot1_move();
  void rot2_move();
  void rot3_move();
  void rot4_move();
  void rot5_move();

  void arm12_move();
  void arm34_move();
  void arm5_move();
  void arm6_move();
  void arm7_move();

  void A_move();
  void C_move();
  void B_move();
  void D_move();

  void stop_agv();

  void home1_cb(const std_msgs::UInt8::ConstPtr& home_sig);

  void nav_control();
  void XY_control();
  void YAW_control();
  void DIAG_control();
  double lrf_tracking_ctrl(double Kp, double dx, double vmax, double vmin);
  float sign(float in);

  bool obstacle_;
  bool home_flag;

  int rate;
  int move_state; // move : 1, stop : 0
  int init_flag;

  bool tag_, control_;
  int tag_id, line_, line_id, control_id, fault, warning, command;

  float target_pos_x, target_pos_y, target_A;
  float goal_x, goal_y, goal_A;

  double delX_TOL = 0.03, delY_TOL = 0.03, delTH_TOL = 0.7;
  double delX_ = 0, delY_ = 0, delTH_ = 0, lenX_ = 0, lenY_ = 0, angTH_ = 0;
  double delTH_LSTP = 10, delTH_STA = 10;
  double LVX_MAX = 0.55, LVY_MAX = 0.3, LVTH_MAX = 0.1;

  float current_x, current_y, current_A;

  float line_x, line_y, line_theta;
  float tf_fb_x, tf_fb_y, tf_fb_ori_z, tf_fb_ori_w;

  float cur_x, cur_y, cur_A;

  bool manual_mode;

  int8_t count;

  int init_cnt_ = 0;

  float old_x = 0, old_y = 0, old_A = 0;

  std_msgs::UInt8 state_goal;
  geometry_msgs::Twist vel_;
  geometry_msgs::Pose2D cur_goal_, cur_pose;

  ros::NodeHandle nh;

  vector<float> init_pose_, init_ori;
  vector<float> charge_pose;

  vector<float> wp1_pose, wp2_pose, wp3_pose, wp4_pose, wp5_pose, wp51_pose,
      wp6_pose, wp7_pose, wp8_pose, wp9_pose, wp10_pose, wp11_pose, wp12_pose,
      wp13_pose, wp14_pose, wp15_pose, wp16_pose, home1_pose, cart_pose;

  ros::Publisher vel_pub, goal_pub, reach_pub, waypoint_pub, pos_pub;
  ros::Subscriber reach_sub, goal_sub, init_sub, home1_sub, pgv_sub,
      move_base_feedback_sub, obstacle_sub, joy_sub, motor_status_sub;

  void
  goal_reach_cb(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach);
  void goal_target_cb(const if_node_msgs::cmdMove& goal);
  void init_cb(const std_msgs::UInt8::ConstPtr& init_);
  void pgv_cb(const pgv::vision_msg& pgv_msg);
  void obstacle_cb(const std_msgs::UInt8::ConstPtr& msg);
  void wp_pub();
  void pub_state();
  void joystickControlCallback(const move_control::teleop::ConstPtr& motorCmd_);
  void status_cb(const md2k_can_driver_msgs::motor_status::ConstPtr& msg);
};

NavigationManagement::NavigationManagement()
{

  init();
  param_set();

  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1, true);
  goal_pub = nh.advertise<geometry_msgs::Pose2D>("/current_goal", 1, true);
  pos_pub = nh.advertise<geometry_msgs::Pose2D>("/agv_pos", 1, true);
  waypoint_pub = nh.advertise<if_node_msgs::waypoint>("/agv_wp", 1, true);
  reach_pub = nh.advertise<std_msgs::UInt8>("/IFNode/goal_reach", 1, true);

  goal_sub = nh.subscribe("/MOVE/CMD", 10,
                          &NavigationManagement::goal_target_cb, this);
  init_sub =
      nh.subscribe("/init_pose", 1, &NavigationManagement::init_cb, this);
  home1_sub =
      nh.subscribe("/home_pose", 1, &NavigationManagement::home1_cb, this);
  pgv_sub = nh.subscribe("/pgv_data", 10, &NavigationManagement::pgv_cb, this);
  obstacle_sub = nh.subscribe("/obstacle_info", 1,
                              &NavigationManagement::obstacle_cb, this);

  motor_status_sub =
      nh.subscribe("/motor_status", 1, &NavigationManagement::status_cb, this);
  joy_sub = nh.subscribe("cmd_vel_joy", 1,
                         &NavigationManagement::joystickControlCallback, this);
}

NavigationManagement::~NavigationManagement() {}

void NavigationManagement::init()
{
  rate = 100; // 10; //100
  target_pos_x = 0.0;
  target_pos_y = 0.0;
  target_A = 0.0;

  current_x = 0;
  current_y = 0;
  current_A = 0;

  cur_x = 0;
  cur_y = 0;
  cur_A = 0;

  goal_x = 0;
  goal_y = 0;
  goal_A = 0;

  home_flag = 0;

  moveMode_ = MOVE_ZERO_;
  agvStatus_ = AGV_STP;
  lrfMove_ = LRF_NONE_;

  start_ = start_null;
  stop_ = stop_null;
  move_point_ = no_point_;

  init_flag = 0;

  mode_agv_ = mode_stop_;

  obstacle_ = false;
  manual_mode = false;
  move_state = 0;

  count = 0;
}

void NavigationManagement::wp_pub()
{
  if_node_msgs::waypoint wp_desc;
  switch (start_)
  {
  case from_home:
    wp_desc.start_point = "home";
    break;
  case from_arm12:
    wp_desc.start_point = "arm12";
    break;
  case from_arm34:
    wp_desc.start_point = "arm34";
    break;
  case from_arm5:
    wp_desc.start_point = "arm5";
    break;
  case from_arm6:
    wp_desc.start_point = "arm6";
    break;
  case from_arm7:
    wp_desc.start_point = "arm7";
    break;
  case from_charger:
    wp_desc.start_point = "charger";
    break;
  case from_A:
    wp_desc.start_point = "A";
    break;
  case from_B:
    wp_desc.start_point = "B";
    break;
  case from_C:
    wp_desc.start_point = "C";
    break;
  case from_D:
    wp_desc.start_point = "D";
    break;
  case from_home1:
    wp_desc.start_point = "home1";
    break;
  case from_cart:
    wp_desc.start_point = "cart";
    break;
  case start_null:
    wp_desc.start_point = "no_point";
    break;
  default:
    break;
  }
  switch (stop_)
  {
  case to_home:
    wp_desc.stop_point = "home";
    break;
  case to_arm12:
    wp_desc.stop_point = "arm12";
    break;
  case to_arm34:
    wp_desc.stop_point = "arm34";
    break;
  case to_arm5:
    wp_desc.stop_point = "arm5";
    break;
  case to_arm6:
    wp_desc.stop_point = "arm6";
    break;
  case to_arm7:
    wp_desc.stop_point = "arm7";
    break;
  case to_charger:
    wp_desc.stop_point = "charger";
    break;
  case to_A:
    wp_desc.stop_point = "A";
    break;
  case to_B:
    wp_desc.stop_point = "B";
    break;
  case to_C:
    wp_desc.stop_point = "C";
    break;
  case to_D:
    wp_desc.stop_point = "D";
    break;
  case to_home1:
    wp_desc.stop_point = "home1";
    break;
  case to_cart:
    wp_desc.stop_point = "cart";
    break;
  case stop_null:
    wp_desc.stop_point = "no_point";
    break;
  default:
    break;
  }
  waypoint_pub.publish(wp_desc);
}

void NavigationManagement::status_cb(
    const md2k_can_driver_msgs::motor_status::ConstPtr& msg)
{
  uint8_t m_1 = msg->front_left_bit_Alarm;
  uint8_t m_2 = msg->front_right_bit_Alarm;
  uint8_t m_3 = msg->rear_left_bit_Alarm;
  uint8_t m_4 = msg->rear_right_bit_Alarm;
  if ((m_1 || m_2 || m_3 || m_4) != 0)
  {
    if (mode_agv_ == mode_start_)
      mode_agv_ = motor_er_;
  }
  else
  {
    if (mode_agv_ == motor_er_)
      mode_agv_ = mode_start_;
  }
}

void NavigationManagement::update()
{

  geometry_msgs::TransformStamped transformStamped;
  try
  {
    transformStamped =
        tfBuffer.lookupTransform("map", "base_link", ros::Time(0));

    current_x = transformStamped.transform.translation.x;
    current_y = transformStamped.transform.translation.y;
    tf::Quaternion q(transformStamped.transform.rotation.x,
                     transformStamped.transform.rotation.y,
                     transformStamped.transform.rotation.z,
                     transformStamped.transform.rotation.w);
    double roll, pitch, yaw;
    tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
    current_A = yaw * 180 / M_PI;
  }
  catch (tf2::TransformException& ex)
  {
    ROS_WARN("%s", ex.what());
    ros::Duration(0.3).sleep();
    //		continue;
  }

  cur_pose.x = current_x;
  cur_pose.y = current_y;
  cur_pose.theta = current_A * M_PI / 180;
  pos_pub.publish(cur_pose);

  if (manual_mode)
  {
    move_point_ = no_point_;
    mode_agv_ = mode_stop_;
    count = 0;
    stop_agv();
    agvStatus_ = MANUAL;
  }
  else
  {
    switch (mode_agv_)
    {
    case mode_obs_:
    {
      count = 0;
      stop_agv();
      agvStatus_ = OBS;
    }
    break;
    case mode_start_:
    {

      // Add 210608

      if (moveMode_ == MOVE_LRF_)
      {

        float delPosX = current_x - old_x;
        float delPosY = current_y - old_y;
        float delA = current_A - old_A;

        if ((lrfMove_ == LRF_XY_ || lrfMove_ == LRF_DIAG_) &&
            (fabs(delPosX) > 0.5 || fabs(delPosY) > 0.5))
          mode_agv_ = mode_stop_;
        else if (lrfMove_ == LRF_YAW_ && fabs(delA) > 10)
          mode_agv_ = mode_stop_;

        if (delPosX == 0 && delPosY == 0 && delA == 0)
        {
          init_cnt_++;
          if (init_cnt_ >= 200)
            mode_agv_ = mode_stop_;
        }
        else
          init_cnt_ = 0;
      }
      else
        init_cnt_ = 0;

      old_x = current_x;
      old_y = current_y;
      old_A = current_A;

      if (move_point_ == wp10_)
      {
        LVTH_MAX = 0.05;
      }
      else if (move_point_ == wp6_ && stop_ == to_arm12)
      {
        LVTH_MAX = 0.05;
      }
      else
        LVTH_MAX = 0.1;

      if ((start_ != start_null) && (stop_ != stop_null) &&
          (move_point_ != no_point_))
      {
        switch (move_point_)
        {
        case home_:
          RTH();
          break;
        case charger_:
          charge_move();
          break;
        case charger1_:
          charge1_move();
          break;
        case charger2_:
          charge2_move();
          break;
        case home1_:
          home1_move();
          break;
        case cart_:
          cart_move();
          break;
        case wp1_:
          wp1_move();
          break;
        case wp2_:
          wp2_move();
          break;
        case wp3_:
          wp3_move();
          break;
        case wp4_:
          wp4_move();
          break;
        case wp5_:
          wp5_move();
          break;
        case wp51_:
          wp51_move();
          break;
        case wp6_:
          wp6_move();
          break;
        case wp7_:
          wp7_move();
          break;
        case wp8_:
          wp8_move();
          break;
        case wp9_:
          wp9_move();
          break;
        case wp10_:
          wp10_move();
          break;
        case wp11_:
          wp11_move();
          break;
        case wp12_:
          wp12_move();
          break;
        case wp13_:
          wp13_move();
          break;
        case wp14_:
          wp14_move();
          break;
        case wp15_:
          wp15_move();
          break;
        case wp16_:
          wp16_move();
          break;
        case rot1_:
          rot1_move();
          break;
        case rot2_:
          rot2_move();
          break;
        case rot3_:
          rot3_move();
          break;
        case rot4_:
          rot4_move();
          break;
        case rot5_:
          rot5_move();
          break;
        case arm12_:
          arm12_move();
          break;
        case arm34_:
          arm34_move();
          break;
        case arm5_:
          arm5_move();
          break;
        case arm6_:
          arm6_move();
          break;
        case arm7_:
          arm7_move();
          break;
        case A_:
          A_move();
          break;
        case B_:
          B_move();
          break;
        case C_:
          C_move();
          break;
        case D_:
          D_move();
          break;
        case rack_:
          rack_move();
          break;
        case no_point_:
          stop_agv();
          break;
        default:
          break;
        }
      }
      else
      {
        move_point_ = no_point_;
        start_ = start_null;
        stop_ = stop_null;
      }
    }
    break;
    case mode_stop_:
    {
      move_point_ = no_point_;
      start_ = start_null;
      stop_ = stop_null;
      count = 0;
      if (agvStatus_ != GOAL_REACH)
        agvStatus_ = AGV_STP;
      stop_agv();
      old_x = current_x;
      old_y = current_y;
      old_A = current_A;
    }
    break;
    case motor_er_:
    {
      count = 0;
      stop_agv();
      agvStatus_ = MOTOR_ER_;
    }
    break;
    default:
      break;
    }
  }
  pub_state();
}

void NavigationManagement::spin()
{
  ros::Rate loop_rate(rate);
  tf2_ros::TransformListener tfListener(tfBuffer);

  while (ros::ok())
  {
    update();
    ros::spinOnce();
    loop_rate.sleep();
  }
}

void NavigationManagement::pub_state()
{
  switch (agvStatus_)
  {
  case AGV_STP:
    state_goal.data = 0;
    break;
  case MANUAL:
    state_goal.data = 8;
    break;
  case LRF_RUN:
    state_goal.data = 1;
    break;
  case WP_REACH:
    state_goal.data = 2;
    break;
  case GOAL_REACH:
    state_goal.data = 3;
    break;
  case QR_COMP:
    state_goal.data = 5;
    break;
  case LRF_XY_COMP:
    state_goal.data = 6;
    break;
  case OBS:
    state_goal.data = 7;
    break;
  case MOTOR_ER_:
    state_goal.data = 9;
    break;
  case QR_CTRL:
    state_goal.data = 4;
    break;
  default:
    state_goal.data = 0;
    break;
  }
  reach_pub.publish(state_goal);

  /*
  AGV_STP = 0,
  WP_REACH = 2,
  GOAL_REACH = 3,
  QR_COMP = 5,
  LRF_RUN = 1,
  QR_CTRL = 4,
  OBS = 7,
  MANUAL = 8,
  LRF_XY_COMP = 6,
  MOTOR_ER_ = 9
    */
}

float NavigationManagement::sign(float in)
{
  if (in > 0)
    return 1;
  else if (in < 0)
    return -1;
  else
    return 0;
}

void NavigationManagement::goal_target_cb(const if_node_msgs::cmdMove& goal)
{
  ROS_INFO("Goal received ...");

  target_pos_x = goal.pose.position.x;
  target_pos_y = goal.pose.position.y;

  tf::Quaternion q(goal.pose.orientation.x, goal.pose.orientation.y,
                   goal.pose.orientation.z, goal.pose.orientation.w);
  tf::Matrix3x3 m(q);

  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);
  target_A = yaw * 180 / M_PI;

  start_ = start_null;
  stop_ = stop_null;
  count = 0;

  move_state = goal.move;

  if (move_state == 1)
    mode_agv_ = mode_start_;
  else
    mode_agv_ = mode_stop_;

  if ((fabs(current_x - charge_pose[0]) < 1) &&
      (fabs(current_y - charge_pose[1]) < 1) &&
      (fabs(current_A - charge_pose[2]) < 5))
  {
    start_ = from_charger;
  }
  else if ((fabs(current_x - cart_pose[0]) < 0.5) &&
           (fabs(current_y - cart_pose[1]) < 0.5) &&
           (fabs(current_A - cart_pose[2]) < 5))
  {
    start_ = from_cart;
  }
  else if ((current_y >= home1_pose[1] - 6) &&
           (current_x <= home1_pose[0] + 0.4) &&
           (current_x >= home1_pose[0] - 0.4))
  {
    // from home1 area
    start_ = from_home1;
  }
  else if ((current_y >= init_pose_[1] - 5) && (current_x >= wp3_pose[0] + 7))
  // from arm 12
  {
    start_ = from_arm12;
  }
  else if ((current_y >= init_pose_[1] - 5) && (current_x < wp3_pose[0] + 7) &&
           (current_x > init_pose_[0] - 2))
  {
    // from home area
    start_ = from_home;
  }
  else if ((current_y < wp4_pose[1] - 1) && (current_y >= (wp5_pose[1] + 3)) &&
           (current_x > (wp5_pose[0] - 1)) && (current_x < (wp5_pose[0] + 10)))
  // from arm 34
  {
    start_ = from_arm34;
  }
  else if (current_y < (wp5_pose[1] + 3) && (current_x > (wp5_pose[0] - 1)) &&
           (current_x < (wp9_pose[0] - 5)))
  {
    // from arm5
    start_ = from_arm5;
  }

  else if ((current_x >= (wp9_pose[0] - 5)) && (current_x < (wp11_pose[0])))
  {
    // from arm6
    start_ = from_arm6;
  }
  else if (current_x >= (wp11_pose[0]))
  {
    // from arm7
    start_ = from_arm7;
  }
  // Add Dinh 200615
  else if ((current_y > -1) && current_y < 6 &&
           (current_x < (wp5_pose[0] - 1)) && (current_x > home1_pose[0] + 0.4))
  {
    // from C
    start_ = from_C;
  }
  else if ((current_y <= -1) && (current_x < (wp5_pose[0] - 1)) &&
           (current_x > home1_pose[0] + 0.4))
  {
    // from D
    start_ = from_D;
  }
  else if ((current_y > 8) && (current_y < 12.2) &&
           (current_x < (init_pose_[0] - 2)) &&
           (current_x > home1_pose[0] + 0.4))
  {
    // from B
    start_ = from_B;
  }
  else if ((current_y >= 12.2) && (current_x < (init_pose_[0] - 2)) &&
           (current_x > home1_pose[0] + 0.4))
  {
    // from A
    start_ = from_A;
  }
  else
  {
    // from undefined
    start_ = start_null;
    move_point_ = no_point_;
  }

  if ((fabs(target_pos_x - init_pose_[0]) < 0.2) &&
      (fabs(target_pos_y - init_pose_[1]) < 0.2) &&
      (fabs(target_A - init_pose_[2]) < 5))
  // to home
  {

    stop_ = to_home;
    switch (start_)
    {
    case from_charger:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;
      move_point_ = charger2_;
    }
    break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm12_;
    }
    break;
    case from_home:
      move_point_ = home_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm34_;
    }
    break;
    case from_arm5:
    {
      if (current_x > wp5_pose[0] + 5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm5_;
      }
      else
        move_point_ = wp5_;
    }
    break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm6_;
    }
    break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm7_;
    }
    break;
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = A_;
    }
    break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = B_;
    }
    break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = C_;
    }
    break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = D_;
    }
    break;
    case from_cart:
    {
      move_point_ = home1_;
    }
    break;
    case from_home1:
    {
      move_point_ = wp15_;
    }
    break;
    case start_null:
      move_point_ = no_point_;
      break;
    default:
      break;
    }
  }
  else if ((fabs(target_pos_x - charge_pose[0]) < 1) &&
           (fabs(target_pos_y - charge_pose[1]) < 1) &&
           (fabs(target_A - charge_pose[2]) < 5))
  // to charger
  {
    stop_ = to_charger;
    switch (start_)
    {
    case from_charger:
      move_point_ = charger_;
      break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm12_;
    }
    break;
    case from_home:
      move_point_ = wp1_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm34_;
    }
    break;
    case from_arm5:
    {
      if (current_x > wp5_pose[0] + 5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm5_;
      }
      else
        move_point_ = wp5_;
    }
    break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm6_;
    }
    break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm7_;
    }
    break;
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = A_;
    }
    break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = B_;
    }
    break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = C_;
    }
    break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = D_;
    }
    break;
    case from_cart:
    {
      move_point_ = home1_;
    }
    break;
    case from_home1:
    {
      move_point_ = wp15_;
    }
    break;
    case start_null:
      move_point_ = no_point_;
      break;
    default:
      break;
    }
  }
  else if ((target_pos_x > wp3_pose[0] + 7) &&
           (target_pos_y > init_pose_[1] - 5))
  // to arm 12
  {
    stop_ = to_arm12;
    switch (start_)
    {
    case from_charger:
      move_point_ = home_;
      break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm12_;
    }
    break;
    case from_home:
    {
      if (target_pos_y > init_pose_[1])
        move_point_ = wp1_;
      else
        move_point_ = wp2_;
    }
    break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm34_;
    }
    break;
    case from_arm5:
    {
      if (current_x > wp5_pose[0] + 5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm5_;
      }
      else
        move_point_ = wp5_;
    }
    break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm6_;
    }
    break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm7_;
    }
    break;
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = A_;
    }
    break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = B_;
    }
    break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = C_;
    }
    break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = D_;
    }
    break;
    case from_cart:
    {
      move_point_ = home1_;
    }
    break;
    case from_home1:
    {
      move_point_ = wp15_;
    }
    break;
    case start_null:
      move_point_ = no_point_;
      break;
    default:
      break;
    }
  }
  else if ((target_pos_y <= wp4_pose[1]) &&
           (target_pos_y > (wp5_pose[1] + 3)) &&
           (target_pos_x > (wp5_pose[0] - 1)) &&
           (target_pos_x < (wp5_pose[0] + 8)))
  // to arm 34
  {
    stop_ = to_arm34;
    switch (start_)
    {
    case from_charger:
      move_point_ = rot2_;
      break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm12_;
    }
    break;
    case from_home:
      move_point_ = wp3_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm34_;
    }
    break;
    case from_arm5:
    {
      if (current_x > wp5_pose[0] + 6)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm5_;
      }
      else
        move_point_ = wp5_;
    }
    break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm6_;
    }
    break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm7_;
    }
    break;
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = A_;
    }
    break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = B_;
    }
    break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = C_;
    }
    break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = D_;
    }
    break;
    case from_cart:
    {
      move_point_ = home1_;
    }
    break;
    case from_home1:
    {
      move_point_ = wp15_;
    }
    break;
    case start_null:
      move_point_ = no_point_;
      break;
    default:
      break;
    }
  }

  else if (target_pos_x > (wp5_pose[0] + 8))
  // to arm 5 , 6, 7
  {

    if (target_pos_x < wp9_pose[0] - 5)
      stop_ = to_arm5;
    else if (target_pos_x < wp11_pose[0])
      stop_ = to_arm6;
    else
      stop_ = to_arm7;

    switch (start_)
    {
    case from_charger:
      move_point_ = rot2_;
      break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm12_;
    }
    break;
    case from_home:
      move_point_ = wp3_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm34_;
    }
    break;
    case from_arm5:
    {
      if (current_x > wp5_pose[0] + 5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm5_;
      }
      else
        move_point_ = wp5_;
    }
    break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm6_;
    }
    break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm7_;
    }
    break;
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = A_;
    }
    break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = B_;
    }
    break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = C_;
    }
    break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = D_;
    }
    break;
    case from_cart:
    {
      move_point_ = home1_;
    }
    break;
    case from_home1:
    {
      move_point_ = wp16_;
    }
    break;
    case start_null:
      move_point_ = no_point_;
      break;
    default:
      break;
    }
  }

  else if ((target_pos_y > 8) && (target_pos_y <= 12.6) &&
           (target_pos_x <= init_pose_[0] - 1) && fabs(target_A) > 170)
  // to B rack
  {
    stop_ = to_B;

    switch (start_)
    {
    case from_charger:
      move_point_ = wp6_;
      break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm12_;
    }
    break;
    case from_home:
      move_point_ = wp6_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm34_;
    }
    break;
    case from_arm5:
    {
      if (current_x > wp5_pose[0] + 5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm5_;
      }
      else
        move_point_ = wp5_;
    }
    break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm6_;
    }
    break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm7_;
    }
    break;
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = A_;
    }
    break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = B_;
    }
    break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = C_;
    }
    break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = D_;
    }
    break;
    case from_cart:
    {
      move_point_ = home1_;
    }
    break;
    case from_home1:
    {
      move_point_ = wp15_;
    }
    break;
    case start_null:
      move_point_ = no_point_;
      break;
    default:
      break;
    }
  }

  else if ((target_pos_y > 12.6) && (target_pos_x <= init_pose_[0] - 1) &&
           fabs(target_A) < 5)
  // to A rack
  {
    stop_ = to_A;

    switch (start_)
    {
    case from_charger:
      move_point_ = wp6_;
      break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm12_;
    }
    break;
    case from_home:
      move_point_ = wp6_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm34_;
    }
    break;
    case from_arm5:
    {
      if (current_x > wp5_pose[0] + 5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm5_;
      }
      else
        move_point_ = wp5_;
    }
    break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm6_;
    }
    break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm7_;
    }
    break;
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = A_;
    }
    break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = B_;
    }
    break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = C_;
    }
    break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = D_;
    }
    break;
    case from_cart:
    {
      move_point_ = home1_;
    }
    break;
    case from_home1:
    {
      move_point_ = wp15_;
    }
    break;
    case start_null:
      move_point_ = no_point_;
      break;
    default:
      break;
    }
  }

  else if ((target_pos_y < 6) && (target_pos_y >= -1) &&
           (target_pos_x < ((wp5_pose[0] - 1))) && fabs(target_A) < 10)
  // to C rack
  {
    stop_ = to_C;

    switch (start_)
    {
    case from_charger:
      move_point_ = rot2_;
      break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm12_;
    }
    break;
    case from_home:
      move_point_ = wp3_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm34_;
    }
    break;
    case from_arm5:
    {
      if (current_x > wp5_pose[0] + 5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm5_;
      }
      else
        move_point_ = wp5_;
    }
    break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm6_;
    }
    break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm7_;
    }
    break;
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = A_;
    }
    break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = B_;
    }
    break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = C_;
    }
    break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = D_;
    }
    break;
    case from_cart:
    {
      move_point_ = home1_;
    }
    break;
    case from_home1:
    {
      move_point_ = wp16_;
    }
    break;
    case start_null:
      move_point_ = no_point_;
      break;
    default:
      break;
    }
  }

  else if ((target_pos_y < -1) && (target_pos_x < ((wp5_pose[0] - 1))) &&
           fabs(target_A) > 170)
  // to D rack
  {
    stop_ = to_D;

    switch (start_)
    {
    case from_charger:
    {
      move_point_ = rot2_;
    }
    break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm12_;
    }
    break;
    case from_home:
      move_point_ = wp3_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm34_;
    }
    break;
    case from_arm5:
    {
      if (current_x > wp5_pose[0] + 5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm5_;
      }
      else
        move_point_ = wp5_;
    }
    break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm6_;
    }
    break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm7_;
    }
    break;
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = A_;
    }
    break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = B_;
    }
    break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = C_;
    }
    break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = D_;
    }
    break;
    case from_cart:
    {
      move_point_ = home1_;
    }
    break;
    case from_home1:
    {
      move_point_ = wp16_;
    }
    break;
    case start_null:
      move_point_ = no_point_;
      break;
    default:
      break;
    }
  }

  else if ((fabs(target_pos_x - cart_pose[0]) < 0.2) &&
           (fabs(target_pos_y - cart_pose[1]) < 0.2) &&
           (fabs(target_A - cart_pose[2]) < 5))
  // to cart
  {
    stop_ = to_cart;

    switch (start_)
    {
    case from_charger:
      move_point_ = rot2_;
      break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm12_;
    }
    break;
    case from_home:
      move_point_ = wp6_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm34_;
    }
    break;
    case from_arm5:
    {
      if (current_x > wp5_pose[0] + 5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm5_;
      }
      else
        move_point_ = wp5_;
    }
    break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm6_;
    }
    break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm7_;
    }
    break;
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = A_;
    }
    break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = B_;
    }
    break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = C_;
    }
    break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = D_;
    }
    break;
    case from_cart:
    {
      move_point_ = cart_;
    }
    break;
    case from_home1:
    {
      move_point_ = cart_;
    }
    break;
    case start_null:
      move_point_ = no_point_;
      break;
    default:
      break;
    }
  }

  else // target not in the list
  {
    start_ = start_null;
    stop_ = stop_null;
    move_point_ = no_point_;
  }
  wp_pub();
}

void NavigationManagement::home1_cb(const std_msgs::UInt8::ConstPtr& home_sig)
{
  home_flag = home_sig->data;
  target_pos_x = home1_pose[0];
  target_pos_y = home1_pose[1];

  target_A = home1_pose[2];

  start_ = start_null;
  count = 0;

  move_state = 1;

  if (move_state == 1)
    mode_agv_ = mode_start_;
  else
    mode_agv_ = mode_stop_;

  stop_ = to_home1;

  if ((fabs(current_x - charge_pose[0]) < 0.2) &&
      (fabs(current_y - charge_pose[1]) < 0.2) &&
      (fabs(current_A - charge_pose[2]) < 0.5))
  {
    start_ = from_charger;
  }
  else if ((fabs(current_x - cart_pose[0]) < 0.3) &&
           (fabs(current_y - cart_pose[1]) < 0.3) &&
           (fabs(current_A - cart_pose[2]) < 0.5))
  {
    start_ = from_cart;
  }
  else if ((current_y >= home1_pose[1] - 6) &&
           (current_x <= home1_pose[0] + 0.4) &&
           (current_x >= home1_pose[0] - 0.4))
  {
    // from home1 area
    start_ = from_home1;
  }
  else if ((current_y >= init_pose_[1] - 5) && (current_x >= wp3_pose[0] + 7))
  // from arm 12
  {
    start_ = from_arm12;
  }
  else if ((current_y >= init_pose_[1] - 5) && (current_x < wp3_pose[0] + 7) &&
           (current_x > init_pose_[0] - 2))
  {
    // from home area
    start_ = from_home;
  }
  else if ((current_y < init_pose_[1] - 5) && (current_y >= wp4_pose[1] - 1) &&
           (current_x < wp3_pose[0] + 7) && (current_x > init_pose_[0] - 2))
  {
    // from charger area
    start_ = from_charger;
  }
  else if ((current_y < wp4_pose[1] - 1) && (current_y >= (wp5_pose[1] + 3)) &&
           (current_x > (wp5_pose[0] - 1)) && (current_x < (wp5_pose[0] + 10)))
  // from arm 34
  {
    start_ = from_arm34;
  }
  else if (current_y < (wp5_pose[1] + 3) && (current_x > (wp5_pose[0] - 1)) &&
           (current_x < (wp9_pose[0] - 5)))
  {
    // from arm5
    start_ = from_arm5;
  }

  else if ((current_x >= (wp9_pose[0] - 5)) && (current_x < (wp11_pose[0])))
  {
    // from arm6
    start_ = from_arm6;
  }
  else if (current_x >= (wp11_pose[0]))
  {
    // from arm7
    start_ = from_arm7;
  }
  else if ((current_y > -1) && current_y < 6 &&
           (current_x < (wp5_pose[0] - 1)) && (current_x > home1_pose[0] + 0.4))
  {
    // from C
    start_ = from_C;
  }
  else if ((current_y <= -1) && (current_x < (wp5_pose[0] - 1)) &&
           (current_x > home1_pose[0] + 0.4))
  {
    // from D
    start_ = from_D;
  }
  else if ((current_y > 8) && (current_y < 12.2) &&
           (current_x < (init_pose_[0] - 2)) &&
           (current_x > home1_pose[0] + 0.4))
  {
    // from B
    start_ = from_B;
  }
  else if ((current_y >= 12.2) && (current_x < (init_pose_[0] - 2)) &&
           (current_x > home1_pose[0] + 0.4))
  {
    // from A
    start_ = from_A;
  }
  else
  {
    // from undefined
    start_ = start_null;
    move_point_ = no_point_;
  }

  switch (start_)
  {
  case from_charger:
    move_point_ = rot2_;
    break;
  case from_arm12:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = arm12_;
  }
  break;
  case from_home:
    move_point_ = wp6_;
    break;
  case from_arm34:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = arm34_;
  }
  break;
  case from_arm5:
  {
    if (current_x > wp5_pose[0] + 5)
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm5_;
    }
    else
      move_point_ = wp5_;
  }
  break;
  case from_arm6:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = arm6_;
  }
  break;
  case from_arm7:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = arm7_;
  }
  break;
  case from_A:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = A_;
  }
  break;
  case from_B:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = B_;
  }
  break;
  case from_C:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = C_;
  }
  break;
  case from_D:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = D_;
  }
  break;
  case from_cart:
    move_point_ = home1_;
    break;
  case from_home1:
    move_point_ = home1_;
    break;
  case start_null:
    move_point_ = no_point_;
    break;
  default:
    break;
  }
  wp_pub();
}

void NavigationManagement::init_cb(const std_msgs::UInt8::ConstPtr& init_)
{
  ROS_INFO("Init Position");
  init_flag = init_->data;
  //	ROS_INFO("init_flag : %d",init_flag);
}

void NavigationManagement::pgv_cb(const pgv::vision_msg& pgv_msg)
{
  control_id = pgv_msg.ControlID;
  control_ = pgv_msg.Control;
  tag_ = pgv_msg.Tag;
  tag_id = pgv_msg.TagID;
  line_ = pgv_msg.Line;
  line_id = pgv_msg.LineID;
  line_x = pgv_msg.X;
  line_y = pgv_msg.Y;
  line_theta = pgv_msg.theta;
  fault = pgv_msg.Fault;
  warning = pgv_msg.Warning;
  command = pgv_msg.command;
}

double NavigationManagement::lrf_tracking_ctrl(double Kp, double dx,
                                               double vmax, double vmin)
{
  double v_cal = Kp * dx;
  if (v_cal >= 0)
  {
    if (v_cal > vmax)
      v_cal = vmax;
    else if (v_cal < vmin)
      v_cal = vmin;
  }
  else
  {
    if (v_cal < -vmax)
      v_cal = -vmax;
    else if (v_cal > -vmin)
      v_cal = -vmin;
  }
  return v_cal;
}

void NavigationManagement::XY_control()
{
  delX_ = goal_x - current_x;
  delY_ = goal_y - current_y;
  lenX_ = fabs(goal_x - cur_x);
  lenY_ = fabs(goal_y - cur_y);
  vel_.angular.z = 0;

  if (fabs(delY_) <= delY_TOL && fabs(delX_) <= delX_TOL)
  {
    vel_.linear.x = 0;
    vel_.linear.y = 0;
    agvStatus_ = LRF_XY_COMP;
  }
  else
  {
    agvStatus_ = LRF_RUN;
    if (cur_A > -10 && cur_A < 10) // Around 0 deg
    {
      if (lenX_ > 2)
        vel_.linear.x = lrf_tracking_ctrl(0.5, delX_, LVX_MAX, 0.005);
      else
        vel_.linear.x = lrf_tracking_ctrl(0.5, delX_, 0.3, 0.005);

      if (lenY_ > 2)
        vel_.linear.y = lrf_tracking_ctrl(0.5, delY_, LVY_MAX, 0.005);
      else
        vel_.linear.y = lrf_tracking_ctrl(0.5, delY_, 0.25, 0.005);
    }
    else if (cur_A > 80 && cur_A < 100)
    {
      if (lenY_ > 2)
        vel_.linear.x = lrf_tracking_ctrl(0.5, delY_, LVX_MAX, 0.005);
      else
        vel_.linear.x = lrf_tracking_ctrl(0.5, delY_, 0.3, 0.005);

      if (lenX_ > 2)
        vel_.linear.y = -lrf_tracking_ctrl(0.5, delX_, LVY_MAX, 0.005);
      else
        vel_.linear.y = -lrf_tracking_ctrl(0.5, delX_, 0.25, 0.005);
    }
    else if (cur_A > 80 && cur_A < 100)
    {
      if (lenY_ > 2)
        vel_.linear.x = lrf_tracking_ctrl(0.5, delY_, LVX_MAX, 0.005);
      else
        vel_.linear.x = lrf_tracking_ctrl(0.5, delY_, 0.3, 0.005);

      if (lenX_ > 2)
        vel_.linear.y = -lrf_tracking_ctrl(0.5, delX_, LVY_MAX, 0.005);
      else
        vel_.linear.y = -lrf_tracking_ctrl(0.5, delX_, 0.25, 0.005);
    }
    else if ((cur_A > 170 && cur_A <= 180) || (cur_A < -170 && cur_A >= -180))
    {
      if (lenY_ > 2)
        vel_.linear.y = -lrf_tracking_ctrl(0.5, delY_, LVY_MAX, 0.005);
      else
        vel_.linear.y = -lrf_tracking_ctrl(0.5, delY_, 0.25, 0.005);

      if (lenX_ > 2)
        vel_.linear.x = -lrf_tracking_ctrl(0.5, delX_, LVX_MAX, 0.005);
      else
        vel_.linear.x = -lrf_tracking_ctrl(0.5, delX_, 0.3, 0.005);
    }
    else if (cur_A >= -100 && cur_A <= -80) // around -90
    {
      if (lenY_ > 2)
        vel_.linear.x = -lrf_tracking_ctrl(0.5, delY_, LVX_MAX, 0.005);
      else
        vel_.linear.x = -lrf_tracking_ctrl(0.5, delY_, 0.3, 0.005);
      if (lenX_ > 2)
        vel_.linear.y = lrf_tracking_ctrl(0.5, delX_, LVY_MAX, 0.005);
      else
        vel_.linear.y = lrf_tracking_ctrl(0.5, delX_, 0.25, 0.005);
    }
  }
  vel_pub.publish(vel_);
}

void NavigationManagement::DIAG_control()
{
  delX_ = goal_x - current_x;
  delY_ = goal_y - current_y;
  lenX_ = fabs(goal_x - cur_x);
  lenY_ = fabs(goal_y - cur_y);
  vel_.angular.z = 0;

  if (fabs(delY_) <= delY_TOL && fabs(delX_) <= delX_TOL)
  {
    vel_.linear.x = 0;
    vel_.linear.y = 0;
    agvStatus_ = LRF_DIAG_COMP;
  }
  else if (fabs(delY_) <= 1 && fabs(delX_) <= 1)
  {
    agvStatus_ = LRF_RUN;
    if (cur_A > -10 && cur_A < 10) // Around 0 deg
    {
      vel_.linear.x = lrf_tracking_ctrl(0.5, delX_, 0.3, 0.005);
      vel_.linear.y = lrf_tracking_ctrl(0.5, delY_, 0.25, 0.005);
    }
    else if (cur_A > 80 && cur_A < 100)
    {
      vel_.linear.x = lrf_tracking_ctrl(0.5, delY_, 0.3, 0.005);
      vel_.linear.y = -lrf_tracking_ctrl(0.5, delX_, 0.25, 0.005);
    }
    else if (cur_A > 80 && cur_A < 100)
    {
      vel_.linear.x = lrf_tracking_ctrl(0.5, delY_, 0.3, 0.005);
      vel_.linear.y = -lrf_tracking_ctrl(0.5, delX_, 0.25, 0.005);
    }
    else if ((cur_A > 170 && cur_A <= 180) || (cur_A < -170 && cur_A >= -180))
    {
      vel_.linear.y = -lrf_tracking_ctrl(0.5, delY_, 0.25, 0.005);
      vel_.linear.x = -lrf_tracking_ctrl(0.5, delX_, 0.3, 0.005);
    }
    else if (cur_A >= -100 && cur_A <= -80) // around -90
    {
      vel_.linear.x = -lrf_tracking_ctrl(0.5, delY_, 0.3, 0.005);
      vel_.linear.y = lrf_tracking_ctrl(0.5, delX_, 0.25, 0.005);
    }
  }
  else
  {
    agvStatus_ = LRF_RUN;
    vel_.linear.x = lrf_tracking_ctrl(0.5, delX_, LVX_MAX, 0.005);
    vel_.linear.y = fabs(vel_.linear.x) * lenY_ / lenX_ * sign(delY_);
  }
  vel_pub.publish(vel_);
}

void NavigationManagement::YAW_control()
{
  delTH_ = goal_A - current_A;
  if (delTH_ >= 180)
    delTH_ = delTH_ - 360;
  else if (delTH_ <= -180)
    delTH_ = delTH_ + 360;
  vel_.linear.x = 0;
  vel_.linear.y = 0;
  if (fabs(delTH_) <= delTH_TOL)
  {
    vel_.angular.z = 0;
    agvStatus_ = WP_REACH;
    lrfMove_ = LRF_NONE_;
  }
  else
  {
    vel_.angular.z = lrf_tracking_ctrl(0.01, delTH_, LVTH_MAX, 0.008);
  }
  vel_pub.publish(vel_);
}

void NavigationManagement::nav_control()
{
  switch (moveMode_)
  {
  case MOVE_LRF_:
  {
    switch (lrfMove_)
    {
    case LRF_XY_:
      XY_control();
      break;
    case LRF_YAW_:
      YAW_control();
      break;
    case LRF_DIAG_:
      DIAG_control();
      break;
    }
  }
  break;
  case MOVE_QR_:
  {
    qr_move();
  }
  break;

  case MOVE_ZERO_:
    stop_agv();
    break;
  }
}

void NavigationManagement::obstacle_cb(const std_msgs::UInt8::ConstPtr& msg)
{

  if (msg->data == 0)
  {
    if (mode_agv_ == mode_obs_)
      mode_agv_ = mode_start_;
  }
  else
  {
    if (mode_agv_ == mode_start_)
      mode_agv_ = mode_obs_;
  }
}

void NavigationManagement::RTH() // return to home
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_home)
    {
      goal_x = target_pos_x;
      goal_y = target_pos_y;
      goal_A = target_A;
    }
    else
    {
      goal_x = init_pose_[0];
      goal_y = init_pose_[1];
      goal_A = init_pose_[2];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    moveMode_ = MOVE_QR_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;

    if (stop_ == to_home)
    {
      home_flag = 0;
      moveMode_ = MOVE_ZERO_;
      mode_agv_ = mode_stop_;
      agvStatus_ = GOAL_REACH;
      move_point_ = no_point_;
    }
    else
    {
      if (stop_ == to_arm12)
      {
        if (target_pos_y > init_pose_[1])
        {
          move_point_ = wp1_;
        }
        else
        {
          move_point_ = wp2_;
        }
      }
      else if (stop_ == to_charger)
        move_point_ = wp1_;
      else if (stop_ == to_arm34)
        move_point_ = wp3_;
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp3_;
      else if (stop_ == to_A || stop_ == to_B)
        move_point_ = wp6_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp3_;
      else if (stop_ == to_home1)
        move_point_ = wp6_;
      else if (stop_ == to_cart)
        move_point_ = wp6_;
      else
        move_point_ = no_point_;
    }
  }
}

void NavigationManagement::home1_move() // move to ST11
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = home1_pose[0];
    goal_y = home1_pose[1];
    goal_A = home1_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    moveMode_ = MOVE_QR_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;

    if (stop_ == to_home1)
    {
      home_flag = 0;
      moveMode_ = MOVE_ZERO_;
      mode_agv_ = mode_stop_;
      agvStatus_ = GOAL_REACH;
      move_point_ = no_point_;
    }
    else
    {
      if (stop_ == to_cart)
        move_point_ = cart_;
      else if (stop_ == to_A || stop_ == to_B)
        move_point_ = wp15_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp16_;
      else if (stop_ == to_arm12 || stop_ == to_arm34 || stop_ == to_charger ||
               stop_ == to_home)
        move_point_ = wp15_;
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp16_;
      else
        move_point_ = no_point_;
    }
  }
}

void NavigationManagement::cart_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = target_pos_x;
    goal_y = target_pos_y;
    goal_A = target_A;

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    moveMode_ = MOVE_QR_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;
    moveMode_ = MOVE_ZERO_;
    mode_agv_ = mode_stop_;
    agvStatus_ = GOAL_REACH;
    move_point_ = no_point_;
  }
}

void NavigationManagement::wp1_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp1_pose[0];
    goal_y = wp1_pose[1];
    goal_A = wp1_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_arm12)
      {
        move_point_ = arm12_;
      }
      else if (stop_ == to_charger)
        move_point_ = charger1_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp2_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp2_pose[0];
    goal_y = wp2_pose[1];
    goal_A = wp2_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_arm12)
      {
        move_point_ = arm12_;
      }
      else
      {
        move_point_ = no_point_;
      }
    }
    count = 0;
  }
}

void NavigationManagement::wp3_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp3_pose[0];
    goal_y = wp3_pose[1];
    goal_A = wp3_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_arm12)
        move_point_ = rot1_;
      else if (stop_ == to_charger)
        move_point_ = wp4_;
      else if (stop_ == to_arm34)
        move_point_ = arm34_;
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp5_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp8_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp4_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp3_pose[0];
    goal_y = wp3_pose[1];
    goal_A = wp3_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = wp3_;
    }
    else
    {
      if (stop_ == to_charger)
        move_point_ = charger_;
      else if (stop_ == to_arm34)
        move_point_ = rot2_;
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = rot2_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = rot2_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp5_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp5_pose[0];
    goal_y = wp5_pose[1];
    goal_A = wp5_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = rot3_;
    }
    else
    {
      if (stop_ == to_charger)
        move_point_ = rot3_;
      else if (stop_ == to_arm12)
        move_point_ = rot3_;
      else if (stop_ == to_arm34)
        move_point_ = rot3_;
      else if (stop_ == to_arm5)
        move_point_ = arm5_;
      else if (stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp51_;
      else if (stop_ == to_A || stop_ == to_B)
        move_point_ = rot3_;
      else if (stop_ == to_C || stop_ == to_D || stop_ == to_home1 ||
               stop_ == to_cart)
        move_point_ = wp8_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp51_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp51_pose[0];
    goal_y = wp51_pose[1];
    goal_A = wp51_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_; // Modify 210125
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp9_;
      else
        move_point_ = wp5_;
    }
    else
      move_point_ = no_point_;
    count = 0;
  }
}

void NavigationManagement::wp6_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_A || stop_ == to_B)
    {
      goal_A = target_A;
    }
    else if (stop_ == to_home1 || stop_ == to_cart)
    {
      goal_A = 0;
    }
    else if (stop_ == to_arm12)
    {
      goal_A = target_A;
    }
    else
    {
      goal_A = init_pose_[2];
    }

    goal_x = wp6_pose[0];
    goal_y = wp6_pose[1];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_A)
        move_point_ = A_;
      else if (stop_ == to_B)
        move_point_ = B_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = home1_;
      else if (stop_ == to_arm12)
      {
        if (target_pos_y > init_pose_[1])
        {
          move_point_ = wp1_;
        }
        else
        {
          move_point_ = wp2_;
        }
      }
      else
        move_point_ = home_;
    }
    count = 0;
  }
}

void NavigationManagement::wp7_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp7_pose[0];
    goal_y = wp7_pose[1];
    goal_A = wp7_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_C || stop_ == to_D)
      {
        move_point_ = wp8_;
      }
      else if (stop_ == to_arm34)
      {
        move_point_ = arm34_;
      }
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      {
        move_point_ = wp5_;
      }
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = rot5_;
      else
      {
        move_point_ = no_point_;
      }
    }
    count = 0;
  }
}

void NavigationManagement::wp8_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_C || stop_ == to_D)
    {
      goal_A = target_A;
      if (stop_ == to_C)
      {
        goal_y = wp8_pose[1] + 1;
      }
      else
      {
        goal_y = wp8_pose[1] - 1;
      }
    }
    else if (stop_ == to_home1 || stop_ == to_cart)
    {
      goal_y = wp8_pose[1];
      goal_A = 0;
    }
    else
    {
      goal_y = wp8_pose[1];
      goal_A = wp3_pose[2];
    }

    goal_x = wp8_pose[0];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_C)
        move_point_ = C_;
      else if (stop_ == to_D)
        move_point_ = D_;
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp5_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = wp16_;
      else
      {
        move_point_ = no_point_;
      }
    }
    count = 0;
  }
}

void NavigationManagement::wp9_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp9_pose[0];
    goal_y = wp9_pose[1];
    goal_A = wp9_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm6 || stop_ == to_arm7)
      {
        move_point_ = wp10_;
      }
      else
      {
        move_point_ = wp51_;
      }
    }
    else
    {
      move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp10_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;
    if (stop_ == to_arm6)
    {
      goal_x = wp10_pose[0];
      goal_y = wp10_pose[1];
      goal_A = target_A;
    }
    else
    {
      goal_x = wp10_pose[0];
      goal_y = wp10_pose[1];
      goal_A = wp10_pose[2];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();
  if (start_ == from_arm6)
  {
    if (agvStatus_ == LRF_XY_COMP)
    {
      moveMode_ = MOVE_LRF_;
      lrfMove_ = LRF_YAW_;
    }
    else if (agvStatus_ == WP_REACH)
    {
      if (stop_ != stop_null)
      {
        if (stop_ == to_arm6)
        {
          move_point_ = arm6_;
        }
        else if (stop_ == to_arm7)
        {
          move_point_ = wp11_;
        }
        else
        {
          move_point_ = wp9_;
        }
      }
      else
      {
        move_point_ = no_point_;
      }
      count = 0;
    }
  }
  else
  {
    if (agvStatus_ == LRF_XY_COMP)
    {
      moveMode_ = MOVE_LRF_;
      lrfMove_ = LRF_YAW_;
    }
    else if (agvStatus_ == WP_REACH)
    {
      if (stop_ != stop_null)
      {
        if (stop_ == to_arm6)
        {
          move_point_ = arm6_;
        }
        else if (stop_ == to_arm7)
        {
          move_point_ = wp11_;
        }
        else
        {
          move_point_ = wp9_;
        }
      }
      else
      {
        move_point_ = no_point_;
      }
      count = 0;
    }
  }
}

void NavigationManagement::wp11_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp11_pose[0];
    goal_y = wp11_pose[1];
    goal_A = wp11_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;

    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();
  if (stop_ == to_arm7)
  {
    if (agvStatus_ == LRF_XY_COMP)
    {
      moveMode_ = MOVE_LRF_;
      lrfMove_ = LRF_YAW_;
    }
    else if (agvStatus_ == WP_REACH)
    {
      move_point_ = wp12_;
      count = 0;
    }
  }
  else
  {
    if (agvStatus_ == LRF_XY_COMP)
    {
      moveMode_ = MOVE_LRF_;
      lrfMove_ = LRF_YAW_;
    }
    else if (agvStatus_ == WP_REACH)
    {
      move_point_ = wp10_;
      count = 0;
    }
  }
}

void NavigationManagement::wp12_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp12_pose[0];
    goal_y = wp12_pose[1];
    goal_A = wp12_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;

    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();
  if (start_ == from_arm7)
  {
    if (agvStatus_ == LRF_XY_COMP)
    {
      moveMode_ = MOVE_LRF_;
      lrfMove_ = LRF_YAW_;
    }
    else if (agvStatus_ == WP_REACH)
    {
      if (stop_ != stop_null)
      {
        move_point_ = wp11_;
      }
      else
      {
        move_point_ = no_point_;
      }
      count = 0;
    }
  }
  else
  {
    if (agvStatus_ == LRF_XY_COMP)
    {
      moveMode_ = MOVE_LRF_;
      lrfMove_ = LRF_YAW_;
    }
    else if (agvStatus_ == WP_REACH)
    {
      if (stop_ != stop_null)
      {
        move_point_ = wp13_;
      }
      else
      {
        move_point_ = no_point_;
      }
      count = 0;
    }
  }
}

void NavigationManagement::wp13_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp13_pose[0];
    goal_y = wp13_pose[1];
    goal_A = wp13_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm7)
      {
        move_point_ = wp14_;
      }
      else
      {
        move_point_ = wp12_;
      }
    }
    else
    {
      move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp14_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp14_pose[0];
    goal_y = wp14_pose[1];
    goal_A = wp14_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm7)
      {
        move_point_ = arm7_;
      }
      else
      {
        move_point_ = wp13_;
      }
    }
    else
    {
      move_point_ = no_point_;
    }
    count = 0;
  }
}
void NavigationManagement::wp15_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp15_pose[0];
    goal_y = wp15_pose[1];
    if (stop_ == to_A || stop_ == to_B)
    {
      goal_A = target_A;
    }
    else
    {
      goal_A = wp15_pose[2];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_A)
        move_point_ = A_;
      else if (stop_ == to_B)
        move_point_ = B_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = home1_;
      else if (stop_ == to_arm34)
        move_point_ = wp7_;
      else
        move_point_ = wp6_;
    }
    else
    {
      move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp16_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp16_pose[0];
    goal_y = wp16_pose[1];
    if (stop_ == to_C || stop_ == to_D)
    {
      goal_A = target_A;
    }
    else
    {
      goal_A = wp16_pose[2];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_C)
        move_point_ = C_;
      else if (stop_ == to_D)
        move_point_ = D_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = home1_;
      else if (stop_ == to_A || stop_ == to_B)
        move_point_ = home1_;
      else
        move_point_ = rot4_;
    }
    else
    {
      move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::rot1_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp3_pose[0];
    goal_y = wp3_pose[1];
    if (target_pos_y > init_pose_[1])
    {
      goal_A = wp1_pose[2];
    }
    else
    {
      goal_A = wp2_pose[2];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_arm12)
        move_point_ = arm12_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::rot2_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp4_pose[0];
    goal_y = wp4_pose[1];
    goal_A = wp3_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_arm34)
      {
        move_point_ = arm34_;
      }
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      {
        move_point_ = wp5_;
      }
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp8_;
      else if (stop_ == to_A || stop_ == to_B || stop_ == to_home1 ||
               stop_ == to_cart)
        move_point_ = rot5_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::rot3_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp5_pose[0];
    goal_y = wp5_pose[1];
    goal_A = wp3_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = wp3_;
    }
    else
    {
      if (stop_ == to_arm34)
      {
        move_point_ = arm34_;
      }
      else if (stop_ == to_arm12)
      {
        move_point_ = rot1_;
      }
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp8_;
      else if (stop_ == to_A || stop_ == to_B)
        move_point_ = rot5_;
      else if (stop_ = to_charger)
        move_point_ = wp4_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = wp8_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::rot4_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp8_pose[0];
    if (start_ = from_C)
      goal_y = wp8_pose[1] + 1;
    else if (start_ = from_D)
      goal_y = wp8_pose[1] - 1;
    else
      goal_y = wp8_pose[1];
    goal_A = wp3_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = wp3_;
    }
    else
    {
      if (stop_ == to_home)
      {
        move_point_ = wp3_;
      }
      else if (stop_ == to_arm34)
      {
        move_point_ = arm34_;
      }
      else if (stop_ == to_arm12)
      {
        move_point_ = rot1_;
      }
      else if (stop_ == to_charger)
      {
        move_point_ = wp4_;
      }
      else if (stop_ == to_A || stop_ == to_B)
      {
        move_point_ = rot5_;
      }
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = wp8_;
      else
      {
        move_point_ = no_point_;
      }
    }
    count = 0;
  }
}

void NavigationManagement::rot5_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp7_pose[0];
    goal_y = wp7_pose[1];
    goal_A = target_A;

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_A)
    {
      move_point_ = A_;
    }
    else if (stop_ == to_B)
      move_point_ = B_;
    else if (stop_ == to_home1 || stop_ == to_cart)
      move_point_ = home1_;
    else
    {
      move_point_ = no_point_;
    }

    count = 0;
  }
}

void NavigationManagement::A_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_y = wp6_pose[1];

    if (stop_ == to_A)
    {
      goal_x = target_pos_x;
      goal_A = target_A;
    }
    else
    {
      goal_x = cur_x;
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_A)
    {
      move_point_ = rack_;
    }
    else if (stop_ == to_B)
      move_point_ = wp6_;
    else if (stop_ == to_home)
      move_point_ = wp6_;
    else if (stop_ == to_charger)
      move_point_ = wp6_;
    else if (stop_ == to_C || stop_ == to_D)
      move_point_ = wp7_;
    else if (stop_ == to_arm12)
      move_point_ = wp6_;
    else if (stop_ == to_arm34)
      move_point_ = wp7_;
    else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      move_point_ = wp7_;
    else if (stop_ == to_home1 || stop_ == to_cart)
      move_point_ = home1_;
    else
      move_point_ = no_point_;
    count = 0;
  }
}

void NavigationManagement::B_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_y = wp6_pose[1];

    if (stop_ == to_B)
    {
      goal_x = target_pos_x;
      goal_A = target_A;
    }
    else
    {
      goal_x = cur_x;
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_B)
    {
      move_point_ = rack_;
    }
    else if (stop_ == to_A)
      move_point_ = wp6_;
    else if (stop_ == to_home)
      move_point_ = wp6_;
    else if (stop_ == to_charger)
      move_point_ = wp6_;
    else if (stop_ == to_C || stop_ == to_D)
      move_point_ = wp7_;
    else if (stop_ == to_arm12)
      move_point_ = wp6_;
    else if (stop_ == to_arm34)
      move_point_ = wp7_;
    else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      move_point_ = wp7_;
    else if (stop_ == to_home1 || stop_ == to_cart)
      move_point_ = home1_;
    else
      move_point_ = no_point_;
    count = 0;
  }
}

void NavigationManagement::C_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_C)
    {
      goal_x = target_pos_x;
      goal_A = target_A;
      goal_y = target_pos_y - 1.0;
    }
    else
    {
      goal_x = cur_x;
      goal_A = cur_A;
      goal_y = wp8_pose[1];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_C)
    {
      move_point_ = rack_;
    }
    else if (stop_ == to_D)
      move_point_ = wp8_;
    else if (stop_ == to_home)
      move_point_ = rot4_;
    else if (stop_ == to_charger)
      move_point_ = rot4_;
    else if (stop_ == to_A || stop_ == to_B)
      move_point_ = rot4_;
    else if (stop_ == to_arm12)
      move_point_ = rot4_;
    else if (stop_ == to_arm34)
      move_point_ = rot4_;
    else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      move_point_ = wp8_;
    else if (stop_ == to_home1 || stop_ == to_cart)
      move_point_ = wp16_;
    else
      move_point_ = no_point_;
    count = 0;
  }
}

void NavigationManagement::D_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_D)
    {
      goal_x = target_pos_x;
      goal_A = target_A;
      goal_y = target_pos_y + 1.0;
    }
    else
    {
      goal_x = cur_x;
      goal_A = cur_A;
      goal_y = wp8_pose[1];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_D)
    {
      move_point_ = rack_;
    }
    else if (stop_ == to_C)
      move_point_ = wp8_;
    else if (stop_ == to_home)
      move_point_ = rot4_;
    else if (stop_ == to_charger)
      move_point_ = rot4_;
    else if (stop_ == to_A || stop_ == to_B)
      move_point_ = rot4_;
    else if (stop_ == to_arm12)
      move_point_ = rot4_;
    else if (stop_ == to_arm34)
      move_point_ = rot4_;
    else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      move_point_ = wp8_;
    else if (stop_ == to_home1 || stop_ == to_cart)
      move_point_ = wp16_;
    else
      move_point_ = no_point_;

    count = 0;
  }
}

void NavigationManagement::arm12_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (start_ == from_arm12)
    {
      goal_x = cur_x;
      goal_y = wp1_pose[1];
      goal_A = cur_A;
    }
    else
    {
      goal_x = target_pos_x;
      goal_y = wp1_pose[1];
      goal_A = target_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_charger)
      {
        move_point_ = wp3_;
      }
      else if (stop_ == to_arm12)
      {
        if (start_ == from_arm12)
        {
          move_point_ = arm12_;
        }
        else
          move_point_ = rack_;
      }
      else if (stop_ == to_arm34)
      {
        move_point_ = wp3_;
      }
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      {
        move_point_ = wp3_;
      }
      else if (stop_ == to_A || stop_ == to_B)
        move_point_ = home_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp3_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = home_;
      else
      {
        move_point_ = no_point_;
      }
    }

    count = 0;
  }
}

void NavigationManagement::arm34_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_arm34)
    {
      goal_x = wp3_pose[0];
      goal_y = target_pos_y;
      goal_A = target_A;
    }
    else
    {
      goal_x = wp3_pose[0];
      goal_y = cur_y;
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = wp3_;
    }
    else
    {
      if (stop_ == to_charger)
      {
        move_point_ = wp4_;
      }
      else if (stop_ == to_arm12)
      {
        move_point_ = rot1_;
      }
      else if (stop_ == to_arm34)
      {
        if (start_ == from_arm34)
        {
          move_point_ = arm34_;
        }
        else
          move_point_ = rack_;
      }
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      {
        move_point_ = wp5_;
      }
      else if (stop_ == to_A || stop_ == to_B)
        move_point_ = rot5_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp8_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = rot5_;
      else
      {
        move_point_ = no_point_;
      }
    }

    count = 0;
  }
}

void NavigationManagement::arm5_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_arm5)
    {
      goal_x = target_pos_x;
      goal_y = wp5_pose[1];
      goal_A = target_A;
    }
    else
    {
      goal_x = cur_x;
      goal_y = wp5_pose[1];
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = rot3_;
    }
    else
    {
      if (stop_ == to_charger)
      {
        move_point_ = rot3_;
      }
      else if (stop_ == to_arm12)
      {
        move_point_ = rot3_;
      }
      else if (stop_ == to_arm34)
      {
        move_point_ = wp5_;
      }
      else if (stop_ == to_arm5)
      {
        move_point_ = rack_;
      }
      else if (stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp51_;
      else if (stop_ == to_A || stop_ == to_B)
        move_point_ = rot3_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp5_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = wp5_;
      else
      {
        move_point_ = no_point_;
      }
    }

    count = 0;
  }
}

void NavigationManagement::arm6_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_arm6)
    {
      goal_x = wp10_pose[0];
      goal_y = target_pos_y;
      goal_A = target_A;
    }
    else
    {
      goal_x = wp10_pose[0];
      goal_y = cur_y;
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm6)
      {
        move_point_ = rack_;
      }
      else
      {
        move_point_ = wp10_;
      }
    }
    count = 0;
  }
}

void NavigationManagement::arm7_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_arm7)
    {
      goal_x = target_pos_x;
      goal_y = wp14_pose[1];
      goal_A = target_A;
    }
    else
    {
      goal_x = cur_x;
      goal_y = wp14_pose[1];
      goal_A = cur_A;
    }
    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm7)
      {
        move_point_ = rack_;
      }
      else
      {
        move_point_ = wp14_;
      }
    }
    count = 0;
  }
}

void NavigationManagement::charge_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = target_pos_x;
    goal_y = target_pos_y;
    goal_A = target_A;

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    moveMode_ = MOVE_QR_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;
    moveMode_ = MOVE_ZERO_;
    mode_agv_ = mode_stop_;
    agvStatus_ = GOAL_REACH;
    move_point_ = no_point_;
  }
}

void NavigationManagement::charge1_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = target_pos_x + 0.7;
    goal_y = init_pose_[1];
    goal_A = charge_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    moveMode_ = MOVE_QR_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;
    if (stop_ == to_charger)
      move_point_ = charger2_;
    else if (stop_ = to_home)
      move_point_ = home_;
    else
      move_point_ = no_point_;
  }
}

void NavigationManagement::charge2_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_charger)
    {
      goal_x = target_pos_x + 0.7;
      goal_y = target_pos_y;
      goal_A = target_A;
    }
    else
    {
      goal_x = cur_x + 0.7;
      goal_y = cur_y;
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    count = 0;
    if (stop_ == to_charger)
      move_point_ = charger_;
    else if (stop_ = to_home)
      move_point_ = charger1_;
    else
      move_point_ = no_point_;
  }
}

void NavigationManagement::param_set()
{

  if (nh.getParam("/init_pose", init_pose_))
  {
    for (vector<int>::size_type i = 0; i < init_pose_.size(); ++i)
    {
      ROS_INFO("init_pose : %.2f", init_pose_[i]);
    }
  }

  if (nh.getParam("/init_ori", init_ori))
  {
    for (vector<int>::size_type i = 0; i < init_ori.size(); ++i)
    {
      ROS_INFO("init_ori : %.2f", init_ori[i]);
    }
  }

  if (nh.getParam("/charge_pose", charge_pose))
  {
    for (vector<int>::size_type i = 0; i < charge_pose.size(); ++i)
    {
      ROS_INFO("charge_pose : %.2f", charge_pose[i]);
    }
  }

  if (nh.getParam("/wp1_pose", wp1_pose))
  {
    for (vector<int>::size_type i = 0; i < wp1_pose.size(); ++i)
    {
      ROS_INFO("wp1_pose : %.2f", wp1_pose[i]);
    }
  }

  if (nh.getParam("/wp2_pose", wp2_pose))
  {
    for (vector<int>::size_type i = 0; i < wp2_pose.size(); ++i)
    {
      ROS_INFO("wp2_pose : %.2f", wp2_pose[i]);
    }
  }

  if (nh.getParam("/wp3_pose", wp3_pose))
  {
    for (vector<int>::size_type i = 0; i < wp3_pose.size(); ++i)
    {
      ROS_INFO("wp3_pose : %.2f", wp3_pose[i]);
    }
  }

  if (nh.getParam("/wp4_pose", wp4_pose))
  {
    for (vector<int>::size_type i = 0; i < wp4_pose.size(); ++i)
    {
      ROS_INFO("wp4_pose : %.2f", wp4_pose[i]);
    }
  }

  if (nh.getParam("/wp5_pose", wp5_pose))
  {
    for (vector<int>::size_type i = 0; i < wp5_pose.size(); ++i)
    {
      ROS_INFO("wp5_pose : %.2f", wp5_pose[i]);
    }
  }

  if (nh.getParam("/wp51_pose", wp51_pose))
  {
    for (vector<int>::size_type i = 0; i < wp51_pose.size(); ++i)
    {
      ROS_INFO("wp51_pose : %.2f", wp51_pose[i]);
    }
  }

  if (nh.getParam("/wp6_pose", wp6_pose))
  {
    for (vector<int>::size_type i = 0; i < wp6_pose.size(); ++i)
    {
      ROS_INFO("wp6_pose : %.2f", wp6_pose[i]);
    }
  }

  if (nh.getParam("/wp7_pose", wp7_pose))
  {
    for (vector<int>::size_type i = 0; i < wp7_pose.size(); ++i)
    {
      ROS_INFO("wp7_pose : %.2f", wp7_pose[i]);
    }
  }

  if (nh.getParam("/wp8_pose", wp8_pose))
  {
    for (vector<int>::size_type i = 0; i < wp8_pose.size(); ++i)
    {
      ROS_INFO("wp8_pose : %.2f", wp8_pose[i]);
    }
  }

  if (nh.getParam("/wp9_pose", wp9_pose))
  {
    for (vector<int>::size_type i = 0; i < wp9_pose.size(); ++i)
    {
      ROS_INFO("wp9_pose : %.2f", wp9_pose[i]);
    }
  }

  if (nh.getParam("/wp10_pose", wp10_pose))
  {
    for (vector<int>::size_type i = 0; i < wp10_pose.size(); ++i)
    {
      ROS_INFO("wp10_pose : %.2f", wp10_pose[i]);
    }
  }

  if (nh.getParam("/wp11_pose", wp11_pose))
  {
    for (vector<int>::size_type i = 0; i < wp11_pose.size(); ++i)
    {
      ROS_INFO("wp11_pose : %.2f", wp11_pose[i]);
    }
  }

  if (nh.getParam("/wp12_pose", wp12_pose))
  {
    for (vector<int>::size_type i = 0; i < wp12_pose.size(); ++i)
    {
      ROS_INFO("wp12_pose : %.2f", wp12_pose[i]);
    }
  }

  if (nh.getParam("/wp13_pose", wp13_pose))
  {
    for (vector<int>::size_type i = 0; i < wp13_pose.size(); ++i)
    {
      ROS_INFO("wp13_pose : %.2f", wp13_pose[i]);
    }
  }

  if (nh.getParam("/wp14_pose", wp14_pose))
  {
    for (vector<int>::size_type i = 0; i < wp14_pose.size(); ++i)
    {
      ROS_INFO("wp14_pose : %.2f", wp14_pose[i]);
    }
  }

  if (nh.getParam("/wp15_pose", wp15_pose))
  {
    for (vector<int>::size_type i = 0; i < wp15_pose.size(); ++i)
    {
      ROS_INFO("wp15_pose : %.2f", wp15_pose[i]);
    }
  }

  if (nh.getParam("/wp16_pose", wp16_pose))
  {
    for (vector<int>::size_type i = 0; i < wp16_pose.size(); ++i)
    {
      ROS_INFO("wp16_pose : %.2f", wp16_pose[i]);
    }
  }

  if (nh.getParam("/home1_pose", home1_pose))
  {
    for (vector<int>::size_type i = 0; i < home1_pose.size(); ++i)
    {
      ROS_INFO("home1_pose : %.2f", home1_pose[i]);
    }
  }

  if (nh.getParam("/cart_pose", cart_pose))
  {
    for (vector<int>::size_type i = 0; i < cart_pose.size(); ++i)
    {
      ROS_INFO("cart_pose : %.2f", cart_pose[i]);
    }
  }
}

void NavigationManagement::rack_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = target_pos_x;
    goal_y = target_pos_y;
    goal_A = target_A;

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    moveMode_ = MOVE_QR_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;
    moveMode_ = MOVE_ZERO_;
    mode_agv_ = mode_stop_;
    agvStatus_ = GOAL_REACH;
    move_point_ = no_point_;
  }
}

void NavigationManagement::joystickControlCallback(
    const move_control::teleop::ConstPtr& motorCmd_)
{
  if (motorCmd_->control_mode == 1 || motorCmd_->control_mode == 2)
    manual_mode = true;
  else
    manual_mode = false;
}

void NavigationManagement::stop_agv()
{
  vel_.linear.x = 0;
  vel_.linear.y = 0;
  vel_.angular.z = 0;
  vel_pub.publish(vel_);
}

void NavigationManagement::qr_move()
{

  if (tag_ == true)
  {
    if (fabs(line_x) <= 2 && fabs(line_y) <= 2 && line_theta > 179 &&
        line_theta < 181)
    {
      vel_.linear.x = 0;
      vel_.linear.y = 0;
      vel_.angular.z = 0;
      agvStatus_ = QR_COMP;
    }
    else
    {
      if (fabs(line_x) > 2)
        vel_.linear.x = -QR_VEL * sign(line_x);
      else
        vel_.linear.x = 0;

      if (fabs(line_y) > 2)
        vel_.linear.y = -QR_VEL * sign(line_y);
      else
        vel_.linear.y = 0;

      if (line_theta > 140 && line_theta < 220)
      {
        if (line_theta >= 181 || line_theta <= 179)
          vel_.angular.z = QR_AVEL * sign(line_theta - 180);
        else
          vel_.angular.z = 0;
      }
      else
        vel_.angular.z = 0;
      agvStatus_ = QR_CTRL;
    }
  }
  else
  {
    vel_.linear.x = 0;
    vel_.linear.y = 0;
    vel_.angular.z = 0;
    agvStatus_ = QR_COMP;
  }
  vel_pub.publish(vel_);
}
} // namespace agv_wp

int main(int argc, char** argv)
{
  ros::init(argc, argv, "AGV_nav_action");

  agv_wp::NavigationManagement AGV_nav;
  AGV_nav.spin();
  return 0;
}
