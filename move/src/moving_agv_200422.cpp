#include <ros/ros.h>
#include <vector>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose.h>
#include <actionlib_msgs/GoalStatus.h>
#include <actionlib_msgs/GoalStatusArray.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_broadcaster.h>
#include <if_node_msgs/cmdMove.h>
//#include <nav_msgs/Feedback.h>
using namespace std;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

namespace gil{
	enum lee{
		status_0_=0,
		status_1_,
		status_2_,
		status_3_,
		status_4_,
		status_5_,
		status_6_,
		status_7_,
		status_8_
	};

	enum wp{
		wp_1, wp_2, wp_3, wp_4, wp_5, wp_6, wp_7
	};

	typedef struct status{
		bool pending;
		bool active;
		bool complete;

	} status;

	class move{
		public:
			move();
			void init();
			void spin();
			void update();
			void param_set();
			void wp_move();
			void RTH();
			void init_pose();
			void run();

		private:
			bool run_;
			bool target_;
			bool wp_;
			bool goal_status;
			int rate;
			int move_state; //move : 1, stop : 0
			int wp_state;
			int init_flag;
			int home_flag;
			float target_pos_x, target_pos_y, target_pos_z,
			      target_ori_x, target_ori_y, target_ori_z, target_ori_w;
			std_msgs::UInt8 state_goal;
			geometry_msgs::Twist vel;
			geometry_msgs::PoseStamped target_goal;
			actionlib_msgs::GoalStatus goal_Status;
			move_base_msgs::MoveBaseGoal goal;

			ros::NodeHandle nh;

			vector<float> wp1_pose, wp2_pose, wp3_pose, wp4_pose, wp5_pose;
			vector<float> wp1_ori, wp2_ori, wp3_ori, wp4_ori, wp5_ori;

			ros::Publisher vel_pub, goal_pub, reach_pub, init_pub;
			ros::Subscriber reach_sub, goal_sub, init_sub, home_sub;

			void goal_reach_callback(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach);
		//	void goal_target_callback(const geometry_msgs::PoseStamped& goal);
			void goal_target_callback(const if_node_msgs::cmdMove& goal);
			void init_callback(const std_msgs::UInt8::ConstPtr& init_);
			void home_callback(const std_msgs::UInt8::ConstPtr& home_);
	};
}

void gil::move::init()
{
//	printf("variables initial\n");
	ROS_INFO("variables initial");

	rate = 100; //10; //100
	target_pos_x = 0.0;
	target_pos_y = 0.0;
	target_pos_z = 0.0;

	target_ori_x = 0.0;
	target_ori_y = 0.0;
	target_ori_z = 0.0;
	target_ori_w = 0.0;

	target_ = false;
	run_ = true;
	wp_ = false;
	goal_status = true;

	init_flag = 0;

}

gil::move::move()
{
	init();
	param_set();
//	ros::NodeHandle nh;

	vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1, true);
	goal_pub = nh.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1, true);
	reach_pub = nh.advertise<std_msgs::UInt8>("/IFNode/goal_reach", 1, true);
	init_pub = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>("/initialpose", 1, true);

	reach_sub = nh.subscribe("/move_base/status", 10, &gil::move::goal_reach_callback, this);
//	goal_sub = nh.subscribe("/goal_target", 10, &gil::move::goal_target_callback, this);
	goal_sub = nh.subscribe("/MOVE/CMD", 10, &gil::move::goal_target_callback, this);
	init_sub = nh.subscribe("/init_pose", 1, &gil::move::init_callback, this);
	home_sub = nh.subscribe("/home_pose", 1, &gil::move::home_callback, this);

}

void gil::move::update()
{
	ros::Time::now();

	if(home_flag){ //return to home
		RTH();
		home_flag = 0;
	}

	if(init_flag){ //AGV init pose
		init_pose();
		init_flag = 0;
	}

   	if(wp_)
		wp_move();

	reach_pub.publish(state_goal);

	if(target_){
		run();
		target_ = false;
	}

}

void gil::move::spin()
{
	ros::Rate loop_rate(rate);

	while(run_)
	{
		update();
		ros::spinOnce();
		loop_rate.sleep();
	}

}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "moving_agv");
	ROS_INFO("==== moving start ====");
	gil::move *mv = new gil::move();
	mv->spin();

	gil::status st;
	st.active = true;

	return 0;
}


//void gil::move::goal_target_callback(const geometry_msgs::PoseStamped& goal)
void gil::move::goal_target_callback(const if_node_msgs::cmdMove& goal)
{
//	ROS_INFO("goal_target_callback");
	ROS_INFO("ACS send the coordinate data to AGV");

	target_pos_x = goal.pose.position.x;
	target_pos_y = goal.pose.position.y;
 	target_pos_z = goal.pose.position.z;

	target_ori_x = goal.pose.orientation.x;
	target_ori_y = goal.pose.orientation.y;
	target_ori_z = goal.pose.orientation.z;
	target_ori_w = goal.pose.orientation.w;

	if(target_pos_x > 0){
		wp_state = wp_1;
		ROS_INFO("wp_state : %d",wp_state);
	}

	move_state = goal.move;

	target_ = true;
	wp_ = true;

/*
	printf("target_pos_x : %.2f\t",target_pos_x);
	printf("target_pos_y : %.2f\t",target_pos_y);
	printf("target_pos_z : %.2f\t",target_pos_z);
	printf("target_pos_w : %.2f\n",target_ori_w);
	printf("move state : %d\n",move_state);
*/

}

void gil::move::goal_reach_callback(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach)
{
	if(!goal_reach->status_list.empty())
	{
//		actionlib_msgs::GoalStatus goal_Status = goal_reach->status_list[0];
		goal_Status = goal_reach->status_list[0];
		state_goal.data = goal_Status.status;
//		state_goal.data = goal_reach->status_list[0];

		switch(goal_Status.status){
			case status_0_:
				ROS_INFO("AGV STATUS : PENDING[%d]",goal_Status.status);
				break;

			case status_1_:
//				ROS_INFO("AGV STATUS : ACTIVE[%d]",goal_Status.status);
				ROS_INFO("AGV STATUS : ACTIVE");
				break;

			case status_3_:
//				ROS_INFO("AGV STATUS : COMPLETE[%d]",goal_Status.status);
				ROS_INFO("AGV STATUS : GOAL REACH");
				break;

			case status_4_:
				ROS_INFO("AGV STATUS : ABORTED[%d]",goal_Status.status);
				break;

			case status_8_:
				ROS_INFO("AGV STATUS : RECALLED[%d]",goal_Status.status);
				break;

			default:
				break;
		}

	}
	else
	{
		printf("AGV STATUS : IDLING\n");
	}
}

void gil::move::init_callback(const std_msgs::UInt8::ConstPtr& init_)
{
//	printf("AGV Pose & Orientation Initial\n");

	init_flag = init_->data;

	printf("init_flag : %d\n", init_flag);

}

void gil::move::home_callback(const std_msgs::UInt8::ConstPtr& home_)
{
	ROS_INFO("Return To home sweet home");
	home_flag = home_->data;
	ROS_INFO("home_flag : %d",home_flag);
}

void gil::move::RTH()
{
	target_goal.pose.position.x = 0.0;
	target_goal.pose.position.y = 0.0;
	target_goal.pose.position.z = 0.0;

	target_goal.pose.orientation.x = 0.0;
	target_goal.pose.orientation.y = 0.0;
	target_goal.pose.orientation.z = 0.0;
	target_goal.pose.orientation.w = 0.99;

	goal_pub.publish(target_goal);
}

void gil::move::init_pose()
{
	geometry_msgs::PoseWithCovarianceStamped init_;
	init_.header.stamp = ros::Time::now();
	init_.header.frame_id = "map";

	init_.pose.pose.position.x = 0.0;
	init_.pose.pose.position.y = 0.0;
	init_.pose.pose.position.z = 0.0;

	init_.pose.pose.orientation.x = 0.0;
	init_.pose.pose.orientation.y = 0.0;
	init_.pose.pose.orientation.z = 0.0;
	init_.pose.pose.orientation.w = 0.99;

	init_pub.publish(init_);

//	init_flag = 0;

	printf("AGV InitialPose Complete\n");

}

void gil::move::param_set()
{
	if(nh.getParam("/wp1_pose",wp1_pose)){
		for(vector<int>::size_type i=0;i<wp1_pose.size();++i){
			ROS_INFO("wp1_pose : %.2f",wp1_pose[i]);
		}
	}

	if(nh.getParam("/wp1_ori",wp1_ori)){
		for(vector<int>::size_type i=0;i<wp1_ori.size();++i){
			ROS_INFO("wp1_ori : %.2f",wp1_ori[i]);
		}
	}
}

void gil::move::wp_move()
{
	goal.target_pose.header.stamp = ros::Time::now();
	target_goal.header.frame_id = "map";
        goal.target_pose.header.frame_id = "map";

	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0)))
		ROS_INFO("Waiting for the move_base action server");
	if(move_state == 1){
		goal.target_pose.pose.position.x = wp1_pose[0];
        	goal.target_pose.pose.position.y = wp1_pose[1];
		goal.target_pose.pose.orientation.z = wp1_ori[2];
        	goal.target_pose.pose.orientation.w = wp1_ori[3];//0.99;

		ROS_INFO("AGV move to the Waypoint1");

         	ac.sendGoal(goal);

        	ac.waitForResult();

        	if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                	ROS_INFO("AGV has arrived to the Waypoint1");
                	target_ = true;
			wp_ = false;

        	}
        	else{
                	ROS_INFO("AGV failed for some reaseon");
        	}
	}

}

void gil::move::run()
{
	ROS_INFO("AGV Running");
	ROS_INFO("target_pos_x : %.2f",target_pos_x);
	ROS_INFO("target_pos_y : %.2f",target_pos_y);
	ROS_INFO("target_pos_z : %.2f",target_pos_z);
	ROS_INFO("target_pos_w : %.2f",target_ori_w);

	target_goal.pose.position.x = target_pos_x;
	target_goal.pose.position.y = target_pos_y;
	target_goal.pose.position.z = target_pos_z;

	target_goal.pose.orientation.x = target_ori_x;
	target_goal.pose.orientation.y = target_ori_y;
	target_goal.pose.orientation.z = target_ori_z;
	target_goal.pose.orientation.w = target_ori_w;

	if(move_state == 1) // move : 1, stop : 0
		goal_pub.publish(target_goal);

}
