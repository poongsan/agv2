//written by lee

#include "agv.h"
/*
#include <ros/ros.h>
#include <vector>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose.h>
#include <actionlib_msgs/GoalStatus.h>
#include <actionlib_msgs/GoalStatusArray.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_broadcaster.h>
#include <if_node_msgs/cmdMove.h>
#include <pgv/vision_msg.h>
//#include <nav_msgs/Feedback.h>
*/

using namespace std;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

namespace gil{
	enum lee{
		status_0_=0,
		status_1_,
		status_2_,
		status_3_,
		status_4_,
		status_5_,
		status_6_,
		status_7_,
		status_8_
	};

	enum wp{
		wp_1, wp_2, wp_3, wp_4, wp_5, wp_6, wp_7
	};

	typedef struct status{
		bool pending;
		bool active;
		bool complete;

	} status;

	class move{
		public:
			move();
			void init();
			void spin();
			void update();
			void param_set();
			void wp_move();
			void RTH();
			void init_pose();
			void run();
			void precision_move();

		private:
			bool run_;
			bool target_;
			bool wp_;
			bool goal_status;
			bool tag_, control_;
			int tag_id, line_, line_id, control_id, fault, warning, command;
			int rate;
			int move_state; //move : 1, stop : 0
			int wp_state;
			int init_flag;
			int home_flag;
			float target_pos_x, target_pos_y, target_pos_z,
			      target_ori_x, target_ori_y, target_ori_z, target_ori_w;
			float line_x, line_y, line_theta;
			std_msgs::UInt8 state_goal;
			geometry_msgs::Twist vel;
			geometry_msgs::PoseStamped target_goal;
			actionlib_msgs::GoalStatus goal_Status;
			move_base_msgs::MoveBaseGoal goal;

			ros::NodeHandle nh;

			vector<float> wp1_pose, wp2_pose, wp3_pose, wp4_pose, wp5_pose;
			vector<float> wp1_ori, wp2_ori, wp3_ori, wp4_ori, wp5_ori;

			ros::Publisher vel_pub, goal_pub, reach_pub, init_pub;
			ros::Subscriber reach_sub, goal_sub, init_sub, home_sub, pgv_sub;

			void goal_reach_cb(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach);
		//	void goal_target_cb(const geometry_msgs::PoseStamped& goal);
			void goal_target_cb(const if_node_msgs::cmdMove& goal);
			void init_cb(const std_msgs::UInt8::ConstPtr& init_);
			void home_cb(const std_msgs::UInt8::ConstPtr& home_);
			void pgv_cb(const pgv::vision_msg& pgv_msg);
	};
}
