#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <actionlib_msgs/GoalStatus.h>
#include <actionlib_msgs/GoalStatusArray.h>
//#include <nav_msgs/Feedback.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_broadcaster.h>
#include <if_node_msgs/cmdMove.h>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

using namespace std;

namespace gil{
	enum lee{
		status_1,
		status_2,
		status_3,
		status_4,
		status_5,
		status_6
	};

	class move{
		public:
			move();
			void init();
			void spin();
			void update();

		private:
			bool run_;
			bool target_;
			int rate;
			int move_state; //move : 1, stop : 0
			float target_pos_x, target_pos_y, target_pos_z,
			      target_ori_x, target_ori_y, target_ori_z, target_ori_w;
			std_msgs::UInt8 state_goal;
			geometry_msgs::Twist vel;
			geometry_msgs::PoseStamped target_goal;
			actionlib_msgs::GoalStatus goal_Status;
			move_base_msgs::MoveBaseGoal goal;

			ros::Publisher vel_pub, goal_pub, reach_pub;
			ros::Subscriber reach_sub, goal_sub;

			void goal_reach_callback(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach);
		//	void goal_target_callback(const geometry_msgs::PoseStamped& goal);
			void goal_target_callback(const if_node_msgs::cmdMove& goal);
	};
}

void gil::move::init()
{
	printf("variables initial\n");
	rate = 10; //100
	target_pos_x = 0.0;
	target_pos_y = 0.0;
	target_pos_z = 0.0;

	target_ori_x = 0.0;
	target_ori_y = 0.0;
	target_ori_z = 0.0;
	target_ori_w = 0.0;

	target_ = false;
	run_ = true;

}

gil::move::move()
{
	init();
	printf("move constructor\n");
	ros::NodeHandle nh;
//	run_ = true;
//	rate = 100;

	vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1, true);
	goal_pub = nh.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1, true);
	reach_pub = nh.advertise<std_msgs::UInt8>("/IFNode/goal_reach", 1, true);

	reach_sub = nh.subscribe("/move_base/status", 10, &gil::move::goal_reach_callback, this);
//	goal_sub = nh.subscribe("/goal_target", 10, &gil::move::goal_target_callback, this);
	goal_sub = nh.subscribe("/MOVE/CMD", 10, &gil::move::goal_target_callback, this);

}

void gil::move::update()
{
	ros::Time::now();
	goal.target_pose.header.stamp = ros::Time::now();
	target_goal.header.frame_id = "map";
        goal.target_pose.header.frame_id = "map";

//	if(move_state == 1){
		MoveBaseClient ac("move_base", true);

		while(!ac.waitForServer(ros::Duration(5.0)))
			ROS_INFO("Waiting for the move_base action server");

 	if(move_state == 1){
		goal.target_pose.pose.position.x = 0.0;
        	goal.target_pose.pose.position.y = -2.0;
        	goal.target_pose.pose.orientation.w = 0.99;
//	}

	reach_pub.publish(state_goal);

//	goal.target_pose.pose.position.x = 0.0;
//	goal.target_pose.pose.position.y = -2.0;
//	goal.target_pose.pose.orientation.w = 0.99;

	ROS_INFO("AGV move to the Waypoint1");
//	if(move_state == 1) // move : 1, stop : 0
		ac.sendGoal(goal);

	ac.waitForResult();

	if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
		ROS_INFO("AGV has arrived to the Waypoint1");
	else
		ROS_INFO("AGV failed for some reaseon");
	}

	while(target_)
	{
//		target_goal.header.frame_id = "map";
//		goal.target_pose.header.frame_id = "map";

		printf("test1\n");

		target_goal.pose.position.x = target_pos_x;
		target_goal.pose.position.y = target_pos_y;
		target_goal.pose.position.z = target_pos_z;

		target_goal.pose.orientation.x = target_ori_x;
		target_goal.pose.orientation.y = target_ori_y;
		target_goal.pose.orientation.z = target_ori_z;
		target_goal.pose.orientation.w = target_ori_w;

		if(move_state == 1) // move : 1, stop : 0
			goal_pub.publish(target_goal);

		target_ = false;


	}

}

void gil::move::spin()
{
//	printf("spin()\n");
	ros::Rate loop_rate(rate);

	while(run_)
	{
		update();
		ros::spinOnce();
		loop_rate.sleep();
	}

}

//void gil::move::goal_target_callback(const geometry_msgs::PoseStamped& goal)
void gil::move::goal_target_callback(const if_node_msgs::cmdMove& goal)
{
	printf("goal_target_callback\n");

	target_pos_x = goal.pose.position.x;
	target_pos_y = goal.pose.position.y;
 	target_pos_z = goal.pose.position.z;

	target_ori_x = goal.pose.orientation.x;
	target_ori_y = goal.pose.orientation.y;
	target_ori_z = goal.pose.orientation.z;
	target_ori_w = goal.pose.orientation.w;

	move_state = goal.move;

	target_ = true;

	printf("target_pos_x : %.2f\t",target_pos_x);
	printf("target_pos_y : %.2f\t",target_pos_y);
	printf("target_pos_z : %.2f\t",target_pos_z);
	printf("target_pos_w : %.2f\n",target_ori_w);
	printf("move state : %d\n",move_state);
//	printf("move state : %d\n",goal.move);
/*
	target_pos_x = goal.pose.position.x;
        target_pos_y = goal.pose.position.y;
        target_pos_z = goal.pose.position.z;

        target_ori_x = goal.pose.orientation.x;
        target_ori_y = goal.pose.orientation.y;
        target_ori_z = goal.pose.orientation.z;
        target_ori_w = goal.pose.orientation.w;

	target_ = true;

        printf("target_pos_x : %.2f\t",target_pos_x);
        printf("target_pos_y : %.2f\t",target_pos_y);
        printf("target_pos_z : %.2f\t",target_pos_z);
        printf("target_pos_w : %.2f\n",target_ori_w)
*/

}

void gil::move::goal_reach_callback(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach)
{
	if(!goal_reach->status_list.empty())
	{
//		actionlib_msgs::GoalStatus goal_Status = goal_reach->status_list[0];
		goal_Status = goal_reach->status_list[0];
		state_goal.data = goal_Status.status;
//		state_goal.data = goal_reach->status_list[0];

	//	printf("agv status: %d\n",goal_Status.status);
	//	printf("agv status: %d\n",goal_reach->status_list[0]);
/*
		 if(goal_reach->status_list[0] == 1)
                {
                        printf("AGV STATUS(%d) : ACTIVE\n",goal_Status.status);
                }
*/

/*
                else if(state_goal.data == 4)
                {
                        printf("AGV STATUS(%d) : ABORTED\n",goal_Status.status);
                }
                else if(state_goal.data == 3)
                {
                        printf("AGV STATUS(%d) : COMPLETE\n",goal_Status.status);
                }
*/


		if(state_goal.data == 1)
		{
	//		printf("AGV STATUS(%d) : ACTIVE\n",goal_Status.status);
	//		ROS_INFO("AGV STATUS : ACTIVE (%d)",goal_Status.status);
		}
		else if(state_goal.data == 4)
		{
	//		printf("AGV STATUS(%d) : ABORTED\n",goal_Status.status);
	//		ROS_INFO("AGV STATUS : ABORTED (%d)",goal_Status.status);
		}
		else if(state_goal.data == 3)
		{
	//		printf("AGV STATUS(%d) : COMPLETE\n",goal_Status.status);
	//		ROS_INFO("AGV STATUS : COMPLETE (%d)",goal_Status.status);
		}

	}
	else
	{
		printf("AGV STATUS : IDLING\n");
	}


}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "moving_agv");
	ROS_INFO("==== moving start ====");
	gil::move *mv = new gil::move();

	mv->spin();

	return 0;
}
