#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <tf/LinearMath/Transform.h>

#include <geometry_msgs/PointStamped.h>

int main(int argc, char** argv)
{
	ros::init(argc, argv, "tf_state");

	ros::NodeHandle n;

	ros::Publisher pub_tf_base_link;
	pub_tf_base_link = n.advertise<geometry_msgs::PointStamped>("/agv_base_position", 1);

	geometry_msgs::PointStamped pose_msgs;

	tf2_ros::Buffer tfBuffer;
	tf2_ros::TransformListener tfListener(tfBuffer);

	ros::Rate rate(10.0);
	while (n.ok())
	{
		geometry_msgs::TransformStamped transformStamped;

		try
		{
			transformStamped = tfBuffer.lookupTransform("map", "base_link", ros::Time(0));

			double roll, pitch, yaw;
			tf::Quaternion q(
				transformStamped.transform.rotation.x,
				transformStamped.transform.rotation.y,
				transformStamped.transform.rotation.z,
				transformStamped.transform.rotation.w);
			tf::Matrix3x3(q).getRPY(roll, pitch, yaw);

//			ROS_INFO("[X:%f, Y:%f, Th:%f]", transformStamped.transform.translation.x, transformStamped.transform.translation.y, yaw);

			pose_msgs.point.x = transformStamped.transform.translation.x;
			pose_msgs.point.y = transformStamped.transform.translation.y;
			pose_msgs.point.z = yaw;

			pub_tf_base_link.publish(pose_msgs);
		}
		catch (tf2::TransformException &ex)
		{
			ROS_WARN("%s",ex.what());
			ros::Duration(0.3).sleep();
			continue;
		}

		rate.sleep();
	}
	return 0;
}

