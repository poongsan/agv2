// AGV 1 movement 200609


#include <ros/ros.h>
#include "ros/time.h"
#include <vector>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose.h>
#include <actionlib_msgs/GoalStatus.h>
#include <actionlib_msgs/GoalStatusArray.h>
#include <actionlib_msgs/GoalID.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_broadcaster.h>
#include <if_node_msgs/cmdMove.h>
#include <if_node_msgs/waypoint.h>
#include <pgv/vision_msg.h>
#include <obstacle_detector/obstacles_detect.h>
#include <nav_msgs/Odometry.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/LinearMath/Transform.h>
#include <geometry_msgs/PointStamped.h>
#include <move_base_msgs/MoveBaseActionFeedback.h>
//#include <nav_msgs/Feedback.h>

#include "move_control/teleop.h"

#define QR_VEL 0.019
// Add Dinh 200527
#define QR_AVEL 0.009
#define CART_VEL 0.016
#define QR_NUM 5

using namespace std;

namespace agv_wp {


typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
   tf2_ros::Buffer tfBuffer;

  enum goal_reach_status{
    status_0_=0,status_1_,status_2_,status_3_,status_4_,status_5_,status_6_,status_7_,status_8_
  };

  enum QR_Move{
    move_x_, move_y_, move_theta_
  };
  // Add Dinh 200601
  enum start_pos{
    start_null = 0, from_home, from_charger, from_cart, from_ABL, from_ABU, from_CD
  };

  enum stop_pos{
    stop_null = 0, to_home, to_charger, to_cart, to_ABL, to_ABU, to_CD
  };

  // Add Dinh 200602
  enum Move_Point{
    no_point_=0, manual_move, home_, cart_, charger_, charger1_, ABL_, ABU_, CD_, wp1_, wp2_, point1_, point2_, point3_, point4_, rot1_, rot2_, rack_, h_reset_, qr_reset_
  };

  typedef struct status{
    bool pending;
    bool active;
    bool complete;

  } status;

    QR_Move _move;
    start_pos start_;
    stop_pos stop_;
    Move_Point move_point_;



  class NavigationManagement{
  public:
    NavigationManagement();
    ~NavigationManagement();
    void init();
    void spin();
    void update();
    void param_set();

  private:

    void charge_move();
    void charge1_move();
    void init_pose();
    void home_reset();
    void rack_move();
    void qr_move();

    // Add Dinh 200601

    void RTH();
    void wp1_move();
    void wp2_move();

    void rot1_move();
    void rot2_move();

    void point1_move();
    void point2_move();
    void point3_move();
    void point4_move();


    void ABL_move();
    void ABU_move();
    void CD_move();

    void cart_move();

    bool cancelAllGoals(double timeout);
    void stop_agv();


    // Add Dinh 200528
    MoveBaseClient  *AGV_move_base;

    // Add Dinh 200601

    bool tag_, control_;
    bool obstacle_;

    int rate;
    int move_state; //move : 1, stop : 0
    int init_flag;
    int home_flag;

    int tag_id, line_, line_id, control_id, fault, warning, command;

    int qr_count;
    float target_pos_x, target_pos_y, target_ori_z, target_ori_w;

    // Add Dinh 200528
    float current_x, current_y, current_ori_z, current_ori_w;

    float line_x, line_y, line_theta;
    float tf_fb_x, tf_fb_y, tf_fb_ori_z, tf_fb_ori_w;

    // Add Dinh 200601
    float cur_x, cur_y, cur_oz, cur_ow;

    // Add Dinh 200608
    bool manual_mode;

    std_msgs::UInt8 state_goal;
    geometry_msgs::Twist vel_;
    geometry_msgs::PoseStamped target_goal;
    geometry_msgs::PoseStamped obstacle_goal;
    actionlib_msgs::GoalStatus goal_Status;
    move_base_msgs::MoveBaseGoal goal;
    geometry_msgs::PoseStamped pose_msgs;


    ros::NodeHandle nh;


    vector<float> init_pose_, init_ori;
    vector<float> charge_pose, charge_ori;
    vector<float> charge1_pose, charge1_ori;
    // Add DInh 200609
    vector<float> cart_init_pose, cart_init_ori;

    // Modify Dinh 200609
    vector<float> wp1_pose, wp2_pose, wp3_pose, wp4_pose, wp5_pose, wp6_pose, point1_pose, point2_pose, point3_pose;
    vector<float> wp1_ori, wp2_ori, wp3_ori, wp4_ori, wp5_ori, wp6_ori,point1_ori, point2_ori, point3_ori;


    ros::Publisher vel_pub, goal_pub, reach_pub, init_pub, cancel_pub, tf_base_link_pub, qr_ang_pub, waypoint_pub; // Add Dinh 200602
    ros::Subscriber reach_sub, goal_sub, init_sub, home_sub, pgv_sub, move_base_feedback_sub, obstacle_sub, joy_sub;

    void goal_reach_cb(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach);
    void goal_target_cb(const if_node_msgs::cmdMove& goal);
    void init_cb(const std_msgs::UInt8::ConstPtr& init_);
    void home_cb(const std_msgs::UInt8::ConstPtr& home_sig);
    void pgv_cb(const pgv::vision_msg& pgv_msg);
    void tf_feedback_cb(const move_base_msgs::MoveBaseActionFeedback& feedback_);
    void obstacle_cb(const obstacle_detector::obstacles_detect& obstacle_msg);
    void wp_pub();

    // Add Dinh 200608
    void joystickControlCallback(const move_control::teleop::ConstPtr& motorCmd_);

  };

NavigationManagement::NavigationManagement()
{

  init();
  param_set();
  AGV_move_base =  new MoveBaseClient("move_base",true);

  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1, true);
//	goal_pub = nh.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1, true);
  goal_pub = nh.advertise<geometry_msgs::PoseStamped>("/current_goal", 1, true);
  reach_pub = nh.advertise<std_msgs::UInt8>("/IFNode/goal_reach", 1, true);
  init_pub = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>("/initialpose", 1, true);
  cancel_pub = nh.advertise<actionlib_msgs::GoalID>("/move_base/cancel", 1, true);
  tf_base_link_pub = nh.advertise<geometry_msgs::PoseStamped>("/agv_pos", 1, true);
  // Add DInh 200602
  waypoint_pub = nh.advertise<if_node_msgs::waypoint>("/agv_wp", 1, true);

  reach_sub = nh.subscribe("/move_base/status", 10, &NavigationManagement::goal_reach_cb, this);
//	goal_sub = nh.subscribe("/goal_target", 10, &NavigationManagement::goal_target_cb, this);
  goal_sub = nh.subscribe("/MOVE/CMD", 10, &NavigationManagement::goal_target_cb, this);
  init_sub = nh.subscribe("/init_pose", 1, &NavigationManagement::init_cb, this);
  home_sub = nh.subscribe("/home_pose", 1, &NavigationManagement::home_cb, this);
  pgv_sub = nh.subscribe("/pgv_data", 10, &NavigationManagement::pgv_cb, this);
  move_base_feedback_sub = nh.subscribe("/move_base/feedback", 10, &NavigationManagement::tf_feedback_cb ,this);
  obstacle_sub = nh.subscribe("/obstacle_info", 1, &NavigationManagement::obstacle_cb, this);

  // Add Dinh 200608
    joy_sub = nh.subscribe("cmd_vel_joy", 1, &NavigationManagement::joystickControlCallback, this);

}


NavigationManagement::~NavigationManagement()
{
}

void NavigationManagement::init()
{
//	ROS_INFO("variables initial");

  rate = 100; //10; //100
  target_pos_x = 0.0;
  target_pos_y = 0.0;

  target_ori_z = 0.0;
  target_ori_w = 0.99;

  // Add Dinh 200528
  current_x = 0;
  current_y = 0;
  current_ori_w = 0;
  current_ori_z = 0;

  // Add Dinh 200601
  cur_x = 0;
  cur_y = 0;
  cur_ow = 0;
  cur_oz = 0;

  // Add Dinh 200601
  home_flag = 0;


  start_ = start_null;
  stop_ = stop_null;
  move_point_ = no_point_;


  obstacle_ = false;
  // Add Dinh 200520
  init_flag = 0;
  qr_count = 0;

  manual_mode = false;
}

bool NavigationManagement::cancelAllGoals(double timeout)
{
  actionlib::SimpleClientGoalState goal_state = AGV_move_base->getState();
  if ((goal_state != actionlib::SimpleClientGoalState::ACTIVE) &&
      (goal_state != actionlib::SimpleClientGoalState::PENDING) &&
      (goal_state != actionlib::SimpleClientGoalState::RECALLED) &&
      (goal_state != actionlib::SimpleClientGoalState::PREEMPTED))
  {
    // We cannot cancel a REJECTED, ABORTED, SUCCEEDED or LOST goal
    ROS_WARN("Cannot cancel move base goal, as it has %s state!", goal_state.toString().c_str());
    return true;
  }

  ROS_INFO("Canceling move base goal with %s state...", goal_state.toString().c_str());
  AGV_move_base->cancelAllGoals();
  if (AGV_move_base->waitForResult(ros::Duration(timeout)) == false)
  {
    ROS_WARN("Cancel move base goal didn't finish after %.2f seconds: %s",
             timeout, goal_state.toString().c_str());
    return false;
  }

  ROS_INFO("Cancel move base goal succeed. New state is %s", goal_state.toString().c_str());
  return true;
}
void NavigationManagement::wp_pub()
{
  if_node_msgs::waypoint wp_desc;
  switch(start_)
  {
  case from_home: wp_desc.start_point = "home"; break;
  case from_charger: wp_desc.start_point = "charger"; break;
  case from_cart: wp_desc.start_point = "cart"; break;
  case from_ABL: wp_desc.start_point = "AB low"; break;
  case from_ABU: wp_desc.start_point = "AB up"; break;
  case from_CD: wp_desc.start_point = "CD"; break;
  case start_null: wp_desc.start_point = "no_point"; break;
  default: break;
  }
  switch(stop_)
  {
  case to_home: wp_desc.stop_point = "home"; break;
  case to_charger: wp_desc.stop_point = "charger"; break;
  case to_cart: wp_desc.stop_point = "cart"; break;
  case to_ABL:  wp_desc.stop_point = "AB low"; break;
  case to_ABU:  wp_desc.stop_point = "AB up"; break;
  case to_CD:  wp_desc.stop_point = "CD"; break;
  case stop_null: wp_desc.stop_point = "no_point"; break;
  default: break;
  }
  waypoint_pub.publish(wp_desc);
}

void NavigationManagement::update()
{
  // Add Dinh 200601

  geometry_msgs::TransformStamped transformStamped;
  try
  {
    transformStamped = tfBuffer.lookupTransform("map", "base_link", ros::Time(0));

    current_x =  transformStamped.transform.translation.x;
    current_y =   transformStamped.transform.translation.y ;
    current_ori_w =   transformStamped.transform.rotation.w;
    current_ori_z =   transformStamped.transform.rotation.z;
  }
  catch(tf2::TransformException &ex)
  {
    ROS_WARN("%s", ex.what());
    ros::Duration(0.3).sleep();
//		continue;
  }

  pose_msgs.pose.position.x = current_x;
  pose_msgs.pose.position.y = current_y;
  pose_msgs.pose.orientation.z = current_ori_z;
  pose_msgs.pose.orientation.w = current_ori_w;
  tf_base_link_pub.publish(pose_msgs);

  if(init_flag)
  { //AGV init pose
    init_pose();
    init_flag = 0;
  }

  if((start_ != start_null) && (stop_ != stop_null) &&(move_point_ !=no_point_))
  {
    switch(move_point_)
    {
    case home_:
        RTH();
        break;
    case charger_:
      charge_move();
      break;
    case charger1_:
      charge1_move();
      break;
    case cart_:
      cart_move();
      break;
    case wp1_:
      wp1_move();
      break;
    case wp2_:
      wp2_move();
      break;
    case rot1_:
      rot1_move();
      break;
    case rot2_:
      rot2_move();
      break;
    case point1_:
      point1_move();
      break;
    case point2_:
      point2_move();
      break;
    case point3_:
      point3_move();
      break;
    case point4_:
      point4_move();
      break;
    case ABL_:
      ABL_move();
      break;
    case ABU_:
      ABU_move();
      break;
    case CD_:
      CD_move();
      break;
    case h_reset_:
      home_reset();
      break;
    case qr_reset_:
      qr_move();
      break;
    case rack_:
      rack_move();
      break;
    case manual_move:
      cancelAllGoals(0.01);
      break;
    default:
      break;
    }

  }

//	reach_pub.publish(state_goal);

}

void NavigationManagement::spin()
{
  ros::Rate loop_rate(rate);
  tf2_ros::TransformListener tfListener(tfBuffer);

  while(ros::ok())
  {
    update();
    ros::spinOnce();
    loop_rate.sleep();
  }

}


void NavigationManagement::goal_target_cb(const if_node_msgs::cmdMove& goal)
{
  ROS_INFO("AGV received the coordinate data from ACS");

  target_pos_x = goal.pose.position.x;
  target_pos_y = goal.pose.position.y;

  target_ori_z = goal.pose.orientation.z;
  target_ori_w = goal.pose.orientation.w;

  start_ = start_null;
  stop_ = stop_null;

  cancelAllGoals(0.01);
  move_state = goal.move;
  if((fabs(current_x - charge_pose[0]) < 0.2) && (fabs(current_y - charge_pose[1]) < 0.2) && (fabs(current_ori_w - charge_ori[3]) < 0.2))
    // from charger -> move directly home
  {
    start_ = from_charger;
  }
  else if((fabs(current_x - cart_init_pose[0]) < 0.3) && (fabs(current_y - cart_init_pose[1]) < 0.3) && (fabs(current_ori_w - cart_init_ori[3]) < 0.3))
    // from charger -> move directly home
  {
    start_ = from_cart;
  }
  else if ((current_y >= init_pose_[1] - 6) && (current_x<= init_pose_[0]+0.4) && (current_x>=init_pose_[0]-0.4))
  {
    // from home area
    start_ = from_home;
  }
  else if((current_y < charge_pose[1] +3) && (current_y >= charge_pose[1]-0.5) && (current_x< charge_pose[0]+1) && (current_x>charge_pose[0]-3))
  {
    // from charger area
    start_ = from_charger;
  }

  else if((current_y < 6) && (current_x<43) && (current_x>=init_pose_[0]+0.4))
  {
    // from CD
    start_ = from_CD;
  }
  else if(current_y >8 && current_x< 2)
  {
    // from ABL
    start_ = from_ABL;
  }
  else if((current_y >8) && (current_x<43) && (current_x>=init_pose_[0]+0.4))
  {
    // from ABU
    start_ = from_ABU;
  }
  else
  {
    // from undefined
    start_ = start_null;
    move_point_ = no_point_;
  }

  // Add Dinh 200601
  if((fabs(target_pos_x - init_pose_[0]) < 0.2) && (fabs(target_pos_y - init_pose_[1]) < 0.2) && (fabs(target_ori_w - init_ori[3]) < 0.2))
    // to home
  {
    stop_ = to_home;
    switch (start_)
    {
    case from_charger: move_point_ = home_; break;
    case from_home: move_point_ = home_; break;
    case from_cart: move_point_ = home_; break;
    case from_ABL:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABL_;
    }
      break;
    case from_ABU:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABU_;
    }
      break;
    case from_CD:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = CD_;
    }
      break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }

  }
  else if((fabs(target_pos_x - charge_pose[0]) < 0.2) && (fabs(target_pos_y - charge_pose[1]) < 0.2) && (fabs(target_ori_w - charge_ori[3]) < 0.2))
    // to charger
  {
    stop_ = to_charger;
    switch (start_)
    {
    case from_charger: move_point_ = charger1_; break;
    case from_home: move_point_ = charger1_; break;
    case from_cart: move_point_ = home_; break;
    case from_ABL:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABL_;
    }
      break;
    case from_ABU:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABU_;
    }
      break;
    case from_CD:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = CD_;
    }
      break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }
  }
  else if((fabs(target_pos_x - cart_init_pose[0]) < 0.2) && (fabs(target_pos_y - cart_init_pose[1]) < 0.2) && (fabs(target_ori_w - cart_init_ori[3]) < 0.2))
    // to cart
  {
    stop_ = to_cart;
    switch (start_)
    {
    case from_charger: move_point_ = home_; break;
    case from_home: move_point_ = cart_; break;
    case from_cart: move_point_ = cart_; break;
    case from_ABL:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABL_;
    }
      break;
    case from_ABU:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABU_;
    }
      break;
    case from_CD:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = CD_;
    }
      break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }
  }

  else if((target_pos_y >8) && (target_pos_x <= 2))
    // to ABL rack
  {
    stop_ = to_ABL;

    switch (start_)
    {
    case from_charger: move_point_ = point1_; break;
    case from_home: move_point_ = point1_; break;
    case from_cart: move_point_ = home_; break;
    case from_ABL:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABL_;
    }
      break;
    case from_ABU:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABU_;
    }
      break;
    case from_CD:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = CD_;
    }
      break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }

  }

  else if((target_pos_y >8) && (target_pos_x < 43) && (target_pos_x >=init_pose_[0]+1))
    // to ABU rack
  {
    stop_ = to_ABU;

    switch (start_)
    {
    case from_charger: move_point_ = home_; break;
    case from_home: move_point_ = wp1_; break;
    case from_cart: move_point_ = home_; break;
    case from_ABL:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABL_;
    }
      break;
    case from_ABU:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABU_;
    }
      break;
    case from_CD:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = CD_;
    }
      break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }

  }

  else if((target_pos_y <6) && (target_pos_x < 43) && (target_pos_x >=init_pose_[0]+1))
    // to CD rack
  {
    stop_ = to_CD;

    switch (start_)
    {
    case from_charger: move_point_ = wp2_; break;
    case from_home: move_point_ = wp2_; break;
    case from_cart: move_point_ = home_; break;
    case from_ABL:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABL_;
    }
      break;
    case from_ABU:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABU_;
    }
      break;
    case from_CD:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = CD_;
    }
      break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }
  }

  else // target not in the list
  {
      start_ = start_null;
      stop_ = stop_null;
      move_point_ = no_point_;

  }
  wp_pub();
}


void NavigationManagement::goal_reach_cb(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach)
{
  if(!goal_reach->status_list.empty()){
//		actionlib_msgs::GoalStatus goal_Status = goal_reach->status_list[0];
    goal_Status = goal_reach->status_list[0];
    state_goal.data = goal_Status.status;
//		state_goal.data = goal_reach->status_list[0];
//		cancel.id = "";

    switch(goal_Status.status){
      case status_0_:
        ROS_INFO("AGV STATUS : PENDING[%d]",goal_Status.status);
        break;

      case status_1_:
//				ROS_INFO("AGV STATUS : ACTIVE[%d]",goal_Status.status);
//				ROS_INFO("AGV STATUS : ACTIVE");
        break;

      case status_3_:
//				ROS_INFO("AGV STATUS : COMPLETE[%d]",goal_Status.status);
//				ROS_INFO("AGV STATUS : GOAL REACH");
//				cancel_pub.publish(cancel);
//				ROS_INFO("Cancel Movement");
        break;

      case status_4_:
        ROS_INFO("AGV STATUS : ABORTED[%d]",goal_Status.status);
        break;

      case status_8_:
        ROS_INFO("AGV STATUS : RECALLED[%d]",goal_Status.status);
        break;

      default:
        break;
    }

  }
  else
  {
//		ROS_INFO("AGV STATUS : IDLING");
  }
}

void NavigationManagement::init_cb(const std_msgs::UInt8::ConstPtr& init_)
{
  ROS_INFO("Init Position");
  init_flag = init_->data;
//	ROS_INFO("init_flag : %d",init_flag);
}

void NavigationManagement::home_cb(const std_msgs::UInt8::ConstPtr& home_sig)
{
  home_flag = home_sig->data;
  target_pos_x = init_pose_[0];
  target_pos_y = init_pose_[1];

  target_ori_z = init_ori[2];
  target_ori_w = init_ori[3];

  start_ = start_null;
  stop_ = stop_null;

  cancelAllGoals(0.01);

  if((fabs(current_x - charge_pose[0]) < 0.2) && (fabs(current_y - charge_pose[1]) < 0.2) && (fabs(current_ori_w - charge_ori[3]) < 0.2))
    // from charger -> move directly home
  {
    start_ = from_charger;
  }
  else if((fabs(current_x - cart_init_pose[0]) < 0.3) && (fabs(current_y - cart_init_pose[1]) < 0.3) && (fabs(current_ori_w - cart_init_ori[3]) < 0.3))
    // from charger -> move directly home
  {
    start_ = from_cart;
  }
  else if ((current_y >= init_pose_[1] - 3) && (current_x<= init_pose_[0]+0.6) && (current_x>=init_pose_[0]-0.6))
  {
    // from home area
    start_ = from_home;
  }
  else if((current_y < charge_pose[1] +3) && (current_y >= charge_pose[1]-0.5) && (current_x< charge_pose[0]+1) && (current_x>charge_pose[0]-3))
  {
    // from charger area
    start_ = from_charger;
  }

  else if((current_y < 6) && (current_x<43) && (current_x>=init_pose_[0]+0.4))
  {
    // from CD
    start_ = from_CD;
  }
  else if(current_y >8 && current_x< 2)
  {
    // from ABL
    start_ = from_ABL;
  }
  else if((current_y >8) && (current_x<43) && (current_x>=init_pose_[0]+0.4))
  {
    // from ABU
    start_ = from_ABU;
  }
  else
  {
    // from undefined
    start_ = start_null;
    move_point_ = no_point_;
  }

    stop_ = to_home;

    switch (start_)
    {
    case from_charger:
    {
      move_point_ = home_;
    }
      break;

    case from_home:
    {
      move_point_ = home_;
    }
      break;
    case from_cart:
    {
      move_point_ = home_;
    }
      break;
    case from_ABL:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABL_;
    }
      break;
    case from_ABU:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = ABU_;
    }
      break;
    case from_CD:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = CD_;
    }
      break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }

}

void NavigationManagement::pgv_cb(const pgv::vision_msg& pgv_msg)
{
  control_id = pgv_msg.ControlID;
  control_ = pgv_msg.Control;
  tag_ = pgv_msg.Tag;
  tag_id = pgv_msg.TagID;
  line_ = pgv_msg.Line;
  line_id = pgv_msg.LineID;
  line_x = pgv_msg.X;
  line_y = pgv_msg.Y;
  line_theta = pgv_msg.theta;
  fault = pgv_msg.Fault;
  warning = pgv_msg.Warning;
  command = pgv_msg.command;

}


void NavigationManagement::tf_feedback_cb(const move_base_msgs::MoveBaseActionFeedback& feedback_)
{
  tf_fb_x	= feedback_.feedback.base_position.pose.position.x;
  tf_fb_y	= feedback_.feedback.base_position.pose.position.y;
  tf_fb_ori_z = feedback_.feedback.base_position.pose.orientation.z;
  tf_fb_ori_w = feedback_.feedback.base_position.pose.orientation.w;

}



void NavigationManagement::obstacle_cb(const obstacle_detector::obstacles_detect& obstacle_msg)
{
//	ROS_INFO("obstacle_cb");
//	ROS_INFO("obstacle number : %d",obstacle_msg.number);


  if(!obstacle_msg.number){
  //	ROS_INFO("No Obstacle detected");
  }
  else if(obstacle_msg.number){
//		ROS_INFO("1 Obstacle detected");
//		cancel_pub.publish(cancel);
//		obstacle_ = true;
//		home_flag = false;
//		wp_ = false;
//		ROS_INFO("AGV Moving Cancel");
  }
  else if(obstacle_msg.number==2){
//		ROS_INFO("2 Obstacle detected");
//		cancel_pub.publish(cancel);
//		obstacle_ = true;
//		home_flag = false;
//		wp_ = false;
//		ROS_INFO("AGV Moving Cancel");
  }
  else if(obstacle_msg.number==3){
//		ROS_INFO("3 Obstacle detected");
//		cancel_pub.publish(cancel);
//		obstacle_ = true;
//		home_flag = false;
//		wp_ = false;
//		ROS_INFO("AGV Moving Cancel");
  }
  else{

  }


}

void NavigationManagement::RTH()//return to home
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");


  goal.target_pose.pose.position.x = init_pose_[0];
  goal.target_pose.pose.position.y = init_pose_[1];
  goal.target_pose.pose.position.z = init_pose_[2];

  goal.target_pose.pose.orientation.x = init_ori[0];
  goal.target_pose.pose.orientation.y = init_ori[1];
  goal.target_pose.pose.orientation.z = init_ori[2];
  goal.target_pose.pose.orientation.w = init_ori[3];

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("RTH() status = 1");

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {

          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            home_flag = 0;
            // Modify Dinh 200608
            state_goal.data = 3;
            reach_pub.publish(state_goal);
            move_point_ = qr_reset_;
          }
          else
          {
            if(stop_ == to_charger) move_point_ = charger1_;
            else if(stop_ == to_ABL) move_point_ = point1_;
            else if(stop_ == to_ABU) move_point_ = wp1_;
            else if(stop_ == to_CD) move_point_ = wp2_;
            else if(stop_ == to_cart) move_point_ = cart_;
            else move_point_ = no_point_;
          }

        }
        else
        {
                ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::wp1_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

 if(stop_ == to_ABU)
 {
   if(target_pos_y>12)
   {
     goal.target_pose.pose.position.x = wp1_pose[0];
           goal.target_pose.pose.position.y = wp1_pose[1];
     goal.target_pose.pose.position.z = wp1_pose[2];

     goal.target_pose.pose.orientation.x = wp1_ori[0];
     goal.target_pose.pose.orientation.y = wp1_ori[1];
     goal.target_pose.pose.orientation.z = wp1_ori[2];
     goal.target_pose.pose.orientation.w = wp1_ori[3];
   }
   else
   {
     goal.target_pose.pose.position.x = wp2_pose[0];
     goal.target_pose.pose.position.y = wp2_pose[1];
     goal.target_pose.pose.position.z = wp2_pose[2];

     goal.target_pose.pose.orientation.x = wp2_ori[0];
     goal.target_pose.pose.orientation.y = wp2_ori[1];
     goal.target_pose.pose.orientation.z = wp2_ori[2];
     goal.target_pose.pose.orientation.w = wp2_ori[3];
   }
 }
 else {
   goal.target_pose.pose.position.x = init_pose_[0];
   goal.target_pose.pose.position.y = init_pose_[1];
   goal.target_pose.pose.position.z = init_pose_[2];

   goal.target_pose.pose.orientation.x = init_ori[0];
   goal.target_pose.pose.orientation.y = init_ori[1];
   goal.target_pose.pose.orientation.z = init_ori[2];
   goal.target_pose.pose.orientation.w = init_ori[3];
 }

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("RTH() status = 1");

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {


          // Modify Dinh 200525
          if(stop_ == to_ABU) move_point_ = ABU_;
          else move_point_ = home_;

        }
        else
        {
            ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::wp2_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

 if(stop_ == to_CD)
 {
   if(target_pos_y<0)
   {
     goal.target_pose.pose.position.x = wp3_pose[0];
     goal.target_pose.pose.position.y = wp3_pose[1];
     goal.target_pose.pose.position.z = wp3_pose[2];

     goal.target_pose.pose.orientation.x = wp3_ori[0];
     goal.target_pose.pose.orientation.y = wp3_ori[1];
     goal.target_pose.pose.orientation.z = wp3_ori[2];
     goal.target_pose.pose.orientation.w = wp3_ori[3];
   }
   else
   {
     goal.target_pose.pose.position.x = wp4_pose[0];
     goal.target_pose.pose.position.y = wp4_pose[1];
     goal.target_pose.pose.position.z = wp4_pose[2];

     goal.target_pose.pose.orientation.x = wp4_ori[0];
     goal.target_pose.pose.orientation.y = wp4_ori[1];
     goal.target_pose.pose.orientation.z = wp4_ori[2];
     goal.target_pose.pose.orientation.w = wp4_ori[3];
   }
 }
 else {
   goal.target_pose.pose.position.x = wp3_pose[0];
   goal.target_pose.pose.position.y = wp3_pose[1];
   goal.target_pose.pose.position.z = wp3_pose[2];

   goal.target_pose.pose.orientation.x = init_ori[0];
   goal.target_pose.pose.orientation.y = init_ori[1];
   goal.target_pose.pose.orientation.z = init_ori[2];
   goal.target_pose.pose.orientation.w = init_ori[3];
 }

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("RTH() status = 1");

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {


          // Modify Dinh 200525
          if(stop_ == to_CD) move_point_ = CD_;
          else move_point_ = rot1_;

        }
        else
        {
            ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::point1_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = point1_pose[0];
        goal.target_pose.pose.position.y = point1_pose[1];
  goal.target_pose.pose.position.z = point1_pose[2];

  goal.target_pose.pose.orientation.x = point1_ori[0];
  goal.target_pose.pose.orientation.y = point1_ori[1];
  goal.target_pose.pose.orientation.z = point1_ori[2];
  goal.target_pose.pose.orientation.w = point1_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {
          if(stop_ == to_ABL) move_point_ = point2_;
          else if(stop_ == to_CD) move_point_ = wp2_;
          else move_point_ = home_;

        }
        else
        {
                ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::point2_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = point2_pose[0];
        goal.target_pose.pose.position.y = point2_pose[1];
  goal.target_pose.pose.position.z = point2_pose[2];

  goal.target_pose.pose.orientation.x = point2_ori[0];
  goal.target_pose.pose.orientation.y = point2_ori[1];
  goal.target_pose.pose.orientation.z = point2_ori[2];
  goal.target_pose.pose.orientation.w = point2_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {

          if(stop_ == to_ABL) move_point_ = point3_;
          else move_point_ = point1_;
        }
        else
        {
                ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::point3_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = point3_pose[0];
        goal.target_pose.pose.position.y = point3_pose[1];
  goal.target_pose.pose.position.z = point3_pose[2];

  goal.target_pose.pose.orientation.x = point3_ori[0];
  goal.target_pose.pose.orientation.y = point3_ori[1];
  goal.target_pose.pose.orientation.z = point3_ori[2];
  goal.target_pose.pose.orientation.w = point3_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {

          if(stop_ == to_ABL) move_point_ = point4_;
          else move_point_ = point2_;
        }
        else
        {
                ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::point4_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");


  if(target_pos_y >12.5)
  {
    goal.target_pose.pose.position.x = wp5_pose[0];
    goal.target_pose.pose.position.y = wp5_pose[1];
    goal.target_pose.pose.orientation.z = wp5_ori[2];
    goal.target_pose.pose.orientation.w = wp5_ori[3];
  }
  else {
    goal.target_pose.pose.position.x = wp6_pose[0];
    goal.target_pose.pose.position.y = wp6_pose[1];
    goal.target_pose.pose.orientation.z = wp6_ori[2];
    goal.target_pose.pose.orientation.w = wp6_ori[3];
  }


   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {

          if(stop_ == to_ABL) move_point_ = ABL_;
          else move_point_ = rot2_;
        }
        else
        {
                ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::rot1_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  if(cur_y > 0)
  {
    goal.target_pose.pose.position.x = wp4_pose[0];
   goal.target_pose.pose.position.y = wp4_pose[1];
    goal.target_pose.pose.position.z = wp4_pose[2];
  }
  else
  {
    goal.target_pose.pose.position.x = wp3_pose[0];
   goal.target_pose.pose.position.y = wp3_pose[1];
    goal.target_pose.pose.position.z = wp3_pose[2];
  }


    goal.target_pose.pose.orientation.x = init_ori[0];
    goal.target_pose.pose.orientation.y = init_ori[1];
    goal.target_pose.pose.orientation.z = init_ori[2];
    goal.target_pose.pose.orientation.w = init_ori[3];


   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
   state_goal.data = 1;
   reach_pub.publish(state_goal);

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {
          if(stop_ == to_charger) move_point_ = charger1_;
          else if(stop_ == to_ABL) move_point_ = point1_;
          else move_point_ = home_;

        }
        else
        {
                ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::rot2_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp5_pose[0];
  goal.target_pose.pose.position.y = wp5_pose[1];
  goal.target_pose.pose.position.z = wp5_pose[2];

  goal.target_pose.pose.orientation.x = point2_ori[0];
  goal.target_pose.pose.orientation.y = point2_ori[1];
  goal.target_pose.pose.orientation.z = point2_ori[2];
  goal.target_pose.pose.orientation.w = point2_ori[3];


   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
   state_goal.data = 1;
   reach_pub.publish(state_goal);

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {
          if(stop_ == to_ABL) move_point_ = point4_;
          else move_point_ = point3_;

        }
        else
        {
                ROS_INFO("AGV failed for some reason");
        }
}


void NavigationManagement::ABL_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.y = wp5_pose[1];
  goal.target_pose.pose.position.z = wp5_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  if(stop_ == to_ABL)
  {
    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;
  }
  else
  {
    goal.target_pose.pose.position.x = cur_x;
    goal.target_pose.pose.orientation.z = current_ori_z;
    goal.target_pose.pose.orientation.w = current_ori_w;
  }


   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {
          // Modify Dinh 200525
          if(stop_ == to_ABL) move_point_ = rack_;
          else move_point_ = point4_;
        }
        else
        {
           ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::ABU_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.y = wp1_pose[1];
  goal.target_pose.pose.position.z = wp1_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  if(stop_ == to_ABU)
  {
    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;
  }
  else
  {
    goal.target_pose.pose.position.x = cur_x;
    goal.target_pose.pose.orientation.z = current_ori_z;
    goal.target_pose.pose.orientation.w = current_ori_w;
  }


   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {
          if(stop_ == to_ABU) move_point_ = rack_;
          else move_point_ = home_;
        }
        else
        {
           ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::CD_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");



  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  if(stop_ == to_CD)
  {
    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;

    if(target_pos_y < 0)
    {
      goal.target_pose.pose.position.y = wp3_pose[1];
      goal.target_pose.pose.position.z = wp3_pose[2];
    }
    else
    {
      goal.target_pose.pose.position.y = wp4_pose[1];
      goal.target_pose.pose.position.z = wp4_pose[2];
    }
  }
  else
  {
    goal.target_pose.pose.position.x = cur_x;
    goal.target_pose.pose.orientation.z = current_ori_z;
    goal.target_pose.pose.orientation.w = current_ori_w;
    if(cur_y < 0)
    {
      goal.target_pose.pose.position.y = wp3_pose[1];
      goal.target_pose.pose.position.z = wp3_pose[2];
    }
    else
    {
      goal.target_pose.pose.position.y = wp4_pose[1];
      goal.target_pose.pose.position.z = wp4_pose[2];
    }
  }


   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {
          if(stop_ == to_CD)
            move_point_ = rack_;
          else move_point_ = rot1_;
        }
        else
        {
                ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::charge_move()
{
  ROS_INFO("AGV Rotate for Charging");
  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";



  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = target_pos_x;
  goal.target_pose.pose.position.y = target_pos_y;
  goal.target_pose.pose.position.z = charge_pose[2];

  goal.target_pose.pose.orientation.x = charge_ori[0];
  goal.target_pose.pose.orientation.y = charge_ori[1];
  goal.target_pose.pose.orientation.z = target_ori_z;
  goal.target_pose.pose.orientation.w = target_ori_w;
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("charge_move() status = 1");

        AGV_move_base->sendGoal(goal);
        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {
          state_goal.data = 3;
          reach_pub.publish(state_goal);
          move_point_ = qr_reset_;

        }
        else
        {
                ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::cart_move()
{
  ROS_INFO("AGV Rotate for Charging");
  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";



  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = target_pos_x;
  goal.target_pose.pose.position.y = target_pos_y;
  goal.target_pose.pose.position.z = cart_init_pose[2];

  goal.target_pose.pose.orientation.x = cart_init_ori[0];
  goal.target_pose.pose.orientation.y = cart_init_ori[1];
  goal.target_pose.pose.orientation.z = target_ori_z;
  goal.target_pose.pose.orientation.w = target_ori_w;
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("charge_move() status = 1");

        AGV_move_base->sendGoal(goal);
        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {
          state_goal.data = 3;
          reach_pub.publish(state_goal);
          move_point_ = qr_reset_;
        }
        else
        {
                ROS_INFO("AGV failed for some reason");
        }
}

void NavigationManagement::charge1_move()
{
  ROS_INFO("AGV Rotate for Charging");
  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";



  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = charge1_pose[0];
  goal.target_pose.pose.position.y = charge1_pose[1];
  goal.target_pose.pose.position.z = charge1_pose[2];

  goal.target_pose.pose.orientation.x = charge1_ori[0];
  goal.target_pose.pose.orientation.y = charge1_ori[1];
  goal.target_pose.pose.orientation.z = charge1_ori[2];
  goal.target_pose.pose.orientation.w = charge1_ori[3];
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("charge_move() status = 1");

        AGV_move_base->sendGoal(goal);
        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        {
            if(stop_ == to_charger) move_point_ = charger_;
            else move_point_ = home_;
        }
        else
        {
                ROS_INFO("AGV failed for some reason");
        }
}




void NavigationManagement::init_pose()
{
  geometry_msgs::PoseWithCovarianceStamped init_;
  init_.header.stamp = ros::Time::now();
  init_.header.frame_id = "map";

/*	init_.pose.pose.position.x = INIT_POS_X;
  init_.pose.pose.position.y = INIT_POS_Y;*/
  init_.pose.pose.position.x = init_pose_[0];
  init_.pose.pose.position.y = init_pose_[1];
  init_.pose.pose.position.z = 0.0;

  init_.pose.pose.orientation.x = 0.0;
  init_.pose.pose.orientation.y = 0.0;
/*	init_.pose.pose.orientation.z = INIT_ORI_Z;
  init_.pose.pose.orientation.w = INIT_ORI_W;*/
  init_.pose.pose.orientation.z = init_ori[2];
  init_.pose.pose.orientation.w = init_ori[3];

  init_pub.publish(init_);

//	init_flag = 0;

  printf("AGV InitialPose Complete\n");

}

void NavigationManagement::param_set()
{

  if(nh.getParam("/init_pose_",init_pose_)){
    for(vector<int>::size_type i=0;i<init_pose_.size();++i){
      ROS_INFO("init_pose : %.2f",init_pose_[i]);
    }
  }

  if(nh.getParam("/init_ori",init_ori)){
    for(vector<int>::size_type i=0;i<init_ori.size();++i){
      ROS_INFO("init_ori : %.2f",init_ori[i]);
    }
  }

  if(nh.getParam("/cart_init_pose",cart_init_pose)){
    for(vector<int>::size_type i=0;i<cart_init_pose.size();++i){
      ROS_INFO("cart_init_pose : %.2f",cart_init_pose[i]);
    }
  }

  if(nh.getParam("/cart_init_ori",cart_init_ori)){
    for(vector<int>::size_type i=0;i<cart_init_ori.size();++i){
      ROS_INFO("cart_init_ori : %.2f",cart_init_ori[i]);
    }
  }

  if(nh.getParam("/charge_pose",charge_pose)){
    for(vector<int>::size_type i=0;i<charge_pose.size();++i){
      ROS_INFO("charge_pose : %.2f",charge_pose[i]);
    }
  }

  if(nh.getParam("/charge_ori",charge_ori)){
    for(vector<int>::size_type i=0;i<charge_ori.size();++i){
      ROS_INFO("charge_ori : %.2f",charge_ori[i]);
    }
  }

  if(nh.getParam("/wp1_pose",wp1_pose)){
    for(vector<int>::size_type i=0;i<wp1_pose.size();++i){
      ROS_INFO("wp1_pose : %.2f",wp1_pose[i]);
    }
  }

  if(nh.getParam("/wp1_ori",wp1_ori)){
    for(vector<int>::size_type i=0;i<wp1_ori.size();++i){
      ROS_INFO("wp1_ori : %.2f",wp1_ori[i]);
    }
  }

  if(nh.getParam("/wp2_pose",wp2_pose)){
    for(vector<int>::size_type i=0;i<wp2_pose.size();++i){
      ROS_INFO("wp2_pose : %.2f",wp2_pose[i]);
    }
  }

  if(nh.getParam("/wp2_ori",wp2_ori)){
    for(vector<int>::size_type i=0;i<wp2_ori.size();++i){
      ROS_INFO("wp2_ori : %.2f",wp2_ori[i]);
    }
  }

  if(nh.getParam("/wp3_pose",wp3_pose)){
    for(vector<int>::size_type i=0;i<wp3_pose.size();++i){
      ROS_INFO("wp3_pose : %.2f",wp3_pose[i]);
    }
  }

  if(nh.getParam("/wp3_ori",wp3_ori)){
    for(vector<int>::size_type i=0;i<wp3_ori.size();++i){
      ROS_INFO("wp3_ori : %.2f",wp3_ori[i]);
    }
  }

  if(nh.getParam("/wp4_pose",wp4_pose)){
    for(vector<int>::size_type i=0;i<wp4_pose.size();++i){
      ROS_INFO("wp4_pose : %.2f",wp4_pose[i]);
    }
  }

  if(nh.getParam("/wp4_ori",wp4_ori)){
    for(vector<int>::size_type i=0;i<wp4_ori.size();++i){
      ROS_INFO("wp4_ori : %.2f",wp4_ori[i]);
    }
  }
  if(nh.getParam("/wp5_pose",wp5_pose)){
    for(vector<int>::size_type i=0;i<wp5_pose.size();++i){
      ROS_INFO("wp5_pose : %.2f",wp5_pose[i]);
    }
  }

  if(nh.getParam("/wp5_ori",wp5_ori)){
    for(vector<int>::size_type i=0;i<wp5_ori.size();++i){
      ROS_INFO("wp5_ori : %.2f",wp5_ori[i]);
    }
  }

  if(nh.getParam("/wp6_pose",wp6_pose)){
    for(vector<int>::size_type i=0;i<wp6_pose.size();++i){
      ROS_INFO("wp6_pose : %.2f",wp6_pose[i]);
    }
  }

  if(nh.getParam("/wp6_ori",wp6_ori)){
    for(vector<int>::size_type i=0;i<wp6_ori.size();++i){
      ROS_INFO("wp6_ori : %.2f",wp6_ori[i]);
    }
  }

  if(nh.getParam("/point1_pose",point1_pose)){
    for(vector<int>::size_type i=0;i<point1_pose.size();++i){
      ROS_INFO("point1_pose : %.2f",point1_pose[i]);
    }
  }

  if(nh.getParam("/point1_ori",point1_ori)){
    for(vector<int>::size_type i=0;i<point1_ori.size();++i){
      ROS_INFO("point1_ori : %.2f",point1_ori[i]);
    }
  }


  if(nh.getParam("/point2_pose",point2_pose)){
    for(vector<int>::size_type i=0;i<point2_pose.size();++i){
      ROS_INFO("point2_pose : %.2f",point2_pose[i]);
    }
  }

  if(nh.getParam("/point2_ori",point2_ori)){
    for(vector<int>::size_type i=0;i<point2_ori.size();++i){
      ROS_INFO("point2_ori : %.2f",point2_ori[i]);
    }
  }


  if(nh.getParam("/point3_pose",point3_pose)){
    for(vector<int>::size_type i=0;i<point3_pose.size();++i){
      ROS_INFO("point3_pose : %.2f",point3_pose[i]);
    }
  }

  if(nh.getParam("/point3_ori",point3_ori)){
    for(vector<int>::size_type i=0;i<point3_ori.size();++i){
      ROS_INFO("point3_ori : %.2f",point3_ori[i]);
    }
  }


  if(nh.getParam("/charge1_pose",charge1_pose)){
    for(vector<int>::size_type i=0;i<charge1_pose.size();++i){
      ROS_INFO("charge1_pose : %.2f",charge1_pose[i]);
    }
  }

  if(nh.getParam("/charge1_ori",charge1_ori)){
    for(vector<int>::size_type i=0;i<charge1_ori.size();++i){
      ROS_INFO("charge1_ori : %.2f",charge1_ori[i]);
    }
  }

}


void NavigationManagement::home_reset()
{
  if(tag_== true && fabs(current_x-target_pos_x)<= 0.15)
  {
    if(goal_Status.status==3 && ((abs(line_x) > 2 && abs(line_x)<100) || (abs(line_y) > 2 && abs(line_y)<100) || (line_theta >175 && line_theta < 185)))
    {
      if(abs(line_x) > 2 && abs(line_x)<100)
      {
         vel_.linear.x = - line_x/abs(line_x) * QR_VEL;
      }
      else vel_.linear.x = 0;
      if(abs(line_y) > 2 && abs(line_y)<100)
      {
         vel_.linear.y = - line_y/abs(line_y) * QR_VEL;
      }
      else vel_.linear.y = 0;

      if(line_theta >175 && line_theta < 185)
      {
        if(line_theta>=181 || line_theta <=179)
        {
          vel_.angular.z = (line_theta-180)/abs(line_theta-180) * QR_AVEL;
        }
        else {
          vel_.angular.z = 0;
        }
      }
      else vel_.angular.z = 0;

      vel_pub.publish(vel_);
    }
    else
    {

      stop_agv();
    }
  }
  else
  {
    move_point_ = no_point_;
    stop_agv();
  }
}

void NavigationManagement::rack_move()
{
//	ROS_INFO("rack_move");
  ROS_INFO("AGV-precision closely approach to the storage Rack");

  if(move_state == 1)
  {
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.header.frame_id = "map";

    while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
      ROS_INFO("Waiting for the move_base action server");

    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.position.y = target_pos_y;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);
    ROS_INFO("AGV-precision is control to the storage rack");

    state_goal.data = 1;
    reach_pub.publish(state_goal);
  //	ROS_INFO("rack_move() status = 1");

    AGV_move_base->sendGoal(goal);

    AGV_move_base->waitForResult();

    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {

      state_goal.data = 3;
      reach_pub.publish(state_goal);
      move_point_ = qr_reset_;
    }
    else
    {
      ROS_INFO("AGV failed for some reason");
    }
  }
  else{
    ROS_INFO("AGV is not de-activate");
  }

}
// Add Dinh 200608
void NavigationManagement::joystickControlCallback(const move_control::teleop::ConstPtr& motorCmd_)
{
  if(motorCmd_->control_mode ==1 || motorCmd_->control_mode ==2)
  {
    manual_mode = true;
    move_point_ = manual_move;
  }
  else manual_mode = false;
}

void NavigationManagement::stop_agv()
{
  vel_.linear.x = 0;
  vel_.linear.y = 0;
  vel_.angular.z = 0;
  vel_pub.publish(vel_);
}
// Modify 200608

void NavigationManagement::qr_move()
{

  if(tag_== true && fabs(current_x-target_pos_x)<= 0.15)
  {
    if(goal_Status.status==3 && ((abs(line_x) > 2 && abs(line_x)<100) || (abs(line_y) > 2 && abs(line_y)<100) || (line_theta >175 && line_theta < 185)))
    {
      if(abs(line_x) > 2 && abs(line_x)<100)
      {
         vel_.linear.x = - line_x/abs(line_x) * QR_VEL;
      }
      else vel_.linear.x = 0;
      if(abs(line_y) > 2 && abs(line_y)<100)
      {
         vel_.linear.y = - line_y/abs(line_y) * QR_VEL;
      }
      else vel_.linear.y = 0;

      if(line_theta >175 && line_theta < 185)
      {
        if(line_theta>=181 || line_theta <=179)
        {
          vel_.angular.z = (line_theta-180)/abs(line_theta-180) * QR_AVEL;
        }
        else {
          vel_.angular.z = 0;
        }
      }
      else vel_.angular.z = 0;

      vel_pub.publish(vel_);
    }
    else
    {

      stop_agv();
    }
  }
  else
  {
    move_point_ = no_point_;
    stop_agv();
  }

}
}



int main(int argc, char** argv)
{
  ros::init(argc, argv, "AGV_nav_action");

  agv_wp::NavigationManagement AGV_nav;
  AGV_nav.spin();
  agv_wp::status st;
  st.active = true;

  return 0;
}



