//WP0 -> ABU_WP -> precision_move -> qr_move
//WP1 -> CD_WP -> precision_move -> qr_move
//Point1 -> Point2 ->Point3 -> Point4 -> ABL_WP -> precision move ->qr_move (rack reset)

//RTH_2 -> ABU_WP -> wp_move1 -> RTH -> qr_move
//RTH_2 -> CD_WP -> wp_move2 -> RTH -> qr_move
//RTH_2 -> ABL_WP -> Point4 -> RTH2 -> Point3 -> Point2 -> Point1 -> RTH ->qr_move (home_reset)

#include <ros/ros.h>
#include "ros/time.h"
#include <vector>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose.h>
#include <actionlib_msgs/GoalStatus.h>
#include <actionlib_msgs/GoalStatusArray.h>
#include <actionlib_msgs/GoalID.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_broadcaster.h>
#include <if_node_msgs/cmdMove.h>
#include <pgv/vision_msg.h>
//#include <move_control/agv_mode.h>
//#include <move_control/qr_vel.h>
#include <obstacle_detector/obstacles_detect.h>
#include <nav_msgs/Odometry.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/LinearMath/Transform.h>
#include <geometry_msgs/PointStamped.h>
#include <move_base_msgs/MoveBaseActionFeedback.h>
//#include <nav_msgs/Feedback.h>

#define QR_VEL 0.017
// Add Dinh 200527
#define QR_AVEL 0.009
#define CART_VEL 0.016
#define QR_NUM 5

using namespace std;

namespace agv_wp {


typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

  enum goal_reach_status{
    status_0_=0,status_1_,status_2_,status_3_,status_4_,status_5_,status_6_,status_7_,status_8_
  };
  enum Wp{
    wp_1=0, wp_2, wp_3, wp_4, wp_5, wp_6, wp_7
  };
  enum Move_WP1{
    move_1_wp1, move_2_wp1
  };
  enum Move_WP2{
    move_1_wp2, move_2_wp2
  };

  // Add Dinh 200519
  enum Move_WP3{
    move_1_wp3, move_2_wp3
  };

  enum QR_Move{
    move_x_, move_y_, move_theta_
  };
  enum Rth{
    rth_0, rth_1, rth_2, rth_3, rth_11, rth_12, rth_13, rth_14, rth_15, rth_16, rth_17 // Add Dinh 200522
  };

  typedef struct status{
    bool pending;
    bool active;
    bool complete;

  } status;

  //	Move move_;
    Move_WP1 MOVE_WP1;
    Move_WP2 MOVE_WP2;

    // Add Dinh 200519
    Move_WP3 MOVE_WP3;

    QR_Move _move;
    Wp WP_;
    Rth RTH_;


  class NavigationManagement{
  public:
    NavigationManagement();
    ~NavigationManagement();
    void init();
    void spin();
    void update();
    void param_set();
    void wp_move1();
    void wp_move2();

    // Add Dinh 200519
    void point1_move();
    void point2_move();
    void point3_move();
    void point4_move();

    void ABU_WP();
    void ABL_WP();

    void CD_WP();
    void RTH();
    void RTH_2();

    void RTH_ROTATION();
    void charge_move();
    void cart_move();
    void init_pose();
    void home_reset();
    void precision_move();
    void qr_move();

    // Add Dinh 200519
    void RTH_ROTATION2();



  private:
    // Add Dinh 200528
    MoveBaseClient  *AGV_move_base;

    bool run_;
    bool target_;
    // Add Dinh 200521
    bool ABU_WP_, ABL_WP_;
    bool CD_WP_;
    bool wp_;
    bool goal_status;
    bool tag_, control_;
    bool pgv_home;
    bool rack_move;
    bool cart_move_;
    bool h_reset;
    bool qr_reset;
    bool obstacle_;
    // Add Dinh 200521
    bool point1_, point2_, point3_, point4_;

    int tag_id, line_, line_id, control_id, fault, warning, command;
    int rate;
    int move_state; //move : 1, stop : 0
    int wp_state;
    int init_flag;
    int home_flag;
    int rth;
    int qr_count;
    float target_pos_x, target_pos_y, target_pos_z,
          target_ori_x, target_ori_y, target_ori_z, target_ori_w;

    // Add Dinh 200528
    float current_x, current_y, current_ori_z, current_ori_w;

    float line_x, line_y, line_theta;
    float tf_fb_x, tf_fb_y, tf_fb_ori_z, tf_fb_ori_w;
    std_msgs::UInt8 state_goal;
    geometry_msgs::Twist vel_;
    geometry_msgs::PoseStamped target_goal;
    geometry_msgs::PoseStamped obstacle_goal;
    actionlib_msgs::GoalStatus goal_Status;
    actionlib_msgs::GoalID cancel;
    move_base_msgs::MoveBaseGoal goal;
    geometry_msgs::PointStamped pose_msgs;


    ros::NodeHandle nh;

    vector<float> init_pose_, init_ori;
    vector<float> cart_init_pose, cart_init_ori;
    vector<float> charge_pose, charge_ori;

    // Add Dinh 200519
    vector<float> wp1_pose, wp2_pose, wp3_pose, wp4_pose, wp5_pose, wp6_pose, point1_pose, point2_pose, point3_pose;
    vector<float> wp1_ori, wp2_ori, wp3_ori, wp4_ori, wp5_ori, wp6_ori,point1_ori, point2_ori, point3_ori;

    ros::Publisher vel_pub, goal_pub, reach_pub, init_pub, cancel_pub, tf_base_link_pub, qr_ang_pub;
    ros::Subscriber reach_sub, goal_sub, init_sub, home_sub, pgv_sub, move_base_feedback_sub, obstacle_sub;
        //cancel_sub;

    void goal_reach_cb(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach);
  //	void goal_target_cb(const geometry_msgs::PoseStamped& goal);
    void goal_target_cb(const if_node_msgs::cmdMove& goal);
    void init_cb(const std_msgs::UInt8::ConstPtr& init_);
    void home_cb(const std_msgs::UInt8::ConstPtr& home_);
    void pgv_cb(const pgv::vision_msg& pgv_msg);
    void tf_feedback_cb(const move_base_msgs::MoveBaseActionFeedback& feedback_);
    void obstacle_cb(const obstacle_detector::obstacles_detect& obstacle_msg);
  //	void cancel_cb(const actionlib_msgs::GoalID& cancel_);

  };

NavigationManagement::NavigationManagement()
{

  init();
  param_set();
  AGV_move_base =  new MoveBaseClient("move_base",true);

  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1, true);
//	goal_pub = nh.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1, true);
  goal_pub = nh.advertise<geometry_msgs::PoseStamped>("/current_goal", 1, true);
  reach_pub = nh.advertise<std_msgs::UInt8>("/IFNode/goal_reach", 1, true);
  init_pub = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>("/initialpose", 1, true);
  cancel_pub = nh.advertise<actionlib_msgs::GoalID>("/move_base/cancel", 1, true);
  tf_base_link_pub = nh.advertise<geometry_msgs::PointStamped>("/agv_actual_position", 1, true);

  reach_sub = nh.subscribe("/move_base/status", 10, &NavigationManagement::goal_reach_cb, this);
//	goal_sub = nh.subscribe("/goal_target", 10, &NavigationManagement::goal_target_cb, this);
  goal_sub = nh.subscribe("/MOVE/CMD", 10, &NavigationManagement::goal_target_cb, this);
  init_sub = nh.subscribe("/init_pose", 1, &NavigationManagement::init_cb, this);
  home_sub = nh.subscribe("/home_pose", 1, &NavigationManagement::home_cb, this);
  pgv_sub = nh.subscribe("/pgv_data", 10, &NavigationManagement::pgv_cb, this);
  move_base_feedback_sub = nh.subscribe("/move_base/feedback", 10, &NavigationManagement::tf_feedback_cb ,this);
  obstacle_sub = nh.subscribe("/obstacle_info", 1, &NavigationManagement::obstacle_cb, this);
}


NavigationManagement::~NavigationManagement()
{
}

void NavigationManagement::init()
{
//	ROS_INFO("variables initial");

  rate = 100; //10; //100
  target_pos_x = 0.0;
  target_pos_y = 0.0;
  target_pos_z = 0.0;

  target_ori_x = 0.0;
  target_ori_y = 0.0;
  target_ori_z = 0.0;
  target_ori_w = 0.99;

  // Add Dinh 200528
  current_x = 0;
  current_y = 0;
  current_ori_w = 0;
  current_ori_z = 0;

  target_ = false;
//	target_ = true;
  ABU_WP_ = false;
  ABL_WP_ = false;
  CD_WP_ = false;

  point1_ =false;
  point2_=false;
  point3_=false;
  point4_=false;

  run_ = true;
  wp_ = false;
  goal_status = true;
  pgv_home = false;
  rack_move =false;
  h_reset = false;
  qr_reset = false;
  obstacle_ = false;
  // Add Dinh 200520
  cart_move_ = false;
  init_flag = 0;
  rth = 0;
  qr_count = 0;

}

void NavigationManagement::update()
{
  //ros::Time::now();

/*
  if(obstacle_){
    ROS_INFO("AGV move cancel");
    ROS_INFO("Obstacle detected");
    cancel_pub.publish(cancel);
    obstacle_ = false;
  }
  else{
//		ROS_INFO("Obstacle not detected");
  }
*/

  if(home_flag){ //return to home

  //	RTH();
    RTH_2();
  //	home_flag = 0;
    pgv_home = true;

  }

  if(init_flag){ //AGV init pose
    init_pose();
    init_flag = 0;
  }

  // Add Dinh 200520
  if(cart_move_)
  {
    cart_move();
  }


  if(wp_){
    switch(WP_){
      case wp_1:
      //	ROS_INFO("wp_move1()");
        wp_move1();//A-B
        break;

      case wp_2:
      //	ROS_INFO("wp_move2()");
        wp_move2();//C-D
        break;
  // Add Dinh 200519
      case wp_3:
        point1_move();//A-B low
        break;

      default:
        break;

    }
  }

  if(ABU_WP_){
    ABU_WP();
  }
  if(CD_WP_){
    CD_WP();
  }

  if(point1_)
  {
    point1_move();
  }
  if(point2_)
  {
    point2_move();
  }
  if(point3_)
  {
    point3_move();
  }
  if(point4_)
  {
    point4_move();
  }
  if(ABL_WP_)
  {
    ABL_WP();
  }
  if(h_reset){
    home_reset();
  }

  if(rack_move){
    precision_move();
  }

  if(qr_reset){
    qr_move();
  }

//	reach_pub.publish(state_goal);

}

void NavigationManagement::spin()
{
  ros::Rate loop_rate(rate);

  while(run_)
  {
    update();
    ros::spinOnce();
    loop_rate.sleep();
  }

}

void NavigationManagement::goal_target_cb(const if_node_msgs::cmdMove& goal)
{
  ROS_INFO("AGV received the coordinate data from ACS");

  // Add Dinh 200526
  /*
  if(fabs(goal.pose.position.x - init_pose_[0])<0.08 && fabs(goal.pose.position.y - init_pose_[1])<0.08)
  {
    home_flag = true;
  }
  else {
    target_pos_x = goal.pose.position.x;
    target_pos_y = goal.pose.position.y;
    target_pos_z = goal.pose.position.z;

    target_ori_x = goal.pose.orientation.x;
    target_ori_y = goal.pose.orientation.y;
    target_ori_z = goal.pose.orientation.z;
    target_ori_w = goal.pose.orientation.w;

    home_flag = false;
  }
*/
  target_pos_x = goal.pose.position.x;
  target_pos_y = goal.pose.position.y;
  target_pos_z = goal.pose.position.z;

  target_ori_x = goal.pose.orientation.x;
  target_ori_y = goal.pose.orientation.y;
  target_ori_z = goal.pose.orientation.z;
  target_ori_w = goal.pose.orientation.w;


  move_state = goal.move;

  // Add Dinh 200528
  // ------------------ Case 1: current pos is home pos ------------------------- //
  if(fabs(tf_fb_x-init_pose_[0])<0.1 && fabs(tf_fb_y-init_pose_[1])<0.1 && fabs(tf_fb_ori_w - init_ori[3])<0.15)
  {
    if(fabs(target_pos_x-cart_init_pose[0])<0.3 && fabs(target_pos_y-cart_init_pose[1])<0.3){ //AGV move to cart-position
      ROS_INFO("AGV is moving the cart-point");
      cart_move_ = true;
      wp_ = false;

    }
    else if(fabs(target_pos_x-charge_pose[0])<0.1 && fabs(target_pos_y-charge_pose[1])<0.1){ //AGV move to charging station

        ROS_INFO("AGV is movig the charging station");
        charge_move();
        wp_ = false;
    }

    else{
      wp_ = true;
      if(target_pos_y >= 10.0){ //A-B rack
        // Add Dinh 200519
        if(target_pos_x >=5)
        {
          WP_ = wp_1;
          ROS_INFO("A-B Rack Up");

          if(target_pos_y > init_pose_[1]){ //left-side(A-B)
          //	move_ = move_1_;
          //	MOVE_ = move_1_;
            MOVE_WP1 = move_1_wp1;
      //			ROS_INFO("[right turn(A-B)] : %d",move_);
            ROS_INFO("[right turn(A-B)]");
          }
          else if(target_pos_y > 10.0 && target_pos_y < init_pose_[1]){//right-side(A-B)
          //	move_ = move_2_;
          //	MOVE_ = move_2_;
            MOVE_WP1 = move_2_wp1;
        //		ROS_INFO("[left turn(A-B)]: %d",move_);
            ROS_INFO("[left turn(A-B)]");
          }
          else
          {
            ROS_INFO("Check again!! Y value");
          }
        }
        else
        {
          WP_ = wp_3;
          ROS_INFO("A-B Rack Low");
        }


      }
      else if(target_pos_y < 10.0){//C-D rack
        WP_ = wp_2;
        ROS_INFO("C-D Rack");

        RTH(); //because AGV move to C-D rack through the colunm

        if(target_pos_y < 0.0){ //rigit-side(C-D)
        //	move_ = move_3_;
        //	MOVE_ = move_3_;
          MOVE_WP2 = move_1_wp2;
    //			ROS_INFO("[right turn(C-D)] : %d",move_);
          ROS_INFO("[left turn(C-D)]");
        }
        else if(target_pos_y >= 0.0 && target_pos_y <= 6.0){ //left-side(C-D)
        //	move_ = move_4_;
        //	MOVE_ = move_4_;
          MOVE_WP2 = move_2_wp2;
    //			ROS_INFO("[left turn(C-D)] : %d",move_);
          ROS_INFO("[right turn(C-D)]");
        }
        else{
          ROS_INFO("Check again!! Y value");
        }

      }
    }

  }// end case 1
  else {

  }


}

/*
void NavigationManagement::cancel_cb(const actionlib_msgs::GoalID& cancel_)
{
  ROS_INFO("AGV Command canceled");
  cancel_pub.publish(cancel_);

}
*/

void NavigationManagement::goal_reach_cb(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach)
{
  if(!goal_reach->status_list.empty()){
//		actionlib_msgs::GoalStatus goal_Status = goal_reach->status_list[0];
    goal_Status = goal_reach->status_list[0];
    state_goal.data = goal_Status.status;
//		state_goal.data = goal_reach->status_list[0];
//		cancel.id = "";

    switch(goal_Status.status){
      case status_0_:
        ROS_INFO("AGV STATUS : PENDING[%d]",goal_Status.status);
        break;

      case status_1_:
//				ROS_INFO("AGV STATUS : ACTIVE[%d]",goal_Status.status);
//				ROS_INFO("AGV STATUS : ACTIVE");
        break;

      case status_3_:
//				ROS_INFO("AGV STATUS : COMPLETE[%d]",goal_Status.status);
//				ROS_INFO("AGV STATUS : GOAL REACH");
//				cancel_pub.publish(cancel);
//				ROS_INFO("Cancel Movement");
        break;

      case status_4_:
        ROS_INFO("AGV STATUS : ABORTED[%d]",goal_Status.status);
        break;

      case status_8_:
        ROS_INFO("AGV STATUS : RECALLED[%d]",goal_Status.status);
        break;

      default:
        break;
    }

  }
  else
  {
//		ROS_INFO("AGV STATUS : IDLING");
  }
}

void NavigationManagement::init_cb(const std_msgs::UInt8::ConstPtr& init_)
{
  ROS_INFO("Init Position");
  init_flag = init_->data;
//	ROS_INFO("init_flag : %d",init_flag);
}

void NavigationManagement::home_cb(const std_msgs::UInt8::ConstPtr& home_)
{
  ROS_INFO("Return To Home Sweet Home");
  home_flag = home_->data;

  // Add Dinh 200526
    if(target_pos_x>8.5)
    {

    if(target_pos_y >=10.0)
    { //A-B Rack
      rth = 0;
    }
    else if(target_pos_y <6) // CD rack
    {
      RTH_ =rth_0;
    }
    }
    else if(target_pos_x<2) {
      RTH_ = rth_17;
    }


//	ROS_INFO("home_flag : %d",home_flag);

  // Modify Dinh 200521
  //RTH_ = rth_0;
}

void NavigationManagement::pgv_cb(const pgv::vision_msg& pgv_msg)
{
  control_id = pgv_msg.ControlID;
  control_ = pgv_msg.Control;
  tag_ = pgv_msg.Tag;
  tag_id = pgv_msg.TagID;
  line_ = pgv_msg.Line;
  line_id = pgv_msg.LineID;
  line_x = pgv_msg.X;
  line_y = pgv_msg.Y;
  line_theta = pgv_msg.theta;
  fault = pgv_msg.Fault;
  warning = pgv_msg.Warning;
  command = pgv_msg.command;

}



void NavigationManagement::tf_feedback_cb(const move_base_msgs::MoveBaseActionFeedback& feedback_)
{
  tf_fb_x	= feedback_.feedback.base_position.pose.position.x;
  tf_fb_y	= feedback_.feedback.base_position.pose.position.y;
  tf_fb_ori_z = feedback_.feedback.base_position.pose.orientation.z;
  tf_fb_ori_w = feedback_.feedback.base_position.pose.orientation.w;

}



void NavigationManagement::obstacle_cb(const obstacle_detector::obstacles_detect& obstacle_msg)
{
//	ROS_INFO("obstacle_cb");
//	ROS_INFO("obstacle number : %d",obstacle_msg.number);


  if(!obstacle_msg.number){
  //	ROS_INFO("No Obstacle detected");
  }
  else if(obstacle_msg.number){
//		ROS_INFO("1 Obstacle detected");
//		cancel_pub.publish(cancel);
//		obstacle_ = true;
//		home_flag = false;
//		wp_ = false;
//		ROS_INFO("AGV Moving Cancel");
  }
  else if(obstacle_msg.number==2){
//		ROS_INFO("2 Obstacle detected");
//		cancel_pub.publish(cancel);
//		obstacle_ = true;
//		home_flag = false;
//		wp_ = false;
//		ROS_INFO("AGV Moving Cancel");
  }
  else if(obstacle_msg.number==3){
//		ROS_INFO("3 Obstacle detected");
//		cancel_pub.publish(cancel);
//		obstacle_ = true;
//		home_flag = false;
//		wp_ = false;
//		ROS_INFO("AGV Moving Cancel");
  }
  else{

  }


}

void NavigationManagement::RTH()//return to home
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = init_pose_[0];
        goal.target_pose.pose.position.y = init_pose_[1];
  goal.target_pose.pose.position.z = init_pose_[2];

  goal.target_pose.pose.orientation.x = init_ori[0];
  goal.target_pose.pose.orientation.y = init_ori[1];
  goal.target_pose.pose.orientation.z = init_ori[2];
        goal.target_pose.pose.orientation.w = init_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("RTH() status = 1");

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                ROS_INFO("AGV has reached to home");

    state_goal.data = 3;
    reach_pub.publish(state_goal);

    // Modify Dinh 200525
    if(home_flag)
    {
      home_flag = 0;
      rth = 0;
      h_reset = true;
      _move = move_x_;
    }
//		ROS_INFO("RTH() status = 3");
/*
    if(!home_flag){
      wp_ = false;
    }
    else{
      RTH_ = rth_3;
      wp_ = false;
//			ROS_INFO("RTH_ROTATION : rth_3");
    }
*/
        }
        else{
                ROS_INFO("AGV failed for some reason");
        }
// Modify Dinh 200525
  //home_flag = 0;
  //rth = 0;

  //h_reset = true;
  //_move = move_x_;
}

void NavigationManagement::RTH_2()
{
  ROS_INFO("RTH_2()");

  /*obstacle_goal.pose.position.x = init_pose_[0];
  obstacle_goal.pose.position.y = init_pose_[1];
  obstacle_goal.pose.position.z = init_pose_[2];

  obstacle_goal.pose.orientation.x = init_ori[0];
  obstacle_goal.pose.orientation.y = init_ori[1];
  obstacle_goal.pose.orientation.z = init_ori[2];
  obstacle_goal.pose.orientation.w = init_ori[3];

  goal_pub.publish(obstacle_goal);*/
//	ROS_INFO("publish home obstacle_goal");
// Add Dinh 200520
  if(target_pos_x>8.5)
  {

  if(target_pos_y >=10.0)
  { //A-B Rack
    switch(rth)
    {
      case 0:
        ABU_WP();
        break;

      case 1:
        RTH();
        break;

      default:
        break;
    } //end switch

  } //end if target_y1
  else if(target_pos_y<10.0){ //C-D Rack
//		switch(rth){
    switch(RTH_){
      case rth_0:
//				ROS_INFO("case rth_0 : CD_WP");
        CD_WP();
        break;

      case rth_1:
//				ROS_INFO("case rth_1 : wp_move2()");
        wp_move2();
        break;

      case rth_2:
//				ROS_INFO("case rth_2 : RTH_ROTATION");
        RTH_ROTATION();
        break;

      case rth_3:
//				ROS_INFO("case rth_3 : RTH");
        RTH();
        break;

/*			case 3:
        wp_move1();
        break;
*/
      default:
        break;
    } //end switch

    } // end if target_y2
    } // end if target_x1
  else if(target_pos_x<2)
  {
    // Return home for AB low area
    switch(RTH_)
    {
      case rth_11:
        RTH();
      break;
    case rth_12:
      point1_move();
      break;
    case rth_13:
      point2_move();
      break;
    case rth_14:
      point3_move();
      break;
    case rth_15:
      RTH_ROTATION2();
      break;
    case rth_16:
      point4_move();
      break;
    case rth_17:
      ABL_WP();
      break;
    default:
      break;

    } // end switch
  }
  else {
    RTH();
  }
}


void NavigationManagement::RTH_ROTATION()
{
  ROS_INFO("RTH_ROTATION()");

/*
  target_goal.header.stamp = ros::Time::now();
  target_goal.header.frame_id = "map";

  target_goal.pose.position.x = wp3_pose[0];
        target_goal.pose.position.y = wp3_pose[1];
  target_goal.pose.position.z = 0.0;

  target_goal.pose.orientation.x= 0.0;
  target_goal.pose.orientation.y= 0.0;
  target_goal.pose.orientation.z = init_ori[2];
        target_goal.pose.orientation.w = init_ori[3];

  goal_pub.publish(target_goal);

  if(!home_flag){
      wp_ = false;
  }
  else{
    RTH_ = rth_3;
    wp_ = false;
  }
*/
   goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";



        while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
                ROS_INFO("Waiting for the move_base action server");

        if(target_pos_y<0)
{

        goal.target_pose.pose.position.x = wp3_pose[0];
        goal.target_pose.pose.position.y = wp3_pose[1];
        goal.target_pose.pose.position.z = wp3_pose[2];

        goal.target_pose.pose.orientation.x = init_ori[0];
        goal.target_pose.pose.orientation.y = init_ori[1];
        goal.target_pose.pose.orientation.z = init_ori[2];
        goal.target_pose.pose.orientation.w = init_ori[3];
}
else{

  goal.target_pose.pose.position.x = wp4_pose[0];
        goal.target_pose.pose.position.y = wp4_pose[1];
  goal.target_pose.pose.position.z = wp4_pose[2];

  goal.target_pose.pose.orientation.x = init_ori[0];
  goal.target_pose.pose.orientation.y = init_ori[1];
  goal.target_pose.pose.orientation.z = init_ori[2];
        goal.target_pose.pose.orientation.w = init_ori[3];
}
  ROS_INFO("AGV rotate for home");
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("RTH_ROTATION status = 1");

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                ROS_INFO("AGV has rotated completely");

  //	state_goal.data = 3;
  //	reach_pub.publish(state_goal);
  //	ROS_INFO("state_goal.data = 3");

    if(!home_flag){
      wp_ = false;
    }
    else{
      RTH_ = rth_3;
      wp_ = false;
    }
        }
        else{
                ROS_INFO("AGV failed for some reason");
        }

}

void NavigationManagement::RTH_ROTATION2()
{
  ROS_INFO("RTH_ROTATION()");

/*
  target_goal.header.stamp = ros::Time::now();
  target_goal.header.frame_id = "map";

  target_goal.pose.position.x = wp3_pose[0];
        target_goal.pose.position.y = wp3_pose[1];
  target_goal.pose.position.z = 0.0;

  target_goal.pose.orientation.x= 0.0;
  target_goal.pose.orientation.y= 0.0;
  target_goal.pose.orientation.z = init_ori[2];
        target_goal.pose.orientation.w = init_ori[3];

  goal_pub.publish(target_goal);

  if(!home_flag){
      wp_ = false;
  }
  else{
    RTH_ = rth_3;
    wp_ = false;
  }
*/
   goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";



        while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
                ROS_INFO("Waiting for the move_base action server");



  goal.target_pose.pose.position.x = wp5_pose[0];
  goal.target_pose.pose.position.y = wp5_pose[1];
  goal.target_pose.pose.position.z = wp5_pose[2];

  goal.target_pose.pose.orientation.x = point2_ori[0];
  goal.target_pose.pose.orientation.y = point2_ori[1];
  goal.target_pose.pose.orientation.z = point2_ori[2];
  goal.target_pose.pose.orientation.w = point2_ori[3];

  ROS_INFO("AGV rotate for home");
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("RTH_ROTATION status = 1");

        AGV_move_base->sendGoal(goal);

        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                ROS_INFO("AGV has rotated completely");

  //	state_goal.data = 3;
  //	reach_pub.publish(state_goal);
  //	ROS_INFO("state_goal.data = 3");

    if(!home_flag){
      point4_ = false;
    }
    else{
      RTH_ = rth_14;
      point4_ = false;
    }
        }
        else{
                ROS_INFO("AGV failed for some reason");
        }

}

// Need to modify for AGV1 and AGV2
void NavigationManagement::charge_move()
{
  ROS_INFO("AGV Rotate for Charging");
  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";



  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = init_pose_[0];
        goal.target_pose.pose.position.y = init_pose_[1];
  goal.target_pose.pose.position.z = init_pose_[2];

  goal.target_pose.pose.orientation.x = charge_ori[0];
  goal.target_pose.pose.orientation.y = charge_ori[1];
  goal.target_pose.pose.orientation.z = charge_ori[2];
        goal.target_pose.pose.orientation.w = charge_ori[3];
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("charge_move() status = 1");

        AGV_move_base->sendGoal(goal);
        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                ROS_INFO("AGV has rotated completely for charging");

  //	state_goal.data = 3;
  //	reach_pub.publish(state_goal);
  //	ROS_INFO("charge_move() status = 3");
        }
        else{
                ROS_INFO("AGV failed for some reason");
        }


  goal.target_pose.pose.position.x = target_pos_x;
        goal.target_pose.pose.position.y = target_pos_y;
  goal.target_pose.pose.position.z = target_pos_z;

  goal.target_pose.pose.orientation.x = target_ori_x;
  goal.target_pose.pose.orientation.y = target_ori_y;
  goal.target_pose.pose.orientation.z = target_ori_z;
        goal.target_pose.pose.orientation.w = target_ori_w;

  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("charge_move() status = 1");

        AGV_move_base->sendGoal(goal);
        AGV_move_base->waitForResult();

        if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                ROS_INFO("AGV has moved to the charge station completely");

    state_goal.data = 3;
    reach_pub.publish(state_goal);
//		ROS_INFO("charge_move() status = 3");
  }
        else
                ROS_INFO("AGV failed for some reason");

}

void NavigationManagement::init_pose()
{
  geometry_msgs::PoseWithCovarianceStamped init_;
  init_.header.stamp = ros::Time::now();
  init_.header.frame_id = "map";

/*	init_.pose.pose.position.x = INIT_POS_X;
  init_.pose.pose.position.y = INIT_POS_Y;*/
  init_.pose.pose.position.x = init_pose_[0];
  init_.pose.pose.position.y = init_pose_[1];
  init_.pose.pose.position.z = 0.0;

  init_.pose.pose.orientation.x = 0.0;
  init_.pose.pose.orientation.y = 0.0;
/*	init_.pose.pose.orientation.z = INIT_ORI_Z;
  init_.pose.pose.orientation.w = INIT_ORI_W;*/
  init_.pose.pose.orientation.z = init_ori[2];
  init_.pose.pose.orientation.w = init_ori[3];

  init_pub.publish(init_);

//	init_flag = 0;

  printf("AGV InitialPose Complete\n");

}

void NavigationManagement::param_set()
{

  if(nh.getParam("/init_pose_",init_pose_)){
    for(vector<int>::size_type i=0;i<init_pose_.size();++i){
      ROS_INFO("init_pose : %.2f",init_pose_[i]);
    }
  }

  if(nh.getParam("/init_ori",init_ori)){
    for(vector<int>::size_type i=0;i<init_ori.size();++i){
      ROS_INFO("init_ori : %.2f",init_ori[i]);
    }
  }

  if(nh.getParam("/cart_init_pose",cart_init_pose)){
    for(vector<int>::size_type i=0;i<cart_init_pose.size();++i){
      ROS_INFO("cart_init_pose : %.2f",cart_init_pose[i]);
    }
  }

  if(nh.getParam("/cart_init_ori",cart_init_ori)){
    for(vector<int>::size_type i=0;i<cart_init_ori.size();++i){
      ROS_INFO("cart_init_ori : %.2f",cart_init_ori[i]);
    }
  }

  if(nh.getParam("/charge_pose",charge_pose)){
    for(vector<int>::size_type i=0;i<charge_pose.size();++i){
      ROS_INFO("charge_pose : %.2f",charge_pose[i]);
    }
  }

  if(nh.getParam("/charge_ori",charge_ori)){
    for(vector<int>::size_type i=0;i<charge_ori.size();++i){
      ROS_INFO("charge_ori : %.2f",charge_ori[i]);
    }
  }

  if(nh.getParam("/wp1_pose",wp1_pose)){
    for(vector<int>::size_type i=0;i<wp1_pose.size();++i){
      ROS_INFO("wp1_pose : %.2f",wp1_pose[i]);
    }
  }

  if(nh.getParam("/wp1_ori",wp1_ori)){
    for(vector<int>::size_type i=0;i<wp1_ori.size();++i){
      ROS_INFO("wp1_ori : %.2f",wp1_ori[i]);
    }
  }

  if(nh.getParam("/wp2_pose",wp2_pose)){
    for(vector<int>::size_type i=0;i<wp2_pose.size();++i){
      ROS_INFO("wp2_pose : %.2f",wp2_pose[i]);
    }
  }

  if(nh.getParam("/wp2_ori",wp2_ori)){
    for(vector<int>::size_type i=0;i<wp2_ori.size();++i){
      ROS_INFO("wp2_ori : %.2f",wp2_ori[i]);
    }
  }

  if(nh.getParam("/wp3_pose",wp3_pose)){
    for(vector<int>::size_type i=0;i<wp3_pose.size();++i){
      ROS_INFO("wp3_pose : %.2f",wp3_pose[i]);
    }
  }

  if(nh.getParam("/wp3_ori",wp3_ori)){
    for(vector<int>::size_type i=0;i<wp3_ori.size();++i){
      ROS_INFO("wp3_ori : %.2f",wp3_ori[i]);
    }
  }

  if(nh.getParam("/wp4_pose",wp4_pose)){
    for(vector<int>::size_type i=0;i<wp4_pose.size();++i){
      ROS_INFO("wp4_pose : %.2f",wp4_pose[i]);
    }
  }

  if(nh.getParam("/wp4_ori",wp4_ori)){
    for(vector<int>::size_type i=0;i<wp4_ori.size();++i){
      ROS_INFO("wp4_ori : %.2f",wp4_ori[i]);
    }
  }
  if(nh.getParam("/wp5_pose",wp5_pose)){
    for(vector<int>::size_type i=0;i<wp5_pose.size();++i){
      ROS_INFO("wp5_pose : %.2f",wp5_pose[i]);
    }
  }

  if(nh.getParam("/wp5_ori",wp5_ori)){
    for(vector<int>::size_type i=0;i<wp5_ori.size();++i){
      ROS_INFO("wp5_ori : %.2f",wp5_ori[i]);
    }
  }

  if(nh.getParam("/wp6_pose",wp6_pose)){
    for(vector<int>::size_type i=0;i<wp6_pose.size();++i){
      ROS_INFO("wp6_pose : %.2f",wp6_pose[i]);
    }
  }

  if(nh.getParam("/wp6_ori",wp6_ori)){
    for(vector<int>::size_type i=0;i<wp6_ori.size();++i){
      ROS_INFO("wp6_ori : %.2f",wp6_ori[i]);
    }
  }

  if(nh.getParam("/point1_pose",point1_pose)){
    for(vector<int>::size_type i=0;i<point1_pose.size();++i){
      ROS_INFO("point1_pose : %.2f",point1_pose[i]);
    }
  }

  if(nh.getParam("/point1_ori",point1_ori)){
    for(vector<int>::size_type i=0;i<point1_ori.size();++i){
      ROS_INFO("point1_ori : %.2f",point1_ori[i]);
    }
  }


  if(nh.getParam("/point2_pose",point2_pose)){
    for(vector<int>::size_type i=0;i<point2_pose.size();++i){
      ROS_INFO("point2_pose : %.2f",point2_pose[i]);
    }
  }

  if(nh.getParam("/point2_ori",point2_ori)){
    for(vector<int>::size_type i=0;i<point2_ori.size();++i){
      ROS_INFO("point2_ori : %.2f",point2_ori[i]);
    }
  }


  if(nh.getParam("/point3_pose",point3_pose)){
    for(vector<int>::size_type i=0;i<point3_pose.size();++i){
      ROS_INFO("point3_pose : %.2f",point3_pose[i]);
    }
  }

  if(nh.getParam("/point3_ori",point3_ori)){
    for(vector<int>::size_type i=0;i<point3_ori.size();++i){
      ROS_INFO("point3_ori : %.2f",point3_ori[i]);
    }
  }
}

void NavigationManagement::wp_move1() //A-B Rack
{
  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";



  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

//	ROS_INFO("MOVE_WP1 : %d",MOVE_WP1);

  switch(MOVE_WP1){

    case move_1_wp1: //left-side(right-turn)

      if(move_state == 1){
        goal.target_pose.pose.position.x = wp1_pose[0];
              goal.target_pose.pose.position.y = wp1_pose[1];
        goal.target_pose.pose.orientation.z = wp1_ori[2];
              goal.target_pose.pose.orientation.w = wp1_ori[3];

        ROS_INFO("AGV move to the left-side(A-B)");
          // Add Dinh 200519
          obstacle_goal.pose = goal.target_pose.pose;
          goal_pub.publish(obstacle_goal);
        state_goal.data = 1;
        reach_pub.publish(state_goal);
      //	ROS_INFO("wp_move1() status = 1");

              AGV_move_base->sendGoal(goal);

              AGV_move_base->waitForResult();

              if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                      ROS_INFO("AGV has arrived to the left-side(A-B)");
                      //target_ = true;
                ABU_WP_ = true;
          wp_ = false;
        //	rth = 3;

        //	state_goal.data = 3;
        //	reach_pub.publish(state_goal);
        //	ROS_INFO("state_goal.data = 3");
              }
              else{
                      ROS_INFO("AGV failed for some reason");
              }
      }
      else{
        ROS_INFO("AGV is not de-activate");
      }

      break;

    case move_2_wp1: //right-side(left-turn)

      if(move_state == 1){
        goal.target_pose.pose.position.x = wp2_pose[0];
              goal.target_pose.pose.position.y = wp2_pose[1];
        goal.target_pose.pose.orientation.z = wp2_ori[2];
              goal.target_pose.pose.orientation.w = wp2_ori[3];

        ROS_INFO("AGV move to the right-side(A-B)");
  // Add Dinh 200519
          obstacle_goal.pose = goal.target_pose.pose;
          goal_pub.publish(obstacle_goal);
        state_goal.data = 1;
        reach_pub.publish(state_goal);
      //	ROS_INFO("wp_move1() status = 1");

              AGV_move_base->sendGoal(goal);

              AGV_move_base->waitForResult();

              if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                      ROS_INFO("AGV has arrived to the right-side(A-B)");
                      //target_ = true;
                      ABU_WP_ = true;
          wp_ = false;
        //	rth = 3;

        //	state_goal.data = 3;
        //	reach_pub.publish(state_goal);
        //	ROS_INFO("state_goal.data = 3");
              }
              else{
                      ROS_INFO("AGV failed for some reason");
              }
      }
      else{
        ROS_INFO("AGV is not de-activate");
      }

      break;

    default:
      break;
  }

}

void NavigationManagement::point1_move()
{
  //	ROS_INFO("point1");
    goal.target_pose.header.stamp = ros::Time::now();
          goal.target_pose.header.frame_id = "map";



    while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
      ROS_INFO("Waiting for the move_base action server");

    if(move_state == 1){
      goal.target_pose.pose.position.x = point1_pose[0];
            goal.target_pose.pose.position.y = point1_pose[1];
      goal.target_pose.pose.orientation.z = point1_ori[2];
            goal.target_pose.pose.orientation.w = point1_ori[3];

      ROS_INFO("AGV move to the point1");
      // Add Dinh 200519
      obstacle_goal.pose = goal.target_pose.pose;
      goal_pub.publish(obstacle_goal);
      state_goal.data = 1;
      reach_pub.publish(state_goal);
    //	ROS_INFO("wp_move2() status = 1");

            AGV_move_base->sendGoal(goal);

            AGV_move_base->waitForResult();

            if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                    ROS_INFO("AGV has arrived to the point1");
        point1_ = false;
        // Add Dinh 200526
        wp_ = false;
        if(!home_flag){
          point2_ = true;
        //	rack_move = true;
        }
        else{
        //	rth = 2;
          RTH_ = rth_11;
          point2_ = false;

        }
            }
            else{
                    ROS_INFO("AGV failed for some reason");
            }
    }
    else{
      ROS_INFO("AGV is not de-activate");
    }
}

void NavigationManagement::point2_move()
{
  //	ROS_INFO("point2");
    goal.target_pose.header.stamp = ros::Time::now();
          goal.target_pose.header.frame_id = "map";



    while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
      ROS_INFO("Waiting for the move_base action server");

    if(move_state == 1){
      goal.target_pose.pose.position.x = point2_pose[0];
            goal.target_pose.pose.position.y = point2_pose[1];
      goal.target_pose.pose.orientation.z = point2_ori[2];
            goal.target_pose.pose.orientation.w = point2_ori[3];

      ROS_INFO("AGV move to the point2");
      // Add Dinh 200519
      obstacle_goal.pose = goal.target_pose.pose;
      goal_pub.publish(obstacle_goal);
      state_goal.data = 1;
      reach_pub.publish(state_goal);
    //	ROS_INFO("wp_move2() status = 1");

            AGV_move_base->sendGoal(goal);

            AGV_move_base->waitForResult();

            if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                    ROS_INFO("AGV has arrived to the point2)");
        point2_ = false;
        // Add Dinh 200526
        wp_ = false;
        if(!home_flag){
          point3_ = true;
          point1_ = false;

        //	rack_move = true;
        }
        else{
        //	rth = 2;
          RTH_ = rth_12;
          point3_ = false;
        }
            }
            else{
                    ROS_INFO("AGV failed for some reason");
            }
    }
    else{
      ROS_INFO("AGV is not de-activate");
    }
}

void NavigationManagement::point3_move()
{
  //	ROS_INFO("point3");
    goal.target_pose.header.stamp = ros::Time::now();
          goal.target_pose.header.frame_id = "map";



    while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
      ROS_INFO("Waiting for the move_base action server");

    if(move_state == 1){
      goal.target_pose.pose.position.x = point3_pose[0];
            goal.target_pose.pose.position.y = point3_pose[1];
      goal.target_pose.pose.orientation.z = point3_ori[2];
            goal.target_pose.pose.orientation.w = point3_ori[3];

      ROS_INFO("AGV move to the point3");
      // Add Dinh 200519
      obstacle_goal.pose = goal.target_pose.pose;
      goal_pub.publish(obstacle_goal);
      state_goal.data = 1;
      reach_pub.publish(state_goal);
    //	ROS_INFO("wp_move2() status = 1");

            AGV_move_base->sendGoal(goal);

            AGV_move_base->waitForResult();

            if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                    ROS_INFO("AGV has arrived to the point2)");
        point3_ = false;
        // Add Dinh 200526
        wp_ = false;
        if(!home_flag){
          point4_ = true;
          point2_ = false;
        //	rack_move = true;
        }
        else{
        //	rth = 2;
          RTH_ = rth_13;
          point4_ = false;

        }
            }
            else{
                    ROS_INFO("AGV failed for some reason");
            }
    }
    else{
      ROS_INFO("AGV is not de-activate");
    }
}

void NavigationManagement::point4_move()
{
  //	ROS_INFO("point4");
    goal.target_pose.header.stamp = ros::Time::now();
          goal.target_pose.header.frame_id = "map";



    while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
      ROS_INFO("Waiting for the move_base action server");

    if(move_state == 1){
      if(target_pos_y >12.5)
      {
        goal.target_pose.pose.position.x = wp5_pose[0];
              goal.target_pose.pose.position.y = wp5_pose[1];
        goal.target_pose.pose.orientation.z = wp5_ori[2];
              goal.target_pose.pose.orientation.w = wp5_ori[3];
      }
      else {
        goal.target_pose.pose.position.x = wp6_pose[0];
              goal.target_pose.pose.position.y = wp6_pose[1];
        goal.target_pose.pose.orientation.z = wp6_ori[2];
              goal.target_pose.pose.orientation.w = wp6_ori[3];
      }


      ROS_INFO("AGV move to the point4");
      // Add Dinh 200519
      obstacle_goal.pose = goal.target_pose.pose;
      goal_pub.publish(obstacle_goal);
      state_goal.data = 1;
      reach_pub.publish(state_goal);
    //	ROS_INFO("wp_move2() status = 1");

            AGV_move_base->sendGoal(goal);

            AGV_move_base->waitForResult();

            if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                    ROS_INFO("AGV has arrived to the right-side(C-D)");
        point4_ = false;
        // Add Dinh 200526
        wp_ = false;
        if(!home_flag){
          ABL_WP_ = true;
          point3_ = false;
        //	rack_move = true;
        }
        else{
        //	rth = 2;
          RTH_ = rth_15;
          ABL_WP_ = false;

        }
            }
            else{
                    ROS_INFO("AGV failed for some reason");
            }
    }
    else{
      ROS_INFO("AGV is not de-activate");
    }
}

void NavigationManagement::wp_move2()//C-D Rack
{
//	ROS_INFO("wp_move2()");
  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";



  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  switch(MOVE_WP2){

    case move_1_wp2://left-side(right-turn)

      if(move_state == 1){
        goal.target_pose.pose.position.x = wp3_pose[0];
              goal.target_pose.pose.position.y = wp3_pose[1];
        goal.target_pose.pose.orientation.z = wp3_ori[2];
              goal.target_pose.pose.orientation.w = wp3_ori[3];

        ROS_INFO("AGV move to the right-side(C-D)");
          // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);
        state_goal.data = 1;
        reach_pub.publish(state_goal);
      //	ROS_INFO("wp_move2() status = 1");

              AGV_move_base->sendGoal(goal);

              AGV_move_base->waitForResult();

              if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                      ROS_INFO("AGV has arrived to the right-side(C-D)");
                    //	target_ = true;
                    //	CD_WP_ = true;
        //	wp_ = false;

        //	state_goal.data = 3;
        //	reach_pub.publish(state_goal);
        //	ROS_INFO("state_goal.data = 3");

          if(!home_flag){
            CD_WP_ = true;
            wp_ = false;
          //	rack_move = true;
          }
          else{
          //	rth = 2;
            RTH_ = rth_2;
            wp_ = false;
          }
              }
              else{
                      ROS_INFO("AGV failed for some reason");
              }
      }
      else{
        ROS_INFO("AGV is not de-activate");
      }

      break;

    case move_2_wp2://right-side(left-turn)

      if(move_state == 1){
        goal.target_pose.pose.position.x = wp4_pose[0];
              goal.target_pose.pose.position.y = wp4_pose[1];
        goal.target_pose.pose.orientation.z = wp4_ori[2];
              goal.target_pose.pose.orientation.w = wp4_ori[3];

        ROS_INFO("AGV move to the left-side(C-D)");
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);
        state_goal.data = 1;
        reach_pub.publish(state_goal);
      //	ROS_INFO("wp_move2() status = 1");

              AGV_move_base->sendGoal(goal);

              AGV_move_base->waitForResult();

              if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                      ROS_INFO("AGV has arrived to the left-side(C-D)");
                    //	target_ = true;
                    //	CD_WP_ = true;
        //	wp_ = false;


        //	state_goal.data = 3;
        //	reach_pub.publish(state_goal);
        //	ROS_INFO("state_goal.data = 3");

          if(!home_flag){
            CD_WP_ = true;
            wp_ = false;
          //	rack_move = true;
          }
          else{
          //	rth = 2;
            RTH_ = rth_2;
            wp_ = false;
          }
              }
              else{
                      ROS_INFO("AGV failed for some reason");
              }
      }
      else{
        ROS_INFO("AGV is not de-activate");
      }

      break;

    default:
      break;
  }
}

void NavigationManagement::ABU_WP() //A-B Rack Moving
{


  if(move_state == 1){
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.header.frame_id = "map";



    while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
      ROS_INFO("Waiting for the move_base action server");

    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.position.y = wp1_pose[1];

    goal.target_pose.pose.orientation.z = target_ori_z;
          goal.target_pose.pose.orientation.w = target_ori_w;

    ROS_INFO("AGV move to the A-B storage rack");
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

    state_goal.data = 1;
    reach_pub.publish(state_goal);
  //	ROS_INFO("AP_WP() status = 1");

        AGV_move_base->sendGoal(goal);

    AGV_move_base->waitForResult();

    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
      ROS_INFO("AGV has arrived to the A-B storage rack");
      //target_ = false;
      ABU_WP_ = false;

    //	state_goal.data = 3;
    //	reach_pub.publish(state_goal);
    //	ROS_INFO("state_goal.data = 3");

      if(!home_flag){
        rack_move = true;
      }
      else{
        rth = 1;
      }
          }
          else{
            ROS_INFO("AGV failed for some reason");
          }
  }
  else{
    ROS_INFO("AGV is not de-activate");
  }

}
// Add Dinh 200521
void NavigationManagement::ABL_WP() //A-B Low Rack Moving
{
//	ROS_INFO("ABL_WP");
  ROS_INFO("===== AGV is moving to ABL_WP =====");
  ROS_INFO("target_pos_x : %.4f",target_pos_x);
  ROS_INFO("target_pos_y : %.4f",target_pos_y);
  ROS_INFO("target_ori_z : %.4f",target_ori_z);
  ROS_INFO("target_ori_w : %.4f",target_ori_w);
  ROS_INFO("==================================");

  if(move_state == 1){
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.header.frame_id = "map";



    while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
      ROS_INFO("Waiting for the move_base action server");

    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.position.y = wp5_pose[1];

    goal.target_pose.pose.orientation.z = target_ori_z;
          goal.target_pose.pose.orientation.w = target_ori_w;

    ROS_INFO("AGV move to the A-B low storage rack");
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

    state_goal.data = 1;
    reach_pub.publish(state_goal);
  //	ROS_INFO("AP_WP() status = 1");

        AGV_move_base->sendGoal(goal);

    AGV_move_base->waitForResult();

    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
      ROS_INFO("AGV has arrived to the A-B storage rack");
      //target_ = false;
      ABL_WP_ = false;

    //	state_goal.data = 3;
    //	reach_pub.publish(state_goal);
    //	ROS_INFO("state_goal.data = 3");

      if(!home_flag){
        rack_move = true;
        point4_ = false;
      }
      else{
        RTH_ = rth_16;
        rack_move =false;
      }
          }
          else{
            ROS_INFO("AGV failed for some reason");
          }
  }
  else{
    ROS_INFO("AGV is not de-activate");
  }

}

void NavigationManagement::CD_WP() //C-D Rack Moving
{

  if(move_state == 1){
    goal.target_pose.header.stamp = ros::Time::now();
          goal.target_pose.header.frame_id = "map";



    while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
      ROS_INFO("Waiting for the move_base action server");
    // Add Dinh 200520
    if(target_pos_y < 0)
    {
    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.position.y = wp3_pose[1];

    goal.target_pose.pose.orientation.z = target_ori_z;
          goal.target_pose.pose.orientation.w = target_ori_w;

    ROS_INFO("AGV move to the C-D storage rack");
    }
    else {
      goal.target_pose.pose.position.x = target_pos_x;
      goal.target_pose.pose.position.y = wp4_pose[1];

      goal.target_pose.pose.orientation.z = target_ori_z;
            goal.target_pose.pose.orientation.w = target_ori_w;
    }
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

    state_goal.data = 1;
    reach_pub.publish(state_goal);
  //	ROS_INFO("CD_WP() status = 1");

        AGV_move_base->sendGoal(goal);

    AGV_move_base->waitForResult();

    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
      ROS_INFO("AGV has arrived to the C-D storage rack");

    //	state_goal.data = 3;
    //	reach_pub.publish(state_goal);
    //	ROS_INFO("state_goal.data = 3");

      if(!home_flag){
        rack_move = true;
        CD_WP_ = false;
      }
      else{
      //	rth = 1;
        RTH_ = rth_1;
        wp_ = wp_2;
      }

          }
          else{
            ROS_INFO("AGV failed for some reason");
          }
  }
  else{
    ROS_INFO("AGV is not de-activate");
  }
}

void NavigationManagement::home_reset()
{

//	ROS_INFO("home_reset()");

  switch(_move){
    case move_x_:
  //		ROS_INFO("move_x_");
      if(fabs(tf_fb_x-init_pose_[0])<0.1 && goal_Status.status==3){
        if(line_x >= -2.0 && line_x <= 2.0){
          vel_.linear.x = 0.0;
          ROS_INFO("AGV STOP: X");
          _move = move_y_;
        }
        else if(line_x > 0.0 && line_x <= 100.0){
          vel_.linear.x = -QR_VEL;
          ROS_INFO("vel_x: -");
        }
        else if(line_x >= -100.0 && line_x <= 0.0){
          vel_.linear.x = QR_VEL;
          ROS_INFO("vel_x: +");
        }

        vel_pub.publish(vel_);
      }
      break;

    case move_y_:
  //		ROS_INFO("move_y_");
      if(fabs(tf_fb_x-init_pose_[0])<0.1 && goal_Status.status==3){
        if(line_y >= -2.0 && line_y <= 2.0){
          vel_.linear.y = 0.0;
          // Add Dinh 200527
          _move = move_theta_;
          //h_reset = false;
          ROS_INFO("AGV STOP: Y");
        }
        else if(line_y > 0.0 && line_y <= 100.0){
          vel_.linear.y = -QR_VEL;
          ROS_INFO("vel_y: -");
        }
        else if(line_y >= -100.0 && line_y <= 0.0){
          vel_.linear.y = QR_VEL;
          ROS_INFO("vel_y: +");
        }

        vel_pub.publish(vel_);
      }

      break;

      // Add Dinh 200527

  case move_theta_:
//		ROS_INFO("move_y_");
    if(fabs(tf_fb_x-init_pose_[0])<0.1 && goal_Status.status==3)
    {
      if(line_theta >= 179.5 && line_theta <= 180){
        vel_.angular.z = 0.0;
        h_reset = false;
        ROS_INFO("AGV STOP: THETA");
      }
      else if(line_y > 175 && line_y < 179.5){
        vel_.angular.z = -QR_AVEL;
        //ROS_INFO("vel_y: -");
      }
      else if(line_y > 180 && line_y <= 185){
        vel_.angular.z = QR_AVEL;
        //ROS_INFO("vel_y: +");
      }

      vel_pub.publish(vel_);
    }

    break;

    default:

      break;
  }
}

void NavigationManagement::precision_move()
{
//	ROS_INFO("precision_move");
  ROS_INFO("AGV-precision closely approach to the storage Rack");

  if(move_state == 1){
    goal.target_pose.header.stamp = ros::Time::now();
    target_goal.header.frame_id = "map";
          goal.target_pose.header.frame_id = "map";



    while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
      ROS_INFO("Waiting for the move_base action server");

    goal.target_pose.pose.position.x = target_pos_x;
          goal.target_pose.pose.position.y = target_pos_y;
    goal.target_pose.pose.orientation.z = target_ori_z;
          goal.target_pose.pose.orientation.w = target_ori_w;
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);
    ROS_INFO("AGV-precision is control to the storage rack");

    state_goal.data = 1;
    reach_pub.publish(state_goal);
  //	ROS_INFO("precision_move() status = 1");

        AGV_move_base->sendGoal(goal);

    AGV_move_base->waitForResult();

    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
      ROS_INFO("AGV has arrived to the storage rack(QR marker)");
  //		target_ = false;
      rack_move = false;
  //		h_reset = true;
      if(!home_flag)
      {
        qr_reset = true;
        _move = move_x_;
        ABL_WP_ = false;
      }
      else
      {
        if(target_pos_y<6)
        {
          RTH_ = rth_0;
        }
        else {
          if(target_pos_x < 2)
            RTH_ = rth_17;
          else {
            rth = 0;
          }
        }
      }

  //		rth = 1;

    //	state_goal.data = 3;
    //	reach_pub.publish(state_goal);
    //	ROS_INFO("state_goal.data = 3");
          }
          else{
            ROS_INFO("AGV failed for some reason");
          }
  }
  else{
    ROS_INFO("AGV is not de-activate");
  }

}

void NavigationManagement::qr_move()
{
//	ROS_INFO("qr_move()");

  switch(_move){
    case move_x_: //x direction moving

      if(fabs(tf_fb_x-target_pos_x[0])<0.1 && goal_Status.status==3){
        if(line_x >= -2.0 && line_x <= 2.0){
          vel_.linear.x = 0.0;
          ROS_INFO("AGV STOP: X");
          _move = move_y_;
          qr_count++;
          ROS_INFO("qr_count : %d",qr_count);
        }
        else if(line_x > 2.0 && line_x <= 100.0){
          vel_.linear.x = -QR_VEL;
          ROS_INFO("vel_x: -");
        }
        else if(line_x >= -100.0 && line_x < -2.0){
          vel_.linear.x = QR_VEL;
          ROS_INFO("vel_x: +");
        }

        vel_pub.publish(vel_);
      }
      break;

    case move_y_: //y direction moving

      if(fabs(tf_fb_x-target_pos_x[0])<0.1 && goal_Status.status==3){
        if(line_y >= -2.0 && line_y <= 2.0){
        //	vel_.linear.y = 0.0;
        //	qr_reset = false;
        //	rth = 0;
        //	ROS_INFO("AGV STOP: Y");
        //	_move = move_x_;
          qr_count++;
          ROS_INFO("qr_count : %d",qr_count);
        //	if(qr_count>=5){
          if(qr_count>=QR_NUM){
            vel_.linear.y = 0.0;
            //qr_reset = false;
            _move = move_theta_;
            qr_count = 0;
            rth = 0;
          //	_move = move_theta_;
            ROS_INFO("AGV STOP: Y");
          //	qr_reset = false;
            ROS_INFO("qr_reset = false");

            state_goal.data = 3;
            reach_pub.publish(state_goal);
            ROS_INFO("qr_move() status = 3");
          }
          else{
            _move = move_x_;
          }
        }
        else if(line_y > 2.0 && line_y <= 100.0){
          vel_.linear.y = -QR_VEL;
          ROS_INFO("vel_y: -");
        }
        else if(line_y >= -100.0 && line_y < -2.0){
          vel_.linear.y = QR_VEL;
          ROS_INFO("vel_y: +");
        }

        vel_pub.publish(vel_);
      }

      break;

      // Add Dinh 200527

  case move_theta_:
//		ROS_INFO("move_y_");
    if(fabs(tf_fb_x-target_pos_x[0])<0.1 && goal_Status.status==3)
    {
      if(line_theta >= 179.5 && line_theta <= 180){
        vel_.angular.z = 0.0;
        qr_reset = false;
        ROS_INFO("AGV STOP: THETA");
      }
      else if(line_y > 175 && line_y < 179.5){
        vel_.angular.z = -QR_AVEL;
        //ROS_INFO("vel_y: -");
      }
      else if(line_y > 180 && line_y <= 185){
        vel_.angular.z = QR_AVEL;
        //ROS_INFO("vel_y: +");
      }

      vel_pub.publish(vel_);
    }

    break;

    default:

      break;

    // Add Dinh 200521
    if(!home_flag)
    {
      qr_reset = false;
    }
    else
    {
      if(target_pos_y<6)
      {
        RTH_ = rth_0;
      }
      else {
        if(target_pos_x < 2)
          RTH_ = rth_17;
        else {
          rth = 0;
        }
      }
    }
  }


}

void NavigationManagement::cart_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";



  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = target_pos_x;
  goal.target_pose.pose.position.y = target_pos_y;

  goal.target_pose.pose.orientation.z = target_ori_z;
        goal.target_pose.pose.orientation.w = target_ori_w;

  ROS_INFO("AGV move to the cart-position");

  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("cart_move() status = 1");

      AGV_move_base->sendGoal(goal);

  AGV_move_base->waitForResult();

  if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
    ROS_INFO("AGV has arrived to the cart-position");

    state_goal.data = 3;
    reach_pub.publish(state_goal);
//		ROS_INFO("cart_move() status = 3");

/*
    if(!home_flag){
      rack_move = true;
      CD_WP_ = false;
    }
    else{
    //	rth = 1;
      RTH_ = rth_1;
      wp_ = wp_2;
    }
*/
// Add Dinh 200520
    cart_move_ = false;
        }
        else{
          ROS_INFO("AGV failed for some reason");
        }

}

}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "AGV_nav_action");

  agv_wp::NavigationManagement AGV_nav;
  AGV_nav.spin();
  agv_wp::status st;
  st.active = true;

  return 0;
}



