#include <ros/ros.h>
#include <std_msgs/Int8.h>
#include <vector>

using namespace std;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "test1");
	ros::NodeHandle nh;
	ros::Rate loop_rate(10);

	vector<string> str_list;
	vector<float> vec_pose;
	vector<float> vec_ori;

	std_msgs::Int8 num;
	int num1, num2, num3, num4;
	ROS_INFO("TEST1 START");

	ros::Publisher test_pub;
	test_pub = nh.advertise<std_msgs::Int8>("/node_test", 1, true);

	num.data = 1;

	if(nh.getParam("/wp1", num1)){
		ROS_INFO("wp1");
	}

	if(nh.getParam("/wp2", num2)){
		ROS_INFO("wp2");
	}

	if(nh.getParam("/wp3", num3)){
		ROS_INFO("wp3");
	}

	if(nh.getParam("/wp4_pose", vec_pose)){
		ROS_INFO("wp4_pose");
	}

	if(nh.getParam("/wp4_ori", vec_ori)){
		ROS_INFO("wp4_ori");
	}

	if(nh.getParam("/string_list", str_list)){
		ROS_INFO("string_list");
	}

//	while(ros::ok())
//	{
		test_pub.publish(num);

		ROS_INFO("num1 : %d", num1);
		ROS_INFO("num2 : %d", num2);
		ROS_INFO("num3 : %d", num3);

		for(vector<int>::size_type i=0;i<vec_pose.size();++i){
			ROS_INFO("wp4_pose : %.2f",vec_pose[i]);
		//	cout<<vec[i]<<endl;
		}

		for(vector<int>::size_type i=0;i<vec_ori.size();++i){
			ROS_INFO("wp4_ori : %.2f",vec_ori[i]);
		//	cout<<vec[i]<<endl;
		}
//		ROS_INFO("str: %s",str_list);

//		ros::spinOnce();
//		loop_rate.sleep();
//	}

	return 0;
}
