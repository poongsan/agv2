// AGV 2 movement
// 4-arm 1-2: Home position -> wp1-2 (rotate +-90)  -> wp3-4 -> arm_move -> QR
// move charge pos: Home position  -> wp1 -> charge_pos

#include "ros/time.h"
#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <if_node_msgs/cmdMove.h>
#include <if_node_msgs/lampState.h>
#include <if_node_msgs/waypoint.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <nav_msgs/Odometry.h>
#include <obstacle_detector/obstacles_detect.h>
#include <pgv/vision_msg.h>
#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/UInt8.h>
#include <tf/LinearMath/Transform.h>
#include <tf/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>
#include <vector>

#include "md2k_can_driver_msgs/motor_status.h"
#include "move_control/teleop.h"

#define QR_VEL 0.02
#define QR_AVEL 0.009
#define CART_VEL 0.016
#define QR_NUM 5

using namespace std;

namespace agv_wp
{

tf2_ros::Buffer tfBuffer;

enum start_pos
{
  start_null = 0,
  from_home,
  from_arm12,
  from_arm34,
  from_arm5,
  from_arm6,
  from_arm7,
  from_charger,
  from_AU,
  from_BU,
  from_C,
  from_D,
  from_home1,
  from_cart,
  from_BL,
  from_AL
};

enum stop_pos
{
  stop_null = 0,
  to_home,
  to_arm12,
  to_arm34,
  to_arm5,
  to_arm6,
  to_arm7,
  to_charger,
  to_AU,
  to_BU,
  to_C,
  to_D,
  to_home1,
  to_cart,
  to_AL,
  to_BL
};

enum Move_Point
{
  no_point_ = 0,
  home_,
  home1_,
  cart_,
  arm12_,
  charger_,
  charger1_,
  charger2_,
  arm34_,
  arm5_,
  arm6_,
  arm7_,
  AU_,
  BU_,
  AL_,
  BL_,
  C_,
  D_,
  wp1_,
  wp2_,
  wp3_,
  wp4_,
  wp5_,
  wp51_,
  wp52_,
  wp6_,
  wp7_,
  wp8_,
  wp9_,
  wp10_,
  wp11_,
  wp12_,
  wp13_,
  wp14_,
  wp15_,
  wp16_,
  rot1_,
  rot2_,
  rot3_,
  rot4_,
  rot5_,
  rot6_,
  rack_,
  h_reset_,
  point1_,
  point2_,
  point3_,
  point4_
};

enum MODE_AGV
{
  mode_stop_ = 0,
  mode_start_ = 1,
  mode_obs_ = 2,
  motor_er_ = 3,
  mode_emc_ = 4,
  mode_fail_ = 5
};
enum MODE_MOVE
{
  MOVE_ZERO_ = 0,
  MOVE_LRF_ = 1,
  MOVE_QR_ = 2,
};

enum LRF_MOVE
{
  LRF_NONE_ = 0,
  LRF_XY_ = 1,
  LRF_YAW_ = 2,
  LRF_DIAG_ = 3
};

enum AGV_STATUS
{
  AGV_STP = 0,
  WP_REACH = 2,
  GOAL_REACH = 3,
  QR_COMP = 5,
  LRF_RUN = 1,
  QR_CTRL = 4,
  OBS = 7,
  MANUAL = 8,
  LRF_XY_COMP = 6,
  MOTOR_ER_ = 9,
  LRF_FAIL_ = 10,
};

start_pos start_;
stop_pos stop_;
Move_Point move_point_;
MODE_AGV mode_agv_, pre_mode_;
MODE_MOVE moveMode_;
LRF_MOVE lrfMove_;
AGV_STATUS agvStatus_;

class NavigationManagement
{
public:
  NavigationManagement();
  ~NavigationManagement();
  void init();
  void spin();
  void update();
  void param_set();

private:
  void charge_move();
  void charge1_move();
  void charge2_move();
  void rack_move();
  void qr_move();

  void RTH();
  void wp1_move();
  void wp2_move();
  void wp3_move();
  void wp4_move();
  void wp5_move();
  void wp51_move();
  void wp52_move();
  void wp6_move();
  void wp7_move();
  void wp8_move();
  void wp9_move();
  void wp10_move();

  void wp11_move();
  void wp12_move();
  void wp13_move();
  void wp14_move();

  void wp15_move();
  void wp16_move();
  void cart_move();
  void home1_move();

  void rot1_move();
  void rot2_move();
  void rot3_move();
  void rot4_move();
  void rot5_move();

  void arm12_move();
  void arm34_move();
  void arm5_move();
  void arm6_move();
  void arm7_move();

  void AU_move();
  void AL_move();
  void C_move();
  void BU_move();
  void BL_move();
  void D_move();

  void point1_move();
  void point2_move();
  void point3_move();
  void point4_move();

  void stop_agv();

  void home1_cb(const std_msgs::UInt8::ConstPtr& home_sig);

  void nav_control();
  void XY_control();
  void YAW_control();
  void DIAG_control();
  double lrf_tracking_ctrl(double Kp, double dx, double vmax, double vmin);
  float sign(float in);

  bool obstacle_;
  bool home_flag;

  int rate;
  int move_state; // move : 1, stop : 0
  int init_flag;

  bool tag_, control_;
  int tag_id, line_, line_id, control_id, fault, warning, command;

  float target_pos_x, target_pos_y, target_A;
  float goal_x, goal_y, goal_A;

  double delX_TOL = 0.03, delY_TOL = 0.03, delTH_TOL = 0.7;
  double delX_ = 0, delY_ = 0, delTH_ = 0, lenX_ = 0, lenY_ = 0, angTH_ = 0;
  double delTH_LSTP = 10, delTH_STA = 10;
  double LVX_MAX = 0.7, LVY_MAX = 0.3, LVTH_MAX = 0.1;
  double Kp = 0.6;

  float current_x, current_y, current_A;

  float line_x, line_y, line_theta;
  float tf_fb_x, tf_fb_y, tf_fb_ori_z, tf_fb_ori_w;

  float cur_x, cur_y, cur_A;

  float old_x = 0, old_y = 0, old_A = 0;
  int init_cnt_ = 0;
  // Add 210207
  bool recover_ = false;
  float old_vx = 0, old_vy = 0, old_vA = 0;
  int rec_cnt_ = 0;

  bool manual_mode;
  bool lrf_fail = false;

  float man_x = 0, man_y = 0, man_a = 0;

  int8_t count;

  // Add 210718
  uint8_t laser_status_ = 0, pre_laser_status_ = 0;
  uint8_t laser_cnt_ = 0;

  std_msgs::UInt8 state_goal;
  geometry_msgs::Twist vel_;
  geometry_msgs::Pose2D cur_goal_, cur_pose;

  ros::NodeHandle nh;

  vector<float> init_pose_, init_ori;
  vector<float> charge_pose;

  vector<float> wp1_pose, wp2_pose, wp3_pose, wp4_pose, wp5_pose, wp51_pose,
      wp52_pose, wp6_pose, wp7_pose, wp8_pose, wp9_pose, wp10_pose, wp11_pose,
      wp12_pose, wp13_pose, wp14_pose, wp15_pose, wp16_pose, home1_pose,
      cart_pose, point1_pose, point2_pose, point3_pose, point4_pose;

  ros::Publisher vel_pub, goal_pub, reach_pub, waypoint_pub, pos_pub, ala_pub,
      pub_initPos, pubLaser, pub_forkstp;
  ros::Subscriber reach_sub, goal_sub, init_sub, home1_sub, pgv_sub,
      move_base_feedback_sub, obstacle_sub, joy_sub, motor_status_sub, emc_sub,
      laser_status_sub;
  ros::Subscriber sub_mot_status[4];

  void
  goal_reach_cb(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach);
  void goal_target_cb(const if_node_msgs::cmdMove& goal);
  void init_cb(const std_msgs::UInt8::ConstPtr& init_);
  void pgv_cb(const pgv::vision_msg& pgv_msg);
  void obstacle_cb(const std_msgs::UInt8::ConstPtr& msg);
  void wp_pub();
  void pub_state();
  void joystickControlCallback(const move_control::teleop::ConstPtr& motorCmd_);
  void status_cb(const md2k_can_driver_msgs::motor_status::ConstPtr& msg);
  void emc_cb(const std_msgs::Int8::ConstPtr& msg);
  void laserStatusCallback(const std_msgs::UInt8::ConstPtr& msg);

  void FLcallback(const std_msgs::UInt8 msg);
  void FRcallback(const std_msgs::UInt8 msg);
  void RLcallback(const std_msgs::UInt8 msg);
  void RRcallback(const std_msgs::UInt8 msg);

  uint8_t front_left = 0;
  uint8_t front_right = 2;
  uint8_t rear_left = 1;
  uint8_t rear_right = 3;

  //  uint8_t mot_status[4];
  int8_t mot_status[4];
};

NavigationManagement::NavigationManagement()
{

  init();
  param_set();

  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1, true);
  goal_pub = nh.advertise<geometry_msgs::Pose2D>("/current_goal", 1, true);
  pos_pub = nh.advertise<geometry_msgs::Pose2D>("/agv_pos", 1, true);
  waypoint_pub = nh.advertise<if_node_msgs::waypoint>("/agv_wp", 1, true);
  reach_pub = nh.advertise<std_msgs::UInt8>("/IFNode/goal_reach", 1, true);
  ala_pub =
      nh.advertise<if_node_msgs::lampState>("/PLC/SignalTower/CMD", 1, true);

  goal_sub = nh.subscribe("/MOVE/CMD", 10,
                          &NavigationManagement::goal_target_cb, this);
  init_sub =
      nh.subscribe("/init_pose", 1, &NavigationManagement::init_cb, this);
  home1_sub =
      nh.subscribe("/home_pose", 1, &NavigationManagement::home1_cb, this);
  pgv_sub = nh.subscribe("/pgv_data", 10, &NavigationManagement::pgv_cb, this);
  obstacle_sub = nh.subscribe("/obstacle_info", 1,
                              &NavigationManagement::obstacle_cb, this);
  emc_sub = nh.subscribe("/PLC/Error/Emergency", 1,
                         &NavigationManagement::emc_cb, this);

  motor_status_sub =
      nh.subscribe("/motor_status", 1, &NavigationManagement::status_cb, this);
  joy_sub = nh.subscribe("cmd_vel_joy", 1,
                         &NavigationManagement::joystickControlCallback, this);
  laser_status_sub = nh.subscribe(
      "/laserStatus", 1, &NavigationManagement::laserStatusCallback, this);
  /*
  sub_mot_status[front_left] =
      nh.subscribe("/FL/motor_er", 1, &NavigationManagement::FLcallback, this);
  sub_mot_status[front_right] =
      nh.subscribe("/FR/motor_er", 1, &NavigationManagement::FRcallback, this);
  sub_mot_status[rear_left] =
      nh.subscribe("/RL/motor_er", 1, &NavigationManagement::RLcallback, this);
  sub_mot_status[rear_right] =
      nh.subscribe("/RR/motor_er", 1, &NavigationManagement::RRcallback, this);
      */

  pubLaser = nh.advertise<std_msgs::UInt8>("/laser_verified", 1, true);

  pub_initPos = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>(
      "/initialpose", 1, true);

  pub_forkstp = nh.advertise<std_msgs::UInt8>("/LOAD/STP", 1, this);

  goal_x = init_pose_[0];
  goal_y = init_pose_[1];
  goal_A = init_pose_[2];

  cur_goal_.x = goal_x;
  cur_goal_.y = goal_y;
  cur_goal_.theta = goal_A * M_PI / 180;
  goal_pub.publish(cur_goal_);
}

NavigationManagement::~NavigationManagement() {}

void NavigationManagement::init()
{
  rate = 10; // 10; //100
  target_pos_x = 0.0;
  target_pos_y = 0.0;
  target_A = 0.0;

  current_x = 0;
  current_y = 0;
  current_A = 0;

  cur_x = 0;
  cur_y = 0;
  cur_A = 0;

  home_flag = 0;

  moveMode_ = MOVE_ZERO_;
  agvStatus_ = AGV_STP;
  lrfMove_ = LRF_NONE_;

  start_ = start_null;
  stop_ = stop_null;
  move_point_ = no_point_;

  init_flag = 0;

  mode_agv_ = mode_stop_;

  obstacle_ = false;
  manual_mode = false;
  move_state = 0;

  count = 0;

  for (int i = 0; i < 4; i++)
  {
    mot_status[i] = 0;
  }
}

void NavigationManagement::laserStatusCallback(
    const std_msgs::UInt8::ConstPtr& msg)
{
  laser_status_ = msg->data;
}

/*
void NavigationManagement::FLcallback(const std_msgs::UInt8 msg)
{
  mot_status[front_left] = msg.data;
}

void NavigationManagement::FRcallback(const std_msgs::UInt8 msg)
{
  mot_status[front_right] = msg.data;
}

void NavigationManagement::RLcallback(const std_msgs::UInt8 msg)
{
  mot_status[rear_left] = msg.data;
}

void NavigationManagement::RRcallback(const std_msgs::UInt8 msg)
{
  mot_status[rear_right] = msg.data;
}
*/

void NavigationManagement::emc_cb(const std_msgs::Int8::ConstPtr& msg)
{
  if (msg->data == 1)
  {
    if (mode_agv_ == mode_start_)
      mode_agv_ = mode_emc_;
  }
  else
  {
    if (mode_agv_ == mode_emc_)
      mode_agv_ = mode_stop_;
  }
}

void NavigationManagement::wp_pub()
{
  if_node_msgs::waypoint wp_desc;
  switch (start_)
  {
  case from_home:
    wp_desc.start_point = "home";
    break;
  case from_arm12:
    wp_desc.start_point = "arm12";
    break;
  case from_arm34:
    wp_desc.start_point = "arm34";
    break;
  case from_arm5:
    wp_desc.start_point = "arm5";
    break;
  case from_arm6:
    wp_desc.start_point = "arm6";
    break;
  case from_arm7:
    wp_desc.start_point = "arm7";
    break;
  case from_charger:
    wp_desc.start_point = "charger";
    break;
  case from_AU:
    wp_desc.start_point = "AU";
    break;
  case from_BU:
    wp_desc.start_point = "BU";
    break;
  case from_AL:
    wp_desc.start_point = "AL";
    break;
  case from_BL:
    wp_desc.start_point = "BL";
    break;
  case from_C:
    wp_desc.start_point = "C";
    break;
  case from_D:
    wp_desc.start_point = "D";
    break;
  case from_home1:
    wp_desc.start_point = "home1";
    break;
  case from_cart:
    wp_desc.start_point = "cart";
    break;
  case start_null:
    wp_desc.start_point = "no_point";
    break;
  default:
    break;
  }
  switch (stop_)
  {
  case to_home:
    wp_desc.stop_point = "home";
    break;
  case to_arm12:
    wp_desc.stop_point = "arm12";
    break;
  case to_arm34:
    wp_desc.stop_point = "arm34";
    break;
  case to_arm5:
    wp_desc.stop_point = "arm5";
    break;
  case to_arm6:
    wp_desc.stop_point = "arm6";
    break;
  case to_arm7:
    wp_desc.stop_point = "arm7";
    break;
  case to_charger:
    wp_desc.stop_point = "charger";
    break;
  case to_AU:
    wp_desc.stop_point = "AU";
    break;
  case to_BU:
    wp_desc.stop_point = "BU";
    break;
  case to_AL:
    wp_desc.stop_point = "AL";
    break;
  case to_BL:
    wp_desc.stop_point = "BL";
    break;
  case to_C:
    wp_desc.stop_point = "C";
    break;
  case to_D:
    wp_desc.stop_point = "D";
    break;
  case to_home1:
    wp_desc.stop_point = "home1";
    break;
  case to_cart:
    wp_desc.stop_point = "cart";
    break;
  case stop_null:
    wp_desc.stop_point = "no_point";
    break;
  default:
    break;
  }
  waypoint_pub.publish(wp_desc);
}

void NavigationManagement::status_cb(
    const md2k_can_driver_msgs::motor_status::ConstPtr& msg)
{

  mot_status[0] = msg->front_left_bit_Alarm;
  mot_status[1] = msg->front_right_bit_Alarm;
  mot_status[2] = msg->rear_left_bit_Alarm;
  mot_status[3] = msg->rear_right_bit_Alarm;
}

void NavigationManagement::update()
{

  geometry_msgs::TransformStamped transformStamped;
  try
  {
    transformStamped =
        tfBuffer.lookupTransform("map", "base_link", ros::Time(0));

    current_x = transformStamped.transform.translation.x;
    current_y = transformStamped.transform.translation.y;
    tf::Quaternion q(transformStamped.transform.rotation.x,
                     transformStamped.transform.rotation.y,
                     transformStamped.transform.rotation.z,
                     transformStamped.transform.rotation.w);
    double roll, pitch, yaw;
    tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
    current_A = yaw * 180 / M_PI;
  }
  catch (tf2::TransformException& ex)
  {
    ROS_WARN("%s", ex.what());
    ros::Duration(0.3).sleep();
    //		continue;
  }

  cur_pose.x = current_x;
  cur_pose.y = current_y;
  cur_pose.theta = current_A * M_PI / 180;
  pos_pub.publish(cur_pose);
  if_node_msgs::lampState lamp_;

  if (manual_mode)
  {
    move_point_ = no_point_;
    mode_agv_ = mode_stop_;
    count = 0;
    stop_agv();
    agvStatus_ = MANUAL;
    // Add 210608
    lrf_fail = false;

    if (mot_status[front_left] == 0 && mot_status[rear_left] == 0 &&
        mot_status[front_right] == 0 && mot_status[rear_right] == 0)
    {
      if (man_x == 0 && man_y == 0 && man_a == 0)
      {
        lamp_.red = 0;
        lamp_.yellow = 1;
        lamp_.green = 0;
        lamp_.sound = 0;
      }
      else
      {
        lamp_.red = 0;
        lamp_.yellow = 2;
        lamp_.green = 0;
        lamp_.sound = 0;
      }
    }
    else
    {
      // mode_agv_ = motor_er_;
      lamp_.red = 2;
      lamp_.yellow = 2;
      lamp_.green = 0;
      lamp_.sound = 1;
    }
    // Add 210608
    ala_pub.publish(lamp_);
  }
  else
  {
    stop_agv();
    switch (mode_agv_)
    {
    case mode_obs_:
    {
      count = 0;
      stop_agv();
      agvStatus_ = OBS;
      // Add 210608
      lamp_.red = 0;
      lamp_.yellow = 2;
      lamp_.green = 2;
      lamp_.sound = 1;
      lrf_fail = false;
      // init_cnt_ = 0;
      // Add 210608
      ala_pub.publish(lamp_);
      laser_cnt_ = 0;
    }
    break;
      // Add 210609
    case mode_emc_:
    {
      count = 0;
      stop_agv();
      agvStatus_ = AGV_STP;
      // init_cnt_ = 0;
      lrf_fail = false;
      laser_cnt_ = 0;
      lamp_.red = 0;
      lamp_.yellow = 2;
      lamp_.green = 2;

      ala_pub.publish(lamp_);
    }
    break;
    // Add 210625
    case mode_fail_:
    {
      count = 0;
      stop_agv();
      laser_cnt_ = 0;
      // init_cnt_ = 0;
      if (recover_ == true)
      {
        if (rec_cnt_ == 0)
        {
          geometry_msgs::PoseWithCovarianceStamped init_;
          init_.header.stamp = ros::Time::now();
          init_.header.frame_id = "map";

          init_.pose.pose.position.x = old_x + old_vx / rate * 10;
          init_.pose.pose.position.y = old_y + old_vy / rate * 5;
          init_.pose.pose.position.z = 0.0;

          init_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(
              static_cast<double>((old_A * M_PI / 180.0) + old_vA / rate * 2));

          pub_initPos.publish(init_);
          rec_cnt_ = 1;
        }
        else if (rec_cnt_ >= 1 && rec_cnt_ < 10)
        {
          rec_cnt_++;
        }
        else if (rec_cnt_ >= 10)
        {
          recover_ = false;
          rec_cnt_ = 0;
        }
      }
      else
      {
        mode_agv_ = mode_start_;
        agvStatus_ = AGV_STP;
        pre_mode_ = mode_fail_;
      }
    }
    break;

    case mode_start_:
    {
      // Add 210608
      lamp_.red = 0;
      lamp_.yellow = 0;
      lamp_.green = 2;
      lamp_.sound = 0;
      // Add 210608
      ala_pub.publish(lamp_);

      if (mot_status[front_left] == 0 && mot_status[rear_left] == 0 &&
          mot_status[front_right] == 0 && mot_status[rear_right] == 0)
      {
        if (moveMode_ == MOVE_LRF_)
        {
          // Add 210718
          if (laser_status_ == pre_laser_status_)
            laser_cnt_++;
          else
            laser_cnt_ = 0;

          std_msgs::UInt8 laser_test;
          laser_test.data = laser_cnt_;
          pubLaser.publish(laser_test);

          if (laser_cnt_ >= 100)
          {
            laser_cnt_ = 100;
            mode_agv_ = mode_stop_;
            lrf_fail = true;
            agvStatus_ = LRF_FAIL_;
          }

          float delPosX = current_x - old_x;
          float delPosY = current_y - old_y;
          float delA = current_A - old_A;
          if (pre_mode_ != mode_fail_)
          {
            if ((lrfMove_ == LRF_XY_ || lrfMove_ == LRF_DIAG_) &&
                agvStatus_ == LRF_RUN)
            {
              if (fabs(delPosX) > 0.8 || fabs(delPosY) > 0.8)
              {
                mode_agv_ = mode_fail_;
                recover_ = true;
                agvStatus_ = LRF_FAIL_;
                rec_cnt_ = 0;
              }
              else
              {
                lrf_fail = false;
                recover_ = false;
              }
            }
            else if (lrfMove_ == LRF_YAW_ && agvStatus_ == LRF_RUN &&
                     recover_ == false)
            {
              if (fabs(delA) > 40 && fabs(delA) < 320)
              {
                mode_agv_ = mode_fail_;
                recover_ = true;
                agvStatus_ = LRF_FAIL_;
                rec_cnt_ = 0;
              }
              else
              {
                lrf_fail = false;
                recover_ = false;
              }
            }
          }
          else
          {
            // init_cnt_ = 0;
            lrf_fail = false;
            recover_ = false;
          }
          if (pre_mode_ == mode_fail_)
            pre_mode_ = mode_start_;
        }
        else
        {
          // init_cnt_ = 0;
          lrf_fail = false;
        }

        if (move_point_ == wp10_)
        {
          LVTH_MAX = 0.025;
        }
        else if (move_point_ == wp6_ && stop_ == to_arm12)
        {
          LVTH_MAX = 0.05;
        }
        else
          LVTH_MAX = 0.05;

        if ((start_ != start_null) && (stop_ != stop_null) &&
            (move_point_ != no_point_))
        {
          switch (move_point_)
          {
          case home_:
            RTH();
            break;
          case charger_:
            charge_move();
            break;
          case charger1_:
            charge1_move();
            break;
          case charger2_:
            charge2_move();
            break;
          case home1_:
            home1_move();
            break;
          case cart_:
            cart_move();
            break;
          case wp1_:
            wp1_move();
            break;
          case wp2_:
            wp2_move();
            break;
          case wp3_:
            wp3_move();
            break;
          case wp4_:
            wp4_move();
            break;
          case wp5_:
            wp5_move();
            break;
          case wp51_:
            wp51_move();
            break;
          case wp52_:
            wp52_move();
            break;
          case wp6_:
            wp6_move();
            break;
          case wp7_:
            wp7_move();
            break;
          case wp8_:
            wp8_move();
            break;
          case wp9_:
            wp9_move();
            break;
          case wp10_:
            wp10_move();
            break;
          case wp11_:
            wp11_move();
            break;
          case wp12_:
            wp12_move();
            break;
          case wp13_:
            wp13_move();
            break;
          case wp14_:
            wp14_move();
            break;
          case wp15_:
            wp15_move();
            break;
          case wp16_:
            wp16_move();
            break;
          case rot1_:
            rot1_move();
            break;
          case rot2_:
            rot2_move();
            break;
          case rot3_:
            rot3_move();
            break;
          case rot4_:
            rot4_move();
            break;
          case rot5_:
            rot5_move();
            break;
          case arm12_:
            arm12_move();
            break;
          case arm34_:
            arm34_move();
            break;
          case arm5_:
            arm5_move();
            break;
          case arm6_:
            arm6_move();
            break;
          case arm7_:
            arm7_move();
            break;
          case AU_:
            AU_move();
            break;
          case BU_:
            BU_move();
            break;
          case C_:
            C_move();
            break;
          case D_:
            D_move();
            break;
          case rack_:
            rack_move();
            break;
            // Add 210718
          case point1_:
            point1_move();
            break;
          case point2_:
            point2_move();
            break;
          case point3_:
            point3_move();
            break;
          case point4_:
            point4_move();
            break;
          case AL_:
            AL_move();
            break;
          case BL_:
            BL_move();
            break;
          case no_point_:
            stop_agv();
            break;
          default:
            break;
          }
        }
        else
        {
          move_point_ = no_point_;
          start_ = start_null;
          stop_ = stop_null;
          stop_agv();
        }
      }
      else
      {
        mode_agv_ = motor_er_;
        pre_mode_ = mode_start_;
      }
    }
    break;
    case mode_stop_:
    {
      move_point_ = no_point_;
      start_ = start_null;
      stop_ = stop_null;
      moveMode_ = MOVE_ZERO_;
      count = 0;

      /*
      if(agvStatus_ == QR_COMP || agvStatus_ == QR_CTRL)
          agvStatus_ = GOAL_REACH;

      if (agvStatus_ != GOAL_REACH || agvStatus_ != LRF_FAIL_)
        agvStatus_ = AGV_STP;
        */

      if (agvStatus_ == MANUAL)
        agvStatus_ = AGV_STP;

      if (mot_status[front_left] == 0 && mot_status[rear_left] == 0 &&
          mot_status[front_right] == 0 && mot_status[rear_right] == 0)
      {

      }
      else
      {
        mode_agv_ = motor_er_;
        pre_mode_ = mode_stop_;
      }

      stop_agv();
      // Add 210608
      // init_cnt_ = 0;

      if (lrf_fail)
      {
        lamp_.red = 2;
        lamp_.yellow = 0;
        lamp_.green = 0;
        lamp_.sound = 1;
      }
      else
      {
        lamp_.red = 0;
        lamp_.yellow = 0;
        lamp_.green = 1;
        lamp_.sound = 0;
      }
      // Add 210608
      ala_pub.publish(lamp_);
      laser_cnt_ = 0;
    }
    break;
    case motor_er_:
    {
      count = 0;
      stop_agv();
      agvStatus_ = MOTOR_ER_;
      // Add 210608
      lamp_.red = 2;
      lamp_.yellow = 2;
      lamp_.green = 0;
      lamp_.sound = 1;
      // Add 210608
      ala_pub.publish(lamp_);
      laser_cnt_ = 0;
      // Add 210813
      if (mot_status[front_left] == 0 && mot_status[rear_left] == 0 &&
          mot_status[front_right] == 0 && mot_status[rear_right] == 0)
      {
        if (pre_mode_ == mode_start_)
          mode_agv_ = mode_start_;
        else
          mode_agv_ = mode_stop_;
        pre_mode_ = motor_er_;
      }
    }
    break;
    default:
      break;
    }
  }
  // Add 210718
  pre_laser_status_ = laser_status_;
  pub_state();
  vel_pub.publish(vel_);
  if (!recover_)
  {
    old_x = current_x;
    old_y = current_y;
    old_A = current_A;
    old_vx = vel_.linear.x;
    old_vy = vel_.linear.y;
    old_vA = vel_.angular.z;
  }
}

void NavigationManagement::spin()
{
  ros::Rate loop_rate(rate);
  tf2_ros::TransformListener tfListener(tfBuffer);

  while (ros::ok())
  {
    update();
    ros::spinOnce();
    loop_rate.sleep();
  }
}

void NavigationManagement::pub_state()
{
  switch (agvStatus_)
  {
  case AGV_STP:
    state_goal.data = 0;
    break;
  case MANUAL:
    state_goal.data = 8;
    break;
  case LRF_RUN:
    state_goal.data = 1;
    break;
  case WP_REACH:
    state_goal.data = 2;
    break;
  case GOAL_REACH:
    state_goal.data = 3;
    break;
  case QR_COMP:
    state_goal.data = 5;
    break;
  case LRF_XY_COMP:
    state_goal.data = 6;
    break;
  case OBS:
    state_goal.data = 7;
    break;
  case MOTOR_ER_:
    state_goal.data = 9;
    break;
  case QR_CTRL:
    state_goal.data = 4;
    break;
  default:
    state_goal.data = 0;
    break;
  }
  reach_pub.publish(state_goal);

  /*
  AGV_STP = 0,
  WP_REACH = 2,
  GOAL_REACH = 3,
  QR_COMP = 5,
  LRF_RUN = 1,
  QR_CTRL = 4,
  OBS = 7,
  MANUAL = 8,
  LRF_XY_COMP = 6,
  MOTOR_ER_ = 9
    */
}

float NavigationManagement::sign(float in)
{
  if (in > 0)
    return 1;
  else if (in < 0)
    return -1;
  else
    return 0;
}

void NavigationManagement::goal_target_cb(const if_node_msgs::cmdMove& goal)
{
  ROS_INFO("Goal received ...");

  target_pos_x = goal.pose.position.x;
  target_pos_y = goal.pose.position.y;

  tf::Quaternion q(goal.pose.orientation.x, goal.pose.orientation.y,
                   goal.pose.orientation.z, goal.pose.orientation.w);
  tf::Matrix3x3 m(q);

  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);
  target_A = yaw * 180 / M_PI;

  start_ = start_null;
  stop_ = stop_null;
  count = 0;

  move_state = goal.move;

  if (move_state == 1)
  {
    mode_agv_ = mode_start_;
    if ((fabs(current_x - charge_pose[0]) < 3) &&
        (fabs(current_y - charge_pose[1]) < 0.8) &&
        (fabs(current_A - charge_pose[2]) < 5))
    {
      start_ = from_charger;
    }
    else if ((fabs(current_x - cart_pose[0]) < 0.5) &&
             (fabs(current_y - cart_pose[1]) < 0.5) &&
             (fabs(current_A - cart_pose[2]) < 5))
    {
      start_ = from_cart;
    }
    else if ((current_y >= home1_pose[1] - 6) &&
             (current_x <= home1_pose[0] + 0.4) &&
             (current_x >= home1_pose[0] - 0.4))
    {
      // from home1 area
      start_ = from_home1;
    }
    else if ((current_y >= init_pose_[1] - 4) &&
             (current_x >= init_pose_[0] + 7))
    // from arm 12
    {
      start_ = from_arm12;
    }
    else if ((current_y >= init_pose_[1] - 4) &&
             (current_x < init_pose_[0] + 7) && (current_x > init_pose_[0] - 4))
    {
      // from home area
      start_ = from_home;
    }
    else if ((current_y < wp4_pose[1] - 1) &&
             (current_y >= (wp5_pose[1] + 3)) &&
             (current_x > (wp5_pose[0] - 1)) &&
             (current_x < (wp5_pose[0] + 10)))
    // from arm 34
    {
      start_ = from_arm34;
    }
    else if (current_y < (wp5_pose[1] + 3) && (current_x > (wp5_pose[0] - 1)) &&
             (current_x < (wp9_pose[0] - 5)))
    {
      // from arm5
      start_ = from_arm5;
    }

    else if ((current_x >= (wp9_pose[0] - 5)) && (current_x < (wp11_pose[0])))
    {
      // from arm6
      start_ = from_arm6;
    }
    else if (current_x >= (wp11_pose[0]))
    {
      // from arm7
      start_ = from_arm7;
    }
    // Add 200615
    else if ((current_y > -1) && current_y < 6 &&
             (current_x < (wp5_pose[0] - 1)) &&
             (current_x > home1_pose[0] + 0.4))
    {
      // from C
      start_ = from_C;
    }
    else if ((current_y <= -1) && (current_x < (wp5_pose[0] - 1)) &&
             (current_x > home1_pose[0] + 0.4))
    {
      // from D
      start_ = from_D;
    }
    else if ((current_y > 8) && (current_y < 12.2) &&
             (current_x < (init_pose_[0] + 1)) &&
             (current_x > home1_pose[0] + 0.4))
    {
      // from BU
      start_ = from_BU;
    }
    else if ((current_y >= 12.2) && (current_x < (init_pose_[0] + 1)) &&
             (current_x > home1_pose[0] + 0.4))
    {
      // from AU
      start_ = from_AU;
    }
    // Add 210718
    else if ((current_y > 8) && (current_y < 12.2) &&
             (current_x < home1_pose[0] - 6))
    {
      // from BL
      start_ = from_BL;
    }
    else if ((current_y >= 12.2) && (current_x < home1_pose[0] - 6))
    {
      // from AL
      start_ = from_AL;
    }
    else
    {
      // from undefined
      start_ = start_null;
      move_point_ = no_point_;
    }

    if ((fabs(target_pos_x - init_pose_[0]) < 0.2) &&
        (fabs(target_pos_y - init_pose_[1]) < 0.2) &&
        (fabs(target_A - init_pose_[2]) < 5))
    // to home
    {

      stop_ = to_home;
      switch (start_)
      {
      case from_charger:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;
        move_point_ = charger2_;
      }
      break;
      case from_arm12:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm12_;
      }
      break;
      case from_home:
        move_point_ = home_;
        break;
      case from_arm34:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm34_;
      }
      break;
      case from_arm5:
      {
        if (current_x > wp5_pose[0] + 5)
        {
          cur_x = current_x;
          cur_y = current_y;
          cur_A = current_A;

          move_point_ = arm5_;
        }
        else
          move_point_ = wp5_;
      }
      break;
      case from_arm6:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm6_;
      }
      break;
      case from_arm7:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm7_;
      }
      break;
      case from_AU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AU_;
      }
      break;
      case from_BU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BU_;
      }
      break;
      case from_AL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AL_;
      }
      break;
      case from_BL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BL_;
      }
      break;
      case from_C:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = C_;
      }
      break;
      case from_D:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = D_;
      }
      break;
      case from_cart:
      {
        move_point_ = home1_;
      }
      break;
      case from_home1:
      {
        move_point_ = wp15_;
      }
      break;
      case start_null:
        move_point_ = no_point_;
        break;
      default:
        break;
      }
    }
    else if ((fabs(target_pos_x - charge_pose[0]) < 1) &&
             (fabs(target_pos_y - charge_pose[1]) < 1) &&
             (fabs(target_A - charge_pose[2]) < 5))
    // to charger
    {
      stop_ = to_charger;
      switch (start_)
      {
      case from_charger:
        move_point_ = charger_;
        break;
      case from_arm12:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm12_;
      }
      break;
      case from_home:
        move_point_ = charger2_;
        break;
      case from_arm34:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm34_;
      }
      break;
      case from_arm5:
      {
        if (current_x > wp5_pose[0] + 5)
        {
          cur_x = current_x;
          cur_y = current_y;
          cur_A = current_A;

          move_point_ = arm5_;
        }
        else
          move_point_ = wp5_;
      }
      break;
      case from_arm6:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm6_;
      }
      break;
      case from_arm7:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm7_;
      }
      break;
      case from_AU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AU_;
      }
      break;
      case from_BU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BU_;
      }
      break;
      case from_AL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AL_;
      }
      break;
      case from_BL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BL_;
      }
      break;
      case from_C:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = C_;
      }
      break;
      case from_D:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = D_;
      }
      break;
      case from_cart:
      {
        move_point_ = home1_;
      }
      break;
      case from_home1:
      {
        move_point_ = wp15_;
      }
      break;
      case start_null:
        move_point_ = no_point_;
        break;
      default:
        break;
      }
    }
    else if ((target_pos_x > wp3_pose[0] + 7) &&
             (target_pos_y > init_pose_[1] - 5))
    // to arm 12
    {
      stop_ = to_arm12;
      switch (start_)
      {
      case from_charger:
        move_point_ = charger2_;
        break;
      case from_arm12:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm12_;
      }
      break;
      case from_home:
      {
        if (target_pos_y > init_pose_[1])
          move_point_ = wp1_;
        else
          move_point_ = wp2_;
      }
      break;
      case from_arm34:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm34_;
      }
      break;
      case from_arm5:
      {
        if (current_x > wp5_pose[0] + 5)
        {
          cur_x = current_x;
          cur_y = current_y;
          cur_A = current_A;

          move_point_ = arm5_;
        }
        else
          move_point_ = wp5_;
      }
      break;
      case from_arm6:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm6_;
      }
      break;
      case from_arm7:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm7_;
      }
      break;
      case from_AU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AU_;
      }
      break;
      case from_BU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BU_;
      }
      break;
      case from_AL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AL_;
      }
      break;
      case from_BL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BL_;
      }
      break;
      case from_C:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = C_;
      }
      break;
      case from_D:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = D_;
      }
      break;
      case from_cart:
      {
        move_point_ = home1_;
      }
      break;
      case from_home1:
      {
        move_point_ = wp15_;
      }
      break;
      case start_null:
        move_point_ = no_point_;
        break;
      default:
        break;
      }
    }
    else if ((target_pos_y <= wp4_pose[1]) &&
             (target_pos_y > (wp5_pose[1] + 3)) &&
             (target_pos_x > (wp5_pose[0] - 1)) &&
             (target_pos_x < (wp5_pose[0] + 8)))
    // to arm 34
    {
      stop_ = to_arm34;
      switch (start_)
      {
      case from_charger:
        move_point_ = rot2_;
        break;
      case from_arm12:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm12_;
      }
      break;
      case from_home:
        move_point_ = wp3_;
        break;
      case from_arm34:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm34_;
      }
      break;
      case from_arm5:
      {
        if (current_x > wp5_pose[0] + 6)
        {
          cur_x = current_x;
          cur_y = current_y;
          cur_A = current_A;

          move_point_ = arm5_;
        }
        else
          move_point_ = wp5_;
      }
      break;
      case from_arm6:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm6_;
      }
      break;
      case from_arm7:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm7_;
      }
      break;
      case from_AU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AU_;
      }
      break;
      case from_BU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BU_;
      }
      break;
      case from_AL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AL_;
      }
      break;
      case from_BL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BL_;
      }
      break;
      case from_C:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = C_;
      }
      break;
      case from_D:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = D_;
      }
      break;
      case from_cart:
      {
        move_point_ = home1_;
      }
      break;
      case from_home1:
      {
        move_point_ = wp15_;
      }
      break;
      case start_null:
        move_point_ = no_point_;
        break;
      default:
        break;
      }
    }

    else if (target_pos_x > (wp5_pose[0] + 8))
    // to arm 5 , 6, 7
    {
      if (target_pos_x < wp9_pose[0] - 5)
        stop_ = to_arm5;
      else if (target_pos_x < wp11_pose[0])
        stop_ = to_arm6;
      else
        stop_ = to_arm7;

      switch (start_)
      {
      case from_charger:
        move_point_ = rot2_;
        break;
      case from_arm12:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm12_;
      }
      break;
      case from_home:
        move_point_ = wp3_;
        break;
      case from_arm34:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm34_;
      }
      break;
      case from_arm5:
      {
        if (current_x > wp5_pose[0] + 5)
        {
          cur_x = current_x;
          cur_y = current_y;
          cur_A = current_A;

          move_point_ = arm5_;
        }
        else
          move_point_ = wp5_;
      }
      break;
      case from_arm6:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm6_;
      }
      break;
      case from_arm7:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm7_;
      }
      break;
      case from_AU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AU_;
      }
      break;
      case from_BU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BU_;
      }
      break;
      case from_AL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AL_;
      }
      break;
      case from_BL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BL_;
      }
      break;
      case from_C:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = C_;
      }
      break;
      case from_D:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = D_;
      }
      break;
      case from_cart:
      {
        move_point_ = home1_;
      }
      break;
      case from_home1:
      {
        move_point_ = wp16_;
      }
      break;
      case start_null:
        move_point_ = no_point_;
        break;
      default:
        break;
      }
    }

    else if ((target_pos_y > 8) && (target_pos_y <= 12.6) &&
             (target_pos_x <= init_pose_[0] - 1) &&
             (target_pos_x >= home1_pose[0]) && fabs(target_A) > 170)
    // to BU rack
    {
      stop_ = to_BU;

      switch (start_)
      {
      case from_charger:
        move_point_ = wp6_;
        break;
      case from_arm12:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm12_;
      }
      break;
      case from_home:
        move_point_ = wp6_;
        break;
      case from_arm34:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm34_;
      }
      break;
      case from_arm5:
      {
        if (current_x > wp5_pose[0] + 5)
        {
          cur_x = current_x;
          cur_y = current_y;
          cur_A = current_A;

          move_point_ = arm5_;
        }
        else
          move_point_ = wp5_;
      }
      break;
      case from_arm6:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm6_;
      }
      break;
      case from_arm7:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm7_;
      }
      break;
      case from_AU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AU_;
      }
      break;
      case from_BU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BU_;
      }
      break;
      case from_AL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AL_;
      }
      break;
      case from_BL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BL_;
      }
      break;
      case from_C:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = C_;
      }
      break;
      case from_D:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = D_;
      }
      break;
      case from_cart:
      {
        move_point_ = home1_;
      }
      break;
      case from_home1:
      {
        move_point_ = wp15_;
      }
      break;
      case start_null:
        move_point_ = no_point_;
        break;
      default:
        break;
      }
    }

    else if ((target_pos_y > 12.6) && (target_pos_x <= init_pose_[0] - 1) &&
             (target_pos_x >= home1_pose[0]) && fabs(target_A) < 5)
    // to AU rack
    {
      stop_ = to_AU;

      switch (start_)
      {
      case from_charger:
        move_point_ = wp6_;
        break;
      case from_arm12:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm12_;
      }
      break;
      case from_home:
        move_point_ = wp6_;
        break;
      case from_arm34:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm34_;
      }
      break;
      case from_arm5:
      {
        if (current_x > wp5_pose[0] + 5)
        {
          cur_x = current_x;
          cur_y = current_y;
          cur_A = current_A;

          move_point_ = arm5_;
        }
        else
          move_point_ = wp5_;
      }
      break;
      case from_arm6:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm6_;
      }
      break;
      case from_arm7:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm7_;
      }
      break;
      case from_AU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AU_;
      }
      break;
      case from_BU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BU_;
      }
      break;
      case from_AL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AL_;
      }
      break;
      case from_BL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BL_;
      }
      break;
      case from_C:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = C_;
      }
      break;
      case from_D:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = D_;
      }
      break;
      case from_cart:
      {
        move_point_ = home1_;
      }
      break;
      case from_home1:
      {
        move_point_ = wp15_;
      }
      break;
      case start_null:
        move_point_ = no_point_;
        break;
      default:
        break;
      }
    }
    // Add 210718
    else if ((target_pos_y > 8) && (target_pos_y <= 12.6) &&
             (target_pos_x <= home1_pose[0] - 5) && fabs(target_A) > 170)
    // to BL rack
    {
      stop_ = to_BL;

      switch (start_)
      {
      case from_charger:
        move_point_ = wp6_;
        break;
      case from_arm12:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm12_;
      }
      break;
      case from_home:
        move_point_ = wp6_;
        break;
      case from_arm34:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm34_;
      }
      break;
      case from_arm5:
      {
        if (current_x > wp5_pose[0] + 5)
        {
          cur_x = current_x;
          cur_y = current_y;
          cur_A = current_A;

          move_point_ = arm5_;
        }
        else
          move_point_ = wp5_;
      }
      break;
      case from_arm6:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm6_;
      }
      break;
      case from_arm7:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm7_;
      }
      break;
      case from_AU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AU_;
      }
      break;
      case from_BU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BU_;
      }
      break;
      case from_AL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AL_;
      }
      break;
      case from_BL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BL_;
      }
      break;
      case from_C:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = C_;
      }
      break;
      case from_D:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = D_;
      }
      break;
      case from_cart:
      {
        move_point_ = home1_;
      }
      break;
      case from_home1:
      {
        move_point_ = point1_;
      }
      break;
      case start_null:
        move_point_ = no_point_;
        break;
      default:
        break;
      }
    }

    else if ((target_pos_y > 12.6) && (target_pos_x <= home1_pose[0] - 5) &&
             fabs(target_A) < 5)
    // to AL rack
    {
      stop_ = to_AL;

      switch (start_)
      {
      case from_charger:
        move_point_ = wp6_;
        break;
      case from_arm12:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm12_;
      }
      break;
      case from_home:
        move_point_ = wp6_;
        break;
      case from_arm34:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm34_;
      }
      break;
      case from_arm5:
      {
        if (current_x > wp5_pose[0] + 5)
        {
          cur_x = current_x;
          cur_y = current_y;
          cur_A = current_A;

          move_point_ = arm5_;
        }
        else
          move_point_ = wp5_;
      }
      break;
      case from_arm6:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm6_;
      }
      break;
      case from_arm7:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm7_;
      }
      break;
      case from_AU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AU_;
      }
      break;
      case from_BU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BU_;
      }
      break;
      case from_BL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BL_;
      }
      break;
      case from_AL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AL_;
      }
      break;
      case from_C:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = C_;
      }
      break;
      case from_D:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = D_;
      }
      break;
      case from_cart:
      {
        move_point_ = home1_;
      }
      break;
      case from_home1:
      {
        move_point_ = point1_;
      }
      break;
      case start_null:
        move_point_ = no_point_;
        break;
      default:
        break;
      }
    }

    else if ((target_pos_y < 6) && (target_pos_y >= -1) &&
             (target_pos_x < ((wp5_pose[0] - 1))) && fabs(target_A) < 10)
    // to C rack
    {
      stop_ = to_C;

      switch (start_)
      {
      case from_charger:
        move_point_ = rot2_;
        break;
      case from_arm12:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm12_;
      }
      break;
      case from_home:
        move_point_ = wp3_;
        break;
      case from_arm34:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm34_;
      }
      break;
      case from_arm5:
      {
        if (current_x > wp5_pose[0] + 5)
        {
          cur_x = current_x;
          cur_y = current_y;
          cur_A = current_A;

          move_point_ = arm5_;
        }
        else
          move_point_ = wp5_;
      }
      break;
      case from_arm6:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm6_;
      }
      break;
      case from_arm7:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm7_;
      }
      break;
      case from_AU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AU_;
      }
      break;
      case from_BU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BU_;
      }
      break;
      case from_AL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AL_;
      }
      break;
      case from_BL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BL_;
      }
      break;
      case from_C:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = C_;
      }
      break;
      case from_D:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = D_;
      }
      break;
      case from_cart:
      {
        move_point_ = home1_;
      }
      break;
      case from_home1:
      {
        move_point_ = wp16_;
      }
      break;
      case start_null:
        move_point_ = no_point_;
        break;
      default:
        break;
      }
    }

    else if ((target_pos_y < -1) && (target_pos_x < ((wp5_pose[0] - 1))) &&
             fabs(target_A) > 170)
    // to D rack
    {
      stop_ = to_D;

      switch (start_)
      {
      case from_charger:
      {
        move_point_ = rot2_;
      }
      break;
      case from_arm12:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm12_;
      }
      break;
      case from_home:
        move_point_ = wp3_;
        break;
      case from_arm34:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm34_;
      }
      break;
      case from_arm5:
      {
        if (current_x > wp5_pose[0] + 5)
        {
          cur_x = current_x;
          cur_y = current_y;
          cur_A = current_A;

          move_point_ = arm5_;
        }
        else
          move_point_ = wp5_;
      }
      break;
      case from_arm6:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm6_;
      }
      break;
      case from_arm7:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm7_;
      }
      break;
      case from_AU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AU_;
      }
      break;
      case from_BU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BU_;
      }
      break;
      case from_AL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AL_;
      }
      break;
      case from_BL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BL_;
      }
      break;
      case from_C:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = C_;
      }
      break;
      case from_D:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = D_;
      }
      break;
      case from_cart:
      {
        move_point_ = home1_;
      }
      break;
      case from_home1:
      {
        move_point_ = wp16_;
      }
      break;
      case start_null:
        move_point_ = no_point_;
        break;
      default:
        break;
      }
    }

    else if ((fabs(target_pos_x - cart_pose[0]) < 0.2) &&
             (fabs(target_pos_y - cart_pose[1]) < 0.2) &&
             (fabs(target_A - cart_pose[2]) < 5))
    // to cart
    {
      stop_ = to_cart;

      switch (start_)
      {
      case from_charger:
        move_point_ = rot2_;
        break;
      case from_arm12:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm12_;
      }
      break;
      case from_home:
        move_point_ = wp6_;
        break;
      case from_arm34:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm34_;
      }
      break;
      case from_arm5:
      {
        if (current_x > wp5_pose[0] + 5)
        {
          cur_x = current_x;
          cur_y = current_y;
          cur_A = current_A;

          move_point_ = arm5_;
        }
        else
          move_point_ = wp5_;
      }
      break;
      case from_arm6:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm6_;
      }
      break;
      case from_arm7:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = arm7_;
      }
      break;
      case from_AU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AU_;
      }
      break;
      case from_BU:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BU_;
      }
      break;
      case from_AL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = AL_;
      }
      break;
      case from_BL:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = BL_;
      }
      break;
      case from_C:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = C_;
      }
      break;
      case from_D:
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_A = current_A;

        move_point_ = D_;
      }
      break;
      case from_cart:
      {
        move_point_ = cart_;
      }
      break;
      case from_home1:
      {
        move_point_ = cart_;
      }
      break;
      case start_null:
        move_point_ = no_point_;
        break;
      default:
        break;
      }
    }
    else // target not in the list
    {
      start_ = start_null;
      stop_ = stop_null;
      move_point_ = no_point_;
    }
  }
  else
  {
    mode_agv_ = mode_emc_;
    std_msgs::UInt8 fork_msgs;
    fork_msgs.data = 1;
    pub_forkstp.publish(fork_msgs);
  }

  wp_pub();
}

void NavigationManagement::home1_cb(const std_msgs::UInt8::ConstPtr& home_sig)
{
  home_flag = home_sig->data;
  target_pos_x = home1_pose[0];
  target_pos_y = home1_pose[1];

  target_A = home1_pose[2];

  start_ = start_null;
  count = 0;

  move_state = 1;

  if (move_state == 1)
    mode_agv_ = mode_start_;
  else
    mode_agv_ = mode_stop_;

  stop_ = to_home1;

  if ((fabs(current_x - charge_pose[0]) < 0.2) &&
      (fabs(current_y - charge_pose[1]) < 0.2) &&
      (fabs(current_A - charge_pose[2]) < 0.5))
  {
    start_ = from_charger;
  }
  else if ((fabs(current_x - cart_pose[0]) < 0.3) &&
           (fabs(current_y - cart_pose[1]) < 0.3) &&
           (fabs(current_A - cart_pose[2]) < 0.5))
  {
    start_ = from_cart;
  }
  else if ((current_y >= home1_pose[1] - 6) &&
           (current_x <= home1_pose[0] + 0.4) &&
           (current_x >= home1_pose[0] - 0.4))
  {
    // from home1 area
    start_ = from_home1;
  }
  else if ((current_y >= init_pose_[1] - 5) && (current_x >= wp3_pose[0] + 7))
  // from arm 12
  {
    start_ = from_arm12;
  }
  else if ((current_y >= init_pose_[1] - 5) && (current_x < wp3_pose[0] + 7) &&
           (current_x > init_pose_[0] - 2))
  {
    // from home area
    start_ = from_home;
  }
  else if ((current_y < init_pose_[1] - 5) && (current_y >= wp4_pose[1] - 1) &&
           (current_x < wp3_pose[0] + 7) && (current_x > init_pose_[0] - 2))
  {
    // from charger area
    start_ = from_charger;
  }
  else if ((current_y < wp4_pose[1] - 1) && (current_y >= (wp5_pose[1] + 3)) &&
           (current_x > (wp5_pose[0] - 1)) && (current_x < (wp5_pose[0] + 10)))
  // from arm 34
  {
    start_ = from_arm34;
  }
  else if (current_y < (wp5_pose[1] + 3) && (current_x > (wp5_pose[0] - 1)) &&
           (current_x < (wp9_pose[0] - 5)))
  {
    // from arm5
    start_ = from_arm5;
  }

  else if ((current_x >= (wp9_pose[0] - 5)) && (current_x < (wp11_pose[0])))
  {
    // from arm6
    start_ = from_arm6;
  }
  else if (current_x >= (wp11_pose[0]))
  {
    // from arm7
    start_ = from_arm7;
  }
  else if ((current_y > -1) && current_y < 6 &&
           (current_x < (wp5_pose[0] - 1)) && (current_x > home1_pose[0] + 0.4))
  {
    // from C
    start_ = from_C;
  }
  else if ((current_y <= -1) && (current_x < (wp5_pose[0] - 1)) &&
           (current_x > home1_pose[0] + 0.4))
  {
    // from D
    start_ = from_D;
  }
  else if ((current_y > 8) && (current_y < 12.2) &&
           (current_x < (init_pose_[0] - 2)) &&
           (current_x > home1_pose[0] + 0.4))
  {
    // from B
    start_ = from_BU;
  }
  else if ((current_y >= 12.2) && (current_x < (init_pose_[0] - 2)) &&
           (current_x > home1_pose[0] + 0.4))
  {
    // from A
    start_ = from_AU;
  }
  else if ((current_y > 8) && (current_y < 12.2) &&
           (current_x < home1_pose[0] - 6))
  {
    // from BL
    start_ = from_BL;
  }
  else if ((current_y >= 12.2) && (current_x < home1_pose[0] - 6))
  {
    // from AL
    start_ = from_AL;
  }
  else
  {
    // from undefined
    start_ = start_null;
    move_point_ = no_point_;
  }

  switch (start_)
  {
  case from_charger:
    move_point_ = rot2_;
    break;
  case from_arm12:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = arm12_;
  }
  break;
  case from_home:
    move_point_ = wp6_;
    break;
  case from_arm34:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = arm34_;
  }
  break;
  case from_arm5:
  {
    if (current_x > wp5_pose[0] + 5)
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_A = current_A;

      move_point_ = arm5_;
    }
    else
      move_point_ = wp5_;
  }
  break;
  case from_arm6:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = arm6_;
  }
  break;
  case from_arm7:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = arm7_;
  }
  break;
  case from_AU:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = AU_;
  }
  break;
  case from_BU:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = BU_;
  }
  break;
  case from_AL:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = AL_;
  }
  break;
  case from_BL:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = BL_;
  }
  break;
  case from_C:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = C_;
  }
  break;
  case from_D:
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    move_point_ = D_;
  }
  break;
  case from_cart:
    move_point_ = home1_;
    break;
  case from_home1:
    move_point_ = home1_;
    break;
  case start_null:
    move_point_ = no_point_;
    break;
  default:
    break;
  }
  wp_pub();
}

void NavigationManagement::init_cb(const std_msgs::UInt8::ConstPtr& init_)
{
  ROS_INFO("Init Position");
  init_flag = init_->data;
  //	ROS_INFO("init_flag : %d",init_flag);
}

void NavigationManagement::pgv_cb(const pgv::vision_msg& pgv_msg)
{
  control_id = pgv_msg.ControlID;
  control_ = pgv_msg.Control;
  tag_ = pgv_msg.Tag;
  tag_id = pgv_msg.TagID;
  line_ = pgv_msg.Line;
  line_id = pgv_msg.LineID;
  line_x = pgv_msg.X;
  line_y = pgv_msg.Y;
  line_theta = pgv_msg.theta;
  fault = pgv_msg.Fault;
  warning = pgv_msg.Warning;
  command = pgv_msg.command;
}

double NavigationManagement::lrf_tracking_ctrl(double Kp, double dx,
                                               double vmax, double vmin)
{
  double v_cal = Kp * dx;
  if (v_cal >= 0)
  {
    if (v_cal > vmax)
      v_cal = vmax;
    else if (v_cal < vmin)
      v_cal = vmin;
  }
  else
  {
    if (v_cal < -vmax)
      v_cal = -vmax;
    else if (v_cal > -vmin)
      v_cal = -vmin;
  }
  return v_cal;
}

void NavigationManagement::XY_control()
{
  delX_ = goal_x - current_x;
  delY_ = goal_y - current_y;
  lenX_ = fabs(goal_x - cur_x);
  lenY_ = fabs(goal_y - cur_y);
  // vel_.angular.z = 0;

  if (fabs(delY_) <= delY_TOL && fabs(delX_) <= delX_TOL)
  {
    vel_.linear.x = 0;
    vel_.linear.y = 0;
    agvStatus_ = LRF_XY_COMP;
    vel_.angular.z = 0;
  }
  else
  {
    agvStatus_ = LRF_RUN;
    vel_.angular.z = 0;
    if (cur_A > -10 && cur_A < 10) // Around 0 deg
    {
      if (lenX_ > 2)
        vel_.linear.x = lrf_tracking_ctrl(Kp, delX_, LVX_MAX, 0.005);
      else
        vel_.linear.x = lrf_tracking_ctrl(Kp, delX_, 0.3, 0.005);

      if (lenY_ > 2)
        vel_.linear.y = lrf_tracking_ctrl(Kp, delY_, LVY_MAX, 0.005);
      else
        vel_.linear.y = lrf_tracking_ctrl(Kp, delY_, 0.25, 0.005);

      delTH_ = goal_A - current_A;
      if (delTH_ > 180)
        delTH_ = delTH_ - 360;
      else if (delTH_ < -180)
        delTH_ = delTH_ + 360;
      if (fabs(delTH_) > 10 || fabs(delTH_) <= delTH_TOL)
      {
        vel_.angular.z = 0;
      }
      else
      {
        vel_.angular.z = lrf_tracking_ctrl(0.008, delTH_, LVTH_MAX, 0.008);
      }
    }
    else if (cur_A > 80 && cur_A < 100)
    {
      if (lenY_ > 2)
        vel_.linear.x = lrf_tracking_ctrl(Kp, delY_, LVX_MAX, 0.005);
      else
        vel_.linear.x = lrf_tracking_ctrl(Kp, delY_, 0.3, 0.005);

      if (lenX_ > 2)
        vel_.linear.y = -lrf_tracking_ctrl(Kp, delX_, LVY_MAX, 0.005);
      else
        vel_.linear.y = -lrf_tracking_ctrl(Kp, delX_, 0.25, 0.005);
    }
    else if (cur_A > 80 && cur_A < 100)
    {
      if (lenY_ > 2)
        vel_.linear.x = lrf_tracking_ctrl(Kp, delY_, LVX_MAX, 0.005);
      else
        vel_.linear.x = lrf_tracking_ctrl(Kp, delY_, 0.3, 0.005);

      if (lenX_ > 2)
        vel_.linear.y = -lrf_tracking_ctrl(Kp, delX_, LVY_MAX, 0.005);
      else
        vel_.linear.y = -lrf_tracking_ctrl(Kp, delX_, 0.25, 0.005);
    }
    else if ((cur_A > 170 && cur_A <= 180) || (cur_A < -170 && cur_A >= -180))
    {
      if (lenY_ > 2)
        vel_.linear.y = -lrf_tracking_ctrl(Kp, delY_, LVY_MAX, 0.005);
      else
        vel_.linear.y = -lrf_tracking_ctrl(Kp, delY_, 0.25, 0.005);

      if (lenX_ > 2)
        vel_.linear.x = -lrf_tracking_ctrl(Kp, delX_, LVX_MAX, 0.005);
      else
        vel_.linear.x = -lrf_tracking_ctrl(Kp, delX_, 0.3, 0.005);

      delTH_ = goal_A - current_A;
      if (delTH_ > 180)
        delTH_ = delTH_ - 360;
      else if (delTH_ < -180)
        delTH_ = delTH_ + 360;
      if (fabs(delTH_) > 10 || fabs(delTH_) <= delTH_TOL)
      {
        vel_.angular.z = 0;
      }
      else
      {
        vel_.angular.z = lrf_tracking_ctrl(0.008, delTH_, LVTH_MAX, 0.008);
      }
    }
    else if (cur_A >= -100 && cur_A <= -80) // around -90
    {
      if (lenY_ > 2)
        vel_.linear.x = -lrf_tracking_ctrl(Kp, delY_, LVX_MAX, 0.005);
      else
        vel_.linear.x = -lrf_tracking_ctrl(Kp, delY_, 0.3, 0.005);
      if (lenX_ > 2)
        vel_.linear.y = lrf_tracking_ctrl(Kp, delX_, LVY_MAX, 0.005);
      else
        vel_.linear.y = lrf_tracking_ctrl(Kp, delX_, 0.25, 0.005);
    }
  }
  // vel_pub.publish(vel_);
}

void NavigationManagement::DIAG_control()
{
  delX_ = goal_x - current_x;
  delY_ = goal_y - current_y;
  lenX_ = fabs(goal_x - cur_x);
  lenY_ = fabs(goal_y - cur_y);

  delTH_ = goal_A - current_A;
  if (delTH_ >= 180)
    delTH_ = delTH_ - 360;
  else if (delTH_ <= -180)
    delTH_ = delTH_ + 360;
  vel_.linear.x = 0;
  vel_.linear.y = 0;

  if (fabs(delY_) <= delY_TOL && fabs(delX_) <= delX_TOL &&
      fabs(delTH_) <= delTH_TOL)
  {
    vel_.linear.x = 0;
    vel_.linear.y = 0;
    vel_.angular.z = 0;
    agvStatus_ = LRF_XY_COMP;
  }
  else
  {
    agvStatus_ = LRF_RUN;
    vel_.angular.z = lrf_tracking_ctrl(0.01, delTH_, LVTH_MAX, 0.008);
    if (cur_A > -10 && cur_A < 10) // Around 0 deg
    {
      if (lenX_ > 4)
        vel_.linear.x = lrf_tracking_ctrl(Kp, delX_, LVX_MAX, 0.005);
      else
        vel_.linear.x = lrf_tracking_ctrl(Kp, delX_, 0.1, 0.005);

      if (lenY_ > 4)
        vel_.linear.y = lrf_tracking_ctrl(Kp, delY_, LVY_MAX, 0.005);
      else
        vel_.linear.y = lrf_tracking_ctrl(Kp, delY_, 0.1, 0.005);
    }
    else if (cur_A > 80 && cur_A < 100)
    {
      if (lenY_ > 2)
        vel_.linear.x = lrf_tracking_ctrl(Kp, delY_, LVX_MAX, 0.005);
      else
        vel_.linear.x = lrf_tracking_ctrl(Kp, delY_, 0.3, 0.005);

      if (lenX_ > 3)
        vel_.linear.y = -lrf_tracking_ctrl(Kp, delX_, 0.2, 0.005);
      else
        vel_.linear.y = -lrf_tracking_ctrl(Kp, delX_, 0.1, 0.005);
    }
    else if ((cur_A > 170 && cur_A <= 180) || (cur_A < -170 && cur_A >= -180))
    {
      if (lenY_ > 2)
        vel_.linear.y = -lrf_tracking_ctrl(Kp, delY_, LVY_MAX, 0.005);
      else
        vel_.linear.y = -lrf_tracking_ctrl(Kp, delY_, 0.25, 0.005);

      if (lenX_ > 2)
        vel_.linear.x = -lrf_tracking_ctrl(Kp, delX_, LVX_MAX, 0.005);
      else
        vel_.linear.x = -lrf_tracking_ctrl(Kp, delX_, 0.3, 0.005);
    }
    else if (cur_A >= -100 && cur_A <= -80) // around -90
    {
      if (lenY_ > 2)
        vel_.linear.x = -lrf_tracking_ctrl(Kp, delY_, LVX_MAX, 0.005);
      else
        vel_.linear.x = -lrf_tracking_ctrl(Kp, delY_, 0.3, 0.005);

      if (lenX_ > 2)
        vel_.linear.y = lrf_tracking_ctrl(Kp, delX_, 0.2, 0.005);
      else
        vel_.linear.y = lrf_tracking_ctrl(Kp, delX_, 0.15, 0.005);
    }
  }
  // vel_pub.publish(vel_);
}

void NavigationManagement::YAW_control()
{
  delTH_ = goal_A - current_A;
  if (delTH_ >= 180)
    delTH_ = delTH_ - 360;
  else if (delTH_ <= -180)
    delTH_ = delTH_ + 360;
  vel_.linear.x = 0;
  vel_.linear.y = 0;
  if (fabs(delTH_) <= delTH_TOL)
  {
    vel_.angular.z = 0;
    agvStatus_ = WP_REACH;
    lrfMove_ = LRF_NONE_;
  }
  else
  {
    vel_.angular.z = lrf_tracking_ctrl(0.01, delTH_, LVTH_MAX, 0.008);
  }
  // vel_pub.publish(vel_);
}

void NavigationManagement::nav_control()
{
  switch (moveMode_)
  {
  case MOVE_LRF_:
  {
    switch (lrfMove_)
    {
    case LRF_XY_:
      XY_control();
      break;
    case LRF_YAW_:
      YAW_control();
      break;
    case LRF_DIAG_:
      DIAG_control();
      break;
    }
  }
  break;
  case MOVE_QR_:
  {
    qr_move();
  }
  break;

  case MOVE_ZERO_:
    stop_agv();
    break;
  }
}

void NavigationManagement::obstacle_cb(const std_msgs::UInt8::ConstPtr& msg)
{

  if (msg->data == 0)
  {
    if (mode_agv_ == mode_obs_)
      mode_agv_ = mode_start_;
  }
  else
  {
    if (mode_agv_ == mode_start_ && moveMode_ == MOVE_LRF_)
      mode_agv_ = mode_obs_;
  }
}

void NavigationManagement::RTH() // return to home
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_home)
    {
      goal_x = target_pos_x;
      goal_y = target_pos_y;
      goal_A = target_A;
    }
    else
    {
      goal_x = init_pose_[0];
      goal_y = init_pose_[1];
      goal_A = init_pose_[2];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    moveMode_ = MOVE_QR_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;

    if (stop_ == to_home)
    {
      home_flag = 0;
      moveMode_ = MOVE_ZERO_;
      mode_agv_ = mode_stop_;
      agvStatus_ = GOAL_REACH;
      move_point_ = no_point_;
    }
    else
    {
      if (stop_ == to_arm12)
      {
        if (target_pos_y > init_pose_[1])
        {
          move_point_ = wp1_;
        }
        else
        {
          move_point_ = wp2_;
        }
      }
      else if (stop_ == to_charger)
        move_point_ = charger2_;
      else if (stop_ == to_arm34)
        move_point_ = wp8_;
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp5_;
      else if (stop_ == to_AU || stop_ == to_BU)
        move_point_ = wp6_;
      else if (stop_ == to_AL || stop_ == to_BL)
        move_point_ = wp6_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp8_;
      else if (stop_ == to_home1)
        move_point_ = wp6_;
      else if (stop_ == to_cart)
        move_point_ = wp6_;
      else
        move_point_ = no_point_;
    }
  }
}

void NavigationManagement::home1_move() // move to ST11
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = home1_pose[0];
    goal_y = home1_pose[1];
    goal_A = home1_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    moveMode_ = MOVE_QR_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;

    if (stop_ == to_home1)
    {
      home_flag = 0;
      moveMode_ = MOVE_ZERO_;
      mode_agv_ = mode_stop_;
      agvStatus_ = GOAL_REACH;
      move_point_ = no_point_;
    }
    else
    {
      if (stop_ == to_cart)
        move_point_ = cart_;
      else if (stop_ == to_AU || stop_ == to_BU)
        move_point_ = wp15_;
      else if (stop_ == to_AL || stop_ == to_BL)
        move_point_ = point1_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp16_;
      else if (stop_ == to_arm12 || stop_ == to_arm34 || stop_ == to_charger ||
               stop_ == to_home)
        move_point_ = wp15_;
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp16_;
      else
        move_point_ = no_point_;
    }
  }
}

void NavigationManagement::cart_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = target_pos_x;
    goal_y = target_pos_y;
    goal_A = target_A;

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    moveMode_ = MOVE_QR_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;
    moveMode_ = MOVE_ZERO_;
    mode_agv_ = mode_stop_;
    agvStatus_ = GOAL_REACH;
    move_point_ = no_point_;
  }
}

void NavigationManagement::wp1_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = init_pose_[0];
    goal_y = init_pose_[1];
    if (stop_ == to_arm12)
      goal_A = target_A;
    else
      goal_A = wp1_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_arm12)
      {
        move_point_ = arm12_;
      }
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp2_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = init_pose_[0];
    goal_y = init_pose_[1];
    if (stop_ == to_arm12)
      goal_A = target_A;
    else
      goal_A = wp2_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_arm12)
      {
        move_point_ = arm12_;
      }
      else
      {
        move_point_ = no_point_;
      }
    }
    count = 0;
  }
}

void NavigationManagement::wp3_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp3_pose[0];
    goal_y = wp3_pose[1];
    goal_A = wp3_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_arm12)
        move_point_ = rot1_;
      else if (stop_ == to_charger)
        move_point_ = wp4_;
      else if (stop_ == to_arm34)
        move_point_ = arm34_;
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp5_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp8_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp4_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp3_pose[0];
    goal_y = wp3_pose[1];
    goal_A = wp3_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = wp3_;
    }
    else
    {
      if (stop_ == to_charger)
        move_point_ = charger_;
      else if (stop_ == to_arm34)
        move_point_ = rot2_;
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = rot2_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = rot2_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp5_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp5_pose[0];
    goal_y = wp5_pose[1];
    if (stop_ == to_home || stop_ == to_AU || stop_ == to_BU ||
        stop_ == to_arm12)
    {
      goal_A = init_pose_[2];
    }
    else
      goal_A = wp5_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_charger)
        move_point_ = rot3_;
      else if (stop_ == to_arm12)
        move_point_ = rot3_;
      else if (stop_ == to_arm34)
        move_point_ = rot3_;
      else if (stop_ == to_arm5)
        move_point_ = arm5_;
      else if (stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp52_;
      else if (stop_ == to_AU || stop_ == to_BU)
        move_point_ = wp6_;
      else if (stop_ == to_AL || stop_ == to_BL)
        move_point_ = wp8_;
      else if (stop_ == to_C || stop_ == to_D || stop_ == to_home1 ||
               stop_ == to_cart)
        move_point_ = wp8_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp51_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp51_pose[0];
    goal_y = wp51_pose[1];
    goal_A = wp51_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_; // Modify 210125
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp9_;
      else
        move_point_ = wp52_;
    }
    else
      move_point_ = no_point_;
    count = 0;
  }
}

void NavigationManagement::wp52_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp52_pose[0];
    goal_y = wp52_pose[1];
    goal_A = wp52_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp51_;
      else
        move_point_ = wp5_;
    }
    else
      move_point_ = no_point_;
    count = 0;
  }
}

void NavigationManagement::wp6_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_AU || stop_ == to_BU)
    {
      goal_A = target_A;
    }
    else if (stop_ == to_home1 || stop_ == to_cart)
    {
      goal_A = wp15_pose[2];
    }
    else if (stop_ == to_AL || stop_ == to_BL)
    {
      goal_A = wp15_pose[2];
    }
    else if (stop_ == to_arm12)
    {
      goal_A = target_A;
    }
    else
    {
      goal_A = init_pose_[2];
    }

    goal_x = init_pose_[0];
    goal_y = wp6_pose[1];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_AU)
        move_point_ = AU_;
      else if (stop_ == to_BU)
        move_point_ = BU_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = home1_;
      else if (stop_ == to_AL || stop_ == to_BL)
        move_point_ = home1_;
      else if (stop_ == to_arm12)
      {
        if (target_pos_y > init_pose_[1])
        {
          move_point_ = wp1_;
        }
        else
        {
          move_point_ = wp2_;
        }
      }
      else
        move_point_ = home_;
    }
    count = 0;
  }
}

void NavigationManagement::wp7_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = init_pose_[0];
    goal_y = wp7_pose[1];
    goal_A = init_pose_[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_C || stop_ == to_D)
      {
        move_point_ = wp8_;
      }
      else if (stop_ == to_arm34)
      {
        move_point_ = arm34_;
      }
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      {
        move_point_ = wp5_;
      }
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = rot5_;
      else
      {
        move_point_ = no_point_;
      }
    }
    count = 0;
  }
}

void NavigationManagement::wp8_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (start_ == from_C || start_ == from_D)
    {
      if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      {
        goal_A = cur_A;
        goal_y = cur_y;
      }
      else
      {
        goal_y = cur_y;
        goal_A = init_pose_[2];
      }
    }
    else
    {
      if (stop_ == to_C || stop_ == to_D)
      {
        goal_A = target_A;
        if (stop_ == to_C)
        {
          goal_y = wp8_pose[1] + 1;
        }
        else
        {
          goal_y = wp8_pose[1] - 1;
        }
      }
      else if (stop_ == to_home1 || stop_ == to_AL || stop_ == to_BL)
      {
        goal_y = wp8_pose[1];
        goal_A = 0;
      }
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      {
        goal_A = wp8_pose[2];
        goal_y = wp8_pose[1];
      }
    }

    goal_x = wp8_pose[0];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_C)
        move_point_ = C_;
      else if (stop_ == to_D)
        move_point_ = D_;
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp5_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = wp16_;
      else
      {
        move_point_ = no_point_;
      }
    }
    count = 0;
  }
}

void NavigationManagement::wp9_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp9_pose[0];
    goal_y = wp9_pose[1];
    goal_A = wp9_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm6 || stop_ == to_arm7)
      {
        move_point_ = wp10_;
      }
      else
      {
        move_point_ = wp51_;
      }
    }
    else
    {
      move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp10_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;
    if (stop_ == to_arm6)
    {
      goal_x = wp10_pose[0];
      goal_y = wp10_pose[1];
      goal_A = target_A;
    }
    else
    {
      goal_x = wp10_pose[0];
      goal_y = wp10_pose[1];
      goal_A = wp10_pose[2];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();
  if (start_ == from_arm6)
  {
    if (agvStatus_ == LRF_XY_COMP)
    {
      moveMode_ = MOVE_LRF_;
      lrfMove_ = LRF_YAW_;
    }
    else if (agvStatus_ == WP_REACH)
    {
      if (stop_ != stop_null)
      {
        if (stop_ == to_arm6)
        {
          move_point_ = arm6_;
        }
        else if (stop_ == to_arm7)
        {
          move_point_ = wp11_;
        }
        else
        {
          move_point_ = wp9_;
        }
      }
      else
      {
        move_point_ = no_point_;
      }
      count = 0;
    }
  }
  else
  {
    if (agvStatus_ == LRF_XY_COMP)
    {
      moveMode_ = MOVE_LRF_;
      lrfMove_ = LRF_YAW_;
    }
    else if (agvStatus_ == WP_REACH)
    {
      if (stop_ != stop_null)
      {
        if (stop_ == to_arm6)
        {
          move_point_ = arm6_;
        }
        else if (stop_ == to_arm7)
        {
          move_point_ = wp11_;
        }
        else
        {
          move_point_ = wp9_;
        }
      }
      else
      {
        move_point_ = no_point_;
      }
      count = 0;
    }
  }
}

void NavigationManagement::wp11_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp11_pose[0];
    goal_y = wp11_pose[1];
    goal_A = wp11_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;

    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();
  if (stop_ == to_arm7)
  {
    if (agvStatus_ == LRF_XY_COMP)
    {
      moveMode_ = MOVE_LRF_;
      lrfMove_ = LRF_YAW_;
    }
    else if (agvStatus_ == WP_REACH)
    {
      move_point_ = wp12_;
      count = 0;
    }
  }
  else
  {
    if (agvStatus_ == LRF_XY_COMP)
    {
      moveMode_ = MOVE_LRF_;
      lrfMove_ = LRF_YAW_;
    }
    else if (agvStatus_ == WP_REACH)
    {
      move_point_ = wp10_;
      count = 0;
    }
  }
}

void NavigationManagement::wp12_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp12_pose[0];
    goal_y = wp12_pose[1];
    goal_A = wp12_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;

    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();
  if (start_ == from_arm7)
  {
    if (agvStatus_ == LRF_XY_COMP)
    {
      moveMode_ = MOVE_LRF_;
      lrfMove_ = LRF_YAW_;
    }
    else if (agvStatus_ == WP_REACH)
    {
      if (stop_ != stop_null)
      {
        move_point_ = wp11_;
      }
      else
      {
        move_point_ = no_point_;
      }
      count = 0;
    }
  }
  else
  {
    if (agvStatus_ == LRF_XY_COMP)
    {
      moveMode_ = MOVE_LRF_;
      lrfMove_ = LRF_YAW_;
    }
    else if (agvStatus_ == WP_REACH)
    {
      if (stop_ != stop_null)
      {
        move_point_ = wp13_;
      }
      else
      {
        move_point_ = no_point_;
      }
      count = 0;
    }
  }
}

void NavigationManagement::wp13_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp13_pose[0];
    goal_y = wp13_pose[1];
    goal_A = wp13_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm7)
      {
        move_point_ = wp14_;
      }
      else
      {
        move_point_ = wp12_;
      }
    }
    else
    {
      move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp14_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp14_pose[0];
    goal_y = wp14_pose[1];
    goal_A = wp14_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm7)
      {
        move_point_ = arm7_;
      }
      else
      {
        move_point_ = wp13_;
      }
    }
    else
    {
      move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::point1_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = point1_pose[0];
    goal_y = point1_pose[1];
    goal_A = point1_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_AL || stop_ == to_BL)
      move_point_ = point2_;
    else if (stop_ == to_C || stop_ == to_D || stop_ == to_arm5 ||
             stop_ == to_arm6 || stop_ == to_arm7)
      move_point_ = wp16_;
    else if (stop_ == to_AU || stop_ == to_BU || stop_ == to_arm12 ||
             stop_ == to_home || stop_ == to_charger)
      move_point_ = wp15_;
    else
      move_point_ = home1_;

    count = 0;
  }
}

void NavigationManagement::point2_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = point2_pose[0];
    goal_y = point2_pose[1];
    goal_A = point2_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_AL || stop_ == to_BL)
      move_point_ = point3_;
    else
      move_point_ = point1_;
    count = 0;
  }
}

void NavigationManagement::point3_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = point3_pose[0];
    goal_y = point3_pose[1];
    goal_A = point3_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_AL || stop_ == to_BL)
      move_point_ = point4_;
    else
      move_point_ = point2_;

    count = 0;
  }
}

void NavigationManagement::point4_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = point4_pose[0];
    goal_y = point4_pose[1];

    if (stop_ == to_AL || stop_ == to_BL)
    {
      goal_A = target_A;
    }
    else
    {
      goal_A = point4_pose[2];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_AL)
      move_point_ = AL_;
    else if (stop_ == to_BL)
      move_point_ = BL_;
    else
      move_point_ = point3_;

    count = 0;
  }
}

void NavigationManagement::wp15_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp15_pose[0];
    goal_y = wp15_pose[1];
    if (stop_ == to_AU || stop_ == to_BU)
    {
      goal_A = target_A;
    }
    else
    {
      goal_A = wp15_pose[2];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_AU)
        move_point_ = AU_;
      else if (stop_ == to_BU)
        move_point_ = BU_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = home1_;
      else if (stop_ == to_arm12)
        move_point_ = wp6_;
      else if (stop_ == to_home)
        move_point_ = wp6_;
      else if (stop_ == to_charger)
        move_point_ = wp6_;
      else
        move_point_ = wp6_;
    }
    else
    {
      move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::wp16_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp16_pose[0];

    if (stop_ == to_C)
    {
      goal_A = target_A;
      goal_y = wp16_pose[1] + 1;
    }
    else if (stop_ == to_D)
    {
      goal_A = target_A;
      goal_y = wp16_pose[1] - 1;
    }
    else if (stop_ == to_home1 || stop_ == to_AL || stop_ == to_BL)
    {
      if (start_ == from_C)
      {
        goal_y = wp16_pose[1] + 1;
      }
      else if (start_ == from_D)
      {
        goal_y = wp16_pose[1] - 1;
      }
      else
        goal_y = wp16_pose[1];

      goal_A = home1_pose[2];
    }
    else
    {
      goal_A = wp16_pose[2];
      goal_y = wp16_pose[1];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_C)
        move_point_ = C_;
      else if (stop_ == to_D)
        move_point_ = D_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = home1_;
      else if (stop_ == to_AU || stop_ == to_BU)
        move_point_ = home1_;
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp8_;
      else if (stop_ == to_AL || stop_ == to_BL)
        move_point_ = point1_;
    }
    else
    {
      move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::rot1_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp3_pose[0];
    goal_y = wp3_pose[1];
    if (target_pos_y > init_pose_[1])
    {
      goal_A = wp1_pose[2];
    }
    else
    {
      goal_A = wp2_pose[2];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_arm12)
        move_point_ = arm12_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::rot2_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp4_pose[0];
    goal_y = wp4_pose[1];
    goal_A = wp3_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_arm34)
      {
        move_point_ = arm34_;
      }
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      {
        move_point_ = wp5_;
      }
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp8_;
      else if (stop_ == to_AU || stop_ == to_BU || stop_ == to_home1 ||
               stop_ == to_cart)
        move_point_ = rot5_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::rot3_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp5_pose[0];
    goal_y = wp5_pose[1];
    goal_A = init_pose_[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = wp3_;
    }
    else
    {
      if (stop_ == to_arm34)
      {
        move_point_ = arm34_;
      }
      else if (stop_ == to_arm12)
      {
        move_point_ = rot1_;
      }
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp8_;
      else if (stop_ == to_AU || stop_ == to_BU)
        move_point_ = rot5_;
      else if (stop_ = to_charger)
        move_point_ = wp4_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = wp8_;
      else
        move_point_ = no_point_;
    }
    count = 0;
  }
}

void NavigationManagement::rot4_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp8_pose[0];
    if (start_ == from_C)
      goal_y = wp8_pose[1] + 1;
    else if (start_ == from_D)
      goal_y = wp8_pose[1] - 1;
    else
      goal_y = wp8_pose[1];
    goal_A = wp3_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = wp3_;
    }
    else
    {
      if (stop_ == to_home)
      {
        move_point_ = wp3_;
      }
      else if (stop_ == to_arm34)
      {
        move_point_ = arm34_;
      }
      else if (stop_ == to_arm12)
      {
        move_point_ = rot1_;
      }
      else if (stop_ == to_charger)
      {
        move_point_ = wp4_;
      }
      else if (stop_ == to_AU || stop_ == to_BU)
      {
        move_point_ = rot5_;
      }
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = wp8_;
      else
      {
        move_point_ = no_point_;
      }
    }
    count = 0;
  }
}

void NavigationManagement::rot5_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = wp7_pose[0];
    goal_y = wp7_pose[1];
    goal_A = target_A;

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_AU)
    {
      move_point_ = AU_;
    }
    else if (stop_ == to_BU)
      move_point_ = BU_;
    else if (stop_ == to_home1 || stop_ == to_cart)
      move_point_ = home1_;
    else
    {
      move_point_ = no_point_;
    }

    count = 0;
  }
}

void NavigationManagement::AU_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_y = wp6_pose[1];

    if (stop_ == to_AU)
    {
      goal_x = target_pos_x;
      goal_A = target_A;
    }
    else
    {
      goal_x = cur_x;
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_AU)
    {
      move_point_ = rack_;
    }
    else if (stop_ == to_BU)
      move_point_ = wp6_;
    else if (stop_ == to_home)
      move_point_ = wp6_;
    else if (stop_ == to_charger)
      move_point_ = wp6_;
    else if (stop_ == to_C || stop_ == to_D)
      move_point_ = wp7_;
    else if (stop_ == to_arm12)
      move_point_ = wp6_;
    else if (stop_ == to_arm34)
      move_point_ = wp7_;
    else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      move_point_ = wp7_;
    else if (stop_ == to_home1 || stop_ == to_cart)
      move_point_ = home1_;
    else
      move_point_ = no_point_;
    count = 0;
  }
}

void NavigationManagement::BU_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_y = wp6_pose[1];

    if (stop_ == to_BU)
    {
      goal_x = target_pos_x;
      goal_A = target_A;
    }
    else
    {
      goal_x = cur_x;
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_BU)
    {
      move_point_ = rack_;
    }
    else if (stop_ == to_AU)
      move_point_ = wp6_;
    else if (stop_ == to_home)
      move_point_ = wp6_;
    else if (stop_ == to_charger)
      move_point_ = wp6_;
    else if (stop_ == to_C || stop_ == to_D)
      move_point_ = wp7_;
    else if (stop_ == to_arm12)
      move_point_ = wp6_;
    else if (stop_ == to_arm34)
      move_point_ = wp7_;
    else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      move_point_ = wp7_;
    else if (stop_ == to_home1 || stop_ == to_cart)
      move_point_ = home1_;
    else
      move_point_ = no_point_;
    count = 0;
  }
}

void NavigationManagement::AL_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_y = point4_pose[1];

    if (stop_ == to_AL)
    {
      goal_x = target_pos_x;
      goal_A = target_A;
    }
    else
    {
      goal_x = cur_x;
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_AL)
    {
      move_point_ = rack_;
    }
    else
      move_point_ = point4_;
    count = 0;
  }
}

void NavigationManagement::BL_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_y = point4_pose[1];

    if (stop_ == to_BL)
    {
      goal_x = target_pos_x;
      goal_A = target_A;
    }
    else
    {
      goal_x = cur_x;
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_BL)
    {
      move_point_ = rack_;
    }
    else
      move_point_ = point4_;
    count = 0;
  }
}

void NavigationManagement::C_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_C)
    {
      goal_x = target_pos_x;
      goal_A = target_A;
      goal_y = target_pos_y - 1.0;
    }
    else
    {
      goal_x = cur_x;
      goal_A = cur_A;
      goal_y = wp8_pose[1] + 1;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_C)
    {
      move_point_ = rack_;
    }
    else if (stop_ == to_D)
      move_point_ = wp8_;
    else if (stop_ == to_home)
      move_point_ = rot4_;
    else if (stop_ == to_charger)
      move_point_ = rot4_;
    else if (stop_ == to_AU || stop_ == to_BU)
      move_point_ = rot4_;
    else if (stop_ == to_arm12)
      move_point_ = rot4_;
    else if (stop_ == to_arm34)
      move_point_ = rot4_;
    else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      move_point_ = wp8_;
    else if (stop_ == to_home1 || stop_ == to_cart)
      move_point_ = wp16_;
    else
      move_point_ = no_point_;
    count = 0;
  }
}

void NavigationManagement::D_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_D)
    {
      goal_x = target_pos_x;
      goal_A = target_A;
      goal_y = target_pos_y + 1.0;
    }
    else
    {
      goal_x = cur_x;
      goal_A = cur_A;
      goal_y = wp8_pose[1] - 1;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_D)
    {
      move_point_ = rack_;
    }
    else if (stop_ == to_C)
      move_point_ = wp8_;
    else if (stop_ == to_home)
      move_point_ = rot4_;
    else if (stop_ == to_charger)
      move_point_ = rot4_;
    else if (stop_ == to_AU || stop_ == to_BU)
      move_point_ = rot4_;
    else if (stop_ == to_arm12)
      move_point_ = rot4_;
    else if (stop_ == to_arm34)
      move_point_ = rot4_;
    else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      move_point_ = wp8_;
    else if (stop_ == to_home1 || stop_ == to_cart)
      move_point_ = wp16_;
    else
      move_point_ = no_point_;

    count = 0;
  }
}

void NavigationManagement::arm12_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (start_ == from_arm12)
    {
      goal_x = cur_x;
      goal_y = init_pose_[1];
      goal_A = cur_A;
    }
    else
    {
      goal_x = target_pos_x;
      goal_y = init_pose_[1];
      goal_A = target_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = home_;
    }
    else
    {
      if (stop_ == to_charger)
      {
        move_point_ = wp3_;
      }
      else if (stop_ == to_arm12)
      {
        if (start_ == from_arm12)
        {
          move_point_ = arm12_;
        }
        else
          move_point_ = rack_;
      }
      else if (stop_ == to_arm34)
      {
        move_point_ = home_;
      }
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      {
        move_point_ = home_;
      }
      else if (stop_ == to_AU || stop_ == to_BU)
        move_point_ = home_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = home_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = home_;
      else
      {
        move_point_ = no_point_;
      }
    }

    count = 0;
  }
}

void NavigationManagement::arm34_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_arm34)
    {
      goal_x = wp3_pose[0];
      goal_y = target_pos_y;
      goal_A = target_A;
    }
    else
    {
      goal_x = wp3_pose[0];
      goal_y = cur_y;
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = wp3_;
    }
    else
    {
      if (stop_ == to_charger)
      {
        move_point_ = wp4_;
      }
      else if (stop_ == to_arm12)
      {
        move_point_ = rot1_;
      }
      else if (stop_ == to_arm34)
      {
        if (start_ == from_arm34)
        {
          move_point_ = arm34_;
        }
        else
          move_point_ = rack_;
      }
      else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
      {
        move_point_ = wp5_;
      }
      else if (stop_ == to_AU || stop_ == to_BU)
        move_point_ = rot5_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp8_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = rot5_;
      else
      {
        move_point_ = no_point_;
      }
    }

    count = 0;
  }
}

void NavigationManagement::arm5_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_arm5)
    {
      goal_x = target_pos_x;
      goal_y = wp5_pose[1];
      goal_A = target_A;
    }
    else
    {
      goal_x = cur_x;
      goal_y = wp5_pose[1];
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ == to_home)
    {
      move_point_ = rot3_;
    }
    else
    {
      if (stop_ == to_charger)
      {
        move_point_ = rot3_;
      }
      else if (stop_ == to_arm12)
      {
        move_point_ = rot3_;
      }
      else if (stop_ == to_arm34)
      {
        move_point_ = wp5_;
      }
      else if (stop_ == to_arm5)
      {
        move_point_ = rack_;
      }
      else if (stop_ == to_arm6 || stop_ == to_arm7)
        move_point_ = wp51_;
      else if (stop_ == to_AU || stop_ == to_BU)
        move_point_ = rot3_;
      else if (stop_ == to_C || stop_ == to_D)
        move_point_ = wp5_;
      else if (stop_ == to_home1 || stop_ == to_cart)
        move_point_ = wp5_;
      else
      {
        move_point_ = no_point_;
      }
    }

    count = 0;
  }
}

void NavigationManagement::arm6_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_arm6)
    {
      goal_x = wp10_pose[0];
      goal_y = target_pos_y;
      goal_A = target_A;
    }
    else
    {
      goal_x = wp10_pose[0];
      goal_y = cur_y;
      goal_A = cur_A;
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm6)
      {
        move_point_ = rack_;
      }
      else
      {
        move_point_ = wp10_;
      }
    }
    count = 0;
  }
}

void NavigationManagement::arm7_move()
{

  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_arm7)
    {
      goal_x = target_pos_x;
      goal_y = wp14_pose[1];
      goal_A = target_A;
    }
    else
    {
      goal_x = cur_x;
      goal_y = wp14_pose[1];
      goal_A = cur_A;
    }
    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    if (stop_ != stop_null)
    {
      if (stop_ == to_arm7)
      {
        move_point_ = rack_;
      }
      else
      {
        move_point_ = wp14_;
      }
    }
    count = 0;
  }
}

void NavigationManagement::charge_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = target_pos_x;
    goal_y = target_pos_y;
    goal_A = target_A;

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_DIAG_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    moveMode_ = MOVE_QR_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;
    moveMode_ = MOVE_ZERO_;
    mode_agv_ = mode_stop_;
    agvStatus_ = GOAL_REACH;
    move_point_ = no_point_;
  }
}

void NavigationManagement::charge1_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = target_pos_x + 0.7;
    goal_y = init_pose_[1];
    goal_A = charge_pose[2];

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    moveMode_ = MOVE_QR_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;
    if (stop_ == to_charger)
      move_point_ = charger2_;
    else if (stop_ = to_home)
      move_point_ = home_;
    else
      move_point_ = no_point_;
  }
}

void NavigationManagement::charge2_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    if (stop_ == to_charger)
    {
      goal_x = init_pose_[0];
      goal_y = target_pos_y;
      goal_A = target_A;
    }
    else
    {
      goal_x = init_pose_[0];
      goal_y = cur_y;
      goal_A = init_pose_[2];
    }

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    count = 0;
    if (stop_ == to_charger)
      move_point_ = charger_;
    else if (stop_ = to_home)
      move_point_ = home_;
    else
      move_point_ = no_point_;
  }
}

void NavigationManagement::param_set()
{

  if (nh.getParam("/init_pose", init_pose_))
  {
    for (vector<int>::size_type i = 0; i < init_pose_.size(); ++i)
    {
      ROS_INFO("init_pose : %.2f", init_pose_[i]);
    }
  }

  if (nh.getParam("/init_ori", init_ori))
  {
    for (vector<int>::size_type i = 0; i < init_ori.size(); ++i)
    {
      ROS_INFO("init_ori : %.2f", init_ori[i]);
    }
  }

  if (nh.getParam("/charge_pose", charge_pose))
  {
    for (vector<int>::size_type i = 0; i < charge_pose.size(); ++i)
    {
      ROS_INFO("charge_pose : %.2f", charge_pose[i]);
    }
  }

  if (nh.getParam("/wp1_pose", wp1_pose))
  {
    for (vector<int>::size_type i = 0; i < wp1_pose.size(); ++i)
    {
      ROS_INFO("wp1_pose : %.2f", wp1_pose[i]);
    }
  }

  if (nh.getParam("/wp2_pose", wp2_pose))
  {
    for (vector<int>::size_type i = 0; i < wp2_pose.size(); ++i)
    {
      ROS_INFO("wp2_pose : %.2f", wp2_pose[i]);
    }
  }

  if (nh.getParam("/wp3_pose", wp3_pose))
  {
    for (vector<int>::size_type i = 0; i < wp3_pose.size(); ++i)
    {
      ROS_INFO("wp3_pose : %.2f", wp3_pose[i]);
    }
  }

  if (nh.getParam("/wp4_pose", wp4_pose))
  {
    for (vector<int>::size_type i = 0; i < wp4_pose.size(); ++i)
    {
      ROS_INFO("wp4_pose : %.2f", wp4_pose[i]);
    }
  }

  if (nh.getParam("/wp5_pose", wp5_pose))
  {
    for (vector<int>::size_type i = 0; i < wp5_pose.size(); ++i)
    {
      ROS_INFO("wp5_pose : %.2f", wp5_pose[i]);
    }
  }

  if (nh.getParam("/wp51_pose", wp51_pose))
  {
    for (vector<int>::size_type i = 0; i < wp51_pose.size(); ++i)
    {
      ROS_INFO("wp51_pose : %.2f", wp51_pose[i]);
    }
  }

  if (nh.getParam("/wp52_pose", wp52_pose))
  {
    for (vector<int>::size_type i = 0; i < wp52_pose.size(); ++i)
    {
      ROS_INFO("wp52_pose : %.2f", wp52_pose[i]);
    }
  }

  if (nh.getParam("/wp6_pose", wp6_pose))
  {
    for (vector<int>::size_type i = 0; i < wp6_pose.size(); ++i)
    {
      ROS_INFO("wp6_pose : %.2f", wp6_pose[i]);
    }
  }

  if (nh.getParam("/wp7_pose", wp7_pose))
  {
    for (vector<int>::size_type i = 0; i < wp7_pose.size(); ++i)
    {
      ROS_INFO("wp7_pose : %.2f", wp7_pose[i]);
    }
  }

  if (nh.getParam("/wp8_pose", wp8_pose))
  {
    for (vector<int>::size_type i = 0; i < wp8_pose.size(); ++i)
    {
      ROS_INFO("wp8_pose : %.2f", wp8_pose[i]);
    }
  }

  if (nh.getParam("/wp9_pose", wp9_pose))
  {
    for (vector<int>::size_type i = 0; i < wp9_pose.size(); ++i)
    {
      ROS_INFO("wp9_pose : %.2f", wp9_pose[i]);
    }
  }

  if (nh.getParam("/wp10_pose", wp10_pose))
  {
    for (vector<int>::size_type i = 0; i < wp10_pose.size(); ++i)
    {
      ROS_INFO("wp10_pose : %.2f", wp10_pose[i]);
    }
  }

  if (nh.getParam("/wp11_pose", wp11_pose))
  {
    for (vector<int>::size_type i = 0; i < wp11_pose.size(); ++i)
    {
      ROS_INFO("wp11_pose : %.2f", wp11_pose[i]);
    }
  }

  if (nh.getParam("/wp12_pose", wp12_pose))
  {
    for (vector<int>::size_type i = 0; i < wp12_pose.size(); ++i)
    {
      ROS_INFO("wp12_pose : %.2f", wp12_pose[i]);
    }
  }

  if (nh.getParam("/wp13_pose", wp13_pose))
  {
    for (vector<int>::size_type i = 0; i < wp13_pose.size(); ++i)
    {
      ROS_INFO("wp13_pose : %.2f", wp13_pose[i]);
    }
  }

  if (nh.getParam("/wp14_pose", wp14_pose))
  {
    for (vector<int>::size_type i = 0; i < wp14_pose.size(); ++i)
    {
      ROS_INFO("wp14_pose : %.2f", wp14_pose[i]);
    }
  }

  if (nh.getParam("/wp15_pose", wp15_pose))
  {
    for (vector<int>::size_type i = 0; i < wp15_pose.size(); ++i)
    {
      ROS_INFO("wp15_pose : %.2f", wp15_pose[i]);
    }
  }

  if (nh.getParam("/wp16_pose", wp16_pose))
  {
    for (vector<int>::size_type i = 0; i < wp16_pose.size(); ++i)
    {
      ROS_INFO("wp16_pose : %.2f", wp16_pose[i]);
    }
  }

  if (nh.getParam("/home1_pose", home1_pose))
  {
    for (vector<int>::size_type i = 0; i < home1_pose.size(); ++i)
    {
      ROS_INFO("home1_pose : %.2f", home1_pose[i]);
    }
  }

  if (nh.getParam("/cart_pose", cart_pose))
  {
    for (vector<int>::size_type i = 0; i < cart_pose.size(); ++i)
    {
      ROS_INFO("cart_pose : %.2f", cart_pose[i]);
    }
  }

  if (nh.getParam("/point1_pose", point1_pose))
  {
    for (vector<int>::size_type i = 0; i < point1_pose.size(); ++i)
    {
      ROS_INFO("point1_pose : %.2f", point1_pose[i]);
    }
  }

  if (nh.getParam("/point2_pose", point2_pose))
  {
    for (vector<int>::size_type i = 0; i < point2_pose.size(); ++i)
    {
      ROS_INFO("point2_pose : %.2f", point2_pose[i]);
    }
  }

  if (nh.getParam("/point3_pose", point3_pose))
  {
    for (vector<int>::size_type i = 0; i < point3_pose.size(); ++i)
    {
      ROS_INFO("point3_pose : %.2f", point3_pose[i]);
    }
  }

  if (nh.getParam("/point4_pose", point4_pose))
  {
    for (vector<int>::size_type i = 0; i < point4_pose.size(); ++i)
    {
      ROS_INFO("point4_pose : %.2f", point4_pose[i]);
    }
  }
}

void NavigationManagement::rack_move()
{
  if (count == 0)
  {
    cur_x = current_x;
    cur_y = current_y;
    cur_A = current_A;

    goal_x = target_pos_x;
    goal_y = target_pos_y;
    goal_A = target_A;

    cur_goal_.x = goal_x;
    cur_goal_.y = goal_y;
    cur_goal_.theta = goal_A * M_PI / 180;
    goal_pub.publish(cur_goal_);

    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_XY_;
    count = 1;
    agvStatus_ = LRF_RUN;
  }
  nav_control();

  if (agvStatus_ == LRF_XY_COMP)
  {
    moveMode_ = MOVE_LRF_;
    lrfMove_ = LRF_YAW_;
  }
  else if (agvStatus_ == WP_REACH)
  {
    moveMode_ = MOVE_QR_;
  }
  else if (agvStatus_ == QR_COMP)
  {
    count = 0;
    moveMode_ = MOVE_ZERO_;
    mode_agv_ = mode_stop_;
    agvStatus_ = GOAL_REACH;
    move_point_ = no_point_;
  }
}

void NavigationManagement::joystickControlCallback(
    const move_control::teleop::ConstPtr& motorCmd_)
{
  if (motorCmd_->control_mode == 1 || motorCmd_->control_mode == 2)
  {
    manual_mode = true;
    man_x = motorCmd_->linear_x;
    man_y = motorCmd_->linear_y;
    man_a = motorCmd_->angular_z;
  }
  else
  {
    manual_mode = false;
    man_x = 0;
    man_y = 0;
    man_a = 0;
  }
}

void NavigationManagement::stop_agv()
{
  vel_.linear.x = 0;
  vel_.linear.y = 0;
  vel_.angular.z = 0;
  // vel_pub.publish(vel_);
}

void NavigationManagement::qr_move()
{

  if (tag_ == true)
  {
    if (fabs(line_x) <= 5 && fabs(line_y) <= 5 && line_theta > 179 &&
        line_theta < 181)
    {
      vel_.linear.x = 0;
      vel_.linear.y = 0;
      vel_.angular.z = 0;
      agvStatus_ = QR_COMP;
    }
    else
    {
      if (fabs(line_x) > 5)
        vel_.linear.x = -QR_VEL * sign(line_x);
      else
        vel_.linear.x = 0;

      if (fabs(line_y) > 5)
        vel_.linear.y = -QR_VEL * sign(line_y);
      else
        vel_.linear.y = 0;

      if (line_theta > 140 && line_theta < 220)
      {
        if (line_theta >= 181 || line_theta <= 179)
          vel_.angular.z = QR_AVEL * sign(line_theta - 180);
        else
          vel_.angular.z = 0;
      }
      else
        vel_.angular.z = 0;
      agvStatus_ = QR_CTRL;
    }
  }
  else
  {
    vel_.linear.x = 0;
    vel_.linear.y = 0;
    vel_.angular.z = 0;
    agvStatus_ = QR_COMP;
  }

  /*
    vel_.linear.x = 0;
    vel_.linear.y = 0;
    vel_.angular.z = 0;
    // vel_pub.publish(vel_);
    agvStatus_ = QR_COMP;
    */
}
} // namespace agv_wp

int main(int argc, char** argv)
{
  ros::init(argc, argv, "AGV_nav_action");

  agv_wp::NavigationManagement AGV_nav;
  AGV_nav.spin();
  return 0;
}
