//written by lee
//WP0 -> AB_WP -> precision_move -> qr_move
//WP1 -> CD_WP -> precision_move -> qr_move
//RTH_2 -> AB_WP -> wp_move1 -> RTH -> qr_move
//RTH_2 -> CD_WP -> wp_move2 -> RTH -> qr_move

#include <ros/ros.h>
#include <vector>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose.h>
#include <actionlib_msgs/GoalStatus.h>
#include <actionlib_msgs/GoalStatusArray.h>
#include <actionlib_msgs/GoalID.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_broadcaster.h>
#include <if_node_msgs/cmdMove.h>
#include <pgv/vision_msg.h>
#include <move_control/agv_mode.h>
#include <move_control/qr_vel.h>
#include <obstacle_detector/obstacles_detect.h>
#include <nav_msgs/Odometry.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/LinearMath/Transform.h>
#include <geometry_msgs/PointStamped.h>
#include <move_base_msgs/MoveBaseActionFeedback.h>
//#include <nav_msgs/Feedback.h>

#define QR_VEL 0.02
#define CART_VEL 0.02
#define QR_NUM 5

using namespace std;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

namespace gil{
	enum lee{
		status_0_=0,status_1_,status_2_,status_3_,status_4_,status_5_,status_6_,status_7_,status_8_
	};
	enum Wp{
    wp_1=0, wp_2, wp_3, wp_4, wp_5, wp_6, wp_7
	};
	enum Move_WP1{
    move_1_wp1, move_2_wp1
	};
	enum Move_WP2{
		move_1_wp2, move_2_wp2
	};
  // Add Dinh 200519
  enum Move_WP3{
    move_1_wp3, move_2_wp3
  };

	enum QR_Move{
		move_x_, move_y_, move_theta_
	};
	enum Rth{
		rth_0, rth_1, rth_2, rth_3
	};

	typedef struct status{
		bool pending;
		bool active;
		bool complete;

	} status;

	class move{
		public:
			move();
			void init();
			void spin();
			void update();
			void param_set();
			void wp_move1();
			void wp_move2();
      void wp_move3();
      // Add Dinh 200519
      void point1_move();
      void point2_move();
      void ABU_WP();
      void ABL_WP();
			void RTH();
			void RTH_2();
			void RTH_ROTATION();
      // Add Dinh 200519
      void RTH_ROTATION2();
			void charge_move();
			void cart_move();
			void init_pose();
			void agv_run();
			void home_reset();
			void precision_move();
			void qr_move();
			void tf_agv_position();

		private:
			bool run_;
			bool target_;
      bool ABU_WP_, ABL_WP_;
			bool CD_WP_;
			bool wp_;
      bool point1_, point2_;
			bool goal_status;
			bool tag_, control_;
			bool pgv_home;
			bool rack_move;
			bool h_reset;
			bool qr_reset;
			bool obstacle_;
			int tag_id, line_, line_id, control_id, fault, warning, command;
			int rate;
			int move_state; //move : 1, stop : 0
			int wp_state;
			int init_flag;
			int home_flag;
      int rth, rth2;
			int qr_count;
			float target_pos_x, target_pos_y, target_pos_z,
			      target_ori_x, target_ori_y, target_ori_z, target_ori_w;
			float line_x, line_y, line_theta;
			float odom_pos_x, odom_pos_y, odom_pos_z, odom_ori_z, odom_ori_w;
			float tf_fb_x, tf_fb_y, tf_fb_ori_z, tf_fb_ori_w;
			std_msgs::UInt8 state_goal;
			geometry_msgs::Twist vel_;
			geometry_msgs::PoseStamped target_goal;
			geometry_msgs::PoseStamped obstacle_goal;
			actionlib_msgs::GoalStatus goal_Status;
			actionlib_msgs::GoalID cancel;
			move_base_msgs::MoveBaseGoal goal;
			geometry_msgs::PointStamped pose_msgs;

//			tf2_ros::Buffer tfBuffer;
//			tf2_ros::TransformListener tfListener(tfBuffer);
//			nav_msgs::Odometry odom_data;

			ros::NodeHandle nh;

			vector<float> init_pose_, init_ori;
			vector<float> cart_init_pose, cart_init_ori;
			vector<float> charge_pose, charge_ori;
      // Add Dinh 200519
      vector<float> wp1_pose, wp2_pose, wp3_pose, wp4_pose, wp5_pose, wp6_pose, point1_pose, point2_pose;
      vector<float> wp1_ori, wp2_ori, wp3_ori, wp4_ori, wp5_ori, wp5_ori, point1_ori, point2_ori;

			ros::Publisher vel_pub, goal_pub, reach_pub, init_pub, cancel_pub, tf_base_link_pub, qr_ang_pub;
			ros::Subscriber reach_sub, goal_sub, init_sub, home_sub, pgv_sub, odom_sub, move_base_feedback_sub, obstacle_sub;
					//cancel_sub;

			void goal_reach_cb(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach);
		//	void goal_target_cb(const geometry_msgs::PoseStamped& goal);
			void goal_target_cb(const if_node_msgs::cmdMove& goal);
			void init_cb(const std_msgs::UInt8::ConstPtr& init_);
			void home_cb(const std_msgs::UInt8::ConstPtr& home_);
			void pgv_cb(const pgv::vision_msg& pgv_msg);
			void odom_cb(const nav_msgs::Odometry& odom_);
			void tf_feedback_cb(const move_base_msgs::MoveBaseActionFeedback& feedback_);
			void obstacle_cb(const obstacle_detector::obstacles_detect& obstacle_msg);
		//	void cancel_cb(const actionlib_msgs::GoalID& cancel_);

	};

//	Move move_;
	Move_WP1 MOVE_WP1;
	Move_WP2 MOVE_WP2;

  // Add Dinh 200519
  Move_WP3 MOVE_WP3;

	QR_Move _move;
	Wp WP_;
	Rth RTH_;
}

void gil::move::init()
{
//	ROS_INFO("variables initial");

	rate = 100; //10; //100
	target_pos_x = 0.0;
	target_pos_y = 0.0;
	target_pos_z = 0.0;

	target_ori_x = 0.0;
	target_ori_y = 0.0;
	target_ori_z = 0.0;
	target_ori_w = 0.99;

	target_ = false;
//	target_ = true;
  ABU_WP_ = false;
  ABL_WP_ = false;
	CD_WP_ = false;
	run_ = true;
	wp_ = false;
  point1_ = false;
  point2_ = false;
 	goal_status = true;
	pgv_home = false;
	rack_move =false;
	h_reset = false;
	qr_reset = false;
	obstacle_ = false;

	init_flag = 0;
	rth = 0;
	qr_count = 0;

}

gil::move::move()
{
	init();
	param_set();

	vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1, true);
//	goal_pub = nh.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1, true);
	goal_pub = nh.advertise<geometry_msgs::PoseStamped>("/current_goal", 1, true);
	reach_pub = nh.advertise<std_msgs::UInt8>("/IFNode/goal_reach", 1, true);
	init_pub = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>("/initialpose", 1, true);
	cancel_pub = nh.advertise<actionlib_msgs::GoalID>("/move_base/cancel", 1, true);
	tf_base_link_pub = nh.advertise<geometry_msgs::PointStamped>("/agv_actual_position", 1, true);

	reach_sub = nh.subscribe("/move_base/status", 10, &gil::move::goal_reach_cb, this);
//	goal_sub = nh.subscribe("/goal_target", 10, &gil::move::goal_target_cb, this);
	goal_sub = nh.subscribe("/MOVE/CMD", 10, &gil::move::goal_target_cb, this);
	init_sub = nh.subscribe("/init_pose", 1, &gil::move::init_cb, this);
	home_sub = nh.subscribe("/home_pose", 1, &gil::move::home_cb, this);
	pgv_sub = nh.subscribe("/pgv_data", 10, &gil::move::pgv_cb, this);
	odom_sub = nh.subscribe("/odom", 10, &gil::move::odom_cb, this);
	move_base_feedback_sub = nh.subscribe("/move_base/feedback", 10, &gil::move::tf_feedback_cb ,this);
	obstacle_sub = nh.subscribe("/obstacle_info", 1, &gil::move::obstacle_cb, this);
//	cancel_sub = nh.subscribe("/move_base/cancel", 1, &gil::move::cancel_cb, this);

}

void gil::move::update()
{
	ros::Time::now();

/*
	if(obstacle_){
		ROS_INFO("AGV move cancel");
		ROS_INFO("Obstacle detected");
		cancel_pub.publish(cancel);
		obstacle_ = false;
	}
	else{
//		ROS_INFO("Obstacle not detected");
	}
*/

	if(home_flag){ //return to home

	//	RTH();
		RTH_2();
	//	home_flag = 0;
		pgv_home = true;

	}

	if(init_flag){ //AGV init pose
		init_pose();
		init_flag = 0;
	}

	if(wp_){
		switch(WP_){
			case wp_1:
			//	ROS_INFO("wp_move1()");
        wp_move1();//A-B up
				break;

			case wp_2:
			//	ROS_INFO("wp_move2()");
				wp_move2();//C-D
				break;

// Add Dinh 200519
    case wp_3:
      point1_move();//A-B low
      break;

/*
			case wp_3:
				break;

			case wp_4:
				break;

			case wp_5:
				break;

			case wp_6:
				break;
*/
			default:
				break;

		}
	}


  if(ABU_WP_){
    ABU_WP();
	}
  else if(ABL_WP_)
  {
    ABL_WP();
  }
  else if(CD_WP_)
  {
		CD_WP();
	}
// Add Dinh 200519
  if(point1_)
  {
    point1_move();
  }
  else if(point2_)
  {
    point2_move();
  }

	if(h_reset){
		home_reset();
	}

	if(rack_move){
		precision_move();
	}

	if(qr_reset){
		qr_move();
	}

//	reach_pub.publish(state_goal);

}

void gil::move::spin()
{
	ros::Rate loop_rate(rate);

	while(run_)
	{
		update();
		ros::spinOnce();
		loop_rate.sleep();
	}

}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "moving_agv");
	ROS_INFO("=======AGV1 STAND-BY=======");
	gil::move *mv = new gil::move();
	mv->spin();
	gil::status st;
	st.active = true;

	return 0;
}


void gil::move::goal_target_cb(const if_node_msgs::cmdMove& goal)
{
	ROS_INFO("AGV received the coordinate data from ACS");

	target_pos_x = goal.pose.position.x;
	target_pos_y = goal.pose.position.y;
 	target_pos_z = goal.pose.position.z;

	target_ori_x = goal.pose.orientation.x;
	target_ori_y = goal.pose.orientation.y;
	target_ori_z = goal.pose.orientation.z;
	target_ori_w = goal.pose.orientation.w;


  /*obstacle_goal.pose.position.x = target_pos_x;
	obstacle_goal.pose.position.y = target_pos_y;
	obstacle_goal.pose.position.z = target_pos_z;

	obstacle_goal.pose.orientation.x = target_ori_x;
	obstacle_goal.pose.orientation.y = target_ori_y;
	obstacle_goal.pose.orientation.z = target_ori_z;
	obstacle_goal.pose.orientation.w = target_ori_w;

  goal_pub.publish(obstacle_goal);*/

//	ROS_INFO("publish obstacle_goal");

	move_state = goal.move;

	ROS_INFO("==============================");
	ROS_INFO("[ACS -> AGV] pos_x : %.4f",target_pos_x);
	ROS_INFO("[ACS -> AGV] pos_y : %.4f",target_pos_y);
//	ROS_INFO("[ACS -> AGV] z : %.2f",target_pos_z);
	ROS_INFO("[ACS -> AGV] ori_z : %.4f",target_ori_z);
	ROS_INFO("[ACS -> AGV] ori_w : %.4f",target_ori_w);
	ROS_INFO("==============================");

	ROS_INFO("move state : %d",move_state);
//	ROS_INFO("target_pos_y : %.2f",target_pos_y);

	if(target_pos_x == cart_init_pose[0] || target_pos_y == cart_init_pose[1]){ //AGV move to cart-position
		ROS_INFO("AGV is moving the cart-point");
		cart_move();

	}
	else if(target_pos_x == charge_pose[0] || target_pos_y == charge_pose[1]){ //AGV move to charging station
		if(tf_fb_x != init_pose_[0] || tf_fb_y != init_pose_[1]){
			ROS_INFO("you first push the Home Button");
		}
		else{
			ROS_INFO("AGV is movig the charging station");
		//	RTH_2();
			charge_move();
		}
	}
	else{
		wp_ = true;
    if(target_pos_y >= 10.0){ //A-B rack
      // Add Dinh 200519
      if(target_pos_x >=5)
      {
        WP_ = wp_1;
        ROS_INFO("A-B Rack Up");

        if(target_pos_y > init_pose_[1]){ //left-side(A-B)
        //	move_ = move_1_;
        //	MOVE_ = move_1_;
          MOVE_WP1 = move_1_wp1;
    //			ROS_INFO("[right turn(A-B)] : %d",move_);
          ROS_INFO("[right turn(A-B)]");
        }
        else if(target_pos_y > 6.0 && target_pos_y < init_pose_[1]){//right-side(A-B)
        //	move_ = move_2_;
        //	MOVE_ = move_2_;
          MOVE_WP1 = move_2_wp1;
      //		ROS_INFO("[left turn(A-B)]: %d",move_);
          ROS_INFO("[left turn(A-B)]");
        }
        else
        {
          ROS_INFO("Check again!! Y value");
        }
      }
      else
      {
        WP_ = wp_3;
        ROS_INFO("A-B Rack Low");

        if(target_pos_y > init_pose_[1]){ //left-side(A-B)
        //	move_ = move_1_;
        //	MOVE_ = move_1_;

          MOVE_WP3 = move_1_wp3;
    //			ROS_INFO("[right turn(A-B)] : %d",move_);
          ROS_INFO("[right turn(A-B)]");
        }
        else if(target_pos_y > 6.0 && target_pos_y < init_pose_[1]){//right-side(A-B)
        //	move_ = move_2_;
        //	MOVE_ = move_2_;
          MOVE_WP3 = move_2_wp3;
      //		ROS_INFO("[left turn(A-B)]: %d",move_);
          ROS_INFO("[left turn(A-B)]");
        }
        else
        {
          ROS_INFO("Check again!! Y value");
        }
      }

		}
		else if(target_pos_y < 10.0){//C-D rack
			WP_ = wp_2;
			ROS_INFO("C-D Rack");

			RTH(); //because AGV move to C-D rack through the colunm

      if(target_pos_y < 0.0){ //right-side(C-D)
			//	move_ = move_3_;
			//	MOVE_ = move_3_;
				MOVE_WP2 = move_1_wp2;
	//			ROS_INFO("[right turn(C-D)] : %d",move_);
				ROS_INFO("[left turn(C-D)]");
			}
			else if(target_pos_y >= 0.0 && target_pos_y <= 6.0){ //left-side(C-D)
			//	move_ = move_4_;
			//	MOVE_ = move_4_;
				MOVE_WP2 = move_2_wp2;
	//			ROS_INFO("[left turn(C-D)] : %d",move_);
				ROS_INFO("[right turn(C-D)]");
			}
			else{
				ROS_INFO("Check again!! Y value");
			}

		}
	}

}


void gil::move::goal_reach_cb(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach)
{
	if(!goal_reach->status_list.empty()){
//		actionlib_msgs::GoalStatus goal_Status = goal_reach->status_list[0];
		goal_Status = goal_reach->status_list[0];
		state_goal.data = goal_Status.status;
//		state_goal.data = goal_reach->status_list[0];
//		cancel.id = "";

		switch(goal_Status.status){
			case status_0_:
				ROS_INFO("AGV STATUS : PENDING[%d]",goal_Status.status);
				break;

			case status_1_:
//				ROS_INFO("AGV STATUS : ACTIVE[%d]",goal_Status.status);
//				ROS_INFO("AGV STATUS : ACTIVE");
				break;

			case status_3_:
//				ROS_INFO("AGV STATUS : COMPLETE[%d]",goal_Status.status);
//				ROS_INFO("AGV STATUS : GOAL REACH");
//				cancel_pub.publish(cancel);
//				ROS_INFO("Cancel Movement");
				break;

			case status_4_:
				ROS_INFO("AGV STATUS : ABORTED[%d]",goal_Status.status);
				break;

			case status_8_:
				ROS_INFO("AGV STATUS : RECALLED[%d]",goal_Status.status);
				break;

			default:
				break;
		}

	}
	else
	{
//		ROS_INFO("AGV STATUS : IDLING");
	}
}

void gil::move::init_cb(const std_msgs::UInt8::ConstPtr& init_)
{
	ROS_INFO("Init Position");
	init_flag = init_->data;
//	ROS_INFO("init_flag : %d",init_flag);
}

void gil::move::home_cb(const std_msgs::UInt8::ConstPtr& home_)
{
	ROS_INFO("Return To Home Sweet Home");
	home_flag = home_->data;
//	ROS_INFO("home_flag : %d",home_flag);
	RTH_ = rth_0;
}

void gil::move::pgv_cb(const pgv::vision_msg& pgv_msg)
{
	control_id = pgv_msg.ControlID;
	control_ = pgv_msg.Control;
	tag_ = pgv_msg.Tag;
	tag_id = pgv_msg.TagID;
	line_ = pgv_msg.Line;
	line_id = pgv_msg.LineID;
	line_x = pgv_msg.X;
	line_y = pgv_msg.Y;
	line_theta = pgv_msg.theta;
	fault = pgv_msg.Fault;
	warning = pgv_msg.Warning;
	command = pgv_msg.command;

}

void gil::move::odom_cb(const nav_msgs::Odometry& odom_)
{
//	nav_msgs::Odometry odom_data;

	odom_pos_x = odom_.pose.pose.position.x;
	odom_pos_y = odom_.pose.pose.position.y;
	odom_pos_z = odom_.pose.pose.position.z;

	odom_ori_z = odom_.pose.pose.orientation.z;
	odom_ori_w = odom_.pose.pose.orientation.w;


}


void gil::move::tf_feedback_cb(const move_base_msgs::MoveBaseActionFeedback& feedback_)
{
	tf_fb_x	= feedback_.feedback.base_position.pose.position.x;
	tf_fb_y	= feedback_.feedback.base_position.pose.position.y;
	tf_fb_ori_z = feedback_.feedback.base_position.pose.orientation.z;
	tf_fb_ori_w = feedback_.feedback.base_position.pose.orientation.w;

}



void gil::move::obstacle_cb(const obstacle_detector::obstacles_detect& obstacle_msg)
{
//	ROS_INFO("obstacle_cb");
//	ROS_INFO("obstacle number : %d",obstacle_msg.number);


  if(!obstacle_msg.number)
  {
    ROS_INFO("No Obstacle detected");
	}
  else
  {
    ROS_INFO("Obstacle detected");
	}


}

void gil::move::RTH()//return to home
{
	ROS_INFO("RTH()");


	goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0)))
		ROS_INFO("Waiting for the move_base action server");

	goal.target_pose.pose.position.x = init_pose_[0];
  goal.target_pose.pose.position.y = init_pose_[1];
	goal.target_pose.pose.position.z = init_pose_[2];

	goal.target_pose.pose.orientation.x = init_ori[0];
	goal.target_pose.pose.orientation.y = init_ori[1];
	goal.target_pose.pose.orientation.z = init_ori[2];
  goal.target_pose.pose.orientation.w = init_ori[3];

  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

	state_goal.data = 1;
	reach_pub.publish(state_goal);
//	ROS_INFO("RTH() status = 1");

        ac.sendGoal(goal);

        ac.waitForResult();

        if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                ROS_INFO("AGV has reached to home");

		state_goal.data = 3;
		reach_pub.publish(state_goal);
//		ROS_INFO("RTH() status = 3");
/*
		if(!home_flag){
			wp_ = false;
		}
		else{
			RTH_ = rth_3;
			wp_ = false;
//			ROS_INFO("RTH_ROTATION : rth_3");
		}
*/
        }
        else{
                ROS_INFO("AGV failed for some reaseon");
        }

	home_flag = 0;
	rth = 0;

	h_reset = true;
	_move = move_x_;
}

void gil::move::RTH_2()
{
	ROS_INFO("RTH_2()");

  /*obstacle_goal.pose.position.x = init_pose_[0];
	obstacle_goal.pose.position.y = init_pose_[1];
	obstacle_goal.pose.position.z = init_pose_[2];

	obstacle_goal.pose.orientation.x = init_ori[0];
	obstacle_goal.pose.orientation.y = init_ori[1];
	obstacle_goal.pose.orientation.z = init_ori[2];
	obstacle_goal.pose.orientation.w = init_ori[3];

  goal_pub.publish(obstacle_goal);*/
//	ROS_INFO("publish home obstacle_goal");

	if(target_pos_y >=10.0){ //A-B Rack
		switch(rth){
			case 0:
        ABU_WP();
				break;

			case 1:
				RTH();
				break;
/*
			case 2:
			//	wp_move1();
			//	RTH();
				break;

			case 3:
				RTH();
				break;
*/
			default:
				break;
		}

	}
	else if(target_pos_y<10.0){ //C-D Rack
//		switch(rth){
		switch(RTH_){
			case rth_0:
//				ROS_INFO("case rth_0 : CD_WP");
				CD_WP();
				break;

			case rth_1:
//				ROS_INFO("case rth_1 : wp_move2()");
				wp_move2();
				break;

			case rth_2:
//				ROS_INFO("case rth_2 : RTH_ROTATION");
				RTH_ROTATION();
				break;

			case rth_3:
//				ROS_INFO("case rth_3 : RTH");
				RTH();
				break;

/*			case 3:
				wp_move1();
				break;
*/
			default:
				break;
		}

	}
}
void gil::move::RTH_ROTATION()
{
	ROS_INFO("RTH_ROTATION()");

/*
	target_goal.header.stamp = ros::Time::now();
	target_goal.header.frame_id = "map";

	target_goal.pose.position.x = wp3_pose[0];
        target_goal.pose.position.y = wp3_pose[1];
	target_goal.pose.position.z = 0.0;

	target_goal.pose.orientation.x= 0.0;
	target_goal.pose.orientation.y= 0.0;
	target_goal.pose.orientation.z = init_ori[2];
        target_goal.pose.orientation.w = init_ori[3];

	goal_pub.publish(target_goal);

	if(!home_flag){
			wp_ = false;
	}
	else{
		RTH_ = rth_3;
		wp_ = false;
	}
*/

	goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0)))
		ROS_INFO("Waiting for the move_base action server");

  if(target_pos_y<0)
  {
    goal.target_pose.pose.position.x = wp3_pose[0];
          goal.target_pose.pose.position.y = wp3_pose[1];
    goal.target_pose.pose.position.z = wp3_pose[2];

    goal.target_pose.pose.orientation.x = init_ori[0];
    goal.target_pose.pose.orientation.y = init_ori[1];
    goal.target_pose.pose.orientation.z = init_ori[2];
    goal.target_pose.pose.orientation.w = init_ori[3];
  }
  else {
    goal.target_pose.pose.position.x = wp4_pose[0];
    goal.target_pose.pose.position.y = wp4_pose[1];
    goal.target_pose.pose.position.z = wp4_pose[2];

    goal.target_pose.pose.orientation.x = init_ori[0];
    goal.target_pose.pose.orientation.y = init_ori[1];
    goal.target_pose.pose.orientation.z = init_ori[2];
    goal.target_pose.pose.orientation.w = init_ori[3];
  }


	ROS_INFO("AGV rotate for home");
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

	state_goal.data = 1;
	reach_pub.publish(state_goal);
//	ROS_INFO("RTH_ROTATION status = 1");

        ac.sendGoal(goal);

        ac.waitForResult();

        if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                ROS_INFO("AGV has rotated completely");

	//	state_goal.data = 3;
	//	reach_pub.publish(state_goal);
	//	ROS_INFO("state_goal.data = 3");

		if(!home_flag){
			wp_ = false;
		}
		else{
			RTH_ = rth_3;
			wp_ = false;
		}
        }
        else{
                ROS_INFO("AGV failed for some reaseon");
        }

}

void gil::move::charge_move()
{
	ROS_INFO("AGV Rotate for Charging");
	goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0)))
		ROS_INFO("Waiting for the move_base action server");

	goal.target_pose.pose.position.x = init_pose_[0];
        goal.target_pose.pose.position.y = init_pose_[1];
	goal.target_pose.pose.position.z = init_pose_[2];

	goal.target_pose.pose.orientation.x = charge_ori[0];
	goal.target_pose.pose.orientation.y = charge_ori[1];
	goal.target_pose.pose.orientation.z = charge_ori[2];
  goal.target_pose.pose.orientation.w = charge_ori[3];

  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

	state_goal.data = 1;
	reach_pub.publish(state_goal);
//	ROS_INFO("charge_move() status = 1");

        ac.sendGoal(goal);
        ac.waitForResult();

        if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                ROS_INFO("AGV has rotated completely for charging");

	//	state_goal.data = 3;
	//	reach_pub.publish(state_goal);
	//	ROS_INFO("charge_move() status = 3");
        }
        else{
                ROS_INFO("AGV failed for some reaseon");
        }


	goal.target_pose.pose.position.x = target_pos_x;
        goal.target_pose.pose.position.y = target_pos_y;
	goal.target_pose.pose.position.z = target_pos_z;

	goal.target_pose.pose.orientation.x = target_ori_x;
	goal.target_pose.pose.orientation.y = target_ori_y;
	goal.target_pose.pose.orientation.z = target_ori_z;
        goal.target_pose.pose.orientation.w = target_ori_w;

        // Add Dinh 200519
        obstacle_goal.pose = goal.target_pose.pose;
        goal_pub.publish(obstacle_goal);

	state_goal.data = 1;
	reach_pub.publish(state_goal);
//	ROS_INFO("charge_move() status = 1");

        ac.sendGoal(goal);
        ac.waitForResult();

        if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                ROS_INFO("AGV has moved to the charge station completely");

		state_goal.data = 3;
		reach_pub.publish(state_goal);
//		ROS_INFO("charge_move() status = 3");
	}
        else
                ROS_INFO("AGV failed for some reaseon");

}

void gil::move::init_pose()
{
	geometry_msgs::PoseWithCovarianceStamped init_;
	init_.header.stamp = ros::Time::now();
	init_.header.frame_id = "map";

/*	init_.pose.pose.position.x = INIT_POS_X;
	init_.pose.pose.position.y = INIT_POS_Y;*/
	init_.pose.pose.position.x = init_pose_[0];
	init_.pose.pose.position.y = init_pose_[1];
	init_.pose.pose.position.z = 0.0;

	init_.pose.pose.orientation.x = 0.0;
	init_.pose.pose.orientation.y = 0.0;
/*	init_.pose.pose.orientation.z = INIT_ORI_Z;
	init_.pose.pose.orientation.w = INIT_ORI_W;*/
	init_.pose.pose.orientation.z = init_ori[2];
	init_.pose.pose.orientation.w = init_ori[3];

	init_pub.publish(init_);

//	init_flag = 0;

	printf("AGV InitialPose Complete\n");

}

void gil::move::param_set()
{

	if(nh.getParam("/init_pose_",init_pose_)){
		for(vector<int>::size_type i=0;i<init_pose_.size();++i){
//			ROS_INFO("init_pose : %.2f",init_pose_[i]);
		}
	}

	if(nh.getParam("/init_ori",init_ori)){
		for(vector<int>::size_type i=0;i<init_ori.size();++i){
//			ROS_INFO("init_ori : %.2f",init_ori[i]);
		}
	}

	if(nh.getParam("/cart_init_pose",cart_init_pose)){
		for(vector<int>::size_type i=0;i<cart_init_pose.size();++i){
//			ROS_INFO("cart_init_pose : %.2f",cart_init_pose[i]);
		}
	}

	if(nh.getParam("/cart_init_ori",cart_init_ori)){
		for(vector<int>::size_type i=0;i<cart_init_ori.size();++i){
//			ROS_INFO("cart_init_ori : %.2f",cart_init_ori[i]);
		}
	}

	if(nh.getParam("/charge_pose",charge_pose)){
		for(vector<int>::size_type i=0;i<charge_pose.size();++i){
//			ROS_INFO("charge_pose : %.2f",charge_pose[i]);
		}
	}

	if(nh.getParam("/charge_ori",charge_ori)){
		for(vector<int>::size_type i=0;i<charge_ori.size();++i){
//			ROS_INFO("charge_ori : %.2f",charge_ori[i]);
		}
	}

	if(nh.getParam("/wp1_pose",wp1_pose)){
		for(vector<int>::size_type i=0;i<wp1_pose.size();++i){
//			ROS_INFO("wp1_pose : %.2f",wp1_pose[i]);
		}
	}

	if(nh.getParam("/wp1_ori",wp1_ori)){
		for(vector<int>::size_type i=0;i<wp1_ori.size();++i){
//			ROS_INFO("wp1_ori : %.2f",wp1_ori[i]);
		}
	}

	if(nh.getParam("/wp2_pose",wp2_pose)){
		for(vector<int>::size_type i=0;i<wp2_pose.size();++i){
//			ROS_INFO("wp2_pose : %.2f",wp2_pose[i]);
		}
	}

	if(nh.getParam("/wp2_ori",wp2_ori)){
		for(vector<int>::size_type i=0;i<wp2_ori.size();++i){
//			ROS_INFO("wp2_ori : %.2f",wp2_ori[i]);
		}
	}

	if(nh.getParam("/wp3_pose",wp3_pose)){
		for(vector<int>::size_type i=0;i<wp3_pose.size();++i){
//			ROS_INFO("wp3_pose : %.2f",wp3_pose[i]);
		}
	}

	if(nh.getParam("/wp3_ori",wp3_ori)){
		for(vector<int>::size_type i=0;i<wp3_ori.size();++i){
//			ROS_INFO("wp3_ori : %.2f",wp3_ori[i]);
		}
	}

	if(nh.getParam("/wp4_pose",wp4_pose)){
		for(vector<int>::size_type i=0;i<wp4_pose.size();++i){
//			ROS_INFO("wp4_pose : %.2f",wp4_pose[i]);
		}
	}

	if(nh.getParam("/wp4_ori",wp4_ori)){
		for(vector<int>::size_type i=0;i<wp4_ori.size();++i){
//			ROS_INFO("wp4_ori : %.2f",wp4_ori[i]);
		}
	}
}

void gil::move::wp_move1() //A-B Rack UP
{
	goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0)))
		ROS_INFO("Waiting for the move_base action server");

//	ROS_INFO("MOVE_WP1 : %d",MOVE_WP1);

	switch(MOVE_WP1){

		case move_1_wp1: //left-side(right-turn)

			if(move_state == 1){
				goal.target_pose.pose.position.x = wp1_pose[0];
        			goal.target_pose.pose.position.y = wp1_pose[1];
				goal.target_pose.pose.orientation.z = wp1_ori[2];
        			goal.target_pose.pose.orientation.w = wp1_ori[3];

				ROS_INFO("AGV move to the left-side(A-B)");
        // Add Dinh 200519
        obstacle_goal.pose = goal.target_pose.pose;
        goal_pub.publish(obstacle_goal);
				state_goal.data = 1;
				reach_pub.publish(state_goal);
			//	ROS_INFO("wp_move1() status = 1");

         			ac.sendGoal(goal);

        			ac.waitForResult();

        			if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                			ROS_INFO("AGV has arrived to the left-side(A-B)");
                			//target_ = true;
                ABU_WP_ = true;
					wp_ = false;
				//	rth = 3;

				//	state_goal.data = 3;
				//	reach_pub.publish(state_goal);
				//	ROS_INFO("state_goal.data = 3");
        			}
        			else{
                			ROS_INFO("AGV failed for some reaseon");
        			}
			}
			else{
				ROS_INFO("AGV is not de-activate");
			}

			break;

		case move_2_wp1: //right-side(left-turn)

			if(move_state == 1){
				goal.target_pose.pose.position.x = wp2_pose[0];
        			goal.target_pose.pose.position.y = wp2_pose[1];
				goal.target_pose.pose.orientation.z = wp2_ori[2];
        			goal.target_pose.pose.orientation.w = wp2_ori[3];

				ROS_INFO("AGV move to the right-side(A-B)");
        // Add Dinh 200519
        obstacle_goal.pose = goal.target_pose.pose;
        goal_pub.publish(obstacle_goal);
				state_goal.data = 1;
				reach_pub.publish(state_goal);
			//	ROS_INFO("wp_move1() status = 1");

         			ac.sendGoal(goal);

        			ac.waitForResult();

        			if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                			ROS_INFO("AGV has arrived to the right-side(A-B)");
                			//target_ = true;
                      ABU_WP_ = true;
					wp_ = false;
				//	rth = 3;

				//	state_goal.data = 3;
				//	reach_pub.publish(state_goal);
				//	ROS_INFO("state_goal.data = 3");
        			}
        			else{
                			ROS_INFO("AGV failed for some reaseon");
        			}
			}
			else{
				ROS_INFO("AGV is not de-activate");
			}

			break;

		default:
			break;
	}

}

void gil::move::point1_move()
{

}

void gil::move::point2_move()
{

}

void gil::move::wp_move3() //A-B Rack LOW
{
  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  MoveBaseClient ac("move_base", true);

  while(!ac.waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

//	ROS_INFO("MOVE_WP1 : %d",MOVE_WP1);

  switch(MOVE_WP1){

    case move_1_wp1: //left-side(right-turn)

      if(move_state == 1){
        goal.target_pose.pose.position.x = wp5_pose[0];
              goal.target_pose.pose.position.y = wp5_pose[1];
        goal.target_pose.pose.orientation.z = wp5_ori[2];
              goal.target_pose.pose.orientation.w = wp5_ori[3];

        ROS_INFO("AGV move to the left-side(A-B)");
        // Add Dinh 200519
        obstacle_goal.pose = goal.target_pose.pose;
        goal_pub.publish(obstacle_goal);
        state_goal.data = 1;
        reach_pub.publish(state_goal);
      //	ROS_INFO("wp_move1() status = 1");

              ac.sendGoal(goal);

              ac.waitForResult();

              if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                      ROS_INFO("AGV has arrived to the left-side(A-B)");
                      //target_ = true;
          ABL_WP_ = true;
          point2_ = false;
        //	rth = 3;

        //	state_goal.data = 3;
        //	reach_pub.publish(state_goal);
        //	ROS_INFO("state_goal.data = 3");
              }
              else{
                      ROS_INFO("AGV failed for some reaseon");
              }
      }
      else{
        ROS_INFO("AGV is not de-activate");
      }

      break;

    case move_2_wp1: //right-side(left-turn)

      if(move_state == 1){
        goal.target_pose.pose.position.x = wp6_pose[0];
              goal.target_pose.pose.position.y = wp6_pose[1];
        goal.target_pose.pose.orientation.z = wp6_ori[2];
              goal.target_pose.pose.orientation.w = wp6_ori[3];

        ROS_INFO("AGV move to the right-side(A-B)");
        // Add Dinh 200519
        obstacle_goal.pose = goal.target_pose.pose;
        goal_pub.publish(obstacle_goal);
        state_goal.data = 1;
        reach_pub.publish(state_goal);
      //	ROS_INFO("wp_move1() status = 1");

              ac.sendGoal(goal);

              ac.waitForResult();

              if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                      ROS_INFO("AGV has arrived to the right-side(A-B)");
                      //target_ = true;
          ABL_WP_ = true;
          point2_ = false;
        //	rth = 3;

        //	state_goal.data = 3;
        //	reach_pub.publish(state_goal);
        //	ROS_INFO("state_goal.data = 3");
              }
              else{
                      ROS_INFO("AGV failed for some reaseon");
              }
      }
      else{
        ROS_INFO("AGV is not de-activate");
      }

      break;

    default:
      break;
  }

}

void gil::move::wp_move2()//C-D Rack
{
//	ROS_INFO("wp_move2()");
	goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0)))
		ROS_INFO("Waiting for the move_base action server");

	switch(MOVE_WP2){

    case move_1_wp2://right-side(right-turn)

			if(move_state == 1){
				goal.target_pose.pose.position.x = wp3_pose[0];
        			goal.target_pose.pose.position.y = wp3_pose[1];
				goal.target_pose.pose.orientation.z = wp3_ori[2];
        			goal.target_pose.pose.orientation.w = wp3_ori[3];

				ROS_INFO("AGV move to the right-side(C-D)");
        // Add Dinh 200519
        obstacle_goal.pose = goal.target_pose.pose;
        goal_pub.publish(obstacle_goal);
				state_goal.data = 1;
				reach_pub.publish(state_goal);
			//	ROS_INFO("wp_move2() status = 1");

         			ac.sendGoal(goal);

        			ac.waitForResult();

        			if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                			ROS_INFO("AGV has arrived to the right-side(C-D)");
                		//	target_ = true;
                		//	CD_WP_ = true;
				//	wp_ = false;

				//	state_goal.data = 3;
				//	reach_pub.publish(state_goal);
				//	ROS_INFO("state_goal.data = 3");

					if(!home_flag){
						CD_WP_ = true;
						wp_ = false;
					//	rack_move = true;
					}
					else{
					//	rth = 2;
						RTH_ = rth_2;
						wp_ = false;
					}
        			}
        			else{
                			ROS_INFO("AGV failed for some reaseon");
        			}
			}
			else{
				ROS_INFO("AGV is not de-activate");
			}

			break;

    case move_2_wp2://left-side(left-turn)

			if(move_state == 1){
				goal.target_pose.pose.position.x = wp4_pose[0];
        			goal.target_pose.pose.position.y = wp4_pose[1];
				goal.target_pose.pose.orientation.z = wp4_ori[2];
        			goal.target_pose.pose.orientation.w = wp4_ori[3];
              // Add Dinh 200519
              obstacle_goal.pose = goal.target_pose.pose;
              goal_pub.publish(obstacle_goal);
				ROS_INFO("AGV move to the left-side(C-D)");

				state_goal.data = 1;
				reach_pub.publish(state_goal);
			//	ROS_INFO("wp_move2() status = 1");

         			ac.sendGoal(goal);

        			ac.waitForResult();

        			if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
                			ROS_INFO("AGV has arrived to the left-side(C-D)");
                		//	target_ = true;
                		//	CD_WP_ = true;
				//	wp_ = false;


				//	state_goal.data = 3;
				//	reach_pub.publish(state_goal);
				//	ROS_INFO("state_goal.data = 3");

					if(!home_flag){
						CD_WP_ = true;
						wp_ = false;
					//	rack_move = true;
					}
					else{
					//	rth = 2;
						RTH_ = rth_2;
						wp_ = false;
					}
        			}
        			else{
                			ROS_INFO("AGV failed for some reaseon");
        			}
			}
			else{
				ROS_INFO("AGV is not de-activate");
			}

			break;

		default:
			break;
	}
}

void gil::move::ABU_WP() //A-B Rack Moving
{
//	ROS_INFO("AB_WP");
	ROS_INFO("===== AGV is moving to AB_WP =====");
	ROS_INFO("target_pos_x : %.4f",target_pos_x);
	ROS_INFO("target_pos_y : %.4f",target_pos_y);
	ROS_INFO("target_ori_z : %.4f",target_ori_z);
	ROS_INFO("target_ori_w : %.4f",target_ori_w);
	ROS_INFO("==================================");

	if(move_state == 1){
		goal.target_pose.header.stamp = ros::Time::now();
 	 	goal.target_pose.header.frame_id = "map";

		MoveBaseClient ac("move_base", true);

		while(!ac.waitForServer(ros::Duration(5.0)))
			ROS_INFO("Waiting for the move_base action server");

		goal.target_pose.pose.position.x = target_pos_x;
		goal.target_pose.pose.position.y = wp1_pose[1];

		goal.target_pose.pose.orientation.z = target_ori_z;
        	goal.target_pose.pose.orientation.w = target_ori_w;

    // Add Dinh 200519
    obstacle_goal.pose = goal.target_pose.pose;
    goal_pub.publish(obstacle_goal);

		ROS_INFO("AGV move to the A-B storage rack");


		state_goal.data = 1;
		reach_pub.publish(state_goal);
	//	ROS_INFO("AP_WP() status = 1");

     		ac.sendGoal(goal);

		ac.waitForResult();

		if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
			ROS_INFO("AGV has arrived to the A-B storage rack");
			//target_ = false;
      ABU_WP_ = false;

		//	state_goal.data = 3;
		//	reach_pub.publish(state_goal);
		//	ROS_INFO("state_goal.data = 3");

			if(!home_flag){
				rack_move = true;
			}
			else{
				rth = 1;
			}
        	}
        	else{
        		ROS_INFO("AGV failed for some reaseon");
        	}
	}
	else{
		ROS_INFO("AGV is not de-activate");
	}

}

void gil::move::CD_WP() //C-D Rack Moving
{
//	ROS_INFO("CD_WP");
	ROS_INFO("===== AGV is moving to CD_WP =====");
	ROS_INFO("target_pos_x : %.4f",target_pos_x);
	ROS_INFO("target_pos_y : %.4f",target_pos_y);
	ROS_INFO("target_ori_z : %.4f",target_ori_z);
	ROS_INFO("target_ori_w : %.4f",target_ori_w);
	ROS_INFO("==================================");

	if(move_state == 1){
		goal.target_pose.header.stamp = ros::Time::now();
        	goal.target_pose.header.frame_id = "map";

		MoveBaseClient ac("move_base", true);

		while(!ac.waitForServer(ros::Duration(5.0)))
			ROS_INFO("Waiting for the move_base action server");

		goal.target_pose.pose.position.x = target_pos_x;
		goal.target_pose.pose.position.y = wp3_pose[1];

		goal.target_pose.pose.orientation.z = target_ori_z;
        	goal.target_pose.pose.orientation.w = target_ori_w;

		ROS_INFO("AGV move to the C-D storage rack");
    // Add Dinh 200519
    obstacle_goal.pose = goal.target_pose.pose;
    goal_pub.publish(obstacle_goal);

		state_goal.data = 1;
		reach_pub.publish(state_goal);
	//	ROS_INFO("CD_WP() status = 1");

     		ac.sendGoal(goal);

		ac.waitForResult();

		if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
			ROS_INFO("AGV has arrived to the C-D storage rack");

		//	state_goal.data = 3;
		//	reach_pub.publish(state_goal);
		//	ROS_INFO("state_goal.data = 3");

			if(!home_flag){
				rack_move = true;
				CD_WP_ = false;
			}
			else{
			//	rth = 1;
				RTH_ = rth_1;
				wp_ = wp_2;
			}

        	}
        	else{
        		ROS_INFO("AGV failed for some reaseon");
        	}
	}
	else{
		ROS_INFO("AGV is not de-activate");
	}
}

void gil::move::agv_run()
{
	ROS_INFO("===== AGV Running =====");
	ROS_INFO("target_pos_x : %.4f",target_pos_x);
	ROS_INFO("target_pos_y : %.4f",target_pos_y);
	ROS_INFO("target_pos_z : %.4f",target_pos_z);
	ROS_INFO("target_pos_w : %.4f",target_ori_w);
	ROS_INFO("=======================");

	if(move_state == 1){
		goal.target_pose.header.stamp = ros::Time::now();
		target_goal.header.frame_id = "map";
        	goal.target_pose.header.frame_id = "map";

		MoveBaseClient ac("move_base", true);

		while(!ac.waitForServer(ros::Duration(5.0)))
			ROS_INFO("Waiting for the move_base action server");

		goal.target_pose.pose.position.x = target_pos_x;

		if(target_pos_y >=10.0){
        		goal.target_pose.pose.position.y = init_pose_[1];
		}
		else if(target_pos_y<10.0){
			goal.target_pose.pose.position.y = wp3_pose[1];
		}

		goal.target_pose.pose.orientation.z = target_ori_z;
        	goal.target_pose.pose.orientation.w = target_ori_w;

		ROS_INFO("AGV move to the storage rack");

     		ac.sendGoal(goal);

		ac.waitForResult();

		if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
			ROS_INFO("AGV has arrived to the storage rack");
			target_ = false;
			rack_move = true;
        	}
        	else{
        		ROS_INFO("AGV failed for some reaseon");
        	}
	}
	else{
		ROS_INFO("AGV is not de-activate");
	}

}

void gil::move::home_reset()
{

//	ROS_INFO("home_reset()");

	switch(_move){
		case move_x_:
	//		ROS_INFO("move_x_");
			if(tf_fb_x<=(init_pose_[0]+0.1) && goal_Status.status==3){
				if(line_x >= -2.0 && line_x <= 2.0){
					vel_.linear.x = 0.0;
					ROS_INFO("AGV STOP: X");
					_move = move_y_;
				}
				else if(line_x > 0.0 && line_x <= 60.0){
					vel_.linear.x = -QR_VEL;
					ROS_INFO("vel_x: -");
				}
				else if(line_x >= -60.0 && line_x <= 0.0){
					vel_.linear.x = QR_VEL;
					ROS_INFO("vel_x: +");
				}

				vel_pub.publish(vel_);
			}
			break;

		case move_y_:
	//		ROS_INFO("move_y_");
			if(tf_fb_x<=(init_pose_[0]+0.1) && goal_Status.status==3){
				if(line_y >= -2.0 && line_y <= 2.0){
					vel_.linear.y = 0.0;
					h_reset = false;
					ROS_INFO("AGV STOP: Y");
				}
				else if(line_y > 0.0 && line_y <= 60.0){
					vel_.linear.y = -QR_VEL;
					ROS_INFO("vel_y: -");
				}
				else if(line_y >= -60.0 && line_y <= 0.0){
					vel_.linear.y = QR_VEL;
					ROS_INFO("vel_y: +");
				}

				vel_pub.publish(vel_);
			}

			break;

		default:

			break;
	}
}

void gil::move::precision_move()
{
//	ROS_INFO("precision_move");
	ROS_INFO("AGV-precision closely approach to the storage Rack");

	if(move_state == 1){
		goal.target_pose.header.stamp = ros::Time::now();
		target_goal.header.frame_id = "map";
        	goal.target_pose.header.frame_id = "map";

		MoveBaseClient ac("move_base", true);

		while(!ac.waitForServer(ros::Duration(5.0)))
			ROS_INFO("Waiting for the move_base action server");

		goal.target_pose.pose.position.x = target_pos_x;
        	goal.target_pose.pose.position.y = target_pos_y;
		goal.target_pose.pose.orientation.z = target_ori_z;
        	goal.target_pose.pose.orientation.w = target_ori_w;
          // Add Dinh 200519
          obstacle_goal.pose = goal.target_pose.pose;
          goal_pub.publish(obstacle_goal);
		ROS_INFO("AGV-precision is control to the storage rack");

		state_goal.data = 1;
		reach_pub.publish(state_goal);
	//	ROS_INFO("precision_move() status = 1");

     		ac.sendGoal(goal);

		ac.waitForResult();

		if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
			ROS_INFO("AGV has arrived to the storage rack(QR marker)");
	//		target_ = false;
			rack_move = false;
	//		h_reset = true;
			qr_reset = true;
			_move = move_x_;
	//		rth = 1;

		//	state_goal.data = 3;
		//	reach_pub.publish(state_goal);
		//	ROS_INFO("state_goal.data = 3");
        	}
        	else{
        		ROS_INFO("AGV failed for some reaseon");
        	}
	}
	else{
		ROS_INFO("AGV is not de-activate");
	}

}

void gil::move::qr_move()
{
//	ROS_INFO("qr_move()");

	switch(_move){
		case move_x_: //x direction moving

			if(tf_fb_x<=(target_pos_x+0.1) && goal_Status.status==3){
				if(line_x >= -2.0 && line_x <= 2.0){
					vel_.linear.x = 0.0;
					ROS_INFO("AGV STOP: X");
					_move = move_y_;
					qr_count++;
					ROS_INFO("qr_count : %d",qr_count);
				}
				else if(line_x > 2.0 && line_x <= 60.0){
					vel_.linear.x = -QR_VEL;
					ROS_INFO("vel_x: -");
				}
				else if(line_x >= -60.0 && line_x < -2.0){
					vel_.linear.x = QR_VEL;
					ROS_INFO("vel_x: +");
				}

				vel_pub.publish(vel_);
			}
			break;

		case move_y_: //y direction moving

			if(tf_fb_x<=(target_pos_x+0.1) && goal_Status.status==3){
				if(line_y >= -2.0 && line_y <= 2.0){
				//	vel_.linear.y = 0.0;
				//	qr_reset = false;
				//	rth = 0;
				//	ROS_INFO("AGV STOP: Y");
				//	_move = move_x_;
					qr_count++;
					ROS_INFO("qr_count : %d",qr_count);
				//	if(qr_count>=5){
					if(qr_count>=QR_NUM){
						vel_.linear.y = 0.0;
						qr_reset = false;
						qr_count = 0;
						rth = 0;
					//	_move = move_theta_;
						ROS_INFO("AGV STOP: Y");
					//	qr_reset = false;
						ROS_INFO("qr_reset = false");

						state_goal.data = 3;
						reach_pub.publish(state_goal);
						ROS_INFO("qr_move() status = 3");
					}
					else{
						_move = move_x_;
					}
				}
				else if(line_y > 2.0 && line_y <= 60.0){
					vel_.linear.y = -QR_VEL;
					ROS_INFO("vel_y: -");
				}
				else if(line_y >= -60.0 && line_y < -2.0){
					vel_.linear.y = QR_VEL;
					ROS_INFO("vel_y: +");
				}

				vel_pub.publish(vel_);
			}

			break;

		case move_theta_:
			if(line_theta>=179 && line_theta<= 181){
				//	qr_reset = false;
			}
			else if(line_theta<179){

			}
			else if(line_theta>181){

			}

			break;

		default:

			break;
	}


}

void gil::move::cart_move()
{
//	ROS_INFO("cart_move()");

	ROS_INFO("===== AGV is moving to Cart-Point =====");
	ROS_INFO("target_pos_x : %.4f",target_pos_x);
	ROS_INFO("target_pos_y : %.4f",target_pos_y);
	ROS_INFO("target_ori_z : %.4f",target_ori_z);
	ROS_INFO("target_ori_w : %.4f",target_ori_w);
	ROS_INFO("=======================================");

/*
	target_goal.header.stamp = ros::Time::now();
	target_goal.header.frame_id = "map";

	target_goal.pose.position.x = target_pos_x;
	target_goal.pose.position.y = target_pos_y;
	target_goal.pose.position.z = target_pos_z;

	target_goal.pose.orientation.z = target_ori_z;
	target_goal.pose.orientation.w = target_ori_w;

	goal_pub.publish(target_goal);
*/

	goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0)))
		ROS_INFO("Waiting for the move_base action server");

	goal.target_pose.pose.position.x = target_pos_x;
	goal.target_pose.pose.position.y = target_pos_y;

	goal.target_pose.pose.orientation.z = target_ori_z;
        goal.target_pose.pose.orientation.w = target_ori_w;

  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

	ROS_INFO("AGV move to the cart-position");

	state_goal.data = 1;
	reach_pub.publish(state_goal);
//	ROS_INFO("cart_move() status = 1");

     	ac.sendGoal(goal);

	ac.waitForResult();

	if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
		ROS_INFO("AGV has arrived to the cart-position");

		state_goal.data = 3;
		reach_pub.publish(state_goal);
//		ROS_INFO("cart_move() status = 3");

/*
		if(!home_flag){
			rack_move = true;
			CD_WP_ = false;
		}
		else{
		//	rth = 1;
			RTH_ = rth_1;
			wp_ = wp_2;
		}
*/
        }
        else{
        	ROS_INFO("AGV failed for some reaseon");
        }

}

void gil::move::tf_agv_position()
{

//	geometry_msgs::PointStamped pose_msgs;
//	tf2_ros::Buffer tfBuffer;

}


