// AGV 2 movement
// 4-arm 1-2: Home position -> wp1-2 (rotate +-90)  -> wp3-4 -> arm_move -> QR move
// charge pos: Home position  -> wp1 -> charge_pos


#include <ros/ros.h>
#include "ros/time.h"
#include <vector>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose.h>
#include <actionlib_msgs/GoalStatus.h>
#include <actionlib_msgs/GoalStatusArray.h>
#include <actionlib_msgs/GoalID.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_broadcaster.h>
#include <if_node_msgs/cmdMove.h>
#include <if_node_msgs/waypoint.h>
#include <pgv/vision_msg.h>
#include <obstacle_detector/obstacles_detect.h>
#include <nav_msgs/Odometry.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/LinearMath/Transform.h>
#include <geometry_msgs/PointStamped.h>
#include <move_base_msgs/MoveBaseActionFeedback.h>
//#include <nav_msgs/Feedback.h>

#include "move_control/teleop.h"
#include "md2k_can_driver_msgs/motor_status.h"


#define QR_VEL 0.019
// Add Dinh 200527
#define QR_AVEL 0.009
#define CART_VEL 0.016
#define QR_NUM 5

using namespace std;

namespace agv_wp {


typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
   tf2_ros::Buffer tfBuffer;

  enum goal_reach_status{
    status_0_=0,status_1_,status_2_,status_3_,status_4_,status_5_,status_6_,status_7_,status_8_
  };

  enum QR_Move{
    move_x_, move_y_, move_theta_
  };
  // Add Dinh 200601
  enum start_pos{
    start_null = 0,from_home, from_arm12, from_arm34, from_arm5, from_arm6, from_arm7, from_charger, from_A, from_B, from_C, from_D, from_home1, from_cart
  };

  enum stop_pos{
    stop_null = 0,to_home, to_arm12, to_arm34, to_arm5, to_arm6, to_arm7, to_charger, to_A, to_B, to_C, to_D, to_home1, to_cart
  };

  // Add Dinh 200602
  enum Move_Point{
    no_point_=0, home_, home1_, cart_, arm12_, charger_, charger1_, charger2_, arm34_, arm5_, arm6_, arm7_, A_, B_, C_, D_, wp1_, wp2_, wp3_, wp4_, wp5_, wp51_, wp6_, wp7_, wp8_, wp9_, wp10_,  wp11_, wp12_, wp13_, wp14_, wp15_, wp16_, rot1_, rot2_, rot3_, rot4_, rot5_, rot6_, rack_, h_reset_, qr_reset_
  };

  enum MODE_AGV{
      mode_stop_=0, mode_start_=1, mode_obs_=2
  };

  typedef struct status{
    bool pending;
    bool active;
    bool complete;

  } status;

    QR_Move _move;
    start_pos start_;
    stop_pos stop_;
    Move_Point move_point_;
    MODE_AGV mode_agv_;



  class NavigationManagement{
  public:
    NavigationManagement();
    ~NavigationManagement();
    void init();
    void spin();
    void update();
    void param_set();

  private:

    void charge_move();
    void charge1_move();
    void charge2_move();
    void init_pose();
    void home_reset();
    void rack_move();
    void qr_move();

    // Add Dinh 200601

    void RTH();
    void wp1_move();
    void wp2_move();
    void wp3_move();
    void wp4_move();
    void wp5_move();
    void wp51_move();
    void wp6_move();
    void wp7_move();
    void wp8_move();
    void wp9_move();
    void wp10_move();

    void wp11_move();
    void wp12_move();
    void wp13_move();
    void wp14_move();

    // Add Dinh 200615
    void wp15_move();
    void wp16_move();
    void cart_move();
    void home1_move();

    void rot1_move();
    void rot2_move();
    void rot3_move();
    void rot4_move();
    void rot5_move();

    void arm12_move();
    void arm34_move();
    void arm5_move();
    void arm6_move();
    void arm7_move();

    void A_move();
    void C_move();
    void B_move();
    void D_move();

    bool cancelAllGoals(double timeout);
    void stop_agv();

    void home1_cb(const std_msgs::UInt8::ConstPtr& home_sig);


    // Add Dinh 200528
    MoveBaseClient  *AGV_move_base;

    // Add Dinh 200601

    bool tag_, control_;
    bool obstacle_;
    bool home_flag;

    int rate;
    int move_state; //move : 1, stop : 0
    int init_flag;

    int tag_id, line_, line_id, control_id, fault, warning, command;

    int qr_count;
    float target_pos_x, target_pos_y, target_ori_z, target_ori_w;

    // Add Dinh 200528
    float current_x, current_y, current_ori_z, current_ori_w;

    float line_x, line_y, line_theta;
    float tf_fb_x, tf_fb_y, tf_fb_ori_z, tf_fb_ori_w;

    // Add Dinh 200601
    float cur_x, cur_y, cur_oz, cur_ow;

    // Add Dinh 200608
    bool manual_mode;

    int8_t count;

    std_msgs::UInt8 state_goal;
    geometry_msgs::Twist vel_;
    geometry_msgs::PoseStamped target_goal;
    geometry_msgs::PoseStamped obstacle_goal;
    actionlib_msgs::GoalStatus goal_Status;
    move_base_msgs::MoveBaseGoal goal;
    geometry_msgs::PoseStamped pose_msgs;


    ros::NodeHandle nh;


    vector<float> init_pose_, init_ori;
    vector<float> charge_pose, charge_ori;

    // Modify Dinh 200603
    vector<float> wp1_pose, wp2_pose, wp3_pose, wp4_pose, wp5_pose, wp51_pose, wp6_pose, wp7_pose, wp8_pose, wp9_pose, wp10_pose, wp11_pose, wp12_pose, wp13_pose, wp14_pose, wp15_pose, wp16_pose, home1_pose, cart_pose;
    vector<float> wp1_ori, wp2_ori, wp3_ori, wp4_ori, wp5_ori, wp51_ori, wp6_ori, wp7_ori, wp8_ori, wp9_ori, wp10_ori, wp11_ori, wp12_ori, wp13_ori, wp14_ori, wp15_ori, wp16_ori, home1_ori, cart_ori;


    ros::Publisher vel_pub, goal_pub, reach_pub, init_pub, cancel_pub, tf_base_link_pub, qr_ang_pub, waypoint_pub; // Add Dinh 200602
    ros::Subscriber reach_sub, goal_sub, init_sub, home1_sub, pgv_sub, move_base_feedback_sub, obstacle_sub, joy_sub, motorFR_status_sub, motorFL_status_sub, motorRR_status_sub, motorRL_status_sub;

    void goal_reach_cb(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach);
    void goal_target_cb(const if_node_msgs::cmdMove& goal);
    void init_cb(const std_msgs::UInt8::ConstPtr& init_);
    void pgv_cb(const pgv::vision_msg& pgv_msg);
    void tf_feedback_cb(const move_base_msgs::MoveBaseActionFeedback& feedback_);
    void obstacle_cb(const obstacle_detector::obstacles_detect& obstacle_msg);
    void wp_pub();

    // Add Dinh 200608
    void joystickControlCallback(const move_control::teleop::ConstPtr& motorCmd_);

    void FR_status_cb(const md2k_can_driver_msgs::motor_status::ConstPtr& msg);
    void FL_status_cb(const md2k_can_driver_msgs::motor_status::ConstPtr& msg);
    void RR_status_cb(const md2k_can_driver_msgs::motor_status::ConstPtr& msg);
    void RL_status_cb(const md2k_can_driver_msgs::motor_status::ConstPtr& msg);

  };

NavigationManagement::NavigationManagement()
{

  init();
  param_set();
  AGV_move_base =  new MoveBaseClient("move_base",true);

  vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1, true);
//	goal_pub = nh.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1, true);
  goal_pub = nh.advertise<geometry_msgs::PoseStamped>("/current_goal", 1, true);
  reach_pub = nh.advertise<std_msgs::UInt8>("/IFNode/goal_reach", 1, true);
  init_pub = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>("/initialpose", 1, true);
  cancel_pub = nh.advertise<actionlib_msgs::GoalID>("/move_base/cancel", 1, true);
  tf_base_link_pub = nh.advertise<geometry_msgs::PoseStamped>("/agv_pos", 1, true);
  // Add DInh 200602
  waypoint_pub = nh.advertise<if_node_msgs::waypoint>("/agv_wp", 1, true);

  reach_sub = nh.subscribe("/move_base/status", 10, &NavigationManagement::goal_reach_cb, this);
//	goal_sub = nh.subscribe("/goal_target", 10, &NavigationManagement::goal_target_cb, this);
  goal_sub = nh.subscribe("/MOVE/CMD", 10, &NavigationManagement::goal_target_cb, this);
  init_sub = nh.subscribe("/init_pose", 1, &NavigationManagement::init_cb, this);
  pgv_sub = nh.subscribe("/pgv_data", 10, &NavigationManagement::pgv_cb, this);
  move_base_feedback_sub = nh.subscribe("/move_base/feedback", 10, &NavigationManagement::tf_feedback_cb ,this);
  obstacle_sub = nh.subscribe("/obstacle_info", 1, &NavigationManagement::obstacle_cb, this);

  // Add Dinh 200615
  home1_sub = nh.subscribe("/home_pose", 1, &NavigationManagement::home1_cb, this);

  // Add 200727
  motorFR_status_sub = nh.subscribe("/motor_front_right/motor_status", 1, &NavigationManagement::FR_status_cb, this);
  motorFL_status_sub = nh.subscribe("/motor_front_left/motor_status", 1, &NavigationManagement::FL_status_cb, this);
  motorRR_status_sub = nh.subscribe("/motor_rear_right/motor_status", 1, &NavigationManagement::RR_status_cb, this);
  motorRL_status_sub = nh.subscribe("/motor_rear_right/motor_status", 1, &NavigationManagement::RL_status_cb, this);


  // Add Dinh 200608
    joy_sub = nh.subscribe("cmd_vel_joy", 1, &NavigationManagement::joystickControlCallback, this);

}


NavigationManagement::~NavigationManagement()
{
}

void NavigationManagement::init()
{
//	ROS_INFO("variables initial");

  rate = 100; //10; //100
  target_pos_x = 0.0;
  target_pos_y = 0.0;

  target_ori_z = 0.0;
  target_ori_w = 0.99;

  // Add Dinh 200528
  current_x = 0;
  current_y = 0;
  current_ori_w = 0;
  current_ori_z = 0;

  // Add Dinh 200601
  cur_x = 0;
  cur_y = 0;
  cur_ow = 0;
  cur_oz = 0;

  start_ = start_null;
  stop_ = stop_null;
  move_point_ = no_point_;


  obstacle_ = false;
  // Add Dinh 200520
  init_flag = 0;
  qr_count = 0;

  manual_mode = false;

  count = 0;
  mode_agv_ = mode_stop_;
}

bool NavigationManagement::cancelAllGoals(double timeout)
{
  actionlib::SimpleClientGoalState goal_state = AGV_move_base->getState();
  if ((goal_state != actionlib::SimpleClientGoalState::ACTIVE) &&
      (goal_state != actionlib::SimpleClientGoalState::PENDING) &&
      (goal_state != actionlib::SimpleClientGoalState::RECALLED) &&
      (goal_state != actionlib::SimpleClientGoalState::PREEMPTED))
  {
    // We cannot cancel a REJECTED, ABORTED, SUCCEEDED or LOST goal
    ROS_WARN("Cannot cancel move base goal, as it has %s state!", goal_state.toString().c_str());
    return true;
  }

  ROS_INFO("Canceling move base goal with %s state...", goal_state.toString().c_str());
  AGV_move_base->cancelAllGoals();
  if (AGV_move_base->waitForResult(ros::Duration(timeout)) == false)
  {
    ROS_WARN("Cancel move base goal didn't finish after %.2f seconds: %s",
             timeout, goal_state.toString().c_str());
    return false;
  }

  ROS_INFO("Cancel move base goal succeed. New state is %s", goal_state.toString().c_str());
  return true;
}
void NavigationManagement::wp_pub()
{
  if_node_msgs::waypoint wp_desc;
  switch(start_)
  {
    case from_home: wp_desc.start_point = "home"; break;
  case from_arm12: wp_desc.start_point = "arm12"; break;
  case from_arm34: wp_desc.start_point = "arm34"; break;
    case from_arm5: wp_desc.start_point = "arm5"; break;
    case from_arm6: wp_desc.start_point = "arm6"; break;
    case from_arm7: wp_desc.start_point = "arm7"; break;
  case from_charger: wp_desc.start_point = "charger"; break;
  case from_A: wp_desc.start_point = "A"; break;
  case from_B: wp_desc.start_point = "B"; break;
  case from_C: wp_desc.start_point = "C"; break;
  case from_D: wp_desc.start_point = "D"; break;
  case from_home1:  wp_desc.start_point = "home1"; break;
  case from_cart:  wp_desc.start_point = "cart"; break;
  case start_null: wp_desc.start_point = "no_point"; break;
  default: break;
  }
  switch(stop_)
  {
  case to_home: wp_desc.stop_point = "home"; break;
  case to_arm12: wp_desc.stop_point = "arm12"; break;
  case to_arm34: wp_desc.stop_point = "arm34"; break;
  case to_arm5: wp_desc.stop_point = "arm5"; break;
  case to_arm6: wp_desc.stop_point = "arm6"; break;
  case to_arm7: wp_desc.stop_point = "arm7"; break;
  case to_charger: wp_desc.stop_point = "charger"; break;
  case to_A:  wp_desc.stop_point = "A"; break;
  case to_B:  wp_desc.stop_point = "B"; break;
  case to_C:  wp_desc.stop_point = "C"; break;
  case to_D:  wp_desc.stop_point = "D"; break;
  case to_home1: wp_desc.stop_point = "home1"; break;
  case to_cart:  wp_desc.stop_point = "cart"; break;
  case stop_null: wp_desc.stop_point = "no_point"; break;
  default: break;
  }
  waypoint_pub.publish(wp_desc);
}

void NavigationManagement::FR_status_cb(const md2k_can_driver_msgs::motor_status::ConstPtr& msg)
{
    uint8_t m_1 = msg->front_left_bit_Alarm;
    uint8_t m_2 = msg->front_right_bit_Alarm;
    uint8_t m_3 = msg->rear_left_bit_Alarm;
    uint8_t m_4 = msg->rear_right_bit_Alarm;
    if((m_1 || m_2 || m_3 || m_4) !=0)
    {
        mode_agv_ = mode_stop_;
    }
    else
    {
        mode_agv_ = mode_start_;
    }
}

void NavigationManagement::RR_status_cb(const md2k_can_driver_msgs::motor_status::ConstPtr& msg)
{
    uint8_t m_1 = msg->front_left_bit_Alarm;
    uint8_t m_2 = msg->front_right_bit_Alarm;
    uint8_t m_3 = msg->rear_left_bit_Alarm;
    uint8_t m_4 = msg->rear_right_bit_Alarm;
    if((m_1 || m_2 || m_3 || m_4) !=0)
    {
        mode_agv_ = mode_stop_;
    }
    else
    {
        mode_agv_ = mode_start_;
    }
}

void NavigationManagement::FL_status_cb(const md2k_can_driver_msgs::motor_status::ConstPtr& msg)
{
    uint8_t m_1 = msg->front_left_bit_Alarm;
    uint8_t m_2 = msg->front_right_bit_Alarm;
    uint8_t m_3 = msg->rear_left_bit_Alarm;
    uint8_t m_4 = msg->rear_right_bit_Alarm;
    if((m_1 || m_2 || m_3 || m_4) !=0)
    {
        mode_agv_ = mode_stop_;
    }
    else
    {
        mode_agv_ = mode_start_;
    }
}

void NavigationManagement::RL_status_cb(const md2k_can_driver_msgs::motor_status::ConstPtr& msg)
{
    uint8_t m_1 = msg->front_left_bit_Alarm;
    uint8_t m_2 = msg->front_right_bit_Alarm;
    uint8_t m_3 = msg->rear_left_bit_Alarm;
    uint8_t m_4 = msg->rear_right_bit_Alarm;
    if((m_1 || m_2 || m_3 || m_4) !=0)
    {
        mode_agv_ = mode_stop_;
    }
    else
    {
        mode_agv_ = mode_start_;
    }
}

void NavigationManagement::update()
{
  // Add Dinh 200601

  geometry_msgs::TransformStamped transformStamped;
  try
  {
    transformStamped = tfBuffer.lookupTransform("map", "base_link", ros::Time(0));

    current_x =  transformStamped.transform.translation.x;
    current_y =   transformStamped.transform.translation.y ;
    current_ori_w =   transformStamped.transform.rotation.w;
    current_ori_z =   transformStamped.transform.rotation.z;
  }
  catch(tf2::TransformException &ex)
  {
    ROS_WARN("%s", ex.what());
    ros::Duration(0.3).sleep();
//		continue;
  }

  pose_msgs.pose.position.x = current_x;
  pose_msgs.pose.position.y = current_y;
  pose_msgs.pose.orientation.z = current_ori_z;
  pose_msgs.pose.orientation.w = current_ori_w;
  tf_base_link_pub.publish(pose_msgs);

  if(init_flag)
  { //AGV init pose
    init_pose();
    init_flag = 0;
  }

  if(manual_mode)
  {
    cancelAllGoals(0.01);
    move_point_ = no_point_;
    mode_agv_ = mode_stop_;
    count = 0;
  }
  else
  {
      switch(mode_agv_)
      {
          case mode_obs_:
          {
               cancelAllGoals(0.1);
               count = 0;
          }
          break;
          case mode_start_:
          {
              if((start_ != start_null) && (stop_ != stop_null) &&(move_point_ !=no_point_))
              {
                switch(move_point_)
                {
                  case home_:
                    RTH();
                    break;
                case charger_:
                  charge_move();
                  break;
                case charger1_:
                  charge1_move();
                  break;
                case charger2_:
                  charge2_move();
                  break;
                case home1_:
                  home1_move();
                  break;
                case cart_:
                  cart_move();
                  break;
                case wp1_:
                  wp1_move();
                  break;
                case wp2_:
                  wp2_move();
                  break;
                case wp3_:
                  wp3_move();
                  break;
                case wp4_:
                  wp4_move();
                  break;
                case wp5_:
                  wp5_move();
                  break;
                case wp51_:
                  wp51_move();
                  break;
                case wp6_:
                  wp6_move();
                  break;
                case wp7_:
                  wp7_move();
                  break;
                case wp8_:
                  wp8_move();
                  break;
                case wp9_:
                  wp9_move();
                  break;
                case wp10_:
                  wp10_move();
                  break;
                case wp11_:
                  wp11_move();
                  break;
                case wp12_:
                  wp12_move();
                  break;
                case wp13_:
                  wp13_move();
                  break;
                case wp14_:
                  wp14_move();
                  break;
                case wp15_:
                  wp15_move();
                  break;
                case wp16_:
                  wp16_move();
                  break;
                case rot1_:
                  rot1_move();
                  break;
                case rot2_:
                  rot2_move();
                  break;
                case rot3_:
                  rot3_move();
                  break;
                case rot4_:
                  rot4_move();
                  break;
                case rot5_:
                  rot5_move();
                  break;
                case arm12_:
                  arm12_move();
                  break;
                case arm34_:
                  arm34_move();
                  break;
                case arm5_:
                  arm5_move();
                  break;
                case arm6_:
                  arm6_move();
                  break;
                case arm7_:
                  arm7_move();
                  break;
                case A_:
                  A_move();
                  break;
                case B_:
                  B_move();
                  break;
                case C_:
                  C_move();
                  break;
                case D_:
                  D_move();
                  break;
                case h_reset_:
                  home_reset();
                  break;
                case qr_reset_:
                  qr_move();
                  break;
                case rack_:
                  rack_move();
                  break;
                case no_point_:
                  stop_agv();
                  break;
                default:
                  break;
                }
              }
              else
              {
                  move_point_ = no_point_;
                  start_ = start_null;
                  stop_ = stop_null;
              }
          }
          break;
          case mode_stop_:
          {
              cancelAllGoals(0.1);
              move_point_ = no_point_;
              start_ = start_null;
              stop_ = stop_null;
              count = 0;
          }
              break;
          default:
              break;
      }
    }
  }


void NavigationManagement::spin()
{
  ros::Rate loop_rate(rate);
  tf2_ros::TransformListener tfListener(tfBuffer);

  while(ros::ok())
  {
    update();
    ros::spinOnce();
    loop_rate.sleep();
  }
}


void NavigationManagement::goal_target_cb(const if_node_msgs::cmdMove& goal)
{
  ROS_INFO("AGV received the coordinate data from ACS");

  target_pos_x = goal.pose.position.x;
  target_pos_y = goal.pose.position.y;

  target_ori_z = goal.pose.orientation.z;
  target_ori_w = goal.pose.orientation.w;

  start_ = start_null;
  stop_ = stop_null;
  move_state = goal.move;

  cancelAllGoals(0.01);
  if((fabs(current_x - charge_pose[0]) < 0.2) && (fabs(current_y - charge_pose[1]) < 0.2) && (fabs(current_ori_w - charge_ori[3]) < 0.2))
  {
    start_ = from_charger;
  }
  else if((fabs(current_x - cart_pose[0]) < 0.3) && (fabs(current_y - cart_pose[1]) < 0.3) && (fabs(current_ori_w - cart_ori[3]) < 0.3))
  {
    start_ = from_cart;
  }
  else if ((current_y >= home1_pose[1] - 6) && (current_x<= home1_pose[0]+0.4) && (current_x>=home1_pose[0]-0.4))
  {
    // from home1 area
    start_ = from_home1;
  }
  else if((current_y >= init_pose_[1] - 5) && (current_x>= wp3_pose[0]+7))
    // from arm 12
  {
    start_ = from_arm12;
  }
  else if ((current_y >= init_pose_[1] - 5) && (current_x< wp3_pose[0]+7) && (current_x>init_pose_[0]-2))
  {
    // from home area
    start_ = from_home;
  }
  else if((current_y < init_pose_[1] - 5) && (current_y >= wp4_pose[1]-1) && (current_x< wp3_pose[0]+7) && (current_x>init_pose_[0]-2))
  {
    // from charger area
    start_ = from_charger;
  }
  else if((current_y<wp4_pose[1]-1) && (current_y>=(wp5_pose[1]+3)) && (current_x>(wp5_pose[0]-1)) && (current_x<(wp5_pose[0]+10)))
    // from arm 34
  {
    start_ = from_arm34;
  }
  else if(current_y < (wp5_pose[1]+3) && (current_x>(wp5_pose[0]-1)) && (current_x<(wp9_pose[0]-5)))
  {
    // from arm5
    start_ = from_arm5;
  }

else if((current_x>=(wp9_pose[0]-5)) && (current_x<(wp11_pose[0])))
{
  // from arm6
  start_ = from_arm6;
}
else if(current_x>=(wp11_pose[0]))
{
  // from arm7
  start_ = from_arm7;
}
  // Add Dinh 200615
  else if((current_y > -1) && current_y < 6 && (current_x<(wp5_pose[0]-1)) && (current_x> home1_pose[0]+0.4))
  {
    // from C
    start_ = from_C;
  }
  else if((current_y <= -1) && (current_x<(wp5_pose[0]-1)) && (current_x> home1_pose[0]+0.4))
  {
    // from D
    start_ = from_D;
  }
  else if((current_y >8) && (current_y <12.2) && (current_x< (init_pose_[0]-2)) && (current_x> home1_pose[0]+0.4))
  {
    // from B
    start_ = from_B;
  }
  else if((current_y >=12.2) && (current_x< (init_pose_[0]-2)) && (current_x> home1_pose[0]+0.4))
  {
    // from A
    start_ = from_A;
  }
  else
  {
    // from undefined
    start_ = start_null;
    move_point_ = no_point_;
  }

  // Add Dinh 200601
  if((fabs(target_pos_x - init_pose_[0]) < 0.2) && (fabs(target_pos_y - init_pose_[1]) < 0.2) && (fabs(target_ori_w - init_ori[3]) < 0.4))
    // to home
  {

    stop_ = to_home;
    switch (start_)
    {
    case from_charger: move_point_ = charger2_; break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm12_;
    }
      break;
    case from_home: move_point_ = home_; break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm34_;
    }
      break;
    case from_arm5:
    {
      if(current_x> wp5_pose[0]+5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_ow = current_ori_w;
        cur_oz = current_ori_z;
        move_point_ = arm5_;
      }
      else move_point_= wp5_;
    }
      break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm6_;
    }
      break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm7_;
    }
      break;
      // Add Dinh 200615
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = A_;
    }
      break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = B_;
    }
      break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = C_;
    }
      break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = D_;
    }
      break;
    case from_cart: {move_point_ = home1_;} break;
    case from_home1:{ move_point_ = wp15_;} break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }

  }
  else if((fabs(target_pos_x - charge_pose[0]) < 0.2) && (fabs(target_pos_y - charge_pose[1]) < 0.2) && (fabs(target_ori_w - charge_ori[3]) < 0.2))
    // to charger
  {
    stop_ = to_charger;
    switch (start_)
    {
    case from_charger: move_point_ = charger_; break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm12_;
    }
      break;
    case from_home: move_point_ = wp1_; break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm34_;
    }
      break;
    case from_arm5:
    {
      if(current_x> wp5_pose[0]+5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_ow = current_ori_w;
        cur_oz = current_ori_z;
        move_point_ = arm5_;
      }
      else move_point_= wp5_;
    }
      break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm6_;
    }
      break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm7_;
    }
      break;
      // Add Dinh 200615
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = A_;
    }
      break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = B_;
    }
      break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = C_;
    }
      break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = D_;
    }
      break;
    case from_cart: {move_point_ = home1_;} break;
    case from_home1:{ move_point_ = wp15_;} break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }


  }
  else if((target_pos_x > wp3_pose[0]+7) && (target_pos_y > init_pose_[1]-5))
    // to arm 12
  {
    stop_ = to_arm12;
    switch (start_)
    {
    case from_charger: move_point_  = home_;; break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm12_;
    }
      break;
    case from_home:
    {
      if(target_pos_y>init_pose_[1]) move_point_ = wp1_;
      else move_point_ = wp2_;
    }
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm34_;
    }
      break;
    case from_arm5:
    {
      if(current_x> wp5_pose[0]+5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_ow = current_ori_w;
        cur_oz = current_ori_z;
        move_point_ = arm5_;
      }
      else move_point_= wp5_;
    }
      break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm6_;
    }
      break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm7_;
    }
      break;
      // Add Dinh 200615
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = A_;
    }
      break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = B_;
    }
      break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = C_;
    }
      break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = D_;
    }
      break;
    case from_cart: {move_point_ = home1_;} break;
    case from_home1:{ move_point_ = wp15_;} break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }
  }
  else if((target_pos_y <= wp4_pose[1]) && (target_pos_y > (wp5_pose[1]+3)) && (target_pos_x>(wp5_pose[0]-1)) && (target_pos_x<(wp5_pose[0]+8)))
    // to arm 34
  {
    stop_ = to_arm34;
    switch (start_)
    {
    case from_charger: move_point_  = rot2_; break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm12_;
    }
      break;
    case from_home: move_point_ = wp3_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm34_;
    }
      break;
    case from_arm5:
    {
      if(current_x> wp5_pose[0]+6)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_ow = current_ori_w;
        cur_oz = current_ori_z;
        move_point_ = arm5_;
      }
      else move_point_= wp5_;
    }
      break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm6_;
    }
      break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm7_;
    }
      break;
      // Add Dinh 200615
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = A_;
    }
      break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = B_;
    }
      break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = C_;
    }
      break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = D_;
    }
      break;
    case from_cart: {move_point_ = home1_;} break;
    case from_home1:{ move_point_ = wp15_;} break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }
  }

  else if (target_pos_x>(wp5_pose[0]+8))
    // to arm 5 , 6, 7
  {

    if(target_pos_x < wp9_pose[0]-5) stop_ = to_arm5;
    else if(target_pos_x < wp11_pose[0]) stop_ = to_arm6;
    else  stop_ = to_arm7;

    switch (start_)
    {
    case from_charger: move_point_  = rot2_; break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm12_;
    }
      break;
    case from_home: move_point_ = wp3_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm34_;
    }
      break;
    case from_arm5:
    {
      if(current_x> wp5_pose[0]+5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_ow = current_ori_w;
        cur_oz = current_ori_z;
        move_point_ = arm5_;
      }
      else move_point_= wp5_;
    }
      break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm6_;
    }
      break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm7_;
    }
      break;
      // Add Dinh 200615
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = A_;
    }
      break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = B_;
    }
      break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = C_;
    }
      break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = D_;
    }
      break;
    case from_cart: {move_point_ = home1_;} break;
    case from_home1:{ move_point_ = wp16_;} break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }

  }

  else if((target_pos_y >8) && (target_pos_y <=12.2) && (target_pos_x <= init_pose_[0]-2) && (target_pos_x> home1_pose[0]+0.8))
    // to B rack
  {
    stop_ = to_B;

    switch (start_)
    {
    case from_charger: move_point_ = wp6_; break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm12_;
    }
      break;
    case from_home: move_point_ = wp6_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm34_;
    }
      break;
    case from_arm5:
    {
      if(current_x> wp5_pose[0]+5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_ow = current_ori_w;
        cur_oz = current_ori_z;
        move_point_ = arm5_;
      }
      else move_point_= wp5_;
    }
      break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm6_;
    }
      break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm7_;
    }
      break;
      // Add Dinh 200615
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = A_;
    }
      break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = B_;
    }
      break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = C_;
    }
      break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = D_;
    }
      break;
    case from_cart: {move_point_ = home1_;} break;
    case from_home1:{ move_point_ = wp15_;} break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }

  }

  else if((target_pos_y >12.2) && (target_pos_x <= init_pose_[0]-2) && (target_pos_x> home1_pose[0]+0.8))
    // to A rack
  {
    stop_ = to_A;

    switch (start_)
    {
    case from_charger: move_point_ = wp6_; break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm12_;
    }
      break;
    case from_home: move_point_ = wp6_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm34_;
    }
      break;
    case from_arm5:
    {
      if(current_x> wp5_pose[0]+5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_ow = current_ori_w;
        cur_oz = current_ori_z;
        move_point_ = arm5_;
      }
      else move_point_= wp5_;
    }
      break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm6_;
    }
      break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm7_;
    }
      break;
      // Add Dinh 200615
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = A_;
    }
      break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = B_;
    }
      break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = C_;
    }
      break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = D_;
    }
      break;
    case from_cart: {move_point_ = home1_;} break;
    case from_home1:{ move_point_ = wp15_;} break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }

  }

  else if((target_pos_y <6) && (target_pos_y >=-1) && (target_pos_x < ((wp5_pose[0]-1))) && (target_pos_x> home1_pose[0]+0.8))
    // to C rack
  {
    stop_ = to_C;

    switch (start_)
    {
    case from_charger: move_point_  = rot2_; break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm12_;
    }
      break;
    case from_home: move_point_ = wp3_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm34_;
    }
      break;
    case from_arm5:
    {
      if(current_x> wp5_pose[0]+5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_ow = current_ori_w;
        cur_oz = current_ori_z;
        move_point_ = arm5_;
      }
      else move_point_= wp5_;
    }
      break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm6_;
    }
      break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm7_;
    }
      break;
      // Add Dinh 200615
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = A_;
    }
      break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = B_;
    }
      break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = C_;
    }
      break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = D_;
    }
      break;
    case from_cart: {move_point_ = home1_;} break;
    case from_home1:{ move_point_ = wp16_;} break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }
  }

  else if((target_pos_y <-1) && (target_pos_x < ((wp5_pose[0]-1))) && (target_pos_x> home1_pose[0]+0.8))
    // to D rack
  {
    stop_ = to_D;

    switch (start_)
    {
    case from_charger:
    {
      move_point_  = rot2_;
    }
      break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm12_;
    }
      break;
    case from_home: move_point_ = wp3_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm34_;
    }
      break;
    case from_arm5:
    {
      if(current_x> wp5_pose[0]+5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_ow = current_ori_w;
        cur_oz = current_ori_z;
        move_point_ = arm5_;
      }
      else move_point_= wp5_;
    }
      break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm6_;
    }
      break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm7_;
    }
      break;
      // Add Dinh 200615
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = A_;
    }
      break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = B_;
    }
      break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = C_;
    }
      break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = D_;
    }
      break;
    case from_cart: {move_point_ = home1_;} break;
    case from_home1:{ move_point_ = wp16_;} break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }
  }

  // Add Dinh 200615
  else if((fabs(target_pos_x - cart_pose[0]) < 0.2) && (fabs(target_pos_y - cart_pose[1]) < 0.2) && (fabs(target_ori_w - cart_ori[3]) < 0.2))
    // to cart
  {
    stop_ = to_cart;

    switch (start_)
    {
    case from_charger: move_point_  = rot2_; break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm12_;
    }
      break;
    case from_home: move_point_ = wp6_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm34_;
    }
      break;
    case from_arm5:
    {
      if(current_x> wp5_pose[0]+5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_ow = current_ori_w;
        cur_oz = current_ori_z;
        move_point_ = arm5_;
      }
      else move_point_= wp5_;
    }
      break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm6_;
    }
      break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm7_;
    }
      break;
      // Add Dinh 200615
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = A_;
    }
      break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = B_;
    }
      break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = C_;
    }
      break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = D_;
    }
      break;
    case from_cart: {move_point_ = cart_;} break;
    case from_home1:{ move_point_ = cart_;} break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }
  }

  else // target not in the list
  {
    start_ = start_null;
    stop_ = stop_null;
    move_point_ = no_point_;
  }
  wp_pub();
}

void NavigationManagement::home1_cb(const std_msgs::UInt8::ConstPtr& home_sig)
{
  home_flag = home_sig->data;
  target_pos_x = home1_pose[0];
  target_pos_y = home1_pose[1];

  target_ori_z = home1_ori[2];
  target_ori_w = home1_ori[3];

  cancelAllGoals(0.01);

  start_ = start_null;
  stop_ = to_home1;

  if((fabs(current_x - charge_pose[0]) < 0.2) && (fabs(current_y - charge_pose[1]) < 0.2) && (fabs(current_ori_w - charge_ori[3]) < 0.2))
  {
    start_ = from_charger;
  }
  else if((fabs(current_x - cart_pose[0]) < 0.3) && (fabs(current_y - cart_pose[1]) < 0.3) && (fabs(current_ori_w - cart_ori[3]) < 0.3))
  {
    start_ = from_cart;
  }
  else if ((current_y >= home1_pose[1] - 6) && (current_x<= home1_pose[0]+0.4) && (current_x>=home1_pose[0]-0.4))
  {
    // from home1 area
    start_ = from_home1;
  }
  else if((current_y >= init_pose_[1] - 5) && (current_x>= wp3_pose[0]+7))
    // from arm 12
  {
    start_ = from_arm12;
  }
  else if ((current_y >= init_pose_[1] - 5) && (current_x< wp3_pose[0]+7) && (current_x>init_pose_[0]-2))
  {
    // from home area
    start_ = from_home;
  }
  else if((current_y < init_pose_[1] - 5) && (current_y >= wp4_pose[1]-1) && (current_x< wp3_pose[0]+7) && (current_x>init_pose_[0]-2))
  {
    // from charger area
    start_ = from_charger;
  }
  else if((current_y<wp4_pose[1]-1) && (current_y>=(wp5_pose[1]+3)) && (current_x>(wp5_pose[0]-1)) && (current_x<(wp5_pose[0]+10)))
    // from arm 34
  {
    start_ = from_arm34;
  }
  else if(current_y < (wp5_pose[1]+3) && (current_x>(wp5_pose[0]-1)) && (current_x<(wp9_pose[0]-5)))
  {
    // from arm5
    start_ = from_arm5;
  }

else if((current_x>=(wp9_pose[0]-5)) && (current_x<(wp11_pose[0])))
{
  // from arm6
  start_ = from_arm6;
}
else if(current_x>=(wp11_pose[0]))
{
  // from arm7
  start_ = from_arm7;
}
  // Add Dinh 200615
  else if((current_y > -1) && current_y < 6 && (current_x<(wp5_pose[0]-1)) && (current_x> home1_pose[0]+0.4))
  {
    // from C
    start_ = from_C;
  }
  else if((current_y <= -1) && (current_x<(wp5_pose[0]-1)) && (current_x> home1_pose[0]+0.4))
  {
    // from D
    start_ = from_D;
  }
  else if((current_y >8) && (current_y <12.2) && (current_x< (init_pose_[0]-2)) && (current_x> home1_pose[0]+0.4))
  {
    // from B
    start_ = from_B;
  }
  else if((current_y >=12.2) && (current_x< (init_pose_[0]-2)) && (current_x> home1_pose[0]+0.4))
  {
    // from A
    start_ = from_A;
  }
  else
  {
    // from undefined
    start_ = start_null;
    move_point_ = no_point_;
  }


    switch (start_)
    {
    case from_charger: move_point_  = rot2_; break;
    case from_arm12:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm12_;
    }
      break;
    case from_home: move_point_ = wp6_;
      break;
    case from_arm34:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm34_;
    }
      break;
    case from_arm5:
    {
      if(current_x> wp5_pose[0]+5)
      {
        cur_x = current_x;
        cur_y = current_y;
        cur_ow = current_ori_w;
        cur_oz = current_ori_z;
        move_point_ = arm5_;
      }
      else move_point_= wp5_;
    }
      break;
    case from_arm6:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm6_;
    }
      break;
    case from_arm7:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = arm7_;
    }
      break;
      // Add Dinh 200615
    case from_A:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = A_;
    }
      break;
    case from_B:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = B_;
    }
      break;
    case from_C:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = C_;
    }
      break;
    case from_D:
    {
      cur_x = current_x;
      cur_y = current_y;
      cur_ow = current_ori_w;
      cur_oz = current_ori_z;
      move_point_ = D_;
    }
      break;
    case from_cart: move_point_ = home1_; break;
    case from_home1: move_point_ = home1_; break;
     case start_null:
      move_point_ = no_point_;
      break;
    default:break;
    }
    wp_pub();
}

void NavigationManagement::goal_reach_cb(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach)
{
  if(!goal_reach->status_list.empty()){
//		actionlib_msgs::GoalStatus goal_Status = goal_reach->status_list[0];
    goal_Status = goal_reach->status_list[0];
    state_goal.data = goal_Status.status;
//		state_goal.data = goal_reach->status_list[0];
//		cancel.id = "";

    switch(goal_Status.status){
      case status_0_:
        ROS_INFO("AGV STATUS : PENDING[%d]",goal_Status.status);
        break;

      case status_1_:
//				ROS_INFO("AGV STATUS : ACTIVE[%d]",goal_Status.status);
//				ROS_INFO("AGV STATUS : ACTIVE");
        break;

      case status_3_:
//				ROS_INFO("AGV STATUS : COMPLETE[%d]",goal_Status.status);
//				ROS_INFO("AGV STATUS : GOAL REACH");
//				cancel_pub.publish(cancel);
//				ROS_INFO("Cancel Movement");
        break;

      case status_4_:
        ROS_INFO("AGV STATUS : ABORTED[%d]",goal_Status.status);
        break;

      case status_8_:
        ROS_INFO("AGV STATUS : RECALLED[%d]",goal_Status.status);
        break;

      default:
        break;
    }

  }
  else
  {
//		ROS_INFO("AGV STATUS : IDLING");
  }
}

void NavigationManagement::init_cb(const std_msgs::UInt8::ConstPtr& init_)
{
  ROS_INFO("Init Position");
  init_flag = init_->data;
//	ROS_INFO("init_flag : %d",init_flag);
}


void NavigationManagement::pgv_cb(const pgv::vision_msg& pgv_msg)
{
  control_id = pgv_msg.ControlID;
  control_ = pgv_msg.Control;
  tag_ = pgv_msg.Tag;
  tag_id = pgv_msg.TagID;
  line_ = pgv_msg.Line;
  line_id = pgv_msg.LineID;
  line_x = pgv_msg.X;
  line_y = pgv_msg.Y;
  line_theta = pgv_msg.theta;
  fault = pgv_msg.Fault;
  warning = pgv_msg.Warning;
  command = pgv_msg.command;

}


void NavigationManagement::tf_feedback_cb(const move_base_msgs::MoveBaseActionFeedback& feedback_)
{
  tf_fb_x	= feedback_.feedback.base_position.pose.position.x;
  tf_fb_y	= feedback_.feedback.base_position.pose.position.y;
  tf_fb_ori_z = feedback_.feedback.base_position.pose.orientation.z;
  tf_fb_ori_w = feedback_.feedback.base_position.pose.orientation.w;

}



void NavigationManagement::obstacle_cb(const obstacle_detector::obstacles_detect& obstacle_msg)
{
//	ROS_INFO("obstacle_cb");
//	ROS_INFO("obstacle number : %d",obstacle_msg.number);


  if(!obstacle_msg.number){
  //	ROS_INFO("No Obstacle detected");
  }
  else if(obstacle_msg.number){
//		ROS_INFO("1 Obstacle detected");
//		cancel_pub.publish(cancel);
//		obstacle_ = true;
//		wp_ = false;
//		ROS_INFO("AGV Moving Cancel");
  }
  else if(obstacle_msg.number==2){
//		ROS_INFO("2 Obstacle detected");
//		cancel_pub.publish(cancel);
//		obstacle_ = true;
//		wp_ = false;
//		ROS_INFO("AGV Moving Cancel");
  }
  else if(obstacle_msg.number==3){
//		ROS_INFO("3 Obstacle detected");
//		cancel_pub.publish(cancel);
//		obstacle_ = true;

  }
  else{

  }


}

void NavigationManagement::RTH()//return to home
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = init_pose_[0];
        goal.target_pose.pose.position.y = init_pose_[1];
  goal.target_pose.pose.position.z = init_pose_[2];

  goal.target_pose.pose.orientation.x = init_ori[0];
  goal.target_pose.pose.orientation.y = init_ori[1];
  goal.target_pose.pose.orientation.z = init_ori[2];
        goal.target_pose.pose.orientation.w = init_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("RTH() status = 1");

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;

          move_point_ = home_;
          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            // Modify Dinh 200608
            state_goal.data = 3;
            reach_pub.publish(state_goal);
            move_point_ = qr_reset_;
          }
          else
          {
            if(stop_ == to_arm12)
            {
              if(target_pos_y>init_pose_[1])
              {
                move_point_ = wp1_;
              }
              else
              {
                move_point_ = wp2_;
              }
            }
            else if(stop_ == to_charger) move_point_ = wp1_;
            else if(stop_ == to_arm34) move_point_ = wp3_;
            else if(stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7) move_point_ = wp3_;
            else if(stop_ == to_A || stop_ == to_B) move_point_ = wp6_;
            else if(stop_ == to_C || stop_ == to_D) move_point_ = wp3_;
            else if(stop_ == to_home1) move_point_ = wp6_;
            else if(stop_ == to_cart) move_point_ = wp6_;
            else move_point_ = no_point_;
          }

        }
}

void NavigationManagement::home1_move()// move to ST11
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = home1_pose[0];
        goal.target_pose.pose.position.y = home1_pose[1];
  goal.target_pose.pose.position.z = 0;

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  goal.target_pose.pose.orientation.z = home1_ori[2];
  goal.target_pose.pose.orientation.w = home1_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);

   if(count==0)
   {
       AGV_move_base->sendGoal(goal);
       count ++;
   }


     if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
     {
         count = 0;

          // Modify Dinh 200525
          if(stop_ == to_home1)
          {
            // Modify Dinh 200608
            state_goal.data = 3;
            reach_pub.publish(state_goal);
            move_point_ = qr_reset_;
            home_flag = false;
          }
          else
          {
            if(stop_ == to_cart) move_point_ = cart_;
            else if(stop_ == to_A || stop_ == to_B) move_point_ = wp15_;
            else if(stop_ == to_C || stop_ == to_D) move_point_ = wp16_;
            else if(stop_ == to_arm12 || stop_ == to_arm34 || stop_ == to_charger || stop_ == to_home) move_point_ = wp15_;
            else if(stop_ == to_arm5 || stop_ == to_arm6 || stop_ ==  to_arm7)  move_point_ = wp16_;
            else move_point_ = no_point_;
          }

        }
}

// Add Dinh 200615
void NavigationManagement::cart_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = cart_pose[0];
        goal.target_pose.pose.position.y = cart_pose[1];
  goal.target_pose.pose.position.z = cart_pose[2];

  goal.target_pose.pose.orientation.x = cart_ori[0];
  goal.target_pose.pose.orientation.y = cart_ori[1];
  goal.target_pose.pose.orientation.z = cart_ori[2];
        goal.target_pose.pose.orientation.w = cart_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("RTH() status = 1");

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          state_goal.data = 3;
          reach_pub.publish(state_goal);
          move_point_ = qr_reset_;
        }
}

void NavigationManagement::wp1_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp1_pose[0];
        goal.target_pose.pose.position.y = wp1_pose[1];
  goal.target_pose.pose.position.z = wp1_pose[2];

  goal.target_pose.pose.orientation.x = wp1_ori[0];
  goal.target_pose.pose.orientation.y = wp1_ori[1];
  goal.target_pose.pose.orientation.z = wp1_ori[2];
  goal.target_pose.pose.orientation.w = wp1_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("RTH() status = 1");

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;

          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            move_point_ = home_;
          }
          else
          {
            if(stop_ == to_arm12)
            {
              move_point_ = arm12_;
            }
            else if(stop_ == to_charger) move_point_ = charger1_;
            else move_point_ = no_point_;
          }

        }
}

void NavigationManagement::wp2_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp2_pose[0];
        goal.target_pose.pose.position.y = wp2_pose[1];
  goal.target_pose.pose.position.z = wp2_pose[2];

  goal.target_pose.pose.orientation.x = wp2_ori[0];
  goal.target_pose.pose.orientation.y = wp2_ori[1];
  goal.target_pose.pose.orientation.z = wp2_ori[2];
  goal.target_pose.pose.orientation.w = wp2_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;

          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            move_point_ = home_;
          }
          else
          {
            if(stop_ == to_arm12)
            {
              move_point_ = arm12_;
            }
            else {
              move_point_ = no_point_;
            }
          }

        }
}

void NavigationManagement::wp3_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp3_pose[0];
        goal.target_pose.pose.position.y = wp3_pose[1];
  goal.target_pose.pose.position.z = wp3_pose[2];

  goal.target_pose.pose.orientation.x = wp3_ori[0];
  goal.target_pose.pose.orientation.y = wp3_ori[1];
  goal.target_pose.pose.orientation.z = wp3_ori[2];
  goal.target_pose.pose.orientation.w = wp3_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            move_point_ = home_;
          }
          else
          {
            if(stop_ == to_arm12) move_point_ = rot1_;
            else if(stop_ == to_charger) move_point_ = wp4_;
            else if(stop_ == to_arm34) move_point_ = arm34_;
            else if(stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7) move_point_ = wp5_;
            else if(stop_ == to_C || stop_ == to_D) move_point_ = wp8_;
            else move_point_ = no_point_;
          }
        }
}

void NavigationManagement::wp4_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp4_pose[0];
        goal.target_pose.pose.position.y = wp4_pose[1];
  goal.target_pose.pose.position.z = wp4_pose[2];

  goal.target_pose.pose.orientation.x = wp4_ori[0];
  goal.target_pose.pose.orientation.y = wp4_ori[1];
  goal.target_pose.pose.orientation.z = wp4_ori[2];
  goal.target_pose.pose.orientation.w = wp4_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;

          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            move_point_ = wp3_;
          }
          else
          {
            if(stop_ == to_charger) move_point_ = charger_;
            else if(stop_ ==to_arm34) move_point_ = rot2_;
            else if(stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7) move_point_ = rot2_;
            else if(stop_ == to_C || stop_ == to_D) move_point_ = rot2_;
            else move_point_ = no_point_;
          }

        }
}

void NavigationManagement::wp5_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp5_pose[0];
        goal.target_pose.pose.position.y = wp5_pose[1];
  goal.target_pose.pose.position.z = wp5_pose[2];

  goal.target_pose.pose.orientation.x = wp5_ori[0];
  goal.target_pose.pose.orientation.y = wp5_ori[1];
  goal.target_pose.pose.orientation.z = wp5_ori[2];
  goal.target_pose.pose.orientation.w = wp5_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;

          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            move_point_ = rot3_;
          }
          else
          {
            if(stop_ == to_charger) move_point_ = rot3_;
            else if(stop_ == to_arm12) move_point_ = rot3_;
            else if(stop_ == to_arm34) move_point_ = rot3_;
            else if(stop_ == to_arm5) move_point_ = arm5_;
            else if(stop_ == to_arm6 || stop_ == to_arm7) move_point_ = wp51_;
            else if(stop_ == to_A || stop_ == to_B) move_point_ = rot3_;
            else if(stop_ == to_C || stop_ == to_D || stop_ == to_home1 || stop_ == to_cart) move_point_ = wp8_;
            else move_point_ = no_point_;
          }

        }
}

void NavigationManagement::wp51_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp51_pose[0];
        goal.target_pose.pose.position.y = wp51_pose[1];
  goal.target_pose.pose.position.z = wp51_pose[2];

  goal.target_pose.pose.orientation.x = wp51_ori[0];
  goal.target_pose.pose.orientation.y = wp51_ori[1];
  goal.target_pose.pose.orientation.z = wp51_ori[2];
  goal.target_pose.pose.orientation.w = wp51_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }

    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ !=stop_null)
          {
            if(stop_ == to_arm6 || stop_ == to_arm7) move_point_ = wp9_;
            else move_point_ = wp5_;

          }
          else move_point_ = no_point_;

        }
}

void NavigationManagement::wp6_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp6_pose[0];
        goal.target_pose.pose.position.y = wp6_pose[1];
  goal.target_pose.pose.position.z = wp6_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  if(stop_ == to_A || stop_ == to_B)
  {
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;
  }
  else if(stop_ == to_home1 || stop_ == to_cart)
  {
    goal.target_pose.pose.orientation.z = 0;
    goal.target_pose.pose.orientation.w = 1;
  }
  else
  {
    goal.target_pose.pose.orientation.z = init_ori[2];
    goal.target_pose.pose.orientation.w = init_ori[3];
  }

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200603
          if(stop_ !=stop_null)
          {
            if(stop_ == to_A) move_point_ = A_;
            else if(stop_ == to_B) move_point_ = B_;
            else if(stop_ == to_home1 || stop_ == to_cart) move_point_ = home1_;
            else move_point_ = home_;
          }

        }

}


void NavigationManagement::wp7_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp7_pose[0];
        goal.target_pose.pose.position.y = wp7_pose[1];
  goal.target_pose.pose.position.z = wp7_pose[2];

  goal.target_pose.pose.orientation.x = wp7_ori[0];
  goal.target_pose.pose.orientation.y = wp7_ori[1];
  goal.target_pose.pose.orientation.z = wp7_ori[2];
  goal.target_pose.pose.orientation.w = wp7_ori[3];
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200603
          if(stop_ !=stop_null)
          {
            if (stop_ == to_C || stop_ == to_D)
            {
              move_point_ = wp8_;
            }
            else if(stop_ == to_arm34)
            {
              move_point_ = arm34_;
            }
            else if (stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
            {
              move_point_ = wp5_;
            }
            else if(stop_ == to_home1 || stop_ == to_cart) move_point_ = rot5_;
            else
            {
              move_point_ = no_point_;
            }
          }


        }

}

void NavigationManagement::wp8_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp8_pose[0];
      //  goal.target_pose.pose.position.y = wp8_pose[1];
  goal.target_pose.pose.position.z = wp8_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;

// Add Dinh 200615
  if(stop_ == to_C || stop_ == to_D)
  {
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;
    if(stop_ == to_C)
    {
      goal.target_pose.pose.position.y = wp8_pose[1]+1;
    }
    else {
      goal.target_pose.pose.position.y = wp8_pose[1]-1;
    }
  }
  else if(stop_ == to_home1 || stop_ == to_cart)
  {
    goal.target_pose.pose.position.y = wp8_pose[1];
    goal.target_pose.pose.orientation.z = 0;
    goal.target_pose.pose.orientation.w = 1;
  }
  else
  {
    goal.target_pose.pose.position.y = wp8_pose[1];
    goal.target_pose.pose.orientation.z = wp3_ori[2];
    goal.target_pose.pose.orientation.w = wp3_ori[3];
  }
   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;

          // Modify Dinh 200603
          if(stop_ !=stop_null)
          {
            if (stop_ == to_C) move_point_ = C_;
            else if(stop_ == to_D) move_point_ = D_;
            else if(stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7) move_point_ = wp5_;
            else if(stop_ == to_home1 || stop_ == to_cart) move_point_ = wp16_;
            else
            {
              move_point_ = no_point_;
            }
          }

        }
}

void NavigationManagement::wp9_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp9_pose[0];
        goal.target_pose.pose.position.y = wp9_pose[1];
  goal.target_pose.pose.position.z = wp9_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  goal.target_pose.pose.orientation.z = wp9_ori[2];
  goal.target_pose.pose.orientation.w = wp9_ori[3];

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;

          // Modify Dinh 200603
          if(stop_ !=stop_null)
          {
            if (stop_ == to_arm6 || stop_ == to_arm7)
            {
              move_point_ = wp10_;
            }
            else
            {
              move_point_ = wp51_;
            }
          }
          else {
            move_point_ = no_point_;
          }


        }
}

void NavigationManagement::wp10_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
  goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp10_pose[0];
  goal.target_pose.pose.position.y = wp10_pose[1];
  goal.target_pose.pose.position.z = wp10_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  goal.target_pose.pose.orientation.z = wp10_ori[2];
  goal.target_pose.pose.orientation.w = wp10_ori[3];

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200603
          if(stop_ !=stop_null)
          {
            if (stop_ == to_arm6)
            {
              move_point_ = arm6_;
            }
            else if(stop_ == to_arm7)
            {
              move_point_ = wp11_;
            }
            else
            {
              move_point_ = wp9_;
            }
          }
          else {
            move_point_ = no_point_;
          }

        }
}

void NavigationManagement::wp11_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
  goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp11_pose[0];
  goal.target_pose.pose.position.y = wp11_pose[1];
  goal.target_pose.pose.position.z = wp11_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  goal.target_pose.pose.orientation.z = wp11_ori[2];
  goal.target_pose.pose.orientation.w = wp11_ori[3];

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200603
          if(stop_ !=stop_null)
          {
            if (stop_ == to_arm7)
            {
              move_point_ = wp12_;
            }
            else
            {
              move_point_ = wp10_;
            }
          }
          else {
            move_point_ = no_point_;
          }
        }

}

void NavigationManagement::wp12_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
  goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp12_pose[0];
  goal.target_pose.pose.position.y = wp12_pose[1];
  goal.target_pose.pose.position.z = wp12_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  goal.target_pose.pose.orientation.z = wp12_ori[2];
  goal.target_pose.pose.orientation.w = wp12_ori[3];

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200603
          if(stop_ !=stop_null)
          {
            if (stop_ == to_arm7)
            {
              move_point_ = wp13_;
            }
            else
            {
              move_point_ = wp11_;
            }
          }
          else {
            move_point_ = no_point_;
          }
        }
}

void NavigationManagement::wp13_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
  goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp13_pose[0];
  goal.target_pose.pose.position.y = wp13_pose[1];
  goal.target_pose.pose.position.z = wp13_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  goal.target_pose.pose.orientation.z = wp13_ori[2];
  goal.target_pose.pose.orientation.w = wp13_ori[3];

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200603
          if(stop_ !=stop_null)
          {
            if (stop_ == to_arm7)
            {
              move_point_ = wp14_;
            }
            else
            {
              move_point_ = wp12_;
            }
          }
          else {
            move_point_ = no_point_;
          }
        }
}

void NavigationManagement::wp14_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
  goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp14_pose[0];
  goal.target_pose.pose.position.y = wp14_pose[1];
  goal.target_pose.pose.position.z = wp14_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  goal.target_pose.pose.orientation.z = wp14_ori[2];
  goal.target_pose.pose.orientation.w = wp14_ori[3];

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);
  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
        // Modify Dinh 200603
          if(stop_ !=stop_null)
          {
            if (stop_ == to_arm7)
            {
              move_point_ = arm7_;
            }
            else
            {
              move_point_ = wp13_;
            }
          }
          else {
            move_point_ = no_point_;
          }
        }
}
// Add Dinh 200615
void NavigationManagement::wp15_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
  goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp15_pose[0];
  goal.target_pose.pose.position.y = wp15_pose[1];
  goal.target_pose.pose.position.z = wp15_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  if(stop_ == to_A || stop_ == to_B)
  {
    goal.target_pose.pose.orientation.w = target_ori_w;
    goal.target_pose.pose.orientation.z = target_ori_z;
  }
  else
  {
    goal.target_pose.pose.orientation.w = 1;
    goal.target_pose.pose.orientation.z = 0;
  }


   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200603
          if(stop_ !=stop_null)
          {
            if(stop_ == to_A) move_point_ = A_;
            else if(stop_ == to_B)  move_point_ = B_;
            else if(stop_ == to_home1 || stop_ == to_cart) move_point_ = home1_;
            else if(stop_ == to_arm34) move_point_ = wp7_;
            else move_point_ = wp6_;
          }
          else {
            move_point_ = no_point_;
          }
        }
}

// Add Dinh 200615
void NavigationManagement::wp16_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
  goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp16_pose[0];
  //goal.target_pose.pose.position.y = wp16_pose[1];
  goal.target_pose.pose.position.z = wp16_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  if(stop_ == to_C || stop_ == to_D)
  {
    goal.target_pose.pose.orientation.w = target_ori_w;
    goal.target_pose.pose.orientation.z = target_ori_z;
    if(stop_ == to_C)
    {
      goal.target_pose.pose.position.y = wp16_pose[1]+1;
    }
    else
      goal.target_pose.pose.position.y = wp16_pose[1]-1;
  }
  else if(stop_ == to_home1 || stop_ == to_cart)
  {
    goal.target_pose.pose.position.y = wp16_pose[1];
    goal.target_pose.pose.orientation.w = home1_ori[3];
    goal.target_pose.pose.orientation.z = home1_ori[2];
  }
  else
  {
    goal.target_pose.pose.position.y = wp16_pose[1];
    goal.target_pose.pose.orientation.w = 0;
    goal.target_pose.pose.orientation.z = 1;
  }


   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200603
          if(stop_ !=stop_null)
          {
            if(stop_ == to_C) move_point_ = C_;
            else if(stop_ == to_D)  move_point_ = D_;
            else if(stop_ == to_home1 || stop_ == to_cart) move_point_ = home1_;
            else if(stop_ == to_A || stop_ == to_B) move_point_ = home1_;
            else move_point_ = rot4_;
          }
          else
          {
            move_point_ = no_point_;
          }
        }
}


void NavigationManagement::rot1_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp3_pose[0];
        goal.target_pose.pose.position.y = wp3_pose[1];
  goal.target_pose.pose.position.z = wp3_pose[2];

  if(target_pos_y>init_pose_[1])
  {
    goal.target_pose.pose.orientation.x = wp1_ori[0];
    goal.target_pose.pose.orientation.y = wp1_ori[1];
    goal.target_pose.pose.orientation.z = wp1_ori[2];
    goal.target_pose.pose.orientation.w = wp1_ori[3];
  }
  else
  {
    goal.target_pose.pose.orientation.x = wp2_ori[0];
    goal.target_pose.pose.orientation.y = wp2_ori[1];
    goal.target_pose.pose.orientation.z = wp2_ori[2];
    goal.target_pose.pose.orientation.w = wp2_ori[3];
  }

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            move_point_ = home_;
          }
          else
          {
            if(stop_ == to_arm12) move_point_ = arm12_;
            else move_point_ = no_point_;
          }

        }
}



void NavigationManagement::rot2_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp4_pose[0];
        goal.target_pose.pose.position.y = wp4_pose[1];
  goal.target_pose.pose.position.z = wp4_pose[2];

  goal.target_pose.pose.orientation.x = wp3_ori[0];
  goal.target_pose.pose.orientation.y = wp3_ori[1];
  goal.target_pose.pose.orientation.z = wp3_ori[2];
  goal.target_pose.pose.orientation.w = wp3_ori[3];

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            move_point_ = home_;
          }
          else
          {
            if(stop_ == to_arm34)
            {
              move_point_ = arm34_;
            }
            else if(stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7)
            {
              move_point_ = wp5_;
            }
            else if(stop_ == to_C || stop_ == to_D) move_point_ = wp8_;
            else if(stop_ == to_A || stop_ == to_B || stop_ == to_home1 || stop_ == to_cart) move_point_ = rot5_;
            else move_point_ = no_point_;
          }

        }

}


void NavigationManagement::rot3_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp5_pose[0];
        goal.target_pose.pose.position.y = wp5_pose[1];
  goal.target_pose.pose.position.z = wp5_pose[2];

  goal.target_pose.pose.orientation.x = wp3_ori[0];
  goal.target_pose.pose.orientation.y = wp3_ori[1];
  goal.target_pose.pose.orientation.z = wp3_ori[2];
  goal.target_pose.pose.orientation.w = wp3_ori[3];

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            move_point_ = wp3_;
          }
          else
          {
            if(stop_ == to_arm34)
            {
              move_point_ = arm34_;
            }
            else if(stop_ == to_arm12)
            {
              move_point_ = rot1_;
            }
            else if(stop_ == to_C || stop_== to_D) move_point_ = wp8_;
            else if(stop_ == to_A || stop_ == to_B) move_point_ = rot5_;
            else if(stop_ = to_charger) move_point_ = wp4_;
            else if(stop_ == to_home1 || stop_ == to_cart) move_point_ = wp8_;
            else move_point_ = no_point_;
          }

        }

}

void NavigationManagement::rot4_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp8_pose[0];
  //goal.target_pose.pose.position.y = wp8_pose[1];
  goal.target_pose.pose.position.z = wp8_pose[2];
  if(start_ = from_C) goal.target_pose.pose.position.y = wp8_pose[1]+1;
  else if(start_ = from_D) goal.target_pose.pose.position.y = wp8_pose[1]-1;
  else goal.target_pose.pose.position.y = wp8_pose[1];

  goal.target_pose.pose.orientation.x = wp3_ori[0];
  goal.target_pose.pose.orientation.y = wp3_ori[1];
  goal.target_pose.pose.orientation.z = wp3_ori[2];
  goal.target_pose.pose.orientation.w = wp3_ori[3];

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            move_point_ = wp3_;
          }
          else if(stop_ == to_arm34)
          {
            move_point_ = arm34_;
          }
          else if(stop_ == to_arm12)
          {
            move_point_ = rot1_;
          }
          else if(stop_ == to_charger)
          {
            move_point_ = wp4_;
          }
          else if (stop_ == to_A || stop_ == to_B)
          {
            move_point_ = rot5_;
          }
          else if(stop_ ==  to_home1 || stop_ == to_cart)
            move_point_ = wp8_;
          else
          {
            move_point_ = no_point_;
          }


        }

}

void NavigationManagement::rot5_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = wp7_pose[0];
        goal.target_pose.pose.position.y = wp7_pose[1];
  goal.target_pose.pose.position.z = wp7_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  goal.target_pose.pose.orientation.z = target_ori_z;
  goal.target_pose.pose.orientation.w = target_ori_w;

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_A)
          {
            move_point_ = A_;
          }
          else if(stop_ == to_B) move_point_ = B_;
          else if(stop_ == to_home1 || stop_ == to_cart) move_point_ = home1_;
          else
          {
            move_point_ = no_point_;
          }

        }
}

void NavigationManagement::A_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.y = wp6_pose[1];
  goal.target_pose.pose.position.z = wp6_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  if(stop_ == to_A)
  {
    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;

  }
  else
  {
    goal.target_pose.pose.position.x = cur_x;
    goal.target_pose.pose.orientation.z = current_ori_z;
    goal.target_pose.pose.orientation.w = current_ori_w;

  }


   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_A)
          {
            move_point_ = rack_;
          }
          else if(stop_ == to_B) move_point_ = wp6_;
          else if(stop_ == to_home) move_point_ = wp6_;
          else if(stop_ == to_charger) move_point_ = wp6_;
          else if(stop_ == to_C || stop_ == to_D) move_point_ = wp7_;
          else if(stop_ == to_arm12)  move_point_ = wp6_;
          else if(stop_ == to_arm34) move_point_ = wp7_;
          else if(stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7) move_point_ = wp7_;
          else if(stop_ == to_home1 || stop_ == to_cart) move_point_ = home1_;
          else move_point_ = no_point_;

        }
}

void NavigationManagement::B_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.y = wp6_pose[1];
  goal.target_pose.pose.position.z = wp6_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  if(stop_ == to_B)
  {
    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;

  }
  else
  {
    goal.target_pose.pose.position.x = cur_x;
    goal.target_pose.pose.orientation.z = current_ori_z;
    goal.target_pose.pose.orientation.w = current_ori_w;

  }


   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_B)
          {
            move_point_ = rack_;
          }
          else if(stop_ == to_A) move_point_ = wp6_;
          else if(stop_ == to_home) move_point_ = wp6_;
          else if(stop_ == to_charger) move_point_ = wp6_;
          else if(stop_ == to_C || stop_ == to_D) move_point_ = wp7_;
          else if(stop_ == to_arm12)  move_point_ = wp6_;
          else if(stop_ == to_arm34) move_point_ = wp7_;
          else if(stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7) move_point_ = wp7_;
          else if(stop_ == to_home1 || stop_ == to_cart) move_point_ = home1_;
          else move_point_ = no_point_;

        }
}

void NavigationManagement::C_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.z = wp8_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  if(stop_ == to_C)
  {
    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.position.y = target_pos_y-0.8;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;
  }
  else
  {
    goal.target_pose.pose.position.x = cur_x;
    goal.target_pose.pose.position.y = wp8_pose[1]+1;
    goal.target_pose.pose.orientation.z = current_ori_z;
    goal.target_pose.pose.orientation.w = current_ori_w;
  }


   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_C)
          {
            move_point_ = rack_;
          }
          else if(stop_ == to_D) move_point_ = wp8_;
          else if(stop_ == to_home) move_point_ = rot4_;
          else if(stop_ == to_charger) move_point_ = rot4_;
          else if(stop_ == to_A || stop_ == to_B) move_point_ = rot4_;
          else if(stop_ == to_arm12)  move_point_ = rot4_;
          else if(stop_ == to_arm34) move_point_ = rot4_;
          else if(stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7) move_point_ = wp8_;
          else if(stop_ == to_home1 || stop_ == to_cart) move_point_ = wp16_;
          else move_point_ = no_point_;

        }
}

void NavigationManagement::D_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.z = wp8_pose[2];

  goal.target_pose.pose.orientation.x = 0;
  goal.target_pose.pose.orientation.y = 0;
  if(stop_ == to_D)
  {
    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.position.y = target_pos_y+0.8;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;
  }
  else
  {
    goal.target_pose.pose.position.x = cur_x;
    goal.target_pose.pose.position.y = wp8_pose[1];
    goal.target_pose.pose.orientation.z = current_ori_z;
    goal.target_pose.pose.orientation.w = current_ori_w;
  }


   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_D)
          {
            move_point_ = rack_;
          }
          else if(stop_ == to_C) move_point_ = wp8_;
          else if(stop_ == to_home) move_point_ = rot4_;
          else if(stop_ == to_charger) move_point_ = rot4_;
          else if(stop_ == to_A || stop_ == to_B) move_point_ = rot4_;
          else if(stop_ == to_arm12)  move_point_ = rot4_;
          else if(stop_ == to_arm34) move_point_ = rot4_;
          else if(stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7) move_point_ = wp8_;
          else if(stop_ == to_home1 || stop_ == to_cart) move_point_ = wp16_;
          else move_point_ = no_point_;

        }
}

void NavigationManagement::arm12_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  if(start_ == from_arm12)
  {
    goal.target_pose.pose.position.x = cur_x;
    goal.target_pose.pose.position.y = wp1_pose[1];
    goal.target_pose.pose.position.z = wp1_pose[2];

    goal.target_pose.pose.orientation.x = 0;
    goal.target_pose.pose.orientation.y = 0;
    goal.target_pose.pose.orientation.z = cur_oz;
    goal.target_pose.pose.orientation.w = cur_ow;


  }
  else
  {
    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.position.y = wp1_pose[1];
    goal.target_pose.pose.position.z = wp1_pose[2];

    goal.target_pose.pose.orientation.x = 0;
    goal.target_pose.pose.orientation.y = 0;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;
  }

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            move_point_ = home_;

          }
          else
          {
            if(stop_ == to_charger) {move_point_ = wp3_; }
            else if(stop_ == to_arm12)
            {
              if(start_ == from_arm12)
              {
                 move_point_ = arm12_;
              }
              else move_point_ = rack_;
            }
            else if(stop_ == to_arm34) {move_point_ = wp3_; }
            else if(stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7) {move_point_ = wp3_; }
            else if (stop_ == to_A || stop_ == to_B) move_point_ = home_;
            else if (stop_ == to_C || stop_ == to_D) move_point_ = wp3_;
            else if (stop_ == to_home1 || stop_ == to_cart) move_point_ = home_;
            else
            {
               move_point_ = no_point_;
            }
          }

        }

}

void NavigationManagement::arm34_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");
  if(stop_ == to_arm34)
  {
    goal.target_pose.pose.position.x = wp3_pose[0];
    goal.target_pose.pose.position.y = target_pos_y;
    goal.target_pose.pose.position.z = wp3_pose[2];

    goal.target_pose.pose.orientation.x = 0;
    goal.target_pose.pose.orientation.y = 0;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;
  }
  else
  {
      goal.target_pose.pose.position.x = wp3_pose[0];
      goal.target_pose.pose.position.y = cur_y;
      goal.target_pose.pose.position.z = wp3_pose[2];

      goal.target_pose.pose.orientation.x = 0;
      goal.target_pose.pose.orientation.y = 0;
      goal.target_pose.pose.orientation.z = cur_oz;
      goal.target_pose.pose.orientation.w = cur_ow;
  }



   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            move_point_ = wp3_;

          }
          else
          {
            if(stop_ == to_charger){move_point_ = wp4_; }
            else if(stop_ == to_arm12)  {move_point_ = rot1_; }
            else if(stop_ == to_arm34)
            {
              if(start_ == from_arm34)
              {
                move_point_ = arm34_;
              }
              else move_point_ = rack_;
            }
            else if(stop_ == to_arm5 || stop_ == to_arm6 || stop_ == to_arm7) {move_point_ = wp5_; }
            else if (stop_ == to_A || stop_ == to_B) move_point_ = rot5_;
            else if (stop_ == to_C || stop_ == to_D) move_point_ = wp8_;
            else if (stop_ == to_home1 || stop_ == to_cart) move_point_ = rot5_;
            else
            {
              move_point_ = no_point_;
            }
          }

        }
}

void NavigationManagement::arm5_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  if(stop_ == to_arm5)
  {
    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.position.y = wp5_pose[1];
    goal.target_pose.pose.position.z = wp5_pose[2];

    goal.target_pose.pose.orientation.x = 0;
    goal.target_pose.pose.orientation.y = 0;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;

  }
  else
  {
    goal.target_pose.pose.position.x = cur_x;
    goal.target_pose.pose.position.y = wp5_pose[1];
    goal.target_pose.pose.position.z = wp5_pose[2];

    goal.target_pose.pose.orientation.x = 0;
    goal.target_pose.pose.orientation.y = 0;
    goal.target_pose.pose.orientation.z = cur_oz;
    goal.target_pose.pose.orientation.w = cur_ow;
  }

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200525
          if(stop_ == to_home)
          {
            move_point_ = rot3_;

          }
          else
          {
            if(stop_ == to_charger) {move_point_ = rot3_; }
            else if(stop_ == to_arm12) {move_point_ = rot3_; }
            else if(stop_ == to_arm34) {move_point_ = wp5_; }
            else if(stop_ == to_arm5)
            {
               move_point_ = rack_;
            }
            else if(stop_ == to_arm6 || stop_ == to_arm7) move_point_ = wp51_;
            else if(stop_ == to_A || stop_ == to_B) move_point_ = rot3_;
            else if(stop_ == to_C || stop_ == to_D) move_point_ = wp5_;
            else if (stop_ == to_home1 || stop_ == to_cart) move_point_ = wp5_;
            else
            {
              move_point_ = no_point_;
            }
          }

        }
}

void NavigationManagement::arm6_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  if(stop_ == to_arm6)
  {
    goal.target_pose.pose.position.x =  wp10_pose[0];
    goal.target_pose.pose.position.y = target_pos_y;
    goal.target_pose.pose.position.z = wp10_pose[2];

    goal.target_pose.pose.orientation.x = 0;
    goal.target_pose.pose.orientation.y = 0;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;

  }
  else
  {
    goal.target_pose.pose.position.x =  wp10_pose[0];
    goal.target_pose.pose.position.y = cur_y;
    goal.target_pose.pose.position.z = wp10_pose[2];

    goal.target_pose.pose.orientation.x = 0;
    goal.target_pose.pose.orientation.y = 0;
    goal.target_pose.pose.orientation.z = wp10_ori[2];
    goal.target_pose.pose.orientation.w = wp10_ori[3];
  }

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200604
          if(stop_ != stop_null)
          {
              if(stop_ == to_arm6)
              {
                move_point_ = rack_;
              }
              else
              {
                move_point_ = wp10_;
              }
          }
        }
}

void NavigationManagement::arm7_move()
{

  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";

  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  if(stop_ == to_arm7)
  {
    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.position.y = wp14_pose[1];
    goal.target_pose.pose.position.z = wp14_pose[2];

    goal.target_pose.pose.orientation.x = 0;
    goal.target_pose.pose.orientation.y = 0;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;

  }
  else
  {
    goal.target_pose.pose.position.x = cur_x;
    goal.target_pose.pose.position.y = wp14_pose[1];
    goal.target_pose.pose.position.z = wp14_pose[2];

    goal.target_pose.pose.orientation.x = 0;
    goal.target_pose.pose.orientation.y = 0;
    goal.target_pose.pose.orientation.z = cur_oz;
    goal.target_pose.pose.orientation.w = cur_ow;
  }

   // Add Dinh 200519
   obstacle_goal.pose = goal.target_pose.pose;
   goal_pub.publish(obstacle_goal);
  state_goal.data = 1;
  reach_pub.publish(state_goal);

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          // Modify Dinh 200604
          if(stop_ != stop_null)
          {
              if(stop_ == to_arm7)
              {
                move_point_ = rack_;
              }
              else
              {
                move_point_ = wp14_;
              }
          }
        }
}

// Need to modify for AGV1 and AGV2
void NavigationManagement::charge_move()
{
  ROS_INFO("AGV Rotate for Charging");
  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";



  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = target_pos_x;
  goal.target_pose.pose.position.y = target_pos_y;
  goal.target_pose.pose.position.z = charge_pose[2];

  goal.target_pose.pose.orientation.x = charge_ori[0];
  goal.target_pose.pose.orientation.y = charge_ori[1];
  goal.target_pose.pose.orientation.z = target_ori_z;
  goal.target_pose.pose.orientation.w = target_ori_w;
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("charge_move() status = 1");

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
          if(tag_)
          {
             move_point_ = qr_reset_;
          }
          else {
              state_goal.data = 3;
              reach_pub.publish(state_goal);
              start_ = start_null;
              stop_ = stop_null;
              move_point_ = no_point_;
          }

        }
}

void NavigationManagement::charge1_move()
{
  ROS_INFO("AGV Rotate for Charging");
  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";



  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = target_pos_x+0.7;
  goal.target_pose.pose.position.y = init_pose_[1];
  goal.target_pose.pose.position.z = charge_pose[2];

  goal.target_pose.pose.orientation.x = charge_ori[0];
  goal.target_pose.pose.orientation.y = charge_ori[1];
  goal.target_pose.pose.orientation.z = target_ori_z;
  goal.target_pose.pose.orientation.w = target_ori_w;
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("charge_move() status = 1");

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
            if(stop_ == to_charger) move_point_ = charger2_;
            else if(stop_ = to_home) move_point_ = home_;
            else move_point_ = no_point_;

        }
}

void NavigationManagement::charge2_move()
{
  ROS_INFO("AGV Rotate for Charging");
  goal.target_pose.header.stamp = ros::Time::now();
        goal.target_pose.header.frame_id = "map";



  while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
    ROS_INFO("Waiting for the move_base action server");

  goal.target_pose.pose.position.x = target_pos_x+0.7;
  goal.target_pose.pose.position.y = target_pos_y;
  goal.target_pose.pose.position.z = charge_pose[2];

  goal.target_pose.pose.orientation.x = charge_ori[0];
  goal.target_pose.pose.orientation.y = charge_ori[1];
  goal.target_pose.pose.orientation.z = target_ori_z;
  goal.target_pose.pose.orientation.w = target_ori_w;
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);

  state_goal.data = 1;
  reach_pub.publish(state_goal);
//	ROS_INFO("charge_move() status = 1");

  if(count==0)
  {
      AGV_move_base->sendGoal(goal);
      count ++;
  }


    if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        count = 0;
            if(stop_ == to_charger) move_point_ = charger_;
            else if(stop_ = to_home) move_point_ = charger1_;
            else move_point_ = no_point_;

        }
}



void NavigationManagement::init_pose()
{
  geometry_msgs::PoseWithCovarianceStamped init_;
  init_.header.stamp = ros::Time::now();
  init_.header.frame_id = "map";

/*	init_.pose.pose.position.x = INIT_POS_X;
  init_.pose.pose.position.y = INIT_POS_Y;*/
  init_.pose.pose.position.x = init_pose_[0];
  init_.pose.pose.position.y = init_pose_[1];
  init_.pose.pose.position.z = 0.0;

  init_.pose.pose.orientation.x = 0.0;
  init_.pose.pose.orientation.y = 0.0;
/*	init_.pose.pose.orientation.z = INIT_ORI_Z;
  init_.pose.pose.orientation.w = INIT_ORI_W;*/
  init_.pose.pose.orientation.z = init_ori[2];
  init_.pose.pose.orientation.w = init_ori[3];

  init_pub.publish(init_);

//	init_flag = 0;

  printf("AGV InitialPose Complete\n");

}

void NavigationManagement::param_set()
{

  if(nh.getParam("/init_pose_",init_pose_)){
    for(vector<int>::size_type i=0;i<init_pose_.size();++i){
      ROS_INFO("init_pose : %.2f",init_pose_[i]);
    }
  }

  if(nh.getParam("/init_ori",init_ori)){
    for(vector<int>::size_type i=0;i<init_ori.size();++i){
      ROS_INFO("init_ori : %.2f",init_ori[i]);
    }
  }


  if(nh.getParam("/charge_pose",charge_pose)){
    for(vector<int>::size_type i=0;i<charge_pose.size();++i){
      ROS_INFO("charge_pose : %.2f",charge_pose[i]);
    }
  }

  if(nh.getParam("/charge_ori",charge_ori)){
    for(vector<int>::size_type i=0;i<charge_ori.size();++i){
      ROS_INFO("charge_ori : %.2f",charge_ori[i]);
    }
  }

  if(nh.getParam("/wp1_pose",wp1_pose)){
    for(vector<int>::size_type i=0;i<wp1_pose.size();++i){
      ROS_INFO("wp1_pose : %.2f",wp1_pose[i]);
    }
  }

  if(nh.getParam("/wp1_ori",wp1_ori)){
    for(vector<int>::size_type i=0;i<wp1_ori.size();++i){
      ROS_INFO("wp1_ori : %.2f",wp1_ori[i]);
    }
  }

  if(nh.getParam("/wp2_pose",wp2_pose)){
    for(vector<int>::size_type i=0;i<wp2_pose.size();++i){
      ROS_INFO("wp2_pose : %.2f",wp2_pose[i]);
    }
  }

  if(nh.getParam("/wp2_ori",wp2_ori)){
    for(vector<int>::size_type i=0;i<wp2_ori.size();++i){
      ROS_INFO("wp2_ori : %.2f",wp2_ori[i]);
    }
  }

  if(nh.getParam("/wp3_pose",wp3_pose)){
    for(vector<int>::size_type i=0;i<wp3_pose.size();++i){
      ROS_INFO("wp3_pose : %.2f",wp3_pose[i]);
    }
  }

  if(nh.getParam("/wp3_ori",wp3_ori)){
    for(vector<int>::size_type i=0;i<wp3_ori.size();++i){
      ROS_INFO("wp3_ori : %.2f",wp3_ori[i]);
    }
  }

  if(nh.getParam("/wp4_pose",wp4_pose)){
    for(vector<int>::size_type i=0;i<wp4_pose.size();++i){
      ROS_INFO("wp4_pose : %.2f",wp4_pose[i]);
    }
  }

  if(nh.getParam("/wp4_ori",wp4_ori)){
    for(vector<int>::size_type i=0;i<wp4_ori.size();++i){
      ROS_INFO("wp4_ori : %.2f",wp4_ori[i]);
    }
  }


  if(nh.getParam("/wp5_pose",wp5_pose))
  {
    for(vector<int>::size_type i=0;i<wp5_pose.size();++i){
      ROS_INFO("wp5_pose : %.2f",wp5_pose[i]);
    }
  }

  if(nh.getParam("/wp5_ori",wp5_ori))
  {
    for(vector<int>::size_type i=0;i<wp5_ori.size();++i){
      ROS_INFO("wp5_ori : %.2f",wp5_ori[i]);
    }
  }

  if(nh.getParam("/wp51_pose",wp51_pose))
  {
    for(vector<int>::size_type i=0;i<wp51_pose.size();++i){
      ROS_INFO("wp51_pose : %.2f",wp51_pose[i]);
    }
  }

  if(nh.getParam("/wp51_ori",wp51_ori))
  {
    for(vector<int>::size_type i=0;i<wp51_ori.size();++i){
      ROS_INFO("wp51_ori : %.2f",wp51_ori[i]);
    }
  }

  if(nh.getParam("/wp6_pose",wp6_pose))
  {
    for(vector<int>::size_type i=0;i<wp6_pose.size();++i){
      ROS_INFO("wp6_pose : %.2f",wp6_pose[i]);
    }
  }

  if(nh.getParam("/wp6_ori",wp6_ori))
  {
    for(vector<int>::size_type i=0;i<wp6_ori.size();++i){
      ROS_INFO("wp6_ori : %.2f",wp6_ori[i]);
    }
  }
  if(nh.getParam("/wp7_pose",wp7_pose))
  {
    for(vector<int>::size_type i=0;i<wp7_pose.size();++i){
      ROS_INFO("wp7_pose : %.2f",wp7_pose[i]);
    }
  }

  if(nh.getParam("/wp7_ori",wp7_ori))
  {
    for(vector<int>::size_type i=0;i<wp7_ori.size();++i){
      ROS_INFO("wp7_ori : %.2f",wp7_ori[i]);
    }
  }
  if(nh.getParam("/wp8_pose",wp8_pose))
  {
    for(vector<int>::size_type i=0;i<wp8_pose.size();++i){
      ROS_INFO("wp8_pose : %.2f",wp8_pose[i]);
    }
  }

  if(nh.getParam("/wp8_ori",wp8_ori))
  {
    for(vector<int>::size_type i=0;i<wp8_ori.size();++i){
      ROS_INFO("wp8_ori : %.2f",wp8_ori[i]);
    }
  }
  if(nh.getParam("/wp9_pose",wp9_pose))
  {
    for(vector<int>::size_type i=0;i<wp9_pose.size();++i){
      ROS_INFO("wp9_pose : %.2f",wp9_pose[i]);
    }
  }

  if(nh.getParam("/wp9_ori",wp9_ori))
  {
    for(vector<int>::size_type i=0;i<wp9_ori.size();++i){
      ROS_INFO("wp9_ori : %.2f",wp9_ori[i]);
    }
  }

  if(nh.getParam("/wp10_pose",wp10_pose))
  {
    for(vector<int>::size_type i=0;i<wp10_pose.size();++i){
      ROS_INFO("wp10_pose : %.2f",wp10_pose[i]);
    }
  }

  if(nh.getParam("/wp10_ori",wp10_ori))
  {
    for(vector<int>::size_type i=0;i<wp10_ori.size();++i){
      ROS_INFO("wp10_ori : %.2f",wp10_ori[i]);
    }
  }

  if(nh.getParam("/wp11_pose",wp11_pose))
  {
    for(vector<int>::size_type i=0;i<wp11_pose.size();++i){
      ROS_INFO("wp11_pose : %.2f",wp11_pose[i]);
    }
  }

  if(nh.getParam("/wp11_ori",wp11_ori))
  {
    for(vector<int>::size_type i=0;i<wp11_ori.size();++i){
      ROS_INFO("wp11_ori : %.2f",wp11_ori[i]);
    }
  }

  if(nh.getParam("/wp12_pose",wp12_pose))
  {
    for(vector<int>::size_type i=0;i<wp12_pose.size();++i){
      ROS_INFO("wp12_pose : %.2f",wp12_pose[i]);
    }
  }

  if(nh.getParam("/wp12_ori",wp12_ori))
  {
    for(vector<int>::size_type i=0;i<wp12_ori.size();++i){
      ROS_INFO("wp12_ori : %.2f",wp12_ori[i]);
    }
  }

  if(nh.getParam("/wp13_pose",wp13_pose))
  {
    for(vector<int>::size_type i=0;i<wp13_pose.size();++i){
      ROS_INFO("wp13_pose : %.2f",wp13_pose[i]);
    }
  }

  if(nh.getParam("/wp13_ori",wp13_ori))
  {
    for(vector<int>::size_type i=0;i<wp13_ori.size();++i){
      ROS_INFO("wp13_ori : %.2f",wp13_ori[i]);
    }
  }

  if(nh.getParam("/wp14_pose",wp14_pose))
  {
    for(vector<int>::size_type i=0;i<wp14_pose.size();++i){
      ROS_INFO("wp14_pose : %.2f",wp14_pose[i]);
    }
  }

  if(nh.getParam("/wp14_ori",wp14_ori))
  {
    for(vector<int>::size_type i=0;i<wp14_ori.size();++i){
      ROS_INFO("wp14_ori : %.2f",wp14_ori[i]);
    }
  }

  if(nh.getParam("/wp15_pose",wp15_pose))
  {
    for(vector<int>::size_type i=0;i<wp15_pose.size();++i){
      ROS_INFO("wp15_pose : %.2f",wp15_pose[i]);
    }
  }

  if(nh.getParam("/wp15_ori",wp15_ori))
  {
    for(vector<int>::size_type i=0;i<wp15_ori.size();++i){
      ROS_INFO("wp15_ori : %.2f",wp15_ori[i]);
    }
  }

  if(nh.getParam("/wp16_pose",wp16_pose))
  {
    for(vector<int>::size_type i=0;i<wp16_pose.size();++i){
      ROS_INFO("wp16_pose : %.2f",wp16_pose[i]);
    }
  }

  if(nh.getParam("/wp16_ori",wp16_ori))
  {
    for(vector<int>::size_type i=0;i<wp16_ori.size();++i){
      ROS_INFO("wp16_ori : %.2f",wp16_ori[i]);
    }
  }

  if(nh.getParam("/home1_pose",home1_pose))
  {
    for(vector<int>::size_type i=0;i<home1_pose.size();++i){
      ROS_INFO("home1_pose : %.2f",home1_pose[i]);
    }
  }

  if(nh.getParam("/home1_ori",home1_ori))
  {
    for(vector<int>::size_type i=0;i<home1_ori.size();++i){
      ROS_INFO("home1_ori : %.2f",home1_ori[i]);
    }
  }

  if(nh.getParam("/cart_pose",cart_pose))
  {
    for(vector<int>::size_type i=0;i<cart_pose.size();++i){
      ROS_INFO("cart_pose : %.2f",cart_pose[i]);
    }
  }

  if(nh.getParam("/cart_ori",cart_ori))
  {
    for(vector<int>::size_type i=0;i<cart_ori.size();++i){
      ROS_INFO("cart_ori : %.2f",cart_ori[i]);
    }
  }

}


void NavigationManagement::home_reset()
{
  if(tag_== true && fabs(current_x-target_pos_x)<= 0.15)
  {
    if(goal_Status.status==3 && ((abs(line_x) > 2 && abs(line_x)<100) || (abs(line_y) > 2 && abs(line_y)<100) || (line_theta >175 && line_theta < 185)))
    {
      if(abs(line_x) > 2 && abs(line_x)<100)
      {
         vel_.linear.x = - line_x/abs(line_x) * QR_VEL;
      }
      else vel_.linear.x = 0;
      if(abs(line_y) > 2 && abs(line_y)<100)
      {
         vel_.linear.y = - line_y/abs(line_y) * QR_VEL;
      }
      else vel_.linear.y = 0;

      if(line_theta >175 && line_theta < 185)
      {
        if(line_theta>=181 || line_theta <=179)
        {
          vel_.angular.z = (line_theta-180)/abs(line_theta-180) * QR_AVEL;
        }
        else {
          vel_.angular.z = 0;
        }
      }
      else vel_.angular.z = 0;

      vel_pub.publish(vel_);
    }
    else
    {

      stop_agv();
    }
  }
  else
  {
    move_point_ = no_point_;
    stop_agv();
  }
}

void NavigationManagement::rack_move()
{
//	ROS_INFO("rack_move");
  ROS_INFO("AGV-precision closely approach to the storage Rack");


    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.header.frame_id = "map";

    while(!AGV_move_base->waitForServer(ros::Duration(5.0)))
      ROS_INFO("Waiting for the move_base action server");

    goal.target_pose.pose.position.x = target_pos_x;
    goal.target_pose.pose.position.y = target_pos_y;
    goal.target_pose.pose.orientation.z = target_ori_z;
    goal.target_pose.pose.orientation.w = target_ori_w;
  // Add Dinh 200519
  obstacle_goal.pose = goal.target_pose.pose;
  goal_pub.publish(obstacle_goal);
    ROS_INFO("AGV-precision is control to the storage rack");

    state_goal.data = 1;
    reach_pub.publish(state_goal);
  //	ROS_INFO("rack_move() status = 1");

    if(count==0)
    {
        AGV_move_base->sendGoal(goal);
        count ++;
    }


      if(AGV_move_base->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
      {
          count = 0;
      state_goal.data = 3;
      reach_pub.publish(state_goal);
      move_point_ = qr_reset_;
    }


}
// Add Dinh 200608
void NavigationManagement::joystickControlCallback(const move_control::teleop::ConstPtr& motorCmd_)
{
  if(motorCmd_->control_mode ==1 || motorCmd_->control_mode ==2) manual_mode = true;
  else manual_mode = false;
}

void NavigationManagement::stop_agv()
{
  vel_.linear.x = 0;
  vel_.linear.y = 0;
  vel_.angular.z = 0;
  vel_pub.publish(vel_);
}
// Modify 200608

void NavigationManagement::qr_move()
{

  if(tag_== true && fabs(current_x-target_pos_x)<= 0.15)
  {
    if(goal_Status.status==3 && ((abs(line_x) > 2 && abs(line_x)<100) || (abs(line_y) > 2 && abs(line_y)<100) || (line_theta >175 && line_theta < 185)))
    {
      if(abs(line_x) > 2 && abs(line_x)<100)
      {
         vel_.linear.x = - line_x/abs(line_x) * QR_VEL;
      }
      else vel_.linear.x = 0;
      if(abs(line_y) > 2 && abs(line_y)<100)
      {
         vel_.linear.y = - line_y/abs(line_y) * QR_VEL;
      }
      else vel_.linear.y = 0;

      if(line_theta >175 && line_theta < 185)
      {
        if(line_theta>=181 || line_theta <=179)
        {
          vel_.angular.z = (line_theta-180)/abs(line_theta-180) * QR_AVEL;
        }
        else {
          vel_.angular.z = 0;
        }
      }
      else vel_.angular.z = 0;

      vel_pub.publish(vel_);
    }
    else
    {

      stop_agv();
    }
  }
  else
  {
    move_point_ = no_point_;
    stop_agv();
  }

}
}



int main(int argc, char** argv)
{
  ros::init(argc, argv, "AGV_nav_action");

  agv_wp::NavigationManagement AGV_nav;
  AGV_nav.spin();
  agv_wp::status st;
  st.active = true;

  return 0;
}



