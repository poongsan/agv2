#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <actionlib_msgs/GoalStatus.h>
#include <actionlib_msgs/GoalStatusArray.h>
//#include <nav_msgs/Feedback.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>

#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>

using namespace std;

namespace gil{
	enum lee{
		status_1,
		status_2,
		status_3,
		status_4,
		status_5,
		status_6
	};

	class move{
		public:
			move();
			void init();
			void spin();
			void update();

		private:
			bool run_;
			std_msgs::UInt8 state_goal;
			geometry_msgs::Twist vel;

			ros::Publisher vel_pub, goal_pub, reach_pub;
			ros::Subscriber reach_sub; 

			void goal_reach_callback(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach);
	};
} 

void gil::move::init()
{


}

gil::move::move()
{
//	init();
	printf("variables initialize!!\n");
	ros::NodeHandle nh;
	run_ = true;

	vel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1, true);
	goal_pub = nh.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1, true);
	reach_pub = nh.advertise<std_msgs::UInt8>("/IFNode/goal_reach", 1, true);

	reach_sub = nh.subscribe("/move_base/status", 1, &gil::move::goal_reach_callback, this);

}

void gil::move::update() 
{
	ros::Time::now();
	//printf("moving control\n");

}

void gil::move::spin()
{
	ros::Rate loop_rate(10);

	while(run_)
	{
		update();
		ros::spinOnce();
		loop_rate.sleep();
	}

}

void gil::move::goal_reach_callback(const actionlib_msgs::GoalStatusArray::ConstPtr& goal_reach)
{
	if(!goal_reach->status_list.empty())
	{
		actionlib_msgs::GoalStatus goal_Status = goal_reach->status_list[0];
		state_goal.data = goal_Status.status;

		printf("agv status: %d\n",goal_Status.status);

		if(state_goal.data == 1)
		{
			printf("AGV STATUS : ACTIVE\n");
		}
		else if(state_goal.data == 4)
		{
			printf("AGV STATUS : ABORTED\n");
		}
		else if(state_goal.data == 3)
		{
			printf("AGV STATUS : COMPLETE\n");
		}
	}
	else
	{
		printf("AGV STATUS : IDLING\n");
	}

}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "moving_agv");
	ROS_INFO("==== moving start ====");
	gil::move *mv = new gil::move();

	mv->spin();

	return 0;
}
