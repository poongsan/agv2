#include "server.h"
#include <QDebug>

Server::Server(quint16 port, QObject *parent)
    : QTcpServer(parent)
{

    if (!this->listen(QHostAddress::Any, port))
    {
        return;
    }
    else
    {
        //qDebug()<<"Server"<<getHostAddress()<<port<<"is ON";
        connect(this, SIGNAL(newConnection()), this, SLOT(addClient()));
    }
}

QHostAddress Server::getHostAddress() const
{
    QHostAddress hostAddress;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();

    for (int i = 0; i < ipAddressesList.size(); ++i)
    {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost && ipAddressesList.at(i).toIPv4Address())
        {
            hostAddress = QHostAddress(ipAddressesList.at(i).toString());
            break;
        }
    }

    if (hostAddress.isNull())
    {
        hostAddress = QHostAddress(QHostAddress::LocalHost);
    }
    return hostAddress;
}

quint16 Server::getPort() const
{
    return this->serverPort();
}

void Server::addClient()
{
//    qDebug()<<"open : ";
    while(this->hasPendingConnections())
    {
        //qDebug()<<"open : "<<getHostAddress()<<getPort();
        QTcpSocket *socket =  this->nextPendingConnection();
        clients.insert(socket);
        //qDebug()<<client->isOpen();
        connect(socket, SIGNAL(readyRead()), this, SLOT(readReceivedData()));
        connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(displayError(QAbstractSocket::SocketError)));
        connect(socket, SIGNAL(disconnected()), this, SLOT(connectionLost()));
        connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
        // 0323
        bool ok = false;
        if(socket->peerAddress().toIPv4Address()==QHostAddress("192.168.2.2").toIPv4Address())
            ok = true;
            
        //qDebug()<<"peerAddress"<<socket->peerAddress()<<ok<<QHostAddress("192.168.2.2");
        //qDebug()<<"peerPort"<<socket->peerPort();
        //qDebug()<<"peerName"<<socket->peerName();
        //qDebug()<<"peerDesciptor"<<socket->socketDescriptor();
        /////////////////////////////////////
    }
}/*
void Server::incomingConnection(int socketfd)
{   qDebug()<<"imcoming";
    QTcpSocket *socket = new QTcpSocket(this);
    socket->setSocketDescriptor(socketfd);

    clients.insert(socket);
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
}*/

void Server::disconnected()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    clients.remove(socket);
}

void Server::writeData(QByteArray str)
{
    foreach(QTcpSocket *socket, clients)
    {
        if(socket->state() == QAbstractSocket::ConnectedState)
        {
            socket->write(str);
            //qDebug()<<"write Data : " << str;
            //return true;
        }
        else
        {
            //qDebug()<<"close";
            //return false;
        }
    }
}

void Server::readReceivedData()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    QByteArray buffer;
    //qDebug()<< "Server Recieve";
    while(socket->bytesAvailable())
    {
        buffer.append(socket->readLine());
    }
    QString str= QString(buffer);
	
	Q_EMIT readData(buffer);
	Q_EMIT readData(str);
	
	// 0323
    //qDebug()<<"peerAddress"<<socket->peerAddress();
    //qDebug()<<"peerPort"<<socket->peerPort();
    //qDebug()<<"peerName"<<socket->peerName();
    //qDebug()<<"peerDesciptor"<<socket->socketDescriptor();
    /////////////////////////////////////

//	writeData(buffer);
/*
    if(getPort()==49154)
    {

    }
    else if(getPort()==49155)
    {

    }
    else if(getPort()==49157)   //Arduino
    {

    }
    else
    {
        qDebug()<<"Wrong Port Connection"<<getPort();
    }
*/
    //socket->close();
}

void Server::displayError(QAbstractSocket::SocketError error)
{
    Q_UNUSED(error)
    qDebug()<<this->getPort()<<error;
}

void Server::connectionLost()
{
    //qDebug()<<this->getPort()<<"disconnect";
}

