#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QWidget>
#include <QTcpSocket>
#include <QTimer>

class Client : public QObject
{
    Q_OBJECT

public:
    Client(QString ip_addr, quint16 port = 0, QObject * parent = nullptr);

    void setIP(QString);
    void setPort(quint16);
//    bool doConnect();
    bool writeData(QByteArray data);

public slots:
    bool doConnect();
    void doConnect(QString, quint16);
    void doDisConnect();
    void doSend(QString);

    void connectionOpen();
    void connectionLost();
    void displayError(QAbstractSocket::SocketError error);

signals:
    void readData(QByteArray);
    void readData(QString);

    void connected();
    void disconnected();

private slots:
    void readReceivedData();

private:
    QTcpSocket *socket;
    QString ip_addr;
    quint16 port;

    QTimer *timer;
};

#endif // CLIENT_H
