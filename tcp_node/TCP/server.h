#ifndef SERVER_H
#define SERVER_H
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QByteArray>
#include <QNetworkInterface>
#include <QSet>
#include <QObject>
class Server : public QTcpServer
{
    Q_OBJECT
private slots:
    void addClient();
    void readReceivedData();
    void displayError(QAbstractSocket::SocketError error);
    void connectionLost();
    void disconnected();

signals:
	void readData(QByteArray);
	void readData(QString);

protected:
//    void incomingConnection(int socketfd);

public:
    explicit Server(quint16 port = 0, QObject * parent = nullptr);
    QHostAddress getHostAddress() const;
    quint16 getPort() const;

    void writeData(QByteArray str);

private:
    QTcpSocket *client;
    QSet<QTcpSocket*> clients;
};

#endif // SERVER_H
