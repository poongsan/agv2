#include "client.h"
#include <cmath>
#include <QDataStream>
//static inline QByteArray IntToArray(qint32 source);
Client::Client(QString ip_addr, quint16 port, QObject * parent)
    : QObject (parent),
      socket(new QTcpSocket(this)),
      ip_addr(ip_addr),
      port (port),
      timer(new QTimer(this))
{
    qDebug()<<"TCP client Start :"<<ip_addr<<port;

    connect(socket, SIGNAL(readyRead()), this, SLOT(readReceivedData()));

    connect(socket, SIGNAL(connected()), this, SLOT(connectionOpen()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(connectionLost()));
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(displayError(QAbstractSocket::SocketError)));

    connect(timer, SIGNAL(timeout()), this, SLOT(doConnect()));
}

void Client::setIP(QString str_ip)
{
      ip_addr = str_ip;
}

void Client::setPort(quint16 uint16_port)
{
      port = uint16_port;
}

void Client::doConnect(QString str_ip, quint16 uint16_port)
{
	setIP(str_ip);
	setPort(uint16_port);

	while(!doConnect());
}

void Client::doSend(QString str)
{
	QByteArray str_;
	str_.append(str);

	writeData(str_);
}

bool Client::doConnect()
{
    socket->connectToHost(ip_addr, port);
    if(!socket->waitForConnected(100))
    {
        qDebug()<<"client connection"<<false;
        timer->start(5000);
        return false;
    }
    else
    {
        qDebug()<<"client connection"<<true;
        timer->stop();
        return true;
    }
   // return socket->waitForConnected(500);
}
bool Client::writeData(QByteArray data)
{
    if(socket->state() == QAbstractSocket::ConnectedState)
    {
        socket->write(data); //write the data itself;
//        qDebug()<<"write"<<data;
        return socket->waitForBytesWritten(-1);
    }
    else
    {
        qDebug()<<socket->state();
        return false;
    }
}
void Client::doDisConnect()
{
    socket->close();
    socket->deleteLater();
}

void Client::readReceivedData()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    QByteArray buffer;

    while(socket->bytesAvailable())
    {
        buffer.append(socket->readLine());
    }
    QString str= QString(buffer);

	Q_EMIT readData(buffer);
	Q_EMIT readData(str);
}

void Client::displayError(QAbstractSocket::SocketError error)
{
    if(error == QAbstractSocket::RemoteHostClosedError)
    {
//        qDebug()<<"Server Closed"<<getState();
        timer->start(5000);
    }
    qDebug()<<error;
}

void Client:: connectionOpen()
{
    emit connected();
}

void Client::connectionLost()
{
    emit disconnected();
    qDebug()<<"disconnect";
}
