
#include <QtGui>
#include <QApplication>
//#include "agv_protocol/main_window.hpp"
#include "agv_protocol/tcpManager.h"

int main(int argc, char **argv) {

    QCoreApplication app(argc, argv);

    tcpManager *w = new tcpManager(argc,argv, nullptr);
    QObject::connect(w, SIGNAL(finished()), &app, SLOT(quit()));

//   QTimer::singleShot(0, w, SLOT(run()));
    int result = app.exec();

	return result;
}
