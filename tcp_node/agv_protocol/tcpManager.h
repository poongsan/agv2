#ifndef TCP_MANAGER_H
#define TCP_MANAGER_H

#include <QtCore>
#include <QObject>
#include <QTimer>

#include <TCP/client.h>
#include <QNode/qnode.hpp>
#include <agv_protocol/convertToByte.hpp>
#include "stdint.h"

enum CMD_MOVE
{
    MOVE_STOP   = 0,
    MOVE_RUN    = 1,
};

enum CMD_LOAD
{
    LOAD_STOP   = 0,
    TAKE_OUT    = 1,
    TAKE_IN     = 2,
};

enum CMD_CHARGE
{
    CHARGE_OFF   = 0,
    CHARGE_ON    = 1,
};

struct struct_POSE
{
    int64_t X = 0;
    int64_t Y = 0;
    int64_t THETA = 0;
};

struct struct_POWER
{
    int16_t V = 0;
    int16_t I = 0;
    int16_t SOC = 0;
    int16_t charge = 0;
};

struct struct_TASK
{
    int16_t CMD = 0;
    int16_t STATE = 0;
    bool COMPLETE = false;
};

struct struct_FORK
{
    uint16_t WORK = 0;
};

struct struct_MOVE
{
//    int FROM;
//    int DIST;
    uint16_t WORK;
};

struct struct_PLC
{
    int16_t emg;
    int16_t cmd_error;
    int16_t slide_front;
};

struct struct_SENSOR
{
    int16_t top_coil;
    int16_t bot_coil;
    int16_t ir;
};

struct Motor
{
    int8_t fl = 0;
    int8_t fr = 0;
    int8_t rl = 0;
    int8_t rr = 0;
};

class tcpManager : public QObject
{
    Q_OBJECT

public:
    explicit tcpManager(int argc, char** argv, QObject *parent = nullptr);
    ~tcpManager();

	void setIP(std::string);
	void setPort(quint16);

	QHostAddress getHostAddress();

public slots:
    void close();
    void readData(QByteArray);

    void sendSTATE();
    void readPOSE(qint64, qint64, qint64);
    void readPOWER(qint16, qint16, qint16, qint16);
    void readTASK(qint16, qint16);

    void readBMS(quint8 data);

    void sendCOMPLETE(int16_t task_, int16_t state_);

    // Add 200917
    void sendALARM(int16_t alarm_);

    void connect_CMD();
    void disconnect_CMD();

    void connect_STATE();
    void disconnect_STATE();

    void readPLC(qint16, qint16, qint16);
    void readSENSOR(qint16, qint16, qint16);
    void read_motor(qint8, qint8, qint8, qint8);

signals:
    void finished();

    void MOVE(quint32 run, qreal x, qreal y, qreal th);
    void LOAD(quint32 run, QString HL);
    void CHARGE(quint32 run);
    void InitPose();
    void HomePose();
    void FORK_RESET();
    //TODO 0525
    void LOAD_ST10();

private:
    Client *cmd_client;
    Client *state_client;
    QNode *tcp_node;

    QTimer *send_Timer;

    bool isOpen_CMD;
    bool isOpen_STATE;

    struct_TASK task;

    struct_POSE pose_state;
    struct_POWER power_state;

    struct_MOVE move_state;
    struct_FORK fork_state;

    struct_PLC plc_state;
    struct_SENSOR sensor_state;
    Motor motor_;

    uint8_t sebang_soc = 0;
};

#endif
