#include <agv_protocol/tcpManager.h>
#include <agv_protocol/convertToByte.hpp>

#include <QDebug>
#include <QHostAddress>
#include <QNetworkInterface>
//192.168.2.203, 192.168.2.91
#define ACS_IP QString("192.168.2.100")
#define CMD_PORT 30002
#define STATE_PORT 30003

const QHostAddress agv_ip[2] = { QHostAddress(QString("192.168.2.101")) , QHostAddress(QString("192.168.2.102")) };

tcpManager::tcpManager(int argc, char** argv, QObject *parent)
    : QObject(parent)
    , cmd_client(new Client(ACS_IP, CMD_PORT, this))
    , state_client(new Client(ACS_IP, STATE_PORT, this))
    , tcp_node(new QNode(argc, argv))
    , send_Timer(new QTimer(this))
    , isOpen_CMD(false)
    , isOpen_STATE(false)
{
    setObjectName(QString("Task Manager"));

    QObject::connect(cmd_client, SIGNAL(readData(QByteArray)), this, SLOT(readData(QByteArray)));
    QObject::connect(cmd_client, SIGNAL(connected()), this, SLOT(connect_CMD()));
    QObject::connect(cmd_client, SIGNAL(disconnected()), this, SLOT(disconnect_CMD()));

    cmd_client->setObjectName(QString("command Port"));
    cmd_client->setIP(ACS_IP);
    cmd_client->setPort(CMD_PORT);

    QObject::connect(state_client, SIGNAL(connected()), this, SLOT(connect_STATE()));
    QObject::connect(state_client, SIGNAL(disconnected()), this, SLOT(disconnect_STATE()));

    state_client->setObjectName(QString("state Port"));
    state_client->setIP(ACS_IP);
    state_client->setPort(STATE_PORT);

    cmd_client->doConnect();
    state_client->doConnect();

    QObject::connect(tcp_node, SIGNAL(rosShutdown()), this, SLOT(close()));

    QObject::connect(this, SIGNAL(MOVE(quint32, qreal, qreal, qreal)), tcp_node, SLOT(MOVE(quint32, qreal, qreal, qreal)));
    QObject::connect(this, SIGNAL(LOAD(quint32, QString)), tcp_node, SLOT(LOAD(quint32, QString)));
    QObject::connect(this, SIGNAL(FORK_RESET()), tcp_node, SLOT(FORK_RESET()));
    QObject::connect(this, SIGNAL(CHARGE(quint32)), tcp_node, SLOT(CHARGE(quint32)));
    QObject::connect(this, SIGNAL(InitPose()), tcp_node, SLOT(InitPose()));
    QObject::connect(this, SIGNAL(HomePose()), tcp_node, SLOT(HomePose()));
    //TODO 0525
    QObject::connect(this, SIGNAL(LOAD_ST10()), tcp_node, SLOT(LOAD_ST10()));

    QObject::connect(tcp_node, SIGNAL(readPOSE(qint64, qint64, qint64)), this, SLOT(readPOSE(qint64, qint64, qint64)));
    QObject::connect(tcp_node, SIGNAL(readPOWER(qint16, qint16, qint16, qint16)), this, SLOT(readPOWER(qint16, qint16, qint16, qint16)));
    QObject::connect(tcp_node, SIGNAL(readTASK(qint16, qint16)), this, SLOT(readTASK(qint16, qint16)));

    QObject::connect(tcp_node, SIGNAL(readPLC(qint16, qint16, qint16)), this, SLOT(readPLC(qint16, qint16, qint16)));
    QObject::connect(tcp_node, SIGNAL(readSENSOR(qint16, qint16, qint16)), this, SLOT(readSENSOR(qint16, qint16, qint16)));

    QObject::connect(tcp_node, SIGNAL(readBMS(quint8)), this, SLOT(readBMS(quint8)));
    QObject::connect(tcp_node, SIGNAL(read_motor(qint8,qint8,qint8,qint8)), this, SLOT(read_motor(qint8,qint8,qint8,qint8)));

    QObject::connect(send_Timer, SIGNAL(timeout()), this, SLOT(sendSTATE()));
}

tcpManager::~tcpManager()
{

}

void tcpManager::close()
{
    deleteLater();
    emit finished();
}

void tcpManager::setIP(std::string ip_)
{
    cmd_client->setIP(QString::fromStdString(ip_));
    state_client->setIP(QString::fromStdString(ip_));
}

void tcpManager::setPort(quint16 port_)
{
    cmd_client->setPort(port_);
    state_client->setPort(port_);
}

void tcpManager::read_motor(qint8 fl, qint8 fr, qint8 rl, qint8 rr)
{
    motor_.fl = fl;
    motor_.fr = fr;
    motor_.rl = rl;
    motor_.rr = rr;
}


void tcpManager::readBMS(quint8 data)
{
    sebang_soc = data;
}

QHostAddress tcpManager::getHostAddress()
{
    QHostAddress host_ip;
    QList<QHostAddress> ipList = QNetworkInterface::allAddresses();

    for( int i = 0; i < ipList.size(); i++ )
    {
        if(ipList.at(i) != QHostAddress::LocalHost && ipList.at(i).toIPv4Address())
        {
            host_ip = QHostAddress(ipList.at(i).toString());
            break;
        }
    }

    if(host_ip.isNull())
    {
        host_ip = QHostAddress(QHostAddress::LocalHost);
    }

    return host_ip;
}

void tcpManager::readData(QByteArray data)
{
    int size;
    QByteArray ID, MODE, CMD, DATA_X, DATA_Y , DATA_Th, Fork_reset;

    size = data.size();

    ID   = data.mid(0, 1);

    MODE = data.mid(1, 1);

    CMD  = data.mid(2, 1);

    DATA_X = data.mid(3, 8);
    DATA_Y = data.mid(11, 8);
    DATA_Th = data.mid(19, 8);
    Fork_reset = data.mid(28,1);

//    if( agv_ip[ID.toHex().toInt()-1] == getHostAddress() )
//    {
//        qDebug() << ID.toHex()      << ID.toHex().toInt();
//    }
    if(Fork_reset.toHex().toInt() == 1)
    {
        emit FORK_RESET();
    }
    else
    {
    switch( CMD.toHex().toInt() )
    {
        case 0: // MOVE STOP
            emit MOVE(CMD_MOVE::MOVE_STOP, QByteToLong(DATA_X)/1000.0, QByteToLong(DATA_Y)/1000.0, QByteToLong(DATA_Th)/1000.0);
            break;

        case 1: // MOVE
            emit MOVE(CMD_MOVE::MOVE_RUN, QByteToLong(DATA_X)/1000.0, QByteToLong(DATA_Y)/1000.0, QByteToLong(DATA_Th)/1000.0);
            break;

        case 2: // TAKE OUT
            emit LOAD(CMD_LOAD::TAKE_OUT, data.mid(27,1));
            break;

        case 3: // TAKE IN
            emit LOAD(CMD_LOAD::TAKE_IN, data.mid(27,1));
            break;

        case 4: // CHARGE
            emit CHARGE(CMD_CHARGE::CHARGE_ON);
            break;

        case 5: // CHARGE STOP
            emit CHARGE(CMD_CHARGE::CHARGE_OFF);
            break;
        case 6:
            emit InitPose();
            break;

        case 7:
            emit HomePose();
            break;
        //TODO 0525
        case 8:
            emit LOAD_ST10();
            break;
    }
    }
}

void tcpManager::readPOSE(qint64 X_, qint64 Y_, qint64 A_)
{
    pose_state.X = X_;
    pose_state.Y = Y_;
    pose_state.THETA = A_;
}

void tcpManager::readPOWER(qint16 voltage_, qint16 current_, qint16 soc_, qint16 charge_)
{
    power_state.V = voltage_;
    power_state.I = current_;
    power_state.SOC = soc_;
    power_state.charge = charge_;
}

void tcpManager::readTASK(qint16 move_, qint16 load_)
{
    move_state.WORK = move_;
    fork_state.WORK = load_;

//    sendSTATE();
}

void tcpManager::sendSTATE()
{
    if(!isOpen_STATE)
        return;

    // POSE
    QByteArray X, Y, A;

    X.resize(8);
    Y.resize(8);
    A.resize(8);

    X = longTo8Byte(pose_state.X);
    Y = longTo8Byte(pose_state.Y);
    A = longTo8Byte(pose_state.THETA);

    //qDebug()<<X;
    //qDebug()<<Y;
    //qDebug()<<A;
    // POWER
    QByteArray voltage, current, soc, charge;
    voltage.resize(2);
    current.resize(2);
    soc.resize(2);
    charge.resize(2);

    voltage = shortTo2Byte(power_state.V);
    current = shortTo2Byte(power_state.I);
    soc = shortTo2Byte(power_state.SOC);
    charge = shortTo2Byte(power_state.charge);

    // TASK
    QByteArray move, load;

    move.resize(2);
    load.resize(2);

    move = shortTo2Byte(move_state.WORK);
    load = shortTo2Byte(fork_state.WORK);

    // State Bit
//    QByteArray bit_Data;
//    bit_Data.resize(2);

    QByteArray plc;

    QByteArray emg_;
    QByteArray cmd_error_;
    QByteArray slide_front_;

    emg_.resize(2);
    cmd_error_.resize(2);
    slide_front_.resize(2);

    emg_ = shortTo2Byte(plc_state.emg);
    cmd_error_ = shortTo2Byte(plc_state.cmd_error);
    slide_front_ = shortTo2Byte(plc_state.slide_front);

    plc.append(emg_);
    plc.append(cmd_error_);
    plc.append(slide_front_);//6
///////////////////////////////////////////////////////
    QByteArray sensor;

    QByteArray top_coil_;
    QByteArray bot_coil_;
    QByteArray ir_;

    top_coil_.resize(2);
    bot_coil_.resize(2);
    ir_.resize(2);

    top_coil_ = shortTo2Byte(sensor_state.top_coil);
    bot_coil_ = shortTo2Byte(sensor_state.bot_coil);
    ir_ = shortTo2Byte(sensor_state.ir);

    sensor.append(top_coil_);
    sensor.append(bot_coil_);
    sensor.append(ir_);//6



    QByteArray BMS;
    BMS.resize(1);
    BMS[0] = sebang_soc;

    // SEND DATA
    QByteArray STX, ETX, LEN;
    STX.resize(1);
    LEN.resize(2);
    ETX.resize(1);

    QByteArray MOTOR_ER;
    MOTOR_ER.resize(4);
    MOTOR_ER[0] = motor_.fl;
    MOTOR_ER[1] = motor_.fr;
    MOTOR_ER[2] = motor_.rl;
    MOTOR_ER[3] = motor_.rr;

    STX[0] = 0x02;
    LEN = shortTo2Byte(57);//TODO 53 -->57
    ETX[0] = 0x02;

    QByteArray Data;
    Data.append(STX);       // 1-byte
    Data.append(LEN);       // 2-byte
    Data.append(X);         // 8-byte
    Data.append(Y);         // 8-byte
    Data.append(A);         // 8-byte
    Data.append(voltage);   // 2-byte
    Data.append(current);   // 2-byte
    Data.append(soc);       // 2-byte
    Data.append(charge);    // 2-byte
    Data.append(move);      // 2-byte
    Data.append(load);      // 2-byte
    Data.append(plc);       // 6-byte
    Data.append(sensor);    // 6-byte
    Data.append(BMS);       // 1-byte
    // TODO
    Data.append(MOTOR_ER);  // 4-byte
    Data.append(ETX);       // 1-byte


    state_client->writeData(Data);
}

// Add 200917
void tcpManager::sendALARM(int16_t alarm_)
{
    if(!isOpen_STATE)
        return;

    QByteArray ALARM;
    ALARM.resize(2);

    ALARM = shortTo2Byte(alarm_);
    // SEND DATA
    QByteArray STX, ETX, LEN;
    STX.resize(1);
    LEN.resize(2);
    ETX.resize(1);

    STX[0] = 0x03;
    LEN = shortTo2Byte(6);
    ETX[0] = 0x03;

    QByteArray Data;
    Data.append(STX);       // 1-byte
    Data.append(LEN);       // 2-byte
    Data.append(ALARM);      // 2-byte
    Data.append(ETX);       // 1-byte

    state_client->writeData(Data);
}

void tcpManager::sendCOMPLETE(int16_t task_, int16_t state_)
{
    if(!isOpen_STATE)
        return;

    QByteArray TASK, RUN;
    TASK.resize(2);
    RUN.resize(2);

    TASK = shortTo2Byte(task_);
    RUN  = shortTo2Byte(state_);
    // SEND DATA
    QByteArray STX, ETX, LEN;
    STX.resize(1);
    LEN.resize(2);
    ETX.resize(1);

    STX[0] = 0x01;
    LEN = shortTo2Byte(8);
    ETX[0] = 0x01;

    QByteArray Data;
    Data.append(STX);       // 1-byte
    Data.append(LEN);       // 2-byte
    Data.append(TASK);      // 2-byte
    Data.append(RUN);       // 2-byte
    Data.append(ETX);       // 1-byte

    state_client->writeData(Data);
}

void tcpManager::connect_CMD()
{
    isOpen_CMD = true;
}

void tcpManager::disconnect_CMD()
{
    isOpen_CMD = false;
}

void tcpManager::connect_STATE()
{
    isOpen_STATE = true;
    send_Timer->start(200);
}

void tcpManager::disconnect_STATE()
{
    isOpen_STATE = false;
    send_Timer->stop();
}

void tcpManager::readPLC(qint16 emg, qint16 cmd_error, qint16 slide_front)
{
    plc_state.emg = emg;
    plc_state.cmd_error = cmd_error;
    plc_state.slide_front = slide_front;
}

void tcpManager::readSENSOR(qint16 top_coil, qint16 bot_coil, qint16 ir)
{
    sensor_state.top_coil = top_coil;
    sensor_state.bot_coil = bot_coil;
    sensor_state.ir = ir;
}
