#include "agv_protocol/client.h"
#include <cmath>
#include <QDataStream>
//#include <convertToByte.hpp>
namespace protocol_sample {

/*
const int length_STX = 1;
const int length_LENGTH = 2;
const int length_R_addr = 2;
const int length_S_addr = 2;
const int length_CMD = 1;
const int length_CRC_H = 1;
const int length_CRC_L = 1;
const int length_ETX = 1;
*/

Client::Client(QString ip_addr_, quint16 port_, quint16 func_, QObject * parent)
    : QObject(parent)
    , ip_addr(ip_addr_)
    , port (port_)
    , func (func_)
{
    //qDebug()<<"TCP client Start :"<<ip_addr_<<port_;
    //start();
    run();
}

void Client::setIP(QString ip)
{
	ip_addr = ip;
//qDebug()<<ip_addr;
}

void Client::setPORT(quint16 port_)
{
	port = port_;
//qDebug()<<port;
}

void Client::setNO_(quint16 no)
{
    NO_ = no;
}

void Client::setTC_(QString tc)
{
    TC_ = tc;    
}

void Client::setDescriptor(qintptr descriptor_)
{
    descriptor = descriptor_;
    socket->setSocketDescriptor(descriptor);
}

bool Client::doConnect()
{
    socket->connectToHost(ip_addr, port);
	if(!socket->waitForConnected(100))
    {
         //qDebug()<< "Couldn't find the tcp server.";

         return false;
    }
    else
    {
        //qDebug() << socket->socketDescriptor();
        return true;
    }

   // return socket->waitForConnected(500);

}
bool Client::writeData(QByteArray data)
{
    if(socket->state() == QAbstractSocket::ConnectedState)
    {
        socket->write(data);
          
        return socket->waitForBytesWritten(-1);
    }
    else
    {
        //qDebug()<<"write fail: " << socket->state();
        return false;
    }
}
void Client::doDisConnect()
{
//	qDebug()<<"disconnect";
    socket->close();
//    socket->deleteLater();
}

void Client::run()
{
//    qDebug()<< "new socket";
    socket = new QTcpSocket(this);

    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(connectionLost()));

    if(func == 1)//(port == 4002 || port == 4007 || port == 49153 )
        connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
}

void Client::readyRead()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    QByteArray buffer;
//    qDebug()<< "Recieved Data by Server";
	
	// read ByteArray & save to buffer
    while(socket->bytesAvailable())
    {
        buffer.append(socket->readLine());
    }
/////////////////////////////////////////

//	int length = buffer.mid(1,2).toHex().toInt(NULL,16);
    if( port==4001 || port==4001 || port == 49153)
	{
        QString tr_code = QString(buffer.mid(0,4));
        //qDebug()<<tr_code;
        if(tr_code == QString("WA11"))
        {
            //legnth 25
//            int16_t int16_len = static_cast<int16_t>(buffer.mid(4,2).toHex().toUShort(NULL,16));
            if( buffer.length() == 12 )
            {
                QStringList msg;
                msg.append(QString(buffer.mid(0,4)));
                msg.append(QString(buffer.mid(4,4)));
                msg.append(QString(buffer.mid(8,1)));
                msg.append(QString(buffer.mid(9,1)));
                msg.append(QString(buffer.mid(10,1)));
                msg.append(QString(buffer.mid(11,1)));

                Q_EMIT received_msg(msg);
            }
        }
    }
/////////////////////////////////////////
}
void Client::send_STATE(qint16 x, qint16 y, qint16 battery, qint16 state, qint16 no)
{
    QByteArray data_;
    QByteArray tc_code_;
    QByteArray x_;
    QByteArray y_;
    QByteArray battery_;
    QByteArray state_;
    QByteArray spare_;
    
    tc_code_.resize(4);
    x_.resize(2);
    y_.resize(2);
    battery_.resize(2);
    state_.resize(2);
    spare_.resize(1);

    tc_code_= QByteArray::fromStdString(QString("AW21").toStdString());
    x_ = shortTo2Byte(x);
    y_ = shortTo2Byte(y);
    battery_ = shortTo2Byte(battery);
    state_ = shortTo2Byte(state);
    spare_ = QByteArray::number(no);

    data_.append(tc_code_);
    data_.append(x_);
    data_.append(y_);
    data_.append(battery_);
    data_.append(state_);
    data_.append(spare_);

    if( writeData(data_) )
    {
        QStringList msg;
        msg.append(QString(data_.mid(0,4)));
        msg.append(QString::number(QByteToShort(data_.mid(4,2))));
        msg.append(QString::number(QByteToShort(data_.mid(6,2))));
        msg.append(QString::number(QByteToShort(data_.mid(8,2))));
        msg.append(QString::number(QByteToShort(data_.mid(10,2))));
        msg.append(data_.mid(12,1));
//        qDebug()<<msg;
        Q_EMIT send_msg(msg);
    }
}

void Client::send_COMPLETE(QString Tc, QString Pos, QString No, QString height, QString Type)//send_COMPLETE(QString from, QString to, QString no)
{

    QByteArray data_;
    QStringList list_;

    data_.append(Tc);
    data_.append(Pos);
    data_.append(No);
    data_.append(height);
    data_.append(Type);
    data_.append("0");

    if( writeData(data_) )
    {
        list_.append(Tc);
        list_.append(Pos);
        list_.append(No);
        list_.append(height);
        list_.append(Type);
        list_.append("0");

        Q_EMIT send_msg(list_);
    }
}

void Client::connected()
{
    qDebug() <<"CONNECT to Server";
	Q_EMIT isConnected(true);
}

void Client::connectionLost()
{
    qDebug()<<port<<"disconnect";
	Q_EMIT disconnected();
}


QAbstractSocket::SocketState Client::getState()
{
    return socket->state();
}

}
