#ifndef CLIENT_HPP_
#define CLIENT_HPP_

#include <QObject>
#include <QWidget>
#include <QTcpSocket>
#include <QByteArray>
#include <QNetworkInterface>
#include <QSet>
#include <QObject>
#include <QThread>
#include <agv_protocol/convertToByte.hpp>
namespace protocol_sample {

class Client : public QObject
{
    Q_OBJECT

public:
    Client(QString ip_addr_, quint16 port_ = 0, quint16 func_ = 0, QObject * parent = nullptr);
    bool doConnect();
    bool writeData(QByteArray data);
    void doDisConnect();

	void setIP(QString ip);
	void setPORT(quint16 port);
    void setNO_(quint16 no);
    void setTC_(QString tc);
    void setDescriptor(qintptr descriptor_);

    void run();// override;

    QAbstractSocket::SocketState getState();
private Q_SLOTS:
    void readyRead();
    void connected();
	void connectionLost();

Q_SIGNALS:
	void isConnected(bool isConnect);
	void disconnected();

    void received_msg(QStringList msg);
    void send_msg(QStringList msg);

public Q_SLOTS:
    void send_STATE(qint16 x, qint16 y, qint16 state, qint16 battery, qint16 no);
    void send_COMPLETE(QString, QString, QString, QString, QString);

private:
    QTcpSocket *socket;
    QString ip_addr;    
	quint16 port;
    quint16 NO_;    //qintptr socketDescriptor;
    QString TC_;
    
    qintptr descriptor;
    quint16 func;
};
}
#endif // CLIENT_HPP_
