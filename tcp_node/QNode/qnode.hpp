

#ifndef protocol_sample_QNODE_HPP_
#define protocol_sample_QNODE_HPP_

#ifndef Q_MOC_RUN

#include <string>
#include <QThread>
#include <QStringListModel>
#include <QHostAddress>
#include <QNetworkInterface>

#endif

#include <ros/ros.h>
#include <ros/network.h>

//#include <geometry_msgs/PoseStamped.h>
#include <if_node_msgs/cmdMove.h>
#include <if_node_msgs/cmdLoad.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>
#include <if_node_msgs/state.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <sebang_msgs/se_bat.h>
#include <md2k_can_driver_msgs/motor_status.h>

#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QScopedPointer>
#include <QTextStream>

struct POS
{
    int64_t x= 0;
    int64_t y = 0;
    int64_t th = 0;
};

struct POWER
{
    int16_t vol = 0;
    int16_t current = 0;
    int16_t soc = 0;
    int16_t charge = 0;
};

struct TASK
{
    int16_t move = 0;
    int16_t load = 0;
};

struct DATA
{
   int16_t emg = 0;
   int16_t cmd_error = 0;
   int16_t ir = 0;
   int16_t top_coil=0;
   int16_t bot_coil = 0;
   int16_t slide_front = 0;
};

struct MOTOR
{
    int8_t fl = 0;
    int8_t fr = 0;
    int8_t rl = 0;
    int8_t rr = 0;
};

class QNode : public QThread {
    Q_OBJECT

public:
    QNode(int argc, char** argv );
    virtual ~QNode();
    bool init();
    void run();

    QHostAddress getHostAddress() const;
    std::string getFrameID() const;

    void stateCallback(const if_node_msgs::state::ConstPtr& msg);
    void completeCallback(const if_node_msgs::state::ConstPtr& msg);
    void bmscallback(const sebang_msgs::se_bat::ConstPtr& msg);
    void motorCallback_(const md2k_can_driver_msgs::motor_status::ConstPtr& msg);


    static QScopedPointer<QFile> m_logFile;


Q_SIGNALS:
    void loggingUpdated();
    void rosShutdown();

    void readPOSE(qint64, qint64, qint64);
    void readPOWER(qint16, qint16, qint16, qint16);
    void readTASK(qint16, qint16);

    void sendCOMPLETE(int16_t task_, int16_t state_);

    void readPLC(qint16, qint16, qint16);
    void readSENSOR(qint16, qint16, qint16);
    //void readMotEr(quint8 data);
    void readBMS(quint8 data);
    void read_motor(qint8, qint8, qint8, qint8);

public Q_SLOTS:
    void MOVE(quint32 run, qreal x, qreal y, qreal th);
    void LOAD(quint32 run, QString HL);
    void CHARGE(quint32 run);
    void FORK_RESET();

    void InitPose();
    void HomePose();
    //TODO 0525
    void LOAD_ST10();

private:
    int init_argc;
    char** init_argv;

    void writeLogFile(const QString &msg);
    QDate date;

    // TASK CMD
    ros::Publisher  pub_CMD_MOVE;
    ros::Publisher  pub_CMD_LOAD;
    ros::Publisher  pub_CMD_CHARGE;
    // TASK STATE
    ros::Subscriber pub_STATE;
    // INIT POSE
    ros::Publisher  pub_Init_POSE;
    ros::Publisher  pub_Home_POSE, pub_ForkReset;
    // STATE SUB
    ros::Subscriber sub_state;

    ros::Subscriber sub_complete;
    ros::Subscriber sub_motor_;

    // Add 200918
    ros::Subscriber sub_Plc, sub_move_status;

    ros::Subscriber sub_sebang;


    int16_t old_move;
    int16_t old_load;

    uint8_t se_soc_ = 0;

    POS pos_;
    POWER power_;
    TASK task_;
    DATA data_;
    MOTOR motor_;
};

#endif /* protocol_sample_QNODE_HPP_ */
