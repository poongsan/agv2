
#include "../QNode/qnode.hpp"

#include <ros/network.h>
#include <ros/ros.h>
#include <std_msgs/Int8.h>
#include <std_msgs/String.h>
#include <string>

#include <tf/tf.h>

#include <QDebug>
#include <sstream>

QScopedPointer<QFile> QNode::m_logFile(nullptr);

QNode::QNode(int argc, char** argv)
    : init_argc(argc), init_argv(argv), old_move(0), old_load(0)
{
  init();
}

QNode::~QNode()
{
  if (ros::isStarted())
  {
    ros::shutdown(); // explicitly needed since we use ros::start();
    ros::waitForShutdown();
  }
  wait();
}

bool QNode::init()
{
  ros::init(init_argc, init_argv, "tcp_node");
  if (!ros::master::check())
  {
    return false;
  }
  ros::start(); // explicitly needed since our nodehandle is going out of scope.
  ros::NodeHandle n;

  pub_CMD_MOVE = n.advertise<if_node_msgs::cmdMove>("/MOVE/CMD", 1, this);
  pub_CMD_LOAD = n.advertise<if_node_msgs::cmdLoad>("/LOAD/CMD", 1, this);
  pub_CMD_CHARGE =
      n.advertise<geometry_msgs::PoseStamped>("/CHARGE/CMD", 1, this);

  //	pub_Init_POSE =
  //n.advertise<geometry_msgs::PoseWithCovarianceStamped>("/initialpose", 1,
  //this);
  pub_Init_POSE = n.advertise<std_msgs::UInt8>("/init_pose", 1, this);
  pub_Home_POSE = n.advertise<std_msgs::UInt8>("/home_pose", 1, this);
  pub_ForkReset = n.advertise<std_msgs::Int8>("/PLC/Task/CMD", 1, this);

  sub_state = n.subscribe("/AGV/State", 1, &QNode::stateCallback, this);
  sub_complete =
      n.subscribe("/AGV/Complete", 1, &QNode::completeCallback, this);

  sub_sebang = n.subscribe("/bms/sebang", 1, &QNode::bmscallback, this);
  sub_motor_ = n.subscribe("/motor_status", 1, &QNode::motorCallback_, this);

  // sub_battery;//battery

  start();
  return true;
}

void QNode::run()
{
  ros::Rate loop_rate(10);

  QString path("/home/sis-no2/Operation_Log/");
  QDir dir;
  if (!dir.exists(path))
    dir.mkpath(path);

  while (ros::ok())
  {
    if (QDate::currentDate() != date)
    {
      m_logFile.reset(new QFile(
          path + "log_" + QDateTime::currentDateTime().toString("yyyy-MM-dd") +
          ".txt"));
      m_logFile.data()->open(QFile::Append | QFile::Text);
    }
    date = QDate::currentDate();
    ros::spinOnce();

    loop_rate.sleep();
  }
  Q_EMIT
  rosShutdown(); // used to signal the gui for a shutdown (useful to roslaunch)

  ros::shutdown();
}

void QNode::bmscallback(const sebang_msgs::se_bat::ConstPtr& msg)
{
  if (msg->soc != se_soc_)
  {
    QString log_text = "Sebang SOC: " + QString::number(msg->soc);
    writeLogFile(log_text);
  }
  se_soc_ = (uint8_t)msg->soc;
  emit readBMS(se_soc_);
}

void QNode::motorCallback_(
    const md2k_can_driver_msgs::motor_status::ConstPtr& msg)
{
  if(msg->front_left_bit_Alarm != motor_.fl)
  {
      if((msg->front_left_bit_Alarm & 0x01) == 1)
      {
          writeLogFile(QString("motor front left alarm"));
      }
      if(((msg->front_left_bit_Alarm>>1) & 0x01) == 1)
      {
          writeLogFile(QString("motor front left control fail"));
      }
      if(((msg->front_left_bit_Alarm>>2) & 0x01) == 1)
      {
          writeLogFile(QString("motor front left over voltage"));
      }
      if(((msg->front_left_bit_Alarm>>3) & 0x01) == 1)
      {
          writeLogFile(QString("motor front left: over temperature"));
      }
      if(((msg->front_left_bit_Alarm>>4) & 0x01) == 1)
      {
          writeLogFile(QString("motor front left: over load"));
      }
      if(((msg->front_left_bit_Alarm>>5) & 0x01) == 1)
      {
          writeLogFile(QString("motor front left: hall sensor fail"));
      }
      if(((msg->front_left_bit_Alarm>>6) & 0x01) == 1)
      {
          writeLogFile(QString("motor front left: invert signal detect"));
      }
      if(((msg->front_left_bit_Alarm>>7) & 0x01) == 1)
      {
          writeLogFile(QString("motor front left: stall detect"));
      }
  }
  if(msg->front_right_bit_Alarm != motor_.fr)
  {
      if((msg->front_right_bit_Alarm & 0x01) == 1)
      {
          writeLogFile(QString("motor front right alarm"));
      }
      if(((msg->front_right_bit_Alarm>>1) & 0x01) == 1)
      {
          writeLogFile(QString("motor front right control fail"));
      }
      if(((msg->front_right_bit_Alarm>>2) & 0x01) == 1)
      {
          writeLogFile(QString("motor front right over voltage"));
      }
      if(((msg->front_right_bit_Alarm>>3) & 0x01) == 1)
      {
          writeLogFile(QString("motor front right: over temperature"));
      }
      if(((msg->front_right_bit_Alarm>>4) & 0x01) == 1)
      {
          writeLogFile(QString("motor front right: over load"));
      }
      if(((msg->front_right_bit_Alarm>>5) & 0x01) == 1)
      {
          writeLogFile(QString("motor front right: hall sensor fail"));
      }
      if(((msg->front_right_bit_Alarm>>6) & 0x01) == 1)
      {
          writeLogFile(QString("motor front right: invert signal detect"));
      }
      if(((msg->front_right_bit_Alarm>>7) & 0x01) == 1)
      {
          writeLogFile(QString("motor front right: stall detect"));
      }
  }

  if(msg->rear_left_bit_Alarm != motor_.rl)
  {
      if((msg->rear_left_bit_Alarm & 0x01) == 1)
      {
          writeLogFile(QString("motor rear left alarm"));
      }
      if(((msg->rear_left_bit_Alarm>>1) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear left control fail"));
      }
      if(((msg->rear_left_bit_Alarm>>2) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear left over voltage"));
      }
      if(((msg->rear_left_bit_Alarm>>3) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear left: over temperature"));
      }
      if(((msg->rear_left_bit_Alarm>>4) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear left: over load"));
      }
      if(((msg->rear_left_bit_Alarm>>5) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear left: hall sensor fail"));
      }
      if(((msg->rear_left_bit_Alarm>>6) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear left: invert signal detect"));
      }
      if(((msg->rear_left_bit_Alarm>>7) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear left: stall detect"));
      }
  }

  if(msg->rear_right_bit_Alarm != motor_.rr)
  {
      if((msg->rear_right_bit_Alarm & 0x01) == 1)
      {
          writeLogFile(QString("motor rear right alarm"));
      }
      if(((msg->rear_right_bit_Alarm>>1) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear right control fail"));
      }
      if(((msg->rear_right_bit_Alarm>>2) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear right over voltage"));
      }
      if(((msg->rear_right_bit_Alarm>>3) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear right: over temperature"));
      }
      if(((msg->rear_right_bit_Alarm>>4) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear right: over load"));
      }
      if(((msg->rear_right_bit_Alarm>>5) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear right: hall sensor fail"));
      }
      if(((msg->rear_right_bit_Alarm>>6) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear right: invert signal detect"));
      }
      if(((msg->rear_right_bit_Alarm>>7) & 0x01) == 1)
      {
          writeLogFile(QString("motor rear right: stall detect"));
      }
  }

  motor_.fl = msg->front_left_bit_Alarm;
  motor_.fr = msg->front_right_bit_Alarm;
  motor_.rl = msg->rear_left_bit_Alarm;
  motor_.rr = msg->rear_right_bit_Alarm;
  emit read_motor(motor_.fl, motor_.fr, motor_.rl, motor_.rr);
}

void QNode::stateCallback(const if_node_msgs::state::ConstPtr& msg)
{
  // POSE
  pos_.x = msg->X;
  pos_.y = msg->Y;
  pos_.th = msg->A;

  // POWER
  power_.vol = msg->voltage;
  power_.current = msg->current;
  if (msg->soc != power_.soc)
  {
    QString log_text = "AGV SOC: " + QString::number(msg->soc);
    writeLogFile(log_text);
  }
  power_.soc = msg->soc;
  if (msg->charge == 1 && power_.charge == 0)
  {
    QString log_text = "AGV charging";
    writeLogFile(log_text);
  }
  if (msg->charge == 0 && power_.charge == 1)
  {
    QString log_text = "AGV discharging";
    writeLogFile(log_text);
  }
  power_.charge = msg->charge;

  // MOVE
  if (msg->move != task_.move)
  {
    QString log_text = "AGV STATE: " + QString::number(msg->move);
    writeLogFile(log_text);
  }
  task_.move = msg->move;

  // FORK
  if (msg->load != task_.load)
  {
    QString log_text = "FORK STATE: " + QString::number(msg->load);
    writeLogFile(log_text);
  }
  task_.load = msg->load;

  // BIT DATA
  if (msg->emg == 1 && data_.emg == 0)
  {
    QString log_text = "EMC";
    writeLogFile(log_text);
  }
  if (msg->emg == 0 && data_.emg == 1)
  {
    QString log_text = "EMC RELEASE";
    writeLogFile(log_text);
  }
  data_.emg = msg->emg;
  if (msg->cmd_error == 1 && data_.cmd_error == 0)
  {
    QString log_text = "FORK CMD ERROR";
    writeLogFile(log_text);
  }
  if (msg->cmd_error == 0 && data_.cmd_error == 1)
  {
    QString log_text = "FORK CMD RESET";
    writeLogFile(log_text);
  }
  data_.cmd_error = msg->cmd_error;
  data_.ir = msg->ir;

  data_.top_coil = msg->top_coil;
  data_.bot_coil = msg->bot_coil;
  if (msg->slide_front == 1 && data_.slide_front == 0)
  {
    QString log_text = "FORK IN FRONT POSITION";
    writeLogFile(log_text);
  }
  if (msg->slide_front == 0 && data_.slide_front == 1)
  {
    QString log_text = "FORK MOVING BACK";
    writeLogFile(log_text);
  }
  data_.slide_front = msg->slide_front;

  emit readPOSE(pos_.x, pos_.y, pos_.th);
  emit readPOWER(power_.vol, power_.current, power_.soc, power_.charge);
  emit readTASK(task_.move, task_.load);

  // TODO BIT DATA
  emit readPLC(data_.emg, data_.cmd_error, data_.slide_front);
  emit readSENSOR(data_.top_coil, data_.bot_coil, data_.ir);

  if (old_move != task_.move)
  {
    if (task_.move == 1)
      emit sendCOMPLETE(1, 1);
    else if (task_.move == 0)
      emit sendCOMPLETE(1, 0);
    old_move = task_.move;
  }

  if (old_load != task_.load)
  {
    if (task_.load == 1)
      emit sendCOMPLETE(2, 1);
    else if (task_.load == 0)
      emit sendCOMPLETE(2, 0);
    old_load = task_.load;
  }
}
void QNode::FORK_RESET()
{
  std_msgs::Int8 msg;
  msg.data = 0;
  pub_ForkReset.publish(msg);

  QString log_text = "FORK RESET COMMAND";

  writeLogFile(log_text);
}

void QNode::completeCallback(const if_node_msgs::state::ConstPtr& msg) {}

void QNode::MOVE(quint32 run, qreal x, qreal y, qreal th)
{
  // ROS_INFO("MOVE %d", run);
  //    geometry_msgs::PoseStamped cmd_msgs;
  if_node_msgs::cmdMove cmd_msgs;

  cmd_msgs.move = run;
  cmd_msgs.header.seq = run;

  cmd_msgs.header.stamp = ros::Time::now();
  cmd_msgs.header.frame_id = getFrameID(); //"AGV1";

  cmd_msgs.pose.position.x = static_cast<double>(x);
  cmd_msgs.pose.position.y = static_cast<double>(y);

  cmd_msgs.pose.orientation =
      tf::createQuaternionMsgFromYaw(static_cast<double>(th * M_PI / 180.0));

  pub_CMD_MOVE.publish(cmd_msgs);

  QString log_text = "MOVE COMMAND: ";
  if (run == 1)
  {
    log_text += "RUN: ";
    log_text += "X: " + QString::number(x) + ", Y: " + QString::number(y) +
                ", TH: " + QString::number(th);
  }
  else
    log_text += "STOP";

  writeLogFile(log_text);
}
// TODO
void QNode::LOAD_ST10()
{
  if_node_msgs::cmdLoad cmd_msgs;
  // 0 none
  cmd_msgs.cmd = 8;

  cmd_msgs.H_L = 0;

  pub_CMD_LOAD.publish(cmd_msgs);

  QString log_text = "TAKE OUT ST10 COMMAND";
  writeLogFile(log_text);
}

void QNode::LOAD(quint32 run, QString HL)
{
  ROS_INFO("LOAD %d", run);

  if_node_msgs::cmdLoad cmd_msgs;
  // 0 none
  cmd_msgs.cmd = run;
  if (HL == "H")
    cmd_msgs.H_L = 1;
  else if (HL == "L")
    cmd_msgs.H_L = 0;

  pub_CMD_LOAD.publish(cmd_msgs);

  QString log_text = "LOAD COMMAND: ";
  if (run == 2)
  {
    if (HL == "H")
      log_text += "TAKE OUT HIGH";
    else if (HL == "L")
      log_text += "TAKE OUT LOW";
  }
  else if (run == 3)
  {
    if (HL == "H")
      log_text += "TAKE IN HIGH";
    else if (HL == "L")
      log_text += "TAKE IN LOW";
  }
  writeLogFile(log_text);
}

void QNode::CHARGE(quint32 run)
{
  // ROS_INFO("CHARGE");
  pub_CMD_CHARGE;
  QString log_text = "CHARGE COMMAND: ";
  if (run == 0)
    log_text += "STOP";
  else
    log_text += "CHARGE";
  writeLogFile(log_text);
}

void QNode::InitPose()
{
  ROS_INFO("INIT_POSE");
  std_msgs::UInt8 pose_msgs;
  pose_msgs.data = 1;

  pub_Init_POSE.publish(pose_msgs);
}

void QNode::HomePose()
{
  ROS_INFO("HOME_POSE");
  std_msgs::UInt8 pose_msgs;
  pose_msgs.data = 1;

  pub_Home_POSE.publish(pose_msgs);

  QString log_text = "MOVE COMMAND: MOVE TO HOME";
  writeLogFile(log_text);
}

QHostAddress QNode::getHostAddress() const
{
  QHostAddress hostAddress;
  QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();

  for (int i = 0; i < ipAddressesList.size(); ++i)
  {
    if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
        ipAddressesList.at(i).toIPv4Address())
    {
      hostAddress = QHostAddress(ipAddressesList.at(i).toString());
      break;
    }
  }

  if (hostAddress.isNull())
  {
    hostAddress = QHostAddress(QHostAddress::LocalHost);
  }

  return hostAddress;
}

void QNode::writeLogFile(const QString& msg)
{
  QTextStream out(m_logFile.data());
  out << QDateTime::currentDateTime().toString("hh:mm:ss ");
  out << " : " << msg << endl;
  out.flush();
}

std::string QNode::getFrameID() const
{
  QHostAddress hostIP = getHostAddress();
  std::string id_;

  if (hostIP.toString() == "192.168.2.101")
    id_ = "AGV1";
  else if (hostIP.toString() == "192.168.2.102")
    id_ = "AGV2";
  else if (hostIP.toString() == "192.168.2.100")
    id_ = "AGV1";

  return id_;
}
