#include <ros/ros.h>

#include <std_msgs/Int8.h>

#include <if_node_msgs/loadState.h>
#include <if_node_msgs/cmdLoad.h>

#include <iostream>

class Fork
{
    public:
        Fork();
        ~Fork();

        bool init();
        void spin();

        void cmdCallback(const if_node_msgs::cmdLoad::ConstPtr& msg);
        void stateCallback(const if_node_msgs::loadState::ConstPtr& msg);
        void completeCallback(const if_node_msgs::loadState::ConstPtr& msg);
        void errCallback(const std_msgs::Int8::ConstPtr& msg);
    private:
    	ros::NodeHandle nh_;

    	ros::Publisher pub_cmd;
        ros::Subscriber sub_cmd, sub_err;

        ros::Publisher pub_reset;

    	ros::Publisher pub_state;
    	ros::Subscriber sub_state;

    	ros::Publisher pub_complete;
    	ros::Subscriber sub_complete;

    	ros::Publisher pub_coil;

    	ros::Publisher pub_run;

    	int cmd_flag;
        int fork_err = 0;
        int fork_cmd = 0;
};

Fork::Fork() : cmd_flag(0)
{
	init();
}

Fork::~Fork()
{
    if(ros::isStarted())
    {
      ros::shutdown(); // explicitly needed since we use ros::start();
      ros::waitForShutdown();
    }
    //wait();
}

bool Fork::init()
{
    if ( ! ros::master::check() )
    {
        return false;
    }

    pub_cmd = nh_.advertise<std_msgs::Int8>("/PLC/Task/CMD", 1, this);

    sub_cmd = nh_.subscribe("/LOAD/CMD", 1, &Fork::cmdCallback, this);

    pub_reset = nh_.advertise<if_node_msgs::cmdLoad>("/LOAD/CMD", 1, this);

    sub_state = nh_.subscribe("/PLC/Task/Complete", 1, &Fork::completeCallback, this);
    sub_complete = nh_.subscribe("/PLC/Task/State", 1, &Fork::stateCallback, this);

    pub_coil = nh_.advertise<std_msgs::Int8>("/PLC/Senosr/coil", 1, this);
    pub_run = nh_.advertise<std_msgs::Int8>("/LOAD/STATE", 1, this);
    sub_err = nh_.subscribe("PLC/Error/WrongCmd", 1, &Fork::errCallback, this);

    ros::start(); // explicitly needed since our nodehandle is going out of scope.

    spin();

    return true;
}

void Fork::spin()
{
    ros::Rate loop_rate(10);

    while(ros::ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }
}

void Fork::errCallback(const std_msgs::Int8::ConstPtr& msg)
{
    fork_err = msg->data;
}

void Fork::cmdCallback(const if_node_msgs::cmdLoad::ConstPtr& msg)
{
    int IO = msg->cmd; //1:out 2:in
    int HL = msg->H_L; //1:h 0:low

    std_msgs::Int8 cmd_msgs;

    if(IO == 0)
        return;

    if(IO == 1 && HL == 1)
    {
        //1 Top OUT
        cmd_msgs.data = 1;
    }
    else if(IO == 1 && HL == 0)
    {
        //2 Bot OUT
        cmd_msgs.data = 2;
    }
    else if(IO == 2 && HL == 1)
    {
        //3 Top IN
        cmd_msgs.data = 3;
    }
    else if(IO == 2 && HL == 0)
    {
        //4 Bot In
        cmd_msgs.data = 4;
    }
    //TODO 0525
    else if(IO == 8 && HL == 0)
    {
        //4 Bot In
        cmd_msgs.data = 5;
    }

    cmd_flag = cmd_msgs.data;
    pub_cmd.publish(cmd_msgs);
    if(cmd_flag == 5)
        fork_cmd = 2;
    else
        fork_cmd = cmd_flag;
}

void Fork::stateCallback(const if_node_msgs::loadState::ConstPtr& msg)
{
    std_msgs::Int8 state_msgs;
    std_msgs::Int8 cmd_msgs;

    int bot_IN = msg->bot_IN;
    int bot_Out = msg->bot_Out;
    int top_IN = msg->top_IN;
    int top_Out = msg->top_Out;

    top_Out = top_Out*1; //<< 0;
    bot_Out = bot_Out*2; //<< 1;
    top_IN  = top_IN *3; //<< 2;
    bot_IN  = bot_IN *4; //<< 3;

    int bit = top_Out + bot_Out + top_IN + bot_IN;
    // Add 210314

        if( (fork_cmd == bit) && (bit != 0) )
        {
            // progress
            state_msgs.data = fork_cmd;
            pub_run.publish(state_msgs);

            cmd_msgs.data = 0;
            pub_cmd.publish(cmd_msgs);

            if_node_msgs::cmdLoad reset_msgs;
            reset_msgs.cmd = 0;
            pub_reset.publish(reset_msgs);

    //	ROS_INFO("MOVE");
        }


//    pub_run.publish(state_msgs);
}

void Fork::completeCallback(const if_node_msgs::loadState::ConstPtr& msg)
{
    std_msgs::Int8 coil_msgs;
    std_msgs::Int8 state_msgs;

    int bot_IN = msg->bot_IN;
    int bot_Out = msg->bot_Out;
    int top_IN = msg->top_IN;
    int top_Out = msg->top_Out;

    int coil = msg->coil;

    top_Out = top_Out*1; //<< 0;
    bot_Out = bot_Out*2; //<< 1;
    top_IN  = top_IN *3; //<< 2;
    bot_IN  = bot_IN *4; //<< 3;

    int bit = top_Out + bot_Out + top_IN + bot_IN;

    if(fork_cmd==0)
    {
        state_msgs.data = fork_cmd;
        pub_run.publish(state_msgs);
    }
    if( (fork_cmd == bit) && (bit != 0) )
    {
        // complete
        cmd_flag = 0;
        fork_cmd = 0;
//        ROS_INFO("COMP");
    }
//    state_msgs.data = cmd_flag;

    coil_msgs.data = coil;
    pub_coil.publish(coil_msgs);
//    pub_run.publish(state_msgs);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv,"fork_node");
    Fork fork;

    return 0;
}
